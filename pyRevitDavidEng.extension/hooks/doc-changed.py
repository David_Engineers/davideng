#-*- coding: utf-8 -*-
from pyrevit import forms
from pyrevit import EXEC_PARAMS
from pyrevit import DB

command = EXEC_PARAMS.event_args
doc = command.GetDocument()

#Id of added element 
changedIds = EXEC_PARAMS.event_args.GetModifiedElementIds()
deletedIds = EXEC_PARAMS.event_args.GetDeletedElementIds()