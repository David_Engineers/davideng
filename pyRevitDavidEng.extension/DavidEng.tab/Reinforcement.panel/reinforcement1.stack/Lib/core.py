# # -*- coding: utf-8 -*-

import sys
import os
sys.path.append(os.path.dirname(__file__))

import math
import glob
from pyrevit import coreutils, UI, revit, DB, forms, script
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, Structure, BuiltInParameter, Transaction, XYZ, ElementId, Options, Line, ElementClassFilter, \
                              ElementCategoryFilter, Outline, BoundingBoxIntersectsFilter, TransactionGroup, ViewPlan, FilledRegion, SetComparisonResult
from pyrevit import framework
from pyrevit import forms
from pyrevit import revit, DB, UI
from pyrevit import script
from pyrevit import HOST_APP
import DavidEng

from System.Diagnostics import Process

# dependencies
import clr
clr.AddReference('System.Windows.Forms')

# import WPF creator and base Window
import wpf
from System import Windows

# Identify the current Revit file
doc = __revit__.ActiveUIDocument.Document
uidoc = __revit__.ActiveUIDocument

options = Options(ComputeReferences=True, IncludeNonVisibleObjects=True)
options.View = doc.ActiveView

docTitle = Process.GetCurrentProcess().MainWindowTitle

# Parameters constants
DAVIDENG_DETAIL_REBAR_SHAPE = 'DENG_Detail_Rebar_Shape'
DAVIDENG_DETAIL_REBAR_SHAPE_VERSION = 2.00
DAVIDENG_DETAIL_REBAR_DIMENSION = 'DENG_Rebar_Dimension_Line'
DAVIDENG_DETAIL_REBAR_DIMENSION_VERSION = 1.00
DAVIDENG_REBAR_POINTER = 'DENG_Rebar_Pointer'
DAVIDENG_REBAR_POINTER_VERSION = 1.00
FABRIC_SHEET_DETAIL = 'DENG_Fabric Sheet Detail'
FABRIC_SHEET_DETAIL_VERSION = 1.00
FABRIC_SHEET_PLAN = 'DENG_Fabric_Sheet_Plan'
FABRIC_SHEET_PLAN_VERSION = 1.10
GEOMETRY_INSTANCE = "Autodesk.Revit.DB.GeometryInstance"
LINE = "Autodesk.Revit.DB.Line"
ARC = "Autodesk.Revit.DB.Arc"
DAVIDENG_A = 'DENG_A'
DAVIDENG_B = 'DENG_B'
DAVIDENG_C = 'DENG_C'
DAVIDENG_D = 'DENG_D'
DAVIDENG_E = 'DENG_E'
HOOKATSTART_90TO180 = 'DENG_A_Visibility'
HOOKATSTART_0TO90 = 'DENG_B_Visibility'
HOOKATEND_0TO90 = 'DENG_D_Visibility'
HOOKATEND_90TO180 = 'DENG_E_Visibility'
LAYERQUANTITY = 'DENG_Layer_Quantity'
LAYERTEXT = 'DENG_Layer'
MULTIPLIER = 'DENG_Multiplier'
MANUALMULTIPLIER = 'DENG_Manual_Multiplier'
REBARDIAMETER = "DENG_BarDiameter"
BARTOTALLENGTH = 'DENG_TotalBarLength'
SPACING = 'DENG_Spacing'
DIMENSIONLINELENGTH = 'DENG_Length'
SYMBOLCIRCLE = 'DENG_ShowPointer'
MARK = 'DENG_Mark'
TOTALQUANTITY = 'DENG_TotalQuantity'
SHOWNUMBERSOFLAYERS = 'DENG_ShowNumberOfLayers'
VIEWNAME = 'DENG_ViewName'
COMMENTS = 'Comments'
POINTERCIRCLEDIAMETER = 'Circle Diameter'
FABRICSHEETNAME = 'DENG_FabricSheetName'
SHEETMASS = 'Sheet Mass'
SHEETMASSPERUNIT = 'Sheet Mass per Unit Area'
SHEETNAME = 'Name'
SHEETDIVIDEND = 'DENG_Dividend'
SHEETDIVISOR = 'DENG_Divisor'
SHEETMAJORLENGTH = 'DENG_MajorLength'
SHEETMINORLENGTH = 'DENG_MinorLength'
SHEETMAJORWIRETYPE = 'Major Direction Wire Type'
SHEETMINORWIRETYPE = 'Minor Direction Wire Type'
MESHBARDIAMETER = "DENG_MeshBarDiameter"
MESHSIZE = "DENG_MeshSize"
MESHMASSPERUNITAREA = "DENG_MeshMassPerUnitArea"
MESHMASS = "DENG_MeshMass"

# Create the titles of the array
quantitiesArray = [['Bar Mark', 'Bar Diameter [Ø]', 'Total No.', 'Total Length [m]', 'Total Weight [ton]', 'Shape and Measures [cm]', 'A', 'B', 'C', 'D', 'E']]

elementIdAndMark = []

meshElementsToSet = []

# Create the titles of the array of the fabric sheets
fabricSheetQuantitiesArray = [['Mesh Type', 'Bar Diameter [Ø]', 'Full/Partial Mesh Size', 'B [cm]', 'L [cm]', 'Total No.', 'Unit Weight [kg/m²]', 'Total Weight [ton]']]

rebarShapesImagesNames = ['RebarShapeImage_C.jpg', 'RebarShapeImage_ABC.jpg', 'RebarShapeImage_ABCD.jpg', 'RebarShapeImage_ABCDE.jpg', 'RebarShapeImage_BC.jpg', 'RebarShapeImage_BCD.jpg', \
                          'RebarShapeImage_CD.jpg', 'RebarShapeImage_CDE.jpg', 'RebarShapeImage_BCDE.jpg']

checkRow = 1

sheetErrorMessage = "To zoom the invalid element, please run the tool in Plan View."

def CheckIfEqual(segValue, hookVis, hookLen):
    if hookVis == True:
        if hookLen == segValue:
            return True
        else:
            return False
    else:
        if segValue > 0:
            return False
        else:
            return True

def CheckIfExistInArray(rebarShape):
    if (len(quantitiesArray) == 1):
        global startRowToCheck
        startRowToCheck = 1
        return False
    else:
        if (quantitiesArray[startRowToCheck][1] != rebarShape.LookupParameter(REBARDIAMETER).AsDouble()*304.8):
            startRowToCheck = len(quantitiesArray)
        global checkRow
        checkRow = startRowToCheck
        while checkRow < len(quantitiesArray):
            if ((CheckIfEqual(quantitiesArray[checkRow][1], True, rebarShape.LookupParameter(REBARDIAMETER).AsDouble()*304.8)) \
               and (CheckIfEqual(quantitiesArray[checkRow][6], rebarShape.LookupParameter(HOOKATSTART_90TO180).AsInteger(), rebarShape.LookupParameter(DAVIDENG_A).AsValueString())) \
               and (CheckIfEqual(quantitiesArray[checkRow][7], rebarShape.LookupParameter(HOOKATSTART_0TO90).AsInteger(), rebarShape.LookupParameter(DAVIDENG_B).AsValueString())) \
               and (CheckIfEqual(quantitiesArray[checkRow][8], True, rebarShape.LookupParameter(DAVIDENG_C).AsValueString())) \
               and (CheckIfEqual(quantitiesArray[checkRow][9], rebarShape.LookupParameter(HOOKATEND_0TO90).AsInteger(), rebarShape.LookupParameter(DAVIDENG_D).AsValueString())) \
               and (CheckIfEqual(quantitiesArray[checkRow][10], rebarShape.LookupParameter(HOOKATEND_90TO180).AsInteger(), rebarShape.LookupParameter(DAVIDENG_E).AsValueString()))) \
               or ((CheckIfEqual(quantitiesArray[checkRow][1], True, rebarShape.LookupParameter(REBARDIAMETER).AsDouble()*304.8)) \
               and (CheckIfEqual(quantitiesArray[checkRow][10], rebarShape.LookupParameter(HOOKATSTART_90TO180).AsInteger(), rebarShape.LookupParameter(DAVIDENG_A).AsValueString())) \
               and (CheckIfEqual(quantitiesArray[checkRow][9], rebarShape.LookupParameter(HOOKATSTART_0TO90).AsInteger(), rebarShape.LookupParameter(DAVIDENG_B).AsValueString())) \
               and (CheckIfEqual(quantitiesArray[checkRow][8], True, rebarShape.LookupParameter(DAVIDENG_C).AsValueString())) \
               and (CheckIfEqual(quantitiesArray[checkRow][7], rebarShape.LookupParameter(HOOKATEND_0TO90).AsInteger(), rebarShape.LookupParameter(DAVIDENG_D).AsValueString())) \
               and (CheckIfEqual(quantitiesArray[checkRow][6], rebarShape.LookupParameter(HOOKATEND_90TO180).AsInteger(), rebarShape.LookupParameter(DAVIDENG_E).AsValueString()))):
                return True
                break
            else:
                checkRow += 1
        return False

# Calculate how many layers were selected for the rebar shape
def NumberOfLayers(rebarShape):
    layerIndex = 1
    numOfLayers = 0
    while (layerIndex < 10):
        layerIndication = rebarShape.LookupParameter("L." + layerIndex.ToString()).AsInteger()
        if layerIndication == 1:
            numOfLayers += 1
        layerIndex += 1
    return numOfLayers

# Create the name of the rebar shape for the image
def RebarShapeImageName(quantities_array, row):
    sec_visibility_params = ["A", "B", "C", "D", "E"]
    rebar_shape_image_name = "RebarShapeImage_"
    
    for i in range(6, 11):
        if quantities_array[row][i] > 0:
            rebar_shape_image_name += sec_visibility_params[i - 6]
    
    return rebar_shape_image_name

def is_positive_integer(integer):
    """
    Checks if a given integer represents a positive integer.

    Args:
        integer (int): The integer to be checked.

    Returns:
        bool: True if the integer is a positive integer, False otherwise.
    """
    if integer > 0:
        return True
    else:
        return False

def WriteToArray(rebarShape, newFlag, calculatedQuantity, numOfLayers):
    leadersCounter = 0
    if calculatedQuantity == 0:
        barQuantity = int(rebarShape.LookupParameter(LAYERQUANTITY).AsString())
        if numOfLayers > 1:
            barQuantity *= numOfLayers
        
        # Checking the pointers and calculating the multiplier
        matchingRebarPointer = FindRebarPointers()
        if (matchingRebarPointer):
            for pointer in matchingRebarPointer:
                leadersList = pointer.GetLeaders()
                if (leadersList):
                    leadersCounter += len(leadersList)
                for leader in PointerAndLeaderIntersection:
                    if pointer == leader[0]:
                        leadersCounter += leader[1]
            if leadersCounter > 0:
                barQuantity *= leadersCounter
            
            manualMultiplierParam = rebarShape.Symbol.LookupParameter(MANUALMULTIPLIER)
            if manualMultiplierParam is not None:
                manualMultiplierValue = manualMultiplierParam.AsInteger()
                if (is_positive_integer(manualMultiplierValue)):
                    barQuantity *= manualMultiplierValue
    else:
        barQuantity = calculatedQuantity
        if numOfLayers > 1:
            barQuantity *= numOfLayers
        
        manualMultiplierParam = rebarShape.Symbol.LookupParameter(MANUALMULTIPLIER)
        if manualMultiplierParam is not None:
            manualMultiplierValue = manualMultiplierParam.AsInteger()
            if (is_positive_integer(manualMultiplierValue)):
                barQuantity *= manualMultiplierValue

    barLength = float(rebarShape.LookupParameter(BARTOTALLENGTH).AsValueString()) * barQuantity
    barRadius = (rebarShape.LookupParameter(REBARDIAMETER).AsDouble()*304.8)/2
    totalWeight = (math.pi)*math.pow(barRadius, 2)*7860*barLength*math.pow(10, -11)
    if newFlag:
        row = len(quantitiesArray)
        quantitiesArray.append([])
        quantitiesArray[row].append(row)
        quantitiesArray[row].append(rebarShape.LookupParameter(REBARDIAMETER).AsDouble()*304.8)
        quantitiesArray[row].append(barQuantity)
        quantitiesArray[row].append(round(barLength/100, 2))
        quantitiesArray[row].append(totalWeight)
        quantitiesArray[row].append(None)
        WriteOnlyIfVisible(row, rebarShape.LookupParameter(DAVIDENG_A).AsValueString(), rebarShape.LookupParameter(HOOKATSTART_90TO180).AsInteger())
        WriteOnlyIfVisible(row, rebarShape.LookupParameter(DAVIDENG_B).AsValueString(), rebarShape.LookupParameter(HOOKATSTART_0TO90).AsInteger())
        quantitiesArray[row].append(rebarShape.LookupParameter(DAVIDENG_C).AsValueString())
        WriteOnlyIfVisible(row, rebarShape.LookupParameter(DAVIDENG_D).AsValueString(), rebarShape.LookupParameter(HOOKATEND_0TO90).AsInteger())
        WriteOnlyIfVisible(row, rebarShape.LookupParameter(DAVIDENG_E).AsValueString(), rebarShape.LookupParameter(HOOKATEND_90TO180).AsInteger())
        
        rebarShapeImageName = RebarShapeImageName(quantitiesArray, row)
        
        elementIdAndMark.append([rebarShape.Id, row, barQuantity, leadersCounter, rebarShapeImageName])
    else:
        quantitiesArray[checkRow][2] += barQuantity
        quantitiesArray[checkRow][3] += round(barLength/100, 2)
        quantitiesArray[checkRow][4] += totalWeight
        
        rebarShapeImageName = RebarShapeImageName(quantitiesArray, checkRow)
        
        elementIdAndMark.append([rebarShape.Id, checkRow, barQuantity, leadersCounter, rebarShapeImageName])

def WriteOnlyIfVisible(row, segLength, segVisibility):
    if segVisibility:
        quantitiesArray[row].append(segLength)
    else:
        quantitiesArray[row].append(0)

def RebarShapeProperties():
    # Getting the geometry and the bounding box of the rebar shape
    geoRebarShape = rebarShape.get_Geometry(options)
    bBoxRebarShape = geoRebarShape.GetBoundingBox()
    
    # Filter only the lines from the geometry instance
    geoInstanceRebarShape = [(i) for i in geoRebarShape if i.GetType().ToString() == GEOMETRY_INSTANCE]
    lineRebarShape = [(i) for i in geoInstanceRebarShape[0].GetInstanceGeometry() if i.GetType().ToString() == LINE]
    
    # Filter only the last line from the list (the last one is the main one)
    lastLineRebarShape = lineRebarShape[-1]
    
    # If this is the updated family with left and right references (for the temporary dimension)
    if lastLineRebarShape.Reference == None:
        lastLineRebarShape = lineRebarShape[len(lineRebarShape)-3]
    
    # Bringing the start and end point of the line
    startPointLine = lastLineRebarShape.GetEndPoint(0)
    endPointLine = lastLineRebarShape.GetEndPoint(1)
    
    if bBoxRebarShape.Min.X == bBoxRebarShape.Max.X:
        fixedMinX = XYZ(bBoxRebarShape.Min.X - 1, bBoxRebarShape.Min.Y, bBoxRebarShape.Min.Z)
        outlineRebarShape = Outline(fixedMinX, bBoxRebarShape.Max)
    
    if bBoxRebarShape.Min.X == bBoxRebarShape.Max.X:
        fixedMinX = XYZ(bBoxRebarShape.Min.X - 1, bBoxRebarShape.Min.Y, bBoxRebarShape.Min.Z)
        outlineRebarShape = Outline(fixedMinX, bBoxRebarShape.Max)
    elif bBoxRebarShape.Min.Y == bBoxRebarShape.Max.Y:
        fixedMinY = XYZ(bBoxRebarShape.Min.X, bBoxRebarShape.Min.Y - 1, bBoxRebarShape.Min.Z)
        outlineRebarShape = Outline(fixedMinY, bBoxRebarShape.Max)
    else:
        outlineRebarShape = Outline(bBoxRebarShape.Min, bBoxRebarShape.Max)
    
    return startPointLine, endPointLine, outlineRebarShape

def ZoomToElement():
    startPointLine, endPointLine, outlineRebarShape = RebarShapeProperties()
    active_ui_views = uidoc.GetOpenUIViews()
    # find current uiview
    current_ui_view_list = []
    for active_ui_view in active_ui_views:
        if active_ui_view.ViewId == doc.ActiveView.Id:
            current_ui_view_list.append(active_ui_view)
    for current_ui_view in current_ui_view_list:
        current_ui_view.ZoomAndCenterRectangle(outlineRebarShape.MaximumPoint, outlineRebarShape.MinimumPoint)
    
def ColorAndAlertFabricSheet(errorMessage, elementToShow):
    tg = TransactionGroup(doc, 'Color the invalid fabric sheet')
    tg.Start()
    t = Transaction(doc, 'Color the invalid fabric sheet')
    t.Start()
    ogs = DB.OverrideGraphicSettings()
    ogs.SetProjectionLineColor(DB.Color(255, 0, 0))
    revit.doc.ActiveView.SetElementOverrides(elementToShow.Id, ogs)
    t.Commit()
    DavidEng.ErrorWindow(errortext=errorMessage).ShowDialog()
    tg.RollBack()
    
def ColorAndAlertRebarShape(errorMessage):
    tg = TransactionGroup(doc, 'Color the invalid rebar shape')
    tg.Start()
    t = Transaction(doc, 'Color the invalid rebar shape')
    t.Start()
    ogs = DB.OverrideGraphicSettings()
    ogs.SetProjectionLineColor(DB.Color(255, 0, 0))
    revit.doc.ActiveView.SetElementOverrides(rebarShape.Id, ogs)
    t.Commit()
    DavidEng.ErrorWindow(errortext=errorMessage).ShowDialog()
    tg.RollBack()

def ColorAndAlertDimensionLine(errorMessage):
    tg = TransactionGroup(doc, 'Color the invalid dimension lines')
    tg.Start()
    t = Transaction(doc, 'Color the invalid dimension lines')
    t.Start()
    ogs = DB.OverrideGraphicSettings()
    ogs.SetProjectionLineColor(DB.Color(255, 0, 0))
    for rebarDimension in matchingRebarDimensions:
        revit.doc.ActiveView.SetElementOverrides(rebarDimension.Id, ogs)
    t.Commit()
    DavidEng.ErrorWindow(errortext=errorMessage).ShowDialog()
    tg.RollBack()

def check_on_and_between_rebar_shape(geo_element, start_point_line, end_point_line):
    """
    This method takes in a geo_element, along with start_point_line and end_point_line,
    and checks if the line created between start_point_line and end_point_line
    intersects with the arc defined in geo_element.
    """
    # Extract geometry instances from geo_element
    geometry_instances = [i for i in geo_element if i.GetType().ToString() == GEOMETRY_INSTANCE]

    # Extract arc lines from the geometry instances
    arc_lines = [i for i in geometry_instances[0].GetInstanceGeometry() if i.GetType().ToString() == ARC]

    # Get the center point and radius of the arc
    center_point = arc_lines[0].Center
    arc_radius = arc_lines[0].Radius

    # Define the left and right points of the arc
    arc_left_point = XYZ(center_point.X - arc_radius, center_point.Y, center_point.Z)
    arc_right_point = XYZ(center_point.X + arc_radius, center_point.Y, center_point.Z)

    # Create a line bound for the rebar shape and the horizontal dimension of the arc
    rebar_shape_bound = Line.CreateBound(XYZ(start_point_line.X, start_point_line.Y, 0), XYZ(end_point_line.X, end_point_line.Y, 0))
    dimension_horizontal_bound = Line.CreateBound(XYZ(arc_left_point.X, arc_left_point.Y, 0), XYZ(arc_right_point.X, arc_right_point.Y, 0))

    # Check if the rebar shape intersects with the horizontal dimension of the arc
    if rebar_shape_bound.Intersect(dimension_horizontal_bound) == SetComparisonResult.Overlap:
        return True

    # Define the bottom and top points of the arc
    arc_bottom_point = XYZ(center_point.X, center_point.Y - arc_radius, center_point.Z)
    arc_top_point = XYZ(center_point.X, center_point.Y + arc_radius, center_point.Z)

    # Create a line bound for the vertical dimension of the arc
    dimension_vertical_bound = Line.CreateBound(XYZ(arc_bottom_point.X, arc_bottom_point.Y, 0), XYZ(arc_top_point.X, arc_top_point.Y, 0))

    # Check if the rebar shape intersects with the vertical dimension of the arc
    if rebar_shape_bound.Intersect(dimension_vertical_bound) == SetComparisonResult.Overlap:
        return True

    # Return False if there is no intersection
    return False

def check_on_and_between_dimension_line(start_point_line, end_point_line, center_point, arc_radius, rebar_shape_start_point, rebar_shape_end_point):
    """
    This method takes in a center_point and arc_radius, along with start_point_line and end_point_line,
    and checks if the line created between start_point_line and end_point_line
    intersects with the arc defined in center_point and arc_radius.
    """
    
    # Define the left and right points of the arc
    arc_left_point = XYZ(center_point.X - arc_radius, center_point.Y, center_point.Z)
    arc_right_point = XYZ(center_point.X + arc_radius, center_point.Y, center_point.Z)
    # Define the bottom and top points of the arc
    arc_bottom_point = XYZ(center_point.X, center_point.Y - arc_radius, center_point.Z)
    arc_top_point = XYZ(center_point.X, center_point.Y + arc_radius, center_point.Z)

    # Create a line bound for the dimension line
    dimension_line_bound = Line.CreateBound(XYZ(start_point_line.X, start_point_line.Y, 0), XYZ(end_point_line.X, end_point_line.Y, 0))
    # Create a line bound for the rebar shape
    rebar_shape_bound = Line.CreateBound(XYZ(rebar_shape_start_point.X, rebar_shape_start_point.Y, 0), XYZ(rebar_shape_end_point.X, rebar_shape_end_point.Y, 0))
    # Create a line bound for the horizontal dimension of the arc
    dimension_horizontal_bound = Line.CreateBound(XYZ(arc_left_point.X, arc_left_point.Y, 0), XYZ(arc_right_point.X, arc_right_point.Y, 0))
    # Create a line bound for the vertical dimension of the arc
    dimension_vertical_bound = Line.CreateBound(XYZ(arc_bottom_point.X, arc_bottom_point.Y, 0), XYZ(arc_top_point.X, arc_top_point.Y, 0))

    # Check if the rebar shape intersects with the horizontal dimension of the arc
    if dimension_line_bound.Intersect(dimension_horizontal_bound) == SetComparisonResult.Overlap:
        if (rebar_shape_bound.Intersect(dimension_horizontal_bound) == SetComparisonResult.Overlap or rebar_shape_bound.Intersect(dimension_vertical_bound) == SetComparisonResult.Overlap):
            return True

    # Check if the rebar shape intersects with the vertical dimension of the arc
    if dimension_line_bound.Intersect(dimension_vertical_bound) == SetComparisonResult.Overlap:
        if (rebar_shape_bound.Intersect(dimension_horizontal_bound) == SetComparisonResult.Overlap or rebar_shape_bound.Intersect(dimension_vertical_bound) == SetComparisonResult.Overlap):
            return True

    # Return False if there is no intersection
    return False

def FindRebarPointers():
    startPointLine, endPointLine, outlineRebarShape = RebarShapeProperties()
    goodRebarPointer = []
    
    for rebarPointer in rebarPointers:  
        pointerCircleDiameterParam = rebarPointer.LookupParameter(POINTERCIRCLEDIAMETER)
        if pointerCircleDiameterParam is not None:
            pointerCircleDiameter = rebarPointer.LookupParameter(POINTERCIRCLEDIAMETER).AsDouble()*100/2
        else:
            pointerCircleDiameter = rebarPointer.Symbol.LookupParameter(POINTERCIRCLEDIAMETER).AsDouble()*100/2
        pointerCenterPoint = rebarPointer.Location.Point
        
        # Model Bounding Box
        BoxMax = XYZ(pointerCenterPoint.X+pointerCircleDiameter, pointerCenterPoint.Y+pointerCircleDiameter, 0)
        BoxMin = XYZ(pointerCenterPoint.X-pointerCircleDiameter, pointerCenterPoint.Y-pointerCircleDiameter, 0)
        outlineRebarPointer = Outline(BoxMin, BoxMax)
        
        rebarBoxMax = XYZ(outlineRebarShape.MaximumPoint.X, outlineRebarShape.MaximumPoint.Y, 0)
        rebarBoxMin = XYZ(outlineRebarShape.MinimumPoint.X, outlineRebarShape.MinimumPoint.Y, 0)
        outlineRebar = Outline(rebarBoxMin, rebarBoxMax)
        
        filterIntersects = outlineRebar.Intersects(outlineRebarPointer, 0)

        if filterIntersects is True:
            # Checking intersections by lines for more accurate results
            rebarCurve = Line.CreateBound(XYZ(startPointLine.X, startPointLine.Y, 0), XYZ(endPointLine.X, endPointLine.Y, 0))
            pointerCurveHorizontal = Line.CreateBound(XYZ(pointerCenterPoint.X+pointerCircleDiameter, pointerCenterPoint.Y, 0), XYZ(pointerCenterPoint.X-pointerCircleDiameter, pointerCenterPoint.Y, 0))
            pointerCurveVertical = Line.CreateBound(XYZ(pointerCenterPoint.X, pointerCenterPoint.Y+pointerCircleDiameter, 0), XYZ(pointerCenterPoint.X, pointerCenterPoint.Y-pointerCircleDiameter, 0))
            if rebarCurve.Intersect(pointerCurveHorizontal).ToString() == "Overlap" or rebarCurve.Intersect(pointerCurveVertical).ToString() == "Overlap":
                goodRebarPointer.append(rebarPointer)

    return goodRebarPointer

def find_rebar_dimension():
    """
    This method finds and returns a list of good rebar dimensions that intersect with the defined shapes.
    It iterates through the dimension lines and rebar pointers to perform intersection checks and filter out valid dimensions.
    """
    startPointLine, endPointLine, outlineRebarShape = RebarShapeProperties()
    goodRebarDimensions = []
    
    # Iterate through dimension lines
    for dimensionLine in rebarDimensionLine:
        # Check if the dimension line has a circle symbol
        if dimensionLine.LookupParameter(SYMBOLCIRCLE).AsValueString() != 'No':
            # Get the geometry of the dimension line
            geoElement = dimensionLine.get_Geometry(options)
            dimensionLinebBox = geoElement.GetBoundingBox()
            outlineDimensionLine = Outline(dimensionLinebBox.Min, dimensionLinebBox.Max)
            
            # Check if the dimension line intersects with the rebar shape outline
            filterIntersects = outlineRebarShape.Intersects(outlineDimensionLine, 0.000001)
            if filterIntersects:
                # Check if the dimension line is on and between the start and end points of the rebar shape
                onAndBetween = check_on_and_between_rebar_shape(geoElement, startPointLine, endPointLine)
                if onAndBetween:
                    # Add the dimension line to the list of good rebar dimensions
                    goodRebarDimensions.append(dimensionLine)
    
    # Iterate through rebar pointers without leaders
    for rebarPointerWithoutLeader in rebarPointersWithoutLeader:
        # Get the bounding box of the rebar pointer
        rebarPointerbBox = rebarPointerWithoutLeader.get_BoundingBox(doc.ActiveView)
        outlineRebarPointer = Outline(XYZ(rebarPointerbBox.Min.X, rebarPointerbBox.Min.Y, 0),
                                      XYZ(rebarPointerbBox.Max.X, rebarPointerbBox.Max.Y, 0))
        
        # Adjust the rebar shape outline to be in the same coordinate system as the rebar pointer
        outlineRebarShapeMaxPoint = outlineRebarShape.MaximumPoint
        outlineRebarShapeMinPoint = outlineRebarShape.MinimumPoint
        updatedOutlineRebarShape = Outline(XYZ(outlineRebarShapeMinPoint.X, outlineRebarShapeMinPoint.Y, 0),
                                           XYZ(outlineRebarShapeMaxPoint.X, outlineRebarShapeMaxPoint.Y, 0))
                
        # Check if the rebar pointer intersects with the adjusted rebar shape outline
        filterIntersects = updatedOutlineRebarShape.Intersects(outlineRebarPointer, 0.000001)
        
        if filterIntersects:
            # Iterate through dimension lines again
            for dimensionLine in rebarDimensionLine:
                # Check if the dimension line has a circle symbol
                if dimensionLine.LookupParameter(SYMBOLCIRCLE).AsValueString() != 'No':
                    # Get the geometry of the dimension line
                    geoElement = dimensionLine.get_Geometry(options)
                    dimensionLinebBox = geoElement.GetBoundingBox()
                    outlineDimensionLine = Outline(XYZ(dimensionLinebBox.Min.X, dimensionLinebBox.Min.Y, 0),
                                                   XYZ(dimensionLinebBox.Max.X, dimensionLinebBox.Max.Y, 0))
                    filterIntersects = outlineRebarPointer.Intersects(outlineDimensionLine, 0.000001)
                    if filterIntersects:
                        # Extract geometry instances from the dimension line geometry
                        geometryInstance = [i for i in geoElement if i.GetType().ToString() == GEOMETRY_INSTANCE]
                        linesGeometry = [i for i in geometryInstance[0].GetInstanceGeometry() if i.GetType().ToString() == LINE]
                        
                        # Iterate through lines in the dimension line geometry
                        for lineGeometry in linesGeometry:
                            # Get the graphics style of the line
                            graphicsStyle = doc.GetElement(lineGeometry.GraphicsStyleId)
                            if graphicsStyle is not None and graphicsStyle.Name == "Rebar Dimension":
                                # Get the radius and center point of the rebar pointer circle
                                pointerCircleRadius = rebarPointerWithoutLeader.AnnotationSymbolType.LookupParameter(POINTERCIRCLEDIAMETER).AsDouble() * 50
                                pointerCenterPoint = rebarPointerWithoutLeader.Location.Point
                                
                                # Check if the line is on and between the start and end points of the rebar shape
                                onAndBetween = check_on_and_between_dimension_line(lineGeometry.GetEndPoint(0), lineGeometry.GetEndPoint(1), pointerCenterPoint, pointerCircleRadius, startPointLine, endPointLine)
                                if onAndBetween:
                                    # Add the dimension line to the list of good rebar dimensions
                                    goodRebarDimensions.append(dimensionLine)
                                    break

    return goodRebarDimensions

def DimensionToQuantity(rebarShapeSpacing):
    dimensionA = matchingRebarDimensions[0].LookupParameter(DIMENSIONLINELENGTH).AsValueString()
    dimensionLength = float(dimensionA)
    dimensionToQuantity = int(math.ceil((dimensionLength/rebarShapeSpacing)+1))
    return dimensionToQuantity

def RebarShapes():
    for oneRebarShape in sortedDetailRebarShapes:
        global rebarShape
        rebarShape = oneRebarShape
        # Get the quantity value from the rebar shape
        rebarShapeQuantity = rebarShape.LookupParameter(LAYERQUANTITY).AsString()
        # Get the spacing value from the rebar shape
        rebarShapeSpacing = rebarShape.LookupParameter(SPACING).AsString()
        rebarShapeManualMultiplierParam = rebarShape.Symbol.LookupParameter(MANUALMULTIPLIER)
        # Calculate how many layers were selected for the rebar shape
        numOfLayers = NumberOfLayers(rebarShape)
        if (rebarShapeQuantity and rebarShapeSpacing):
            errorMessage = "You can't set values for both the Spacing and Quantity parameters at the same time."
            if sheetFlag:
                errorMessage = errorMessage + "\n\n" + sheetErrorMessage
            else:
                ZoomToElement()
            ColorAndAlertRebarShape(errorMessage)
            break
        elif(numOfLayers > 3):
            errorMessage = "You can't select more than 3 layers for a single rebar shape. Please fix this and run the tool again."
            if sheetFlag:
                errorMessage = errorMessage + "\n\n" + sheetErrorMessage
            else:
                ZoomToElement()
            ColorAndAlertRebarShape(errorMessage)
            break
        elif(rebarShapeManualMultiplierParam is not None and rebarShapeManualMultiplierParam.AsInteger() > 1 and numOfLayers > 0):
            errorMessage = "You can't select layers when the manual multiplier of the rebar is 2 and above. Please fix this and run the tool again."
            if sheetFlag:
                errorMessage = errorMessage + "\n\n" + sheetErrorMessage
            else:
                ZoomToElement()
            ColorAndAlertRebarShape(errorMessage)
            break
        elif(rebarShapeQuantity):
            
            manualMultiplierParam = rebarShape.Symbol.LookupParameter(MANUALMULTIPLIER)
            layerTextParam = rebarShape.Symbol.LookupParameter(LAYERTEXT)
            count_of_plus = layerTextParam.AsString().count("+")
            if manualMultiplierParam and manualMultiplierParam.AsInteger() != (count_of_plus + 1):
                errorMessage = ("The '{}' parameter value for the '{}' family type should be '{}'"
                                " based on the '{}' parameter value."
                                " Please adjust the input for proper alignment."
                                ).format(MANUALMULTIPLIER, rebarShape.Name, (count_of_plus + 1), LAYERTEXT)
                DavidEng.ErrorWindow(errortext=errorMessage).ShowDialog()
                break
            
            if rebarShapeQuantity.isdigit():
                if (CheckIfExistInArray(rebarShape)):
                    WriteToArray(rebarShape, False, 0, numOfLayers)
                else:
                    WriteToArray(rebarShape, True, 0, numOfLayers)
            else:
                errorMessage = "The value you entered for the 'Qunatity' parameter is invalid. Please fix this and run the tool again."
                if sheetFlag:
                    errorMessage = errorMessage + "\n\n" + sheetErrorMessage
                else:
                    ZoomToElement()
                ColorAndAlertRebarShape(errorMessage)
                break
        else:
            global matchingRebarDimensions
            matchingRebarDimensions = find_rebar_dimension()
            if len(matchingRebarDimensions) == 1:
                
                manualMultiplierParam = rebarShape.Symbol.LookupParameter(MANUALMULTIPLIER)
                layerTextParam = rebarShape.Symbol.LookupParameter(LAYERTEXT)
                count_of_plus = layerTextParam.AsString().count("+")
                if manualMultiplierParam and manualMultiplierParam.AsInteger() != (count_of_plus + 1):
                    errorMessage = ("The '{}' parameter value for the '{}' family type should be '{}'"
                                    " based on the '{}' parameter value."
                                    " Please adjust the input for proper alignment."
                                    ).format(MANUALMULTIPLIER, rebarShape.Name, (count_of_plus + 1), LAYERTEXT)
                    DavidEng.ErrorWindow(errortext=errorMessage).ShowDialog()
                    break
                
                if rebarShapeSpacing.isdigit():
                    dimensionToQuantity = DimensionToQuantity(int(rebarShapeSpacing))
                    if (CheckIfExistInArray(rebarShape)):
                        WriteToArray(rebarShape, False, dimensionToQuantity, numOfLayers)
                    else:
                        WriteToArray(rebarShape, True, dimensionToQuantity, numOfLayers)
                else:
                    errorMessage = "The value you entered for the 'Spacing' parameter is invalid. Please fix this and run the tool again."
                    if sheetFlag:
                        errorMessage = errorMessage + "\n\n" + sheetErrorMessage
                    else:
                        ZoomToElement()
                    ColorAndAlertRebarShape(errorMessage)
                    break
            elif len(matchingRebarDimensions) == 0:
                errorMessage = "One of the Rebar Shapes doesn't have either a Dimension Line or Quantity. Please fix this and run the tool again."
                if sheetFlag:
                    errorMessage = errorMessage + "\n\n" + sheetErrorMessage
                else:
                    ZoomToElement()
                ColorAndAlertRebarShape(errorMessage)
                break
            elif len(matchingRebarDimensions) > 1:
                errorMessage = "You have more than one Dimension Line for the same Rebar Shape. Please fix this and run the tool again."
                if sheetFlag:
                    errorMessage = errorMessage + "\n\n" + sheetErrorMessage
                else:
                    ZoomToElement()
                ColorAndAlertDimensionLine(errorMessage)
                break

def FabricSheets():
    # Filter of only Fabric Sheets from all the Detail Items
    fabricSheetPlan = []
    for i in detailItemsCollector:
        if type(i) != FilledRegion:
            if (FABRIC_SHEET_PLAN in i.Symbol.Family.Name):
                fabricSheetPlan.append(i)
                # try:
                #     if float(i.Symbol.LookupParameter('Family Version').AsString()) >= FABRIC_SHEET_PLAN_VERSION:
                #         fabricSheetPlan.append(i)
                # except:
                #     pass
    if cropViewVisible == 1:
        fabricSheetPlan = FailterCropBox(fabricSheetPlan)
    
    global namesAndMassList
    namesAndMassList = []
    if (fabricSheetPlan):
        
        # Sort the list of the Fabric Sheets by the name
        # try to take the FABRICSHEETNAME parameter from the Type or the Instance
        try:
            fabricSheetPlanName = [i.LookupParameter(FABRICSHEETNAME).AsString() for i in fabricSheetPlan]
        except Exception:
            fabricSheetPlanName = [i.Symbol.LookupParameter(FABRICSHEETNAME).AsString() for i in fabricSheetPlan]
        zipped_lists = zip(fabricSheetPlanName, fabricSheetPlan)
        sorted_zipped_lists = sorted(zipped_lists)
        sortedFabricSheetPlan = [element for _, element in sorted_zipped_lists]
        
        sheetNumber = doc.ActiveView.LookupParameter("Sheet Number").AsString()
        if sheetNumber == "---":
            errorMessage = "The View Plan must be placed in a Sheet in order to recognize the Fabric Sheet Details."
            return errorMessage, 0
        else:
            namesList = []
            for fabricSheet in sortedFabricSheetPlan:
                # try to take the FABRICSHEETNAME parameter from the Type or the Instance
                try:
                    fabricSheetName = fabricSheet.LookupParameter(FABRICSHEETNAME).AsString()
                except Exception:
                    fabricSheetName = fabricSheet.Symbol.LookupParameter(FABRICSHEETNAME).AsString()
                if fabricSheetName == "":
                    errorMessage = "You must set the name of the Fabric Sheet."
                    if sheetFlag:
                        errorMessage = errorMessage + "\n\n" + sheetErrorMessage
                    return errorMessage, fabricSheet
                elif not fabricSheetName in namesList:
                    namesList.append(fabricSheetName)

            # Collector of all the Sheets from the project
            sheetsCollector = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Sheets).WhereElementIsNotElementType()
            for sheet in sheetsCollector:
                if sheetNumber == sheet.SheetNumber:
                    matchingSheet = sheet
            viewportsCollector = FilteredElementCollector(doc, matchingSheet.Id).OfCategory(BuiltInCategory.OST_Viewports).WhereElementIsNotElementType()
            for fabricName in namesList:
                matchingDetailFlag = False
                # For each viewport in the sheet
                for viewport in viewportsCollector:
                    detailItemsCollectorViewport = FilteredElementCollector(doc, viewport.ViewId).OfCategory(BuiltInCategory.OST_DetailComponents).WhereElementIsNotElementType()
                    # Filter of only Fabric Sheet Details from all the Detail Items of the Viewport
                    fabricSheetDetails = []
                    for i in detailItemsCollectorViewport:
                       if type(i) != FilledRegion:
                            if FABRIC_SHEET_DETAIL in i.Symbol.Family.Name:
                                fabricSheetDetails.append(i)
                                # try:
                                #     if float(i.Symbol.LookupParameter('Family Version').AsString()) >= FABRIC_SHEET_DETAIL_VERSION:
                                #         fabricSheetDetails.append(i)
                                # except:
                                #     pass
                    if fabricSheetDetails:
                        for fabricSheetDetail in fabricSheetDetails:
                            fabricSheetDetailType = fabricSheetDetail.GetTypeId()
                            if namesAndMassList:
                                for nameAndMass in namesAndMassList:
                                    if doc.GetElement(fabricSheetDetailType).LookupParameter(SHEETNAME).AsString() == nameAndMass[0] \
                                        and fabricName == doc.GetElement(fabricSheetDetailType).LookupParameter(SHEETNAME).AsString():
                                        errorMessage = "You can't have more than one Fabric Sheet Details with the same name of '{}'.".format(fabricName)
                                        return errorMessage, 0
                            if fabricName == doc.GetElement(fabricSheetDetailType).LookupParameter(SHEETNAME).AsString():
                                fabricMass = fabricSheetDetail.LookupParameter(SHEETMASS).AsDouble()/1000
                                fabricMassPerUnit = fabricSheetDetail.LookupParameter(SHEETMASSPERUNIT).AsValueString()
                                fabricMajorWireType = fabricSheetDetail.LookupParameter(SHEETMAJORWIRETYPE).AsDouble()*304.8
                                fabricMinorWireType = fabricSheetDetail.LookupParameter(SHEETMINORWIRETYPE).AsDouble()*304.8
                                fabricMajorLength = fabricSheetDetail.LookupParameter("Major Lenght").AsDouble()*30.48
                                fabricMinorLength = fabricSheetDetail.LookupParameter("Minor Width").AsDouble()*30.48
                                namesAndMassList.append([fabricName, fabricMass, fabricMassPerUnit, fabricMajorWireType, fabricMinorWireType, fabricMajorLength, fabricMinorLength])
                                matchingDetailFlag = True
                                
                            # check if the size of the fabric sheet in the plan view is matching the size of the detail in the sheet
                            if matchingDetailFlag == True:
                                for fabricSheet in sortedFabricSheetPlan:
                                    # try to take the FABRICSHEETNAME parameter from the Type or the Instance
                                    try:
                                        fabricSheetName = fabricSheet.LookupParameter(FABRICSHEETNAME).AsString()
                                    except Exception:
                                        fabricSheetName = fabricSheet.Symbol.LookupParameter(FABRICSHEETNAME).AsString()
                                    fabricSheetMajorLength = fabricSheet.Symbol.LookupParameter(SHEETMAJORLENGTH).AsDouble()*30.48
                                    fabricSheetMinorLength = fabricSheet.Symbol.LookupParameter(SHEETMINORLENGTH).AsDouble()*30.48
                                    for nameAndMass in namesAndMassList:
                                        if fabricSheetName == nameAndMass[0]:
                                            if not (round(fabricSheetMajorLength, 2) == round(nameAndMass[5], 2)
                                                    and round(fabricSheetMinorLength, 2) == round(nameAndMass[6], 2)):
                                                errorMessage = "The size of the fabric sheet in the plan view is not matching the size of the detail in the sheet."
                                                if sheetFlag:
                                                    errorMessage = errorMessage + "\n\n" + sheetErrorMessage
                                                return errorMessage, fabricSheet
                                            break
                                        
                if matchingDetailFlag == False:
                    errorMessage = "Fabric Sheet Name '{}' have no matching detail in the sheet in order to know the Sheet Mass".format(fabricName)
                    return errorMessage, 0
            
            for fabricSheet in sortedFabricSheetPlan:
                if (CheckIfExistInFabricSheetArray(fabricSheet)):
                    WriteToArrayMesh(fabricSheet, False)
                else:
                    WriteToArrayMesh(fabricSheet, True)

            return 0, 0
    else:
        # If there are no Fabric Sheet in the current view
        return -1, -1

def WriteToArrayMesh(fabricSheet, newFlag):
    # try to take the FABRICSHEETNAME parameter from the Type or the Instance
    try:
        fabricSheetName = fabricSheet.LookupParameter(FABRICSHEETNAME).AsString()
    except Exception:
        fabricSheetName = fabricSheet.Symbol.LookupParameter(FABRICSHEETNAME).AsString()
    fabricSheetDividend = fabricSheet.LookupParameter(SHEETDIVIDEND).AsInteger()
    fabricSheetDivisor = fabricSheet.LookupParameter(SHEETDIVISOR).AsInteger()
    fabricSheetMinorLength = doc.GetElement(fabricSheet.GetTypeId()).LookupParameter(SHEETMINORLENGTH).AsDouble()*30.48
    fabricSheetMajorLength = doc.GetElement(fabricSheet.GetTypeId()).LookupParameter(SHEETMAJORLENGTH).AsDouble()*30.48
    
    rowInList = 0
    for fabric in namesAndMassList:
        if fabric[0] == fabricSheetName:
            detailSheetMass = fabric[1]
            detailSheetMassPerUnit = fabric[2]
            detailSheetDiameter = fabric[3]
            break
        else:
            rowInList += 1
    
    if fabricSheetDividend > 0 and fabricSheetDivisor > 0:
        sheetMassToAdd = detailSheetMass*(float(fabricSheetDividend)/float(fabricSheetDivisor))
        meshSize = str(fabricSheetDividend) + "/" + str(fabricSheetDivisor)
    else:
        sheetMassToAdd = detailSheetMass
        meshSize = "Full"

    if newFlag == False:
        fabricSheetQuantitiesArray[checkRow][5] += 1
        fabricSheetQuantitiesArray[checkRow][7] = fabricSheetQuantitiesArray[checkRow][7] + sheetMassToAdd
        meshElementsToSet.append([fabricSheet.Id, detailSheetDiameter, meshSize, detailSheetMassPerUnit, sheetMassToAdd])
    else:
        row = len(fabricSheetQuantitiesArray)
        fabricSheetQuantitiesArray.append([])
        fabricSheetQuantitiesArray[row].append(fabricSheetName)
        fabricSheetQuantitiesArray[row].append(detailSheetDiameter)
        fabricSheetQuantitiesArray[row].append(meshSize)
        fabricSheetQuantitiesArray[row].append(fabricSheetMinorLength)
        fabricSheetQuantitiesArray[row].append(fabricSheetMajorLength)
        fabricSheetQuantitiesArray[row].append(1)
        fabricSheetQuantitiesArray[row].append(detailSheetMassPerUnit)
        fabricSheetQuantitiesArray[row].append(sheetMassToAdd)
        meshElementsToSet.append([fabricSheet.Id, detailSheetDiameter, meshSize, detailSheetMassPerUnit, sheetMassToAdd])

def CheckIfExistInFabricSheetArray(fabricSheet):
    if (len(fabricSheetQuantitiesArray) == 1):
        global startRowToCheck
        startRowToCheck = 1
        return False
    else:
        # try to take the FABRICSHEETNAME parameter from the Type or the Instance
        try:
            fabricSheetNameParam = fabricSheet.LookupParameter(FABRICSHEETNAME).AsString()
        except Exception:
            fabricSheetNameParam = fabricSheet.Symbol.LookupParameter(FABRICSHEETNAME).AsString()
        if fabricSheetQuantitiesArray[startRowToCheck][0] != fabricSheetNameParam:
            startRowToCheck = len(fabricSheetQuantitiesArray)
        global checkRow
        checkRow = startRowToCheck
        while checkRow < len(fabricSheetQuantitiesArray):
            # try to take the FABRICSHEETNAME parameter from the Type or the Instance
            try:
                fabricSheetNameParam = fabricSheet.LookupParameter(FABRICSHEETNAME).AsString()
            except Exception:
                fabricSheetNameParam = fabricSheet.Symbol.LookupParameter(FABRICSHEETNAME).AsString()
            if fabricSheetQuantitiesArray[checkRow][0] == fabricSheetNameParam \
               and fabricSheetQuantitiesArray[checkRow][3] == doc.GetElement(fabricSheet.GetTypeId()).LookupParameter(SHEETMINORLENGTH).AsDouble()*30.48 \
               and fabricSheetQuantitiesArray[checkRow][4] == doc.GetElement(fabricSheet.GetTypeId()).LookupParameter(SHEETMAJORLENGTH).AsDouble()*30.48:
                   return True
            else:
                checkRow += 1
        return False

def ZoomToFabricSheet(elementToShow):
    # Getting the geometry and the bounding box of the rebar shape
    geoFabricSheet = elementToShow.get_Geometry(options)
    bBoxFabricSheet = geoFabricSheet.GetBoundingBox()

    outlineFabricSheet = Outline(bBoxFabricSheet.Min, bBoxFabricSheet.Max)
        
    active_ui_views = uidoc.GetOpenUIViews()
    # find current uiview
    current_ui_view_list = []
    for active_ui_view in active_ui_views:
        if active_ui_view.ViewId == doc.ActiveView.Id:
            current_ui_view_list.append(active_ui_view)
    for current_ui_view in current_ui_view_list:
        current_ui_view.ZoomAndCenterRectangle(outlineFabricSheet.MaximumPoint, outlineFabricSheet.MinimumPoint)

# Filter only the elements that are visible in the active crop box
def FailterCropBox(elementsList):
    cropBox = doc.ActiveView.CropBox
    
    # transform basis of the old coordinate system in the new coordinate // system
    transform = cropBox.Transform
    b0 = transform.BasisX
    b1 = transform.BasisY
    b2 = transform.BasisZ
    origin = transform.Origin
    
    # transform the origin of the old coordinate system in the new coordinate system
    xTempMin = cropBox.Min.X * b0.X + cropBox.Min.Y * b1.X + cropBox.Min.Z * b2.X + origin.X
    yTempMin = cropBox.Min.X * b0.Y + cropBox.Min.Y * b1.Y + cropBox.Min.Z * b2.Y + origin.Y
    # zTempMin = cropBox.Min.X * b0.Z + cropBox.Min.Y * b1.Z + cropBox.Min.Z * b2.Z + origin.Z
    xTempMax = cropBox.Max.X * b0.X + cropBox.Max.Y * b1.X + cropBox.Max.Z * b2.X + origin.X
    yTempMax = cropBox.Max.X * b0.Y + cropBox.Max.Y * b1.Y + cropBox.Max.Z * b2.Y + origin.Y
    # zTempMax = cropBox.Max.X * b0.Z + cropBox.Max.Y * b1.Z + cropBox.Max.Z * b2.Z + origin.Z
    
    newElementsList = []
    view_scope_box = doc.GetElement(doc.ActiveView.get_Parameter(BuiltInParameter.VIEWER_VOLUME_OF_INTEREST_CROP).AsElementId())
    if not (view_scope_box == None):
        view_scope_box_geometry = view_scope_box.get_Geometry(options)
        for element in elementsList:
            geoElement = element.get_Geometry(options)
            elementBBox = geoElement.GetBoundingBox()
            infinate_line_1 = Line.CreateBound(XYZ(elementBBox.Min.X,elementBBox.Min.Y,0), XYZ(elementBBox.Min.X,9999,0))
            infinate_line_2 = Line.CreateBound(XYZ(elementBBox.Max.X,elementBBox.Min.Y,0), XYZ(elementBBox.Max.X,9999,0))
            infinate_line_3 = Line.CreateBound(XYZ(elementBBox.Max.X,elementBBox.Max.Y,0), XYZ(elementBBox.Max.X,9999,0))
            infinate_line_4 = Line.CreateBound(XYZ(elementBBox.Min.X,elementBBox.Max.Y,0), XYZ(elementBBox.Min.X,9999,0))
            intersect_line_1 = 0
            intersect_line_2 = 0
            intersect_line_3 = 0
            intersect_line_4 = 0
            for line in view_scope_box_geometry:
                scope_box_line = Line.CreateBound(XYZ(line.GetEndPoint(0).X,line.GetEndPoint(0).Y,0), XYZ(line.GetEndPoint(1).X,line.GetEndPoint(1).Y,0))
                if (scope_box_line.Intersect(infinate_line_1) == SetComparisonResult.Overlap):
                    intersect_line_1 += 1
                if (scope_box_line.Intersect(infinate_line_2) == SetComparisonResult.Overlap):
                    intersect_line_2 += 1
                if (scope_box_line.Intersect(infinate_line_3) == SetComparisonResult.Overlap):
                    intersect_line_3 += 1
                if (scope_box_line.Intersect(infinate_line_4) == SetComparisonResult.Overlap):
                    intersect_line_4 += 1
            if(intersect_line_1 == 1 or intersect_line_2 == 1 or intersect_line_3 == 1 or intersect_line_4 == 1):
                newElementsList.append(element)
    else:
        outlineCropBox = Outline(XYZ(min(xTempMin, xTempMax), min(yTempMin, yTempMax), 0), XYZ(max(xTempMin, xTempMax), max(yTempMin, yTempMax), 0))
        for element in elementsList:
            geoElement = element.get_Geometry(options)
            elementBBox = geoElement.GetBoundingBox()
            outlineElement = Outline(XYZ(elementBBox.Min.X, elementBBox.Min.Y, 0), XYZ(elementBBox.Max.X, elementBBox.Max.Y, 0))
            checkIntersects = outlineCropBox.Intersects(outlineElement,0)
            if checkIntersects:
                newElementsList.append(element)
    return newElementsList

def lineRect(x1, y1, x2, y2, rxMin, ryMin, rxMax, ryMax):
    # check if the line has hit any of the rectangle's sides
    # uses the Line/Line function below
    left =   lineLine(x1, y1, x2, y2, rxMin, ryMin, rxMin, ryMax)
    right =  lineLine(x1, y1, x2, y2, rxMax, ryMin, rxMax, ryMax)
    top =    lineLine(x1, y1, x2, y2, rxMin, ryMin, rxMax, ryMin)
    bottom = lineLine(x1, y1, x2, y2, rxMin, ryMax, rxMax, ryMax)
    
    # if ANY of the above are true, the line
    # has hit the rectangle
    if (left or right or top or bottom):
        return True
    return False

def lineLine(x1, y1, x2, y2, x3, y3, x4, y4):
    # calculate the direction of the lines
    if (((y4-y3)*(x2-x1) - (x4-x3)*(y2-y1)) == 0):
        return False
    uA = ((x4-x3)*(y1-y3) - (y4-y3)*(x1-x3)) / ((y4-y3)*(x2-x1) - (x4-x3)*(y2-y1))
    uB = ((x2-x1)*(y1-y3) - (y2-y1)*(x1-x3)) / ((y4-y3)*(x2-x1) - (x4-x3)*(y2-y1))
    
    # if uA and uB are between 0-1, lines are colliding
    if (uA >= 0 and uA <= 1 and uB >= 0 and uB <= 1):
        return True
    
    return False

def LeaderCrossPointer():
    for rebarPointer in rebarPointers:    
        crossLeadersList = []
        crossLeadersList.append(rebarPointer)
        crossLeadersCounter = 0
            
        pointerCircleDiameterParam = rebarPointer.LookupParameter(POINTERCIRCLEDIAMETER)
        if pointerCircleDiameterParam is not None:
            pointerCircleDiameter = rebarPointer.LookupParameter(POINTERCIRCLEDIAMETER).AsDouble()*100/2
        else:
            pointerCircleDiameter = rebarPointer.Symbol.LookupParameter(POINTERCIRCLEDIAMETER).AsDouble()*100/2
        pointerCenterPoint = rebarPointer.Location.Point
        
        # Model Bounding Box
        BoxMax = XYZ(pointerCenterPoint.X+pointerCircleDiameter, pointerCenterPoint.Y+pointerCircleDiameter, 0)
        BoxMin = XYZ(pointerCenterPoint.X-pointerCircleDiameter, pointerCenterPoint.Y-pointerCircleDiameter, 0)
        outlineRebarPointer = Outline(BoxMin, BoxMax)
        for secondRebarPointer in rebarPointers:  
            if not secondRebarPointer == rebarPointer:
                bBoxRebarPointerMax = secondRebarPointer.BoundingBox[doc.ActiveView].Max
                bBoxRebarPointerMin = secondRebarPointer.BoundingBox[doc.ActiveView].Min
                pointerBoxMax = XYZ(bBoxRebarPointerMax.X, bBoxRebarPointerMax.Y, 0)
                pointerBoxMin = XYZ(bBoxRebarPointerMin.X, bBoxRebarPointerMin.Y, 0)
                outlineSecondRebarPointer = Outline(pointerBoxMin, pointerBoxMax)
                filterIntersects = outlineSecondRebarPointer.Intersects(outlineRebarPointer, 0)
                
                if filterIntersects:
                    leadersList = secondRebarPointer.GetLeaders()
                    if (leadersList):
                        for leader in leadersList:
                            leaderElbow = leader.Elbow
                            leaderEnd = leader.End
                            checkLeaderIntersect = lineRect(leaderElbow.X, leaderElbow.Y, leaderEnd.X, leaderEnd.Y, BoxMin.X, BoxMin.Y, BoxMax.X, BoxMax.Y)
                            if checkLeaderIntersect:
                                crossLeadersCounter += 1
        crossLeadersList.append(crossLeadersCounter)
        PointerAndLeaderIntersection.append(crossLeadersList)

def main():
    continueFlag = False
    if type(doc.ActiveView) == ViewPlan:
        
        # Check if the user run the tool from Sheet or Plan View in order to know if to zoom the invalid elements or not (in sheet we won't be able to use the zoom function well enough)
        global sheetFlag
        if not "Sheet:" in docTitle:
            sheetFlag = False
        else:
            sheetFlag = True

        global cropViewVisible
        cropViewVisible = doc.ActiveView.LookupParameter("Crop View").AsInteger()
        
        # Collector of all the Detail Components (Detail Items) from the active view
        global detailItemsCollector
        detailItemsCollector = FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_DetailComponents).WhereElementIsNotElementType()

        # Filter of only Detail Rebar Shapes from all the Detail Items
        detailRebarShapes = []
        for i in detailItemsCollector:
            if type(i) != FilledRegion:
                if (DAVIDENG_DETAIL_REBAR_SHAPE in i.Symbol.Family.Name):
                    detailRebarShapes.append(i)
                    # try:
                    #     if float(i.Symbol.LookupParameter('Family Version').AsString()) >= DAVIDENG_DETAIL_REBAR_SHAPE_VERSION:
                    #         detailRebarShapes.append(i)
                    # except:
                    #     pass
        rebarShapesDiameter = [i.LookupParameter(REBARDIAMETER).AsDouble()*304.8 for i in detailRebarShapes]
        zipped_lists = zip(rebarShapesDiameter, detailRebarShapes)
        sorted_zipped_lists = sorted(zipped_lists)
        global sortedDetailRebarShapes
        sortedDetailRebarShapes = [element for _, element in sorted_zipped_lists]
        
        if cropViewVisible == 1:
            sortedDetailRebarShapes = FailterCropBox(sortedDetailRebarShapes)
            
        # Collect all the pointers from the current view
        genericAnnotationsCollector = FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_GenericAnnotation).WhereElementIsNotElementType()
        global rebarPointers
        rebarPointers = []
        for i in genericAnnotationsCollector:
            if DAVIDENG_REBAR_POINTER in i.Symbol.Family.Name:
                rebarPointers.append(i)
                # try:
                #     if float(i.Symbol.LookupParameter('Family Version').AsString()) >= DAVIDENG_REBAR_POINTER_VERSION:
                #         rebarPointers.append(i)
                # except:
                #     pass
                
        global rebarPointersWithoutLeader
        rebarPointersWithoutLeader = []
        for rebarPointer in rebarPointers:
            leaders_list = rebarPointer.GetLeaders()
            if (len(leaders_list) == 0):
                rebarPointersWithoutLeader.append(rebarPointer)

        global PointerAndLeaderIntersection
        PointerAndLeaderIntersection = []
        LeaderCrossPointer()

        # Collect all the detail rebar dimensions from the current view
        detailComponentsCollector = FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_DetailComponents).WhereElementIsNotElementType()
        global rebarDimensionLine
        rebarDimensionLine = []
        for i in detailComponentsCollector:
            if type(i) != FilledRegion:
                if (DAVIDENG_DETAIL_REBAR_DIMENSION in i.Symbol.Family.Name):
                    rebarDimensionLine.append(i)
                    # try:
                    #     if float(i.Symbol.LookupParameter('Family Version').AsString()) >= DAVIDENG_DETAIL_REBAR_DIMENSION_VERSION:
                    #         rebarDimensionLine.append(i)
                    # except:
                    #     pass
        
        RebarShapes()
        if len(elementIdAndMark) == len(sortedDetailRebarShapes) and len(sortedDetailRebarShapes) == 0:
            rebarShapesFlag = "Empty"
        elif len(elementIdAndMark) == len(sortedDetailRebarShapes):
            rebarShapesFlag = True
        else:
            rebarShapesFlag = False
        
        if rebarShapesFlag != False:
            fabricSheetErrorMessage, elementToShow = FabricSheets()
            if fabricSheetErrorMessage == -1:
                fabricSheetFlag = "Empty"
            elif fabricSheetErrorMessage == 0 and elementToShow == 0:
                fabricSheetFlag = True
            else:
                fabricSheetFlag = False
                if elementToShow == 0:
                    DavidEng.ErrorWindow(errortext=fabricSheetErrorMessage).ShowDialog()
                else:
                    if not sheetFlag:
                        ZoomToFabricSheet(elementToShow)
                    ColorAndAlertFabricSheet(fabricSheetErrorMessage, elementToShow)
        else:
            fabricSheetFlag = False
        
        if rebarShapesFlag == "Empty" and fabricSheetFlag == "Empty":
            DavidEng.ErrorWindow(errortext="There is no any rebar shape or fabic sheet to collect from the current view.").ShowDialog()
            return quantitiesArray, fabricSheetQuantitiesArray, continueFlag, False, False
        elif rebarShapesFlag != False and fabricSheetFlag != False:
            tg = TransactionGroup(doc, 'Set values for the Rebar Shapes and Fabric Sheets')
            tg.Start()
            groupFlag = False
            if rebarShapesFlag == True:
                t = Transaction(doc, 'Set values to Rebar Shapes')
                t.Start()
                
                imageElementAndNameList = []
                # Collect all the Reaster Images from the document
                rasterImagesCollector = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_RasterImages)
                rasterImagesCollectorList = [(i) for i in rasterImagesCollector]
                # Create a list of onlt the names of the raster images
                rasterImagesNamesList = [(i.LookupParameter('Type Name').AsString()) for i in rasterImagesCollector]
                # Add the image to the document if it is not exist yet
                for rebarShapesImageName in rebarShapesImagesNames:
                    counter = 0
                    while counter < len(rasterImagesNamesList):
                        if rebarShapesImageName == rasterImagesNamesList[counter]:
                            imageElementAndNameList.append([rebarShapesImageName, rasterImagesCollectorList[counter]])
                            break
                        counter += 1
                    if counter == len(rasterImagesNamesList):
                        file_path = os.path.dirname(__file__) + '\\RebarShapesImages\\' + rebarShapesImageName
                        # The API of importing images has changed from version 2021 of Revit
                        if HOST_APP.is_newer_than(2020):
                            image_type_options = DB.ImageTypeOptions(file_path, False, DB.ImageTypeSource.Import)
                            newImageType = DB.ImageType.Create(doc, image_type_options)
                        else:
                            new_img_element = clr.StrongBox[DB.Element]()
                            import_options = DB.ImageImportOptions()
                            doc.Import(file_path, import_options, doc.ActiveView, new_img_element)
                            for rasterImage in rasterImagesCollector:
                                if rasterImage.LookupParameter('Type Name').AsString() == rebarShapesImageName:
                                    newImageType = rasterImage
                                    break
                        imageElementAndNameList.append([rebarShapesImageName, newImageType])
                
                for element in elementIdAndMark:
                    if doc.GetElement(element[0]).GroupId.IntegerValue < 0:
                        doc.GetElement(element[0]).LookupParameter(MARK).Set(str(element[1]))
                        doc.GetElement(element[0]).LookupParameter(TOTALQUANTITY).Set(int(element[2]))
                        doc.GetElement(element[0]).LookupParameter(VIEWNAME).Set(doc.ActiveView.Name)
                        # This parameter was delete in the latest version. 
                        # Remained for backward compatibility but can be deleted later.
                        show_number_of_layers_param = doc.GetElement(element[0]).LookupParameter(SHOWNUMBERSOFLAYERS)
                        if (show_number_of_layers_param is not None):
                            rebarShapeSpacing = doc.GetElement(element[0]).LookupParameter(SPACING).AsString()
                            if (rebarShapeSpacing):
                                show_number_of_layers_param.Set(1)
                            else:
                                show_number_of_layers_param.Set(0)
                        if element[3] == 0:
                            doc.GetElement(element[0]).LookupParameter(MULTIPLIER).Set("")
                        else:
                            doc.GetElement(element[0]).LookupParameter(MULTIPLIER).Set(str(element[3]))
                            
                        image_name = element[4]
                        if image_name == 'RebarShapeImage_CD':
                            image_name = 'RebarShapeImage_BC'
                        elif image_name == 'RebarShapeImage_CDE':
                            image_name = 'RebarShapeImage_ABC'
                        elif image_name == 'RebarShapeImage_BCDE':
                            image_name = 'RebarShapeImage_ABCD'
                        
                        for imageElementAndName in imageElementAndNameList:
                            if imageElementAndName[0] == (image_name + '.jpg'):
                                doc.GetElement(element[0]).LookupParameter('Image').Set(imageElementAndName[1].Id)
                                break
                    else:
                        groupFlag = True
                t.Commit()
            if fabricSheetFlag == True:
                t = Transaction(doc, 'Set values to Fabric Sheets')
                t.Start()
                for element in meshElementsToSet:
                    if doc.GetElement(element[0]).GroupId.IntegerValue < 0:
                        doc.GetElement(element[0]).LookupParameter(MESHBARDIAMETER).Set(float(element[1])/304.8)
                        doc.GetElement(element[0]).LookupParameter(MESHSIZE).Set(str(element[2]))
                        doc.GetElement(element[0]).LookupParameter(MESHMASSPERUNITAREA).Set(float(element[3].split(" ", 1)[0])/10.76)
                        doc.GetElement(element[0]).LookupParameter(MESHMASS).Set(float(element[4])*1000)
                        doc.GetElement(element[0]).LookupParameter(VIEWNAME).Set(doc.ActiveView.Name)
                    else:
                        groupFlag = True
                t.Commit()
            if groupFlag == True:
                DavidEng.ErrorWindow(errortext="We couldn't set values for Rebar Shapes and Fabric Sheets that are in group.").ShowDialog()
            tg.Assimilate()
            continueFlag = True
            return quantitiesArray, fabricSheetQuantitiesArray, continueFlag, rebarShapesFlag, fabricSheetFlag
        else:
            return quantitiesArray, fabricSheetQuantitiesArray, continueFlag, False, False

    else:
        DavidEng.ErrorWindow(errortext="You can only run this\nadd-in on Plan Views").ShowDialog()
        return quantitiesArray, fabricSheetQuantitiesArray, continueFlag, False, False