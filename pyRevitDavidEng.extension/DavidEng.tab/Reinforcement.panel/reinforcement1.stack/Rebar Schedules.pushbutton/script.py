# # -*- coding: utf-8 -*-

"""Version: 1.3.8
Rebar Schedules Tool collects all the visible rebar families from the current view, calculates the quantities and export it to Excel file."""

# App, Author Name and Version
__title__ = 'Rebar\nSchedules'
__authors__ = 'Yuval Dolin'
__version__ = '1.3.8'

import sys
import os
sys.path.append(os.path.dirname(__file__))
from core import main
from pyrevit import revit, DB
from pyrevit import script
from pyrevit import forms

# Import libraries
import xlsxwriter
import DavidEng
from datetime import datetime

# find the path of RebarShape.jpg
RebarShapeJPG = script.get_bundle_file('RebarShape.jpg')

# Store current document into variable
doc = __revit__.ActiveUIDocument.Document
uidoc = __revit__.ActiveUIDocument

# Retrieve date
now = datetime.now()
date = now.strftime("%Y-%m-%d")

if (DavidEng.checkPermission()):
    
    quantitiesArray, fabricSheetQuantitiesArray, continueFlag, rebarShapesFlag, fabricSheetFlag = main()

    if continueFlag == True:
        # Select folder to export excel
        destinationFolder = forms.pick_folder()
        
        if destinationFolder:
            if rebarShapesFlag == True:
                # Check maximun length of data in columns
                barLengths = [len(x) for x in quantitiesArray[0]]
                for row in quantitiesArray:
                    for cell, lng in zip(row, barLengths):
                        newLength = len(str(cell))
                        if newLength > lng:
                            ind = barLengths.index(lng)
                            barLengths[ind] = newLength
            if fabricSheetFlag == True:
                # Check maximun length of data in columns
                meshLengths = [len(x) for x in fabricSheetQuantitiesArray[0]]
                for row in fabricSheetQuantitiesArray:
                    for cell, lng in zip(row, meshLengths):
                        newLength = len(str(cell))
                        if newLength > lng:
                            ind = meshLengths.index(lng)
                            meshLengths[ind] = newLength
            if rebarShapesFlag == True and fabricSheetFlag == True:
                lengths = barLengths
                for cell, lng in zip(row, meshLengths):
                    ind = meshLengths.index(lng)
                    if meshLengths[ind] > barLengths[ind]:
                        lengths[ind] = meshLengths[ind]
                    else:
                        lengths[ind] = barLengths[ind]
            elif rebarShapesFlag == True:
                lengths = barLengths
            elif fabricSheetFlag == True:
                lengths = meshLengths
                        
            # Export data to excel
            filePath = destinationFolder + "\\"  + doc.ActiveView.Name + "_" + date + ".xlsx"
            if os.path.isfile(filePath) == True:
                fileNameCounter = 2
                while os.path.isfile(filePath) == True:
                    filePath = destinationFolder + "\\"  + doc.ActiveView.Name + "_" + date + " (" + str(fileNameCounter) + ")" + ".xlsx"
                    fileNameCounter += 1
            workbook = xlsxwriter.Workbook(filePath, {'strings_to_numbers': True})
            worksheet = workbook.add_worksheet(doc.ActiveView.Name)
            
            # Define format for cells
            fontSize = 12
            fillColor = "#007c85"
            widthFactor = 1.5
            titleFormat = workbook.add_format({"bold": True, "font_size": fontSize})
            subtitleFormat = workbook.add_format({"bg_color": fillColor, "bold": True, 'font_color': "white", "font_size": fontSize})
            cellFormat = workbook.add_format({"font_size": fontSize, 'align': "left"})
            
            # Set columns width
            for le, i in zip(lengths, range(len(lengths))):
                worksheet.set_column(i, i, le * widthFactor)

            # Start from the first cell. Rows and columns are zero indexed.
            row = 3
            col = 0

            # Set title of schedule
            worksheet.write(0, 0, doc.ActiveView.Name + " Rebar Schedules", titleFormat)
            
            if rebarShapesFlag == True:
                worksheet.insert_image('F1', RebarShapeJPG, {'x_scale': 0.3, 'y_scale': 0.3})
                worksheet.write(1, 0, "", cellFormat)
                worksheet.write(2, 0, "Bars Schedule", titleFormat)
                # Iterate over the data and write it out row by row.
                for item in quantitiesArray:
                    first, rest = item[0], item[1:]
                    for it in item:
                        if row == 3:
                            worksheet.write(row, col, it, subtitleFormat)
                        else:
                            if type(it)==float:
                                it = round(it, 2)
                            worksheet.write(row, col, it, cellFormat)
                        col += 1
                    row += 1
                    col = 0
            
            if fabricSheetFlag == True:
                if rebarShapesFlag == True:
                    row = 5 + len(quantitiesArray)
                    worksheet.write(3 + len(quantitiesArray), 0, "", cellFormat)
                    worksheet.write(4 + len(quantitiesArray), 0, "Meshes Schedule", titleFormat)
                    # Iterate over the data and write it out row by row.
                    for item in fabricSheetQuantitiesArray:
                        first, rest = item[0], item[1:]
                        for it in item:
                            if row == 5 + len(quantitiesArray):
                                worksheet.write(row, col, it, subtitleFormat)
                            else:
                                if type(it)==float:
                                    it = round(it, 2)
                                worksheet.write(row, col, it, cellFormat)
                            col += 1
                        row += 1
                        col = 0
                else:
                    worksheet.write(1, 0, "", cellFormat)
                    worksheet.write(2, 0, "Meshes Schedule", titleFormat)
                    # Iterate over the data and write it out row by row.
                    for item in fabricSheetQuantitiesArray:
                        first, rest = item[0], item[1:]
                        for it in item:
                            if row == 3:
                                worksheet.write(row, col, it, subtitleFormat)
                            else:
                                if type(it)==float:
                                    it = round(it, 2)
                                worksheet.write(row, col, it, cellFormat)
                            col += 1
                        row += 1
                        col = 0
                
                
            workbook.close()
else:
    DavidEng.ErrorWindow(errortext="You don't have permission\nto use the tool").ShowDialog()
