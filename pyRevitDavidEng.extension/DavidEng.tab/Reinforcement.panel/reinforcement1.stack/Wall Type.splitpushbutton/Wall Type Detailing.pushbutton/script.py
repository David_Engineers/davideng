# # -*- coding: utf-8 -*-

"""Gathers the values from the parameter 'Wall Type' (wall parameter that added by the user in Revit) and ask the user to define horizontal and vertical reinforcement parameters.
The script will tag all the walls (that got value for the 'Wall Type' parameter) and create new legend with the wall type table."""
from pyrevit import coreutils, UI, revit, DB, forms, script
from pyrevit.revit.db import query
from pyrevit.framework import List
from Autodesk.Revit import Exceptions
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, BuiltInParameter, Transaction, XYZ, ElementId, Line, ReferenceArray, FamilyInstance, Reference, Options, Edge, Solid, \
                              DimensionType, BuiltInParameterGroup, ElementTypeGroup, TextNoteOptions, HorizontalTextAlignment, SharedParameterElement, Family, DimensionStyleType, TextNoteType
from Autodesk.Revit.UI.Selection import ObjectType, ISelectionFilter

import System
import sys
import os

# dependencies
import clr
clr.AddReference('System.Windows.Forms')

# import WPF creator and base Window
import wpf
import DavidEng
from System import Windows

# Add-in path
sys.path.append(os.path.dirname(__file__))

# find the path of ErrorWindow.xaml
xamlfileError = script.get_bundle_file('ErrorWindow.xaml')

# find the path of SuccessWindow.xaml
xamlfileSuccess = script.get_bundle_file('SuccessWindow.xaml')

# App and Author name
__title__ = 'Wall Type\nDetailing'
__authors__ = 'Yuval Dolin'

# Identify the current Revit file
doc = __revit__.ActiveUIDocument.Document
uidoc = __revit__.ActiveUIDocument
logger = script.get_logger()
app = doc.Application

# All reference in reference will be dimensioned
reference_array = ReferenceArray()

options = Options(ComputeReferences=True, IncludeNonVisibleObjects=True)

# if the user selected elements before runing the add-in
selection = [doc.GetElement(id) for id in uidoc.Selection.GetElementIds()]

VerBarDiaIntIndex = []
VerBarSpaInt = []
VerBarDiaExIndex = []
VerBarSpaEx = []
HorBarDiaIntIndex = []
HorBarSpaInt = []
HorBarDiaExIndex = []
HorBarSpaEx = []
CorBarDiaIndex = []
ConTypeIndex = []
index=0

class ChooseOptionWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name):
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)

    # Setup the Wall Type
    def values_and_detailing(self, sender, args):
        self.Close()
        if len(walltypecollector) == 1:
            WallTypeWindow('WallTypeDetailingOneTypeWindow.xaml', index=0).show(modal=True)
        else:
            WallTypeWindow('WallTypeDetailingFirstWindow.xaml', index=0).show(modal=True)

    # run the script and create NewDimension
    def make_wall_type_detailing_only(self, sender, args):
        self.Close()
        with revit.Transaction('Wall Type Detailing', log_errors=False):
            dimtype = dimType()
            for wall in wall_selection:
                param = wall.LookupParameter('Wall Type').AsString()
                if param:
                    typeDetailing(wall, param, dimtype)

class WallTypeWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name, index):
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)
        self.index = index
        self._setup_walltype()
        self._setup_bardiameters()
        try:
            self.ver_int_bar_dia.SelectedIndex = VerBarDiaIntIndex[self.index]
            self.ver_int_spa.Text = VerBarSpaInt[self.index]
            self.ver_ex_bar_dia.SelectedIndex = VerBarDiaExIndex[self.index]
            self.ver_ex_spa.Text = VerBarSpaEx[self.index]
            self.hor_int_bar_dia.SelectedIndex = HorBarDiaIntIndex[self.index]
            self.hor_int_spa.Text = HorBarSpaInt[self.index]
            self.hor_ex_bar_dia.SelectedIndex = HorBarDiaExIndex[self.index]
            self.hor_ex_spa.Text = HorBarSpaEx[self.index]
            self.cor_bar_dia.SelectedIndex = CorBarDiaIndex[self.index]
            self.con_type.SelectedIndex = ConTypeIndex[self.index]
        except Exception, ex:
            self
        if self.ver_int_bar_dia.SelectedIndex != self.ver_ex_bar_dia.SelectedIndex or self.ver_int_spa.Text != self.ver_ex_spa.Text:
            self.ver_target_not_equal.IsChecked = True
            self.ver_ex_bar_dia.IsEnabled = True
            self.ver_ex_spa.IsEnabled = True
            self.ver_ex_bar_dia.SelectedIndex = VerBarDiaExIndex[self.index]
            self.ver_ex_spa.Text = VerBarSpaEx[self.index]
        else:
            self.ver_ex_bar_dia.IsEnabled = False
            self.ver_ex_spa.IsEnabled = False
        if self.hor_int_bar_dia.SelectedIndex != self.hor_ex_bar_dia.SelectedIndex or self.hor_int_spa.Text != self.hor_ex_spa.Text:
            self.hor_target_not_equal.IsChecked = True
            self.hor_ex_bar_dia.IsEnabled = True
            self.hor_ex_spa.IsEnabled = True
            self.hor_ex_bar_dia.SelectedIndex = HorBarDiaExIndex[self.index]
            self.hor_ex_spa.Text = HorBarSpaEx[self.index]
        else:
            self.hor_ex_bar_dia.IsEnabled = False
            self.hor_ex_spa.IsEnabled = False

    # Setup the Wall Type
    def _setup_walltype(self):
        try:
            self.walltype.Text = walltypecollector[self.index]
        except Exception, ex:
            self

    # Setup Bar Diameters options
    def _setup_bardiameters(self):
        bardiameterslist = [8, 10, 12, 14, 16, 18, 20, 22, 25, 28, 32, 36]
        self.ver_int_bar_dia.ItemsSource = bardiameterslist
        self.ver_int_bar_dia.SelectedIndex = 0
        self.ver_ex_bar_dia.ItemsSource = bardiameterslist
        self.ver_ex_bar_dia.SelectedIndex = 0
        self.hor_int_bar_dia.ItemsSource = bardiameterslist
        self.hor_int_bar_dia.SelectedIndex = 0
        self.hor_ex_bar_dia.ItemsSource = bardiameterslist
        self.hor_ex_bar_dia.SelectedIndex = 0
        corbardiameterslist = bardiameterslist
        corbardiameterslist.append("Other")
        self.cor_bar_dia.ItemsSource = corbardiameterslist
        self.cor_bar_dia.SelectedIndex = 0
        contypelist = ["B30", "B40", "B50", "B60", "B80", "B100", "Other"]
        self.con_type.ItemsSource = contypelist
        self.con_type.SelectedIndex = 0

    # Lock or open the vertical exterior reinforcement
    def ver_target_changed(self, sender, args):
        if self.ver_ex_bar_dia.IsEnabled == False:
            self.ver_ex_bar_dia.IsEnabled = True
            self.ver_ex_spa.IsEnabled = True
        else:
            self.ver_ex_bar_dia.IsEnabled = False
            self.ver_ex_spa.IsEnabled = False
            self.ver_ex_bar_dia.SelectedIndex = self.ver_int_bar_dia.SelectedIndex
            self.ver_ex_spa.Text = self.ver_int_spa.Text

    # Lock or open the horizontal exterior reinforcement
    def hor_target_changed(self, sender, args):
        if self.hor_ex_bar_dia.IsEnabled == False:
            self.hor_ex_bar_dia.IsEnabled = True
            self.hor_ex_spa.IsEnabled = True
        else:
            self.hor_ex_bar_dia.IsEnabled = False
            self.hor_ex_spa.IsEnabled = False
            self.hor_ex_bar_dia.SelectedIndex = self.hor_int_bar_dia.SelectedIndex
            self.hor_ex_spa.Text = self.hor_int_spa.Text

    # Vertical exterior bar diameter equal to vertical interior bar diameter
    def ver_int_bar_dia_change(self, sender, args):
        if self.ver_ex_bar_dia.IsEnabled == False:
            self.ver_ex_bar_dia.SelectedIndex = self.ver_int_bar_dia.SelectedIndex

    # Vertical exterior bar spacing equal to vertical interior bar spacing
    def ver_int_spa_change(self, sender, args):
        if self.ver_ex_bar_dia.IsEnabled == False:
            self.ver_ex_spa.Text = self.ver_int_spa.Text

    # Horizontal exterior bar diameter equal to horizontal interior bar diameter
    def hor_int_bar_dia_change(self, sender, args):
        if self.hor_ex_bar_dia.IsEnabled == False:
            self.hor_ex_bar_dia.SelectedIndex = self.hor_int_bar_dia.SelectedIndex

    # Horizontal exterior bar spacing equal to horizontal interior bar spacing
    def hor_int_spa_change(self, sender, args):
        if self.hor_ex_bar_dia.IsEnabled == False:
            self.hor_ex_spa.Text = self.hor_int_spa.Text

    # Try to change value in the list
    def _list_change(self):
        VerBarDiaIntIndex[self.index] = self.ver_int_bar_dia.SelectedIndex
        VerBarSpaInt[self.index] = self.ver_int_spa.Text
        VerBarDiaExIndex[self.index] = self.ver_ex_bar_dia.SelectedIndex
        VerBarSpaEx[self.index] = self.ver_ex_spa.Text
        HorBarDiaIntIndex[self.index] = self.hor_int_bar_dia.SelectedIndex
        HorBarSpaInt[self.index] = self.hor_int_spa.Text
        HorBarDiaExIndex[self.index] = self.hor_ex_bar_dia.SelectedIndex
        HorBarSpaEx[self.index] = self.hor_ex_spa.Text
        CorBarDiaIndex[self.index] = self.cor_bar_dia.SelectedIndex
        ConTypeIndex[self.index] = self.con_type.SelectedIndex

    # Append value for the list
    def _list_append(self):
        VerBarDiaIntIndex.append(self.ver_int_bar_dia.SelectedIndex)
        VerBarSpaInt.append(self.ver_int_spa.Text)
        VerBarDiaExIndex.append(self.ver_ex_bar_dia.SelectedIndex)
        VerBarSpaEx.append(self.ver_ex_spa.Text)
        HorBarDiaIntIndex.append(self.hor_int_bar_dia.SelectedIndex)
        HorBarSpaInt.append(self.hor_int_spa.Text)
        HorBarDiaExIndex.append(self.hor_ex_bar_dia.SelectedIndex)
        HorBarSpaEx.append(self.hor_ex_spa.Text)
        CorBarDiaIndex.append(self.cor_bar_dia.SelectedIndex)
        ConTypeIndex.append(self.con_type.SelectedIndex)

    # Check if the parameters exist in the project. If not, add this parameters
    def _check_parameters_existence(self, wall):
        if not wall.LookupParameter('Vertical Bar Diameter Interior'):
            add_parameters("Vertical Bar Diameter Interior")
        if not wall.LookupParameter('Vertical Rebar Spacing Interior'):
            add_parameters("Vertical Rebar Spacing Interior")
        if not wall.LookupParameter('Vertical Bar Diameter Exterior'):
            add_parameters("Vertical Bar Diameter Exterior")
        if not wall.LookupParameter('Vertical Rebar Spacing Exterior'):
            add_parameters("Vertical Rebar Spacing Exterior")
        if not wall.LookupParameter('Horizontal Bar Diameter Interior'):
            add_parameters("Horizontal Bar Diameter Interior")
        if not wall.LookupParameter('Horizontal Rebar Spacing Interior'):
            add_parameters("Horizontal Rebar Spacing Interior")
        if not wall.LookupParameter('Horizontal Bar Diameter Exterior'):
            add_parameters("Horizontal Bar Diameter Exterior")
        if not wall.LookupParameter('Horizontal Rebar Spacing Exterior'):
            add_parameters("Horizontal Rebar Spacing Exterior")
        if not wall.LookupParameter('Corner Bar Diameter'):
            add_parameters("Corner Bar Diameter")
        # Delete old 'Concrete Type' parameter
        try:
            myGuid = System.Guid("be916b57-f7e6-46e5-853e-7c417f7cfb60")
            sharedParam = SharedParameterElement.Lookup(doc, myGuid)
            doc.Delete(sharedParam.Id)
        except Exception, ex:
            self
        if not wall.LookupParameter('Concrete Type'):
            add_parameters("Concrete Type")

    # Create new drafting view for the table
    def _create_drafting_view(self):
        # get all views and collect names
        all_graphviews = revit.query.get_all_views(doc=revit.doc)
        all_drafting_names = [revit.query.get_name(x)
                              for x in all_graphviews
                              if x.ViewType == DB.ViewType.DraftingView]
        # get the first style for Drafting views.
        # This will act as the default style
        for view_type in DB.FilteredElementCollector(revit.doc)\
                           .OfClass(DB.ViewFamilyType):
            if view_type.ViewFamily == DB.ViewFamily.Drafting:
                drafting_view_type = view_type
                break
        dest_view = DB.ViewDrafting.Create(revit.doc, drafting_view_type.Id)
        try:
            new_name = doc.ActiveView.LookupParameter("View Name").AsString() + " - " + "Wall Type Table"
            revit.update.set_name(dest_view, new_name)
            nameFlag = True
        except Exception, ex:
            nameFlag = False
        nameIndex = 1
        while nameFlag == False:
            try:
                new_name = doc.ActiveView.LookupParameter("View Name").AsString() + " - " + "Wall Type Table " + str(nameIndex)
                revit.update.set_name(dest_view, new_name)
                nameFlag = True
            except Exception, ex:
                nameFlag = False
                nameIndex = nameIndex + 1
        dest_view.Scale = 50
        return dest_view

    # Create vertical lines for the table
    def _createVlines(self, hline, vline, fieldColWidth, colCount, colWidth, dest_view):
        cArray = DB.CurveArray()
        vn = direction(hline)
        xFormValues = [0]
        
        [xFormValues.append(j) for j in list(frange(fieldColWidth, colCount+1, colWidth))]
        
        for i in xFormValues:
            cArray.Append(vline.CreateTransformed(DB.Transform.CreateTranslation(vn*i)))

        revit.doc.Create.NewDetailCurveArray(dest_view, cArray) 

    # Create horizontal lines for the table
    def _createHlines(self, vline, hline, rowCount, rowHeight, fieldRowCount, fieldRowHeight, dest_view):
        cArray = DB.CurveArray()
        vn = direction(vline)
        xFormValues = []

        for i in list(frange(0, rowCount, rowHeight)):
            [xFormValues.append(j) for j in list(frange(i, fieldRowCount+1, fieldRowHeight))]
        
        for i in xFormValues:
            cArray.Append(hline.CreateTransformed(DB.Transform.CreateTranslation(vn*i)))

        revit.doc.Create.NewDetailCurveArray(dest_view, cArray) 

    # Check user values
    def _checkualues(self):
        self.checkvalue = False
        try:
			float(self.ver_int_spa.Text)
			float(self.ver_ex_spa.Text)
			float(self.hor_int_spa.Text)
			float(self.hor_ex_spa.Text)
        except Exception:
            self.checkvalue = True
        if self.checkvalue == False:
            if float(self.ver_int_spa.Text) <= 0 or float(self.ver_ex_spa.Text) <= 0 or float(self.hor_int_spa.Text) <= 0 or float(self.hor_ex_spa.Text) <= 0:
                return True
            else:
                return False
        else:
            return True

    def _table_text(self, footToCm, height, width, dest_view, opts, horEqualFlag, verEqualFlag):
        DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(width - (75/footToCm), height - (15/footToCm), 0), 'טיפוס קיר', opts)
        DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(width - (225/footToCm), height - (15/footToCm), 0), 'סוג בטון', opts)
        DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ((75/footToCm), height - (3/footToCm), 0), 'N ברזל פינה\n(C)', opts)
        if horEqualFlag == True:
            DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(225/footToCm, height - (1/footToCm), 0), 'ברזל אופקי (B)', opts)
            DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(225/footToCm, height - (27/footToCm), 0), 'פנים/חוץ', opts)
            horWidth = 225/footToCm
        else:
            DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(225/footToCm, height - (1/footToCm), 0), 'ברזל אופקי (B)', opts)
            DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(225/footToCm, height - (27/footToCm), 0), 'חוץ', opts)
            DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(375/footToCm, height - (1/footToCm), 0), 'ברזל אופקי (B)', opts)
            DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(375/footToCm, height - (27/footToCm), 0), 'פנים', opts)
            horWidth = 375/footToCm
        if verEqualFlag == True:
            DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(horWidth + (150/footToCm), height - (1/footToCm), 0), 'ברזל אנכי (A)', opts)
            DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(horWidth + (150/footToCm), height - (27/footToCm), 0), 'פנים/חוץ', opts)
        else:
            DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(horWidth + (150/footToCm), height - (1/footToCm), 0), 'ברזל אנכי (A)', opts)
            DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(horWidth + (150/footToCm), height - (27/footToCm), 0), 'חוץ', opts)
            DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(horWidth + (300/footToCm), height - (1/footToCm), 0), 'ברזל אנכי (A)', opts)
            DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(horWidth + (300/footToCm), height - (27/footToCm), 0), 'פנים', opts)

    def _load_rebar_symbol_family(self):
        projectFamilies = FilteredElementCollector(doc).OfClass(Family)
        familyFlag = False
        for family in projectFamilies:
            if family.Name == "DENG_Rebar Symbol":
                rebarSymbolFamily = family
                familyFlag = True
                break
        if familyFlag == False:
            rebarSymbolFamily = doc.LoadFamily(sys.path[0]+"\DENG_Rebar Symbol.rfa")
            projectFamilies = FilteredElementCollector(doc).OfClass(Family)
            for family in projectFamilies:
                if family.Name == "DENG_Rebar Symbol":
                    rebarSymbolFamily = family
                    break
        symbols = rebarSymbolFamily.GetFamilySymbolIds().GetEnumerator()
        symbols.MoveNext()
        symbol1 = doc.GetElement(symbols.Current)
        if not symbol1.IsActive: symbol1.Activate()
        return symbol1

    def _rebar_symbol(self, footToCm, height, width, dest_view, opts, corbardiameterslist, symbol1, horEqualFlag, verEqualFlag, bardiameterslist):
        index = 0
        walltypeHeight = height - (15/footToCm)
        contypelistHebrew = ["ב-30", "ב-40", "ב-50", "ב-60", "ב-80", "ב-100", "לפי תכנית"]
        for walltype in walltypecollector:
            walltypeHeight = walltypeHeight - (50/footToCm)
            walltypeHeightSymbol = walltypeHeight - (23/footToCm)
            DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(width - (75/footToCm), walltypeHeight, 0), walltype, opts)
            DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(width - (225/footToCm), walltypeHeight, 0), contypelistHebrew[ConTypeIndex[index]], opts)
            if corbardiameterslist[CorBarDiaIndex[index]] == "Other":
                DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(75/footToCm, walltypeHeight, 0), "לפי תכנית", opts)
            else:
                rebarSymbol = doc.Create.NewFamilyInstance(XYZ(70/footToCm, walltypeHeightSymbol, 0), symbol1, dest_view)
                rebarSymbol.LookupParameter('Right Text').Set(str(corbardiameterslist[CorBarDiaIndex[index]]))
                rebarSymbol.LookupParameter('Line Visible').Set(False)
            if horEqualFlag == True:
                rebarSymbol = doc.Create.NewFamilyInstance(XYZ(205/footToCm, walltypeHeightSymbol, 0), symbol1, dest_view)
                rebarSymbol.LookupParameter('Left Text').Set("2")
                rebarSymbol.LookupParameter('Right Text').Set(str(bardiameterslist[HorBarDiaIntIndex[index]]) + "@" + HorBarSpaInt[index])
                rebarSymbol.LookupParameter('Line Visible').Set(False)
                horWidth = 205/footToCm
            else:
                rebarSymbol = doc.Create.NewFamilyInstance(XYZ(200/footToCm, walltypeHeightSymbol, 0), symbol1, dest_view)
                rebarSymbol.LookupParameter('Right Text').Set(str(bardiameterslist[HorBarDiaIntIndex[index]]) + "@" + HorBarSpaInt[index])
                rebarSymbol.LookupParameter('Line Visible').Set(False)
                rebarSymbol = doc.Create.NewFamilyInstance(XYZ(350/footToCm, walltypeHeightSymbol, 0), symbol1, dest_view)
                rebarSymbol.LookupParameter('Right Text').Set(str(bardiameterslist[HorBarDiaExIndex[index]]) + "@" + HorBarSpaEx[index])
                rebarSymbol.LookupParameter('Line Visible').Set(False)
                horWidth = 350/footToCm
            if verEqualFlag == True:
                rebarSymbol = doc.Create.NewFamilyInstance(XYZ(horWidth + (150/footToCm), walltypeHeightSymbol, 0), symbol1, dest_view)
                rebarSymbol.LookupParameter('Left Text').Set("2")
                rebarSymbol.LookupParameter('Right Text').Set(str(bardiameterslist[VerBarDiaIntIndex[index]]) + "@" + VerBarSpaInt[index])
                rebarSymbol.LookupParameter('Line Visible').Set(False)
            else:
                rebarSymbol = doc.Create.NewFamilyInstance(XYZ(horWidth + (150/footToCm), walltypeHeightSymbol, 0), symbol1, dest_view)
                rebarSymbol.LookupParameter('Right Text').Set(str(bardiameterslist[VerBarDiaIntIndex[index]]) + "@" + VerBarSpaInt[index])
                rebarSymbol.LookupParameter('Line Visible').Set(False)
                rebarSymbol = doc.Create.NewFamilyInstance(XYZ(horWidth + (300/footToCm), walltypeHeightSymbol, 0), symbol1, dest_view)
                rebarSymbol.LookupParameter('Right Text').Set(str(bardiameterslist[VerBarDiaExIndex[index]]) + "@" + VerBarSpaEx[index])
                rebarSymbol.LookupParameter('Line Visible').Set(False)
            index = index + 1

    # Text Type
    def _textType(self):
        # Creating collectior instance and collecting all the text types from the model
        textTypesCollector = FilteredElementCollector(doc).OfClass(TextNoteType)
        textType = None
        for types in textTypesCollector:
            if types.get_Parameter(BuiltInParameter.SYMBOL_NAME_PARAM).AsString() == "2.5mm Arial":
                textType = types
                break
        if not textType:
            for types in textTypesCollector:
                if types.get_Parameter(BuiltInParameter.SYMBOL_NAME_PARAM).AsString() != "":
                    duptype = types
                    break
            textType = duptype.Duplicate("2.5mm Arial")
            textType.get_Parameter(BuiltInParameter.TEXT_SIZE).Set(2.5/304.8)
            # Creating collectior instance and collecting all the text types from the model
            textType.get_Parameter(BuiltInParameter.LINE_COLOR).Set(0)
            textType.get_Parameter(BuiltInParameter.TEXT_BACKGROUND).Set(1)
            textType.get_Parameter(BuiltInParameter.LEADER_ARROWHEAD).Set(ElementId(432))
            textType.get_Parameter(BuiltInParameter.TEXT_FONT).Set('Arial')
        return textType.Id

    # Next window
    def next(self, sender, args):
        self.checkvalue = self._checkualues()
		# If all the Bar Spacing are valid  		
        if self.checkvalue == False:
            try:
                self._list_change()
            except Exception, ex:
                self._list_append()
            # Close the Window Form (Xaml)
            self.Close()
            if self.index + 2 < len(walltypecollector):
                WallTypeWindow('WallTypeDetailingWindow.xaml', index=self.index+1).show(modal=True)
            else :
                WallTypeWindow('WallTypeDetailingFinalWindow.xaml', index=self.index+1).show(modal=True)
		# If there is invalid Bar Spacing show Error window
        else:
            # Error if the Load value is invalid (the value must to be float() and greater than 0)  
			ErrorWindow(errortext="You entered an invalid value").ShowDialog()

    # Previous window
    def previous(self, sender, args):
        try:
            self._list_change()
        except Exception, ex:
            self._list_append()
        # Close the Window Form (Xaml)
        self.Close()
        if self.index == 1:
            WallTypeWindow('WallTypeDetailingFirstWindow.xaml', index=0).show(modal=True)
        else :
            WallTypeWindow('WallTypeDetailingWindow.xaml', index=self.index-1).show(modal=True)

    # run the script and create NewDimension, set parameters and generate table.
    def make_wall_type_detailing(self, sender, args):
        try:
            self._list_change()
        except Exception, ex:
            self._list_append()
        bardiameterslist = [8, 10, 12, 14, 16, 18, 20, 22, 25, 28, 32, 36]
        corbardiameterslist = bardiameterslist
        corbardiameterslist.append("Other")
        contypelist = ["B30", "B40", "B50", "B60", "B80", "B100", "Other"]
        self.checkvalue = self._checkualues()
		# If all the Bar Spacing are valid  		
        if self.checkvalue == False:
            self.Close()
            with revit.Transaction('Wall Type Detailing', log_errors=False):
                dimtype = dimType()
                for wall in wall_selection:
                    param = wall.LookupParameter('Wall Type').AsString()
                    self._check_parameters_existence(wall)
                    if param:
                        typeDetailing(wall, param, dimtype)
                        index = 0
                        for walltype in walltypecollector:
                            if walltype == param:
                                footToCm = 30.48
                                footToMm = 304.8
                                wall.LookupParameter('Vertical Bar Diameter Interior').Set(bardiameterslist[VerBarDiaIntIndex[index]]/footToMm)
                                wall.LookupParameter('Vertical Rebar Spacing Interior').Set(int(VerBarSpaInt[index])/footToCm)
                                wall.LookupParameter('Vertical Bar Diameter Exterior').Set(bardiameterslist[VerBarDiaExIndex[index]]/footToMm)
                                wall.LookupParameter('Vertical Rebar Spacing Exterior').Set(int(VerBarSpaEx[index])/footToCm)
                                wall.LookupParameter('Horizontal Bar Diameter Interior').Set(bardiameterslist[HorBarDiaIntIndex[index]]/footToMm)
                                wall.LookupParameter('Horizontal Rebar Spacing Interior').Set(int(HorBarSpaInt[index])/footToCm)
                                wall.LookupParameter('Horizontal Bar Diameter Exterior').Set(bardiameterslist[HorBarDiaExIndex[index]]/footToMm)
                                wall.LookupParameter('Horizontal Rebar Spacing Exterior').Set(int(HorBarSpaEx[index])/footToCm)
                                if corbardiameterslist[CorBarDiaIndex[index]] != "Other":
                                    wall.LookupParameter('Corner Bar Diameter').Set(corbardiameterslist[CorBarDiaIndex[index]]/footToMm)
                                if contypelist[ConTypeIndex[index]] != "Other":
                                    wall.LookupParameter('Concrete Type').Set(contypelist[ConTypeIndex[index]])
                                break
                            index = index + 1
                dest_view = self._create_drafting_view()
                footToCm = 30.48
                width = (150*5)/footToCm
                height = (50*(len(walltypecollector)+1))/footToCm
                colCount = 5
                rowCount = len(walltypecollector) + 2
                rowHeight = 50/footToCm
                fieldColWidth = 0
                fieldRowCount = 0
                fieldRowHeight = 0
                colWidth = (width-fieldColWidth)/colCount
                index = 0
                verEqualFlag = True
                horEqualFlag = True
                for walltype in walltypecollector:
                    if VerBarDiaIntIndex[index] != VerBarDiaExIndex[index] or VerBarSpaInt[index] != VerBarSpaEx[index]:
                        width = width + (150/footToCm)
                        colCount = colCount + 1
                        verEqualFlag = False
                        break
                    index = index + 1
                index = 0
                for walltype in walltypecollector:
                    if HorBarDiaIntIndex[index] != HorBarDiaExIndex[index] or HorBarSpaInt[index] != HorBarSpaEx[index]:
                        width = width + (150/footToCm)
                        colCount = colCount + 1
                        horEqualFlag = False
                        break
                    index = index + 1
                origin = DB.XYZ()
                vline = DB.Line.CreateBound(DB.XYZ(origin.X, origin.Y, origin.Z), DB.XYZ(origin.X, origin.Y + height, origin.Z))
                hline = DB.Line.CreateBound(DB.XYZ(origin.X, origin.Y, origin.Z), DB.XYZ(origin.X + width, origin.Y, origin.Z))
                self._createVlines(hline, vline, fieldColWidth, colCount, colWidth, dest_view)
                self._createHlines(vline, hline, rowCount, rowHeight, fieldRowCount, fieldRowHeight, dest_view)

                widthFrame = width + 20/footToCm
                heightFrame = height + 20/footToCm
                colCount = 1
                rowCount = 2
                rowHeight = heightFrame
                fieldColWidth = 0
                fieldRowCount = 0
                fieldRowHeight = 0
                colWidth = widthFrame
                vline = DB.Line.CreateBound(DB.XYZ(origin.X - 10/footToCm, origin.Y - 10/footToCm, origin.Z), DB.XYZ(origin.X - 10/footToCm, origin.Y - 10/footToCm + heightFrame, origin.Z))
                hline = DB.Line.CreateBound(DB.XYZ(origin.X - 10/footToCm, origin.Y - 10/footToCm, origin.Z), DB.XYZ(origin.X - 10/footToCm + widthFrame, origin.Y - 10/footToCm, origin.Z))
                self._createVlines(hline, vline, fieldColWidth, colCount, colWidth, dest_view)
                self._createHlines(vline, hline, rowCount, rowHeight, fieldRowCount, fieldRowHeight, dest_view)

                widthCellDivision = width - 450/footToCm
                rowCount = 1
                rowHeight = 0
                fieldRowCount = 0
                fieldRowHeight = 0
                hline = DB.Line.CreateBound(DB.XYZ(origin.X + 150/footToCm, origin.Y + height - 25/footToCm, origin.Z), DB.XYZ(origin.X + 150/footToCm + widthCellDivision, origin.Y + height - 25/footToCm, origin.Z))
                self._createHlines(vline, hline, rowCount, rowHeight, fieldRowCount, fieldRowHeight, dest_view)

                typeId = self._textType()
                opts = TextNoteOptions(typeId)
                opts.HorizontalAlignment = HorizontalTextAlignment.Center
                self._table_text(footToCm, height, width, dest_view, opts, horEqualFlag, verEqualFlag)

                symbol1 = self._load_rebar_symbol_family()
                self._rebar_symbol(footToCm, height, width, dest_view, opts, corbardiameterslist, symbol1, horEqualFlag, verEqualFlag, bardiameterslist)

                SuccessWindow(messagetext="Drafting View: '{}' was successfully created. Please add it to your Sheet.".format(dest_view.Name)).ShowDialog()
		# If there is invalid Bar Spacing show Error window
        else:
            # Error if the Load value is invalid (the value must to be float() and greater than 0)  
			ErrorWindow(errortext="You entered an invalid value").ShowDialog()

# Define class to filter elements of category to select 
class CustomSelectionFilter(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.get_Parameter(BuiltInParameter.WALL_BASE_CONSTRAINT).AsValueString()== doc.ActiveView.LookupParameter("Associated Level").AsString() and e.Category.Name == 'Walls':
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

# Show Error Window Form
class ErrorWindow(Windows.Window):
    def __init__(self, errortext):
        self.errortext=errortext
        wpf.LoadComponent(self, xamlfileError)
        self.error.Text = "{}".format(self.errortext)

    def ok(self, sender, args):
        self.Close()

# Show Success Window Form
class SuccessWindow(Windows.Window):
    def __init__(self, messagetext):
        self.messagetext=messagetext
        wpf.LoadComponent(self, xamlfileSuccess)
        self.message.Text = "{}".format(self.messagetext)

    def ok(self, sender, args):
        self.Close()

# Create dimensions and override them with the Wall Type values
def typeDetailing(wall, param, dimtype):
    refArray = ReferenceArray()
    refCheck = []
    line = LineElement(wall)
    lineDir = line.GetEndPoint(1) - line.GetEndPoint(0)
    geoElement = wall.get_Geometry(options)
    #get side references
    for obj in geoElement:
        if isinstance(obj,Solid):
            for f in obj.Faces:
                try:
                    faceNormal = f.FaceNormal
                    if isParallel(faceNormal,lineDir):
                        refCheck.append(f)
                except Exception, ex:
                    continue
    if len(refCheck) > 2:
        # Walls with Openings
        wallStart = refCheck[0]
        wallEnd = refCheck[0]
        horCheck = False
        k = 1
        for ref in refCheck:
            i = 0
            i = i+k
            while i < len(refCheck):
                if round(ref.Origin.X, 4) == round(refCheck[i].Origin.X, 4):
                    horCheck = True
                    break
                else:
                    i = i+1
            if horCheck == True:
                break
            k = k+1
        if horCheck == True:
            for ref in refCheck:
                if ref.Origin.Y < wallStart.Origin.Y:
                    wallStart = ref
                elif ref.Origin.Y > wallEnd.Origin.Y:
                    wallEnd = ref
        else:
            for ref in refCheck:
                if ref.Origin.X < wallStart.Origin.X:
                    wallStart = ref
                elif ref.Origin.X > wallEnd.Origin.X:
                    wallEnd = ref
        refArray.Append(wallStart.Reference)
        refArray.Append(wallEnd.Reference)
    else:
        for ref in refCheck:
            refArray.Append(ref.Reference)
    try:
        dim = doc.Create.NewDimension(doc.ActiveView, line, refArray, dimtype)
        dim.ValueOverride = param;
    except Exception, ex:
        param

# Dimension Type
def dimType():
    # Creating collectior instance and collecting all the dimension types from the model
    dimensiontypes = FilteredElementCollector(doc).OfClass(DimensionType)
    dimtype = None
    for types in dimensiontypes:
        if types.get_Parameter(BuiltInParameter.SYMBOL_NAME_PARAM).AsString() == "Deng - 3.5mm Arial Arrow":
            dimtype = types
            break
    if not dimtype:
        for types in dimensiontypes:
            if types.StyleType == DimensionStyleType.Linear and types.get_Parameter(BuiltInParameter.SYMBOL_NAME_PARAM).AsString() != "":
                duptype = types
                break
        dimtype = duptype.Duplicate("Deng - 3.5mm Arial Arrow")
        dimtype.get_Parameter(BuiltInParameter.TEXT_SIZE).Set(3.5/304.8)
        # Creating collectior instance and collecting all the dimension types from the model
        dimtype.get_Parameter(BuiltInParameter.DIM_LEADER_ARROWHEAD).Set(ElementId(432))
        dimtype.get_Parameter(BuiltInParameter.LINE_COLOR).Set(16512)
    return dimtype

def add_parameters(parameter_name):
    # Get the parameter file
    originalFile = app.SharedParametersFilename
    app.SharedParametersFilename = sys.path[0]+"\TempDavidEngSharedParameters.txt"
    sharedParameterFile = app.OpenSharedParameterFile()
    # Add Categories to Category Set. 
    wallCat = doc.Settings.Categories.get_Item(BuiltInCategory.OST_Walls);
    catset = app.Create.NewCategorySet()
    catset.Insert(wallCat);
    # Txt group name 
    GroupName = sharedParameterFile.Groups.get_Item("Rebar")
    # Txt parameter name
    externalDefinition = GroupName.Definitions.get_Item(parameter_name) 
    # Create the new shared parameter 
    newInstanceBinding = app.Create.NewInstanceBinding(catset)
    # Insert the new parameter into your project.
    doc.ParameterBindings.Insert(externalDefinition, newInstanceBinding, BuiltInParameterGroup.PG_STRUCTURAL)
    # Set "Values can vary by group instance"
    param = query.get_project_parameter(parameter_name, doc=doc)
    param.param_def.SetAllowVaryBetweenGroups(doc, True)
    app.SharedParametersFilename = originalFile

# draw line for the dimension
def LineElement(element):
    footToCm = 30.48
    if round(element.Location.Curve.GetEndPoint(0).Y, 3) == round(element.Location.Curve.GetEndPoint(1).Y, 3):
        offDistX = (element.Width/2) + 30/footToCm
        offDistY = (element.Width/2) + 30/footToCm
    elif round(element.Location.Curve.GetEndPoint(0).X, 3) == round(element.Location.Curve.GetEndPoint(1).X, 3):
        offDistX = - (element.Width/2) - 30/footToCm
        offDistY = - (element.Width/2) - 30/footToCm
    else:
        offDistX = (element.Width/2) - 15/footToCm
        offDistY = (element.Width/2) + 60/footToCm
    pt1 = XYZ(element.Location.Curve.GetEndPoint(0).X+offDistX, element.Location.Curve.GetEndPoint(0).Y+offDistY, 0)
    pt2 = XYZ(element.Location.Curve.GetEndPoint(1).X+offDistX, element.Location.Curve.GetEndPoint(1).Y+offDistY, 0)
    line = Line.CreateBound(pt1,pt2)
    return line

# Collectiong the Wall Types
def WallTypeCollector(wall_selection):
    walltypecollector = []
    for wall in wall_selection:
        param = wall.LookupParameter('Wall Type').AsString()
        if param:
            typeflag = False
            for walltype in walltypecollector:
                if walltype == wall.LookupParameter('Wall Type').AsString():
                    typeflag = True
                    break
            if typeflag == False:
                walltypecollector.append(wall.LookupParameter('Wall Type').AsString())
    return sorted(walltypecollector)

# find crossing wall faces and return true or false
def isParallel(v1,v2):
	return v1.CrossProduct(v2).IsAlmostEqualTo(XYZ(0,0,0))

def frange(start, count, step=1.0):
    ''' "range()" like function which accept float type''' 
    i = start
    for c in range(count):
        yield i
        i += step

def direction(line):
    p = line.GetEndPoint(0)
    q = line.GetEndPoint(1)
    v = q-p
    vn = v.Normalize()
    return vn

if (DavidEng.checkPermission()):
    if doc.ActiveView.LookupParameter("Type").AsValueString() == "Structural Plan":
        # collecting all the starting walls that selected by the user
        if not selection:
            customFilter = CustomSelectionFilter(DB.BuiltInCategory.OST_Walls)
            try:
                wall_selection = [doc.GetElement(reference) for reference in uidoc.Selection.PickObjects(
                    ObjectType.Element, customFilter, "Select Walls")]
            except Exceptions.OperationCanceledException:
                import sys
                sys.exit()
        else:
            wall_selection = []
            for element in selection:
                if element.Category.Name == 'Walls' and element.get_Parameter(BuiltInParameter.WALL_BASE_CONSTRAINT).AsValueString()== doc.ActiveView.LookupParameter("Associated Level").AsString():
                    wall_selection.append(element)

        for wall in wall_selection:
            if wall.LookupParameter('Wall Type'):
                walltypecollector = WallTypeCollector(wall_selection)
                if not len(walltypecollector) == 0:
                    ChooseOptionWindow('SelectOption.xaml').show(modal=True)
                    break
                else:
                    ErrorWindow(errortext="No value was added to the\n'Wall Type' parameter of the selected walls").ShowDialog()
                    break
            else:
                with revit.Transaction('Add Wall Type Parameter', log_errors=False):
                    # Get the parameter file
                    originalFile = app.SharedParametersFilename
                    app.SharedParametersFilename = sys.path[0]+"\TempDavidEngSharedParameters.txt"
                    sharedParameterFile = app.OpenSharedParameterFile()
                    # Add Categories to Category Set. 
                    wallCat = doc.Settings.Categories.get_Item(BuiltInCategory.OST_Walls);
                    catset = app.Create.NewCategorySet()
                    catset.Insert(wallCat);
                    # Txt group name 
                    GroupName = sharedParameterFile.Groups.get_Item("Rebar")
                    # Txt parameter name
                    externalDefinition = GroupName.Definitions.get_Item("Wall Type") 
                    # Create the new shared parameter 
                    newInstanceBinding = app.Create.NewInstanceBinding(catset)
                    # Insert the new parameter into your project.
                    doc.ParameterBindings.Insert(externalDefinition, newInstanceBinding, BuiltInParameterGroup.PG_STRUCTURAL)
                    # Set "Values can vary by group instance"
                    param = query.get_project_parameter("Wall Type", doc=doc)
                    param.param_def.SetAllowVaryBetweenGroups(doc, True)
                    app.SharedParametersFilename = originalFile
                ErrorWindow(errortext="No value was added to the\n'Wall Type' parameter of the selected walls").ShowDialog()
                break
    else:
        ErrorWindow(errortext="You can only run this add-in on Structural Plan view").ShowDialog()
else:
    DavidEng.ErrorWindow(errortext="You don't have permission\nto use the tool").ShowDialog()