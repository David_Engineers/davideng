# # -*- coding: utf-8 -*-

"""Gathers the values from the parameter 'Wall Type' (wall parameter that added by the user in Revit) and ask the user to define zones, horizontal and vertical reinforcement parameters.
The script will override with colors all the walls (that got value for the 'Wall Type' parameter) and create new legend with the wall type table."""
from pyrevit import coreutils, UI, revit, DB, forms, script
from pyrevit.revit.db import query
from pyrevit.framework import List
from pyrevit import HOST_APP
from Autodesk.Revit import Exceptions
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, BuiltInParameter, Transaction, XYZ, ElementId, Line, ReferenceArray, FamilyInstance, Reference, Options, Edge, Solid, \
                              DimensionType, BuiltInParameterGroup, ElementTypeGroup, TextNoteOptions, HorizontalTextAlignment, SharedParameterElement, Family, DimensionStyleType, TextNoteType, Level
from Autodesk.Revit.UI.Selection import ObjectType, ISelectionFilter

import System
import sys
import os

# dependencies
import clr
clr.AddReference('System.Windows.Forms')

# import WPF creator and base Window
import wpf
import DavidEng
from System import Windows

# Add-in path
sys.path.append(os.path.dirname(__file__))

# find the path of ErrorWindow.xaml
xamlfileError = script.get_bundle_file('ErrorWindow.xaml')

# find the path of SuccessWindow.xaml
xamlfileSuccess = script.get_bundle_file('SuccessWindow.xaml')

# App and Author name
__title__ = 'Wall Type\nZones'
__authors__ = 'Yuval Dolin'

# Identify the current Revit file
doc = __revit__.ActiveUIDocument.Document
uidoc = __revit__.ActiveUIDocument
logger = script.get_logger()
app = doc.Application

# All reference in reference will be dimensioned
reference_array = ReferenceArray()

options = Options(ComputeReferences=True, IncludeNonVisibleObjects=True)

# if the user selected elements before runing the add-in
selection = [doc.GetElement(id) for id in uidoc.Selection.GetElementIds()]

VerBarDiaIntIndex = []
VerBarSpaInt = []
VerBarDiaExIndex = []
VerBarSpaEx = []
HorBarDiaIntIndex = []
HorBarSpaInt = []
HorBarDiaExIndex = []
HorBarSpaEx = []
CorBarDiaIndex = []
ConTypeIndex = []
index=0
ZoneLevelIndex = []
RebarHeightIndex = []

# Creating collectior instance and collecting all the Levels from the model and sort them
levels_collector_all = FilteredElementCollector(doc).OfClass(Level).WhereElementIsNotElementType().ToElements()
levels_names_list = []
elevations_list = []
for level in levels_collector_all:
    levels_names_list.append(level.Name)
    elevations_list.append(level.Elevation)
zipped_lists = zip(elevations_list, levels_names_list)
sorted_zipped_lists = sorted(zipped_lists)
sorted_levels_names_list = [element for _, element in sorted_zipped_lists]
sorted_levels_elevations_list = [element for element, _ in sorted_zipped_lists]
sorted_levels_names_list.insert(0, "")
sorted_levels_elevations_list.insert(0, "")

class ChooseOptionWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name):
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)

    # Setup the Wall Type
    def values_and_detailing(self, sender, args):
        self.Close()
        WallTypeWindow('WallTypeZonesWindow.xaml', index=0, walltypelist=[], zoneslist=[]).show(modal=True)

    # run the script and create NewDimension
    def make_wall_zones_detailing_only(self, sender, args):
        self.Close()
        try:
            zonesDetailing()
        except Exception, ex:
            self

class WallTypeWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name, index, walltypelist, zoneslist):
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)
        self.index = index
        self.walltypecollector = walltypelist
        self.zonescollector = zoneslist
        try:
            self._setup_levels()
        except Exception, ex:
            self
        try:
            self.zone1_from.SelectedIndex = ZoneLevelIndex[0]
            self.zone1_to.SelectedIndex = ZoneLevelIndex[1]
            self.zone2_from.SelectedIndex = ZoneLevelIndex[2]
            self.zone2_to.SelectedIndex = ZoneLevelIndex[3]
            self.zone3_from.SelectedIndex = ZoneLevelIndex[4]
            self.zone3_to.SelectedIndex = ZoneLevelIndex[5]
            self.zone4_from.SelectedIndex = ZoneLevelIndex[6]
            self.zone4_to.SelectedIndex = ZoneLevelIndex[7]
            self.rebar_height.SelectedIndex = RebarHeightIndex[0]
        except Exception, ex:
            self
        try:
            self._setup_walltype()
            self._setup_zone()
            self._setup_bardiameters()
        except Exception, ex:
            self
        try:
            self.ver_int_bar_dia.SelectedIndex = VerBarDiaIntIndex[self.index]
            self.ver_int_spa.Text = VerBarSpaInt[self.index]
            self.ver_ex_bar_dia.SelectedIndex = VerBarDiaExIndex[self.index]
            self.ver_ex_spa.Text = VerBarSpaEx[self.index]
            self.hor_int_bar_dia.SelectedIndex = HorBarDiaIntIndex[self.index]
            self.hor_int_spa.Text = HorBarSpaInt[self.index]
            self.hor_ex_bar_dia.SelectedIndex = HorBarDiaExIndex[self.index]
            self.hor_ex_spa.Text = HorBarSpaEx[self.index]
            self.cor_bar_dia.SelectedIndex = CorBarDiaIndex[self.index]
            self.con_type.SelectedIndex = ConTypeIndex[self.index]
        except Exception, ex:
            self
        try:
            if self.ver_int_bar_dia.SelectedIndex != self.ver_ex_bar_dia.SelectedIndex or self.ver_int_spa.Text != self.ver_ex_spa.Text:
                self.ver_target_not_equal.IsChecked = True
                self.ver_ex_bar_dia.IsEnabled = True
                self.ver_ex_spa.IsEnabled = True
                self.ver_ex_bar_dia.SelectedIndex = VerBarDiaExIndex[self.index]
                self.ver_ex_spa.Text = VerBarSpaEx[self.index]
            else:
                self.ver_ex_bar_dia.IsEnabled = False
                self.ver_ex_spa.IsEnabled = False
            if self.hor_int_bar_dia.SelectedIndex != self.hor_ex_bar_dia.SelectedIndex or self.hor_int_spa.Text != self.hor_ex_spa.Text:
                self.hor_target_not_equal.IsChecked = True
                self.hor_ex_bar_dia.IsEnabled = True
                self.hor_ex_spa.IsEnabled = True
                self.hor_ex_bar_dia.SelectedIndex = HorBarDiaExIndex[self.index]
                self.hor_ex_spa.Text = HorBarSpaEx[self.index]
            else:
                self.hor_ex_bar_dia.IsEnabled = False
                self.hor_ex_spa.IsEnabled = False
        except Exception, ex:
            self

    # Setup Levels options
    def _setup_levels(self):
        self.zone1_from.ItemsSource = sorted_levels_names_list
        self.zone1_from.SelectedIndex = 0
        self.zone1_to.ItemsSource = sorted_levels_names_list
        self.zone1_to.SelectedIndex = 0
        self.zone2_from.ItemsSource = sorted_levels_names_list
        self.zone2_from.SelectedIndex = 0
        self.zone2_to.ItemsSource = sorted_levels_names_list
        self.zone2_to.SelectedIndex = 0
        self.zone3_from.ItemsSource = sorted_levels_names_list
        self.zone3_from.SelectedIndex = 0
        self.zone3_to.ItemsSource = sorted_levels_names_list
        self.zone3_to.SelectedIndex = 0
        self.zone4_from.ItemsSource = sorted_levels_names_list
        self.zone4_from.SelectedIndex = 0
        self.zone4_to.ItemsSource = sorted_levels_names_list
        self.zone4_to.SelectedIndex = 0
        rebarHeightList = [1, 2]
        self.rebar_height.ItemsSource = rebarHeightList
        self.rebar_height.SelectedIndex = 0

    # Setup the Wall Type
    def _setup_walltype(self):
        try:
            self.walltype.Text = self.walltypecollector[self.index]
        except Exception, ex:
            self

    # Setup the Zone
    def _setup_zone(self):
        try:
            self.zone.Text = self.zonescollector[self.index]
        except Exception, ex:
            self

    # Setup Bar Diameters options
    def _setup_bardiameters(self):
        bardiameterslist = [8, 10, 12, 14, 16, 18, 20, 22, 25, 28, 32, 36]
        self.ver_int_bar_dia.ItemsSource = bardiameterslist
        self.ver_int_bar_dia.SelectedIndex = 0
        self.ver_ex_bar_dia.ItemsSource = bardiameterslist
        self.ver_ex_bar_dia.SelectedIndex = 0
        self.hor_int_bar_dia.ItemsSource = bardiameterslist
        self.hor_int_bar_dia.SelectedIndex = 0
        self.hor_ex_bar_dia.ItemsSource = bardiameterslist
        self.hor_ex_bar_dia.SelectedIndex = 0
        corbardiameterslist = bardiameterslist
        corbardiameterslist.append("Other")
        self.cor_bar_dia.ItemsSource = corbardiameterslist
        self.cor_bar_dia.SelectedIndex = 0
        contypelist = ["B30", "B40", "B50", "B60", "B80", "B100", "Other"]
        self.con_type.ItemsSource = contypelist
        self.con_type.SelectedIndex = 0

    # Lock or open the vertical exterior reinforcement
    def ver_target_changed(self, sender, args):
        if self.ver_ex_bar_dia.IsEnabled == False:
            self.ver_ex_bar_dia.IsEnabled = True
            self.ver_ex_spa.IsEnabled = True
        else:
            self.ver_ex_bar_dia.IsEnabled = False
            self.ver_ex_spa.IsEnabled = False
            self.ver_ex_bar_dia.SelectedIndex = self.ver_int_bar_dia.SelectedIndex
            self.ver_ex_spa.Text = self.ver_int_spa.Text

    # Lock or open the horizontal exterior reinforcement
    def hor_target_changed(self, sender, args):
        if self.hor_ex_bar_dia.IsEnabled == False:
            self.hor_ex_bar_dia.IsEnabled = True
            self.hor_ex_spa.IsEnabled = True
        else:
            self.hor_ex_bar_dia.IsEnabled = False
            self.hor_ex_spa.IsEnabled = False
            self.hor_ex_bar_dia.SelectedIndex = self.hor_int_bar_dia.SelectedIndex
            self.hor_ex_spa.Text = self.hor_int_spa.Text

    # Vertical exterior bar diameter equal to vertical interior bar diameter
    def ver_int_bar_dia_change(self, sender, args):
        if self.ver_ex_bar_dia.IsEnabled == False:
            self.ver_ex_bar_dia.SelectedIndex = self.ver_int_bar_dia.SelectedIndex

    # Vertical exterior bar spacing equal to vertical interior bar spacing
    def ver_int_spa_change(self, sender, args):
        if self.ver_ex_bar_dia.IsEnabled == False:
            self.ver_ex_spa.Text = self.ver_int_spa.Text

    # Horizontal exterior bar diameter equal to horizontal interior bar diameter
    def hor_int_bar_dia_change(self, sender, args):
        if self.hor_ex_bar_dia.IsEnabled == False:
            self.hor_ex_bar_dia.SelectedIndex = self.hor_int_bar_dia.SelectedIndex

    # Horizontal exterior bar spacing equal to horizontal interior bar spacing
    def hor_int_spa_change(self, sender, args):
        if self.hor_ex_bar_dia.IsEnabled == False:
            self.hor_ex_spa.Text = self.hor_int_spa.Text

    # Try to change value in the list
    def _list_change_zones(self):
        ZoneLevelIndex[0] = self.zone1_from.SelectedIndex
        ZoneLevelIndex[1] = self.zone1_to.SelectedIndex
        ZoneLevelIndex[2] = self.zone2_from.SelectedIndex
        ZoneLevelIndex[3] = self.zone2_to.SelectedIndex
        ZoneLevelIndex[4] = self.zone3_from.SelectedIndex
        ZoneLevelIndex[5] = self.zone3_to.SelectedIndex
        ZoneLevelIndex[6] = self.zone4_from.SelectedIndex
        ZoneLevelIndex[7] = self.zone4_to.SelectedIndex
        RebarHeightIndex[0] = self.rebar_height.SelectedIndex

    # Append value for the list
    def _list_append_zones(self):
        ZoneLevelIndex.append(self.zone1_from.SelectedIndex)
        ZoneLevelIndex.append(self.zone1_to.SelectedIndex)
        ZoneLevelIndex.append(self.zone2_from.SelectedIndex)
        ZoneLevelIndex.append(self.zone2_to.SelectedIndex)
        ZoneLevelIndex.append(self.zone3_from.SelectedIndex)
        ZoneLevelIndex.append(self.zone3_to.SelectedIndex)
        ZoneLevelIndex.append(self.zone4_from.SelectedIndex)
        ZoneLevelIndex.append(self.zone4_to.SelectedIndex)
        RebarHeightIndex.append(self.rebar_height.SelectedIndex)

    # Try to change value in the list
    def _list_change(self):
        VerBarDiaIntIndex[self.index] = self.ver_int_bar_dia.SelectedIndex
        VerBarSpaInt[self.index] = self.ver_int_spa.Text
        VerBarDiaExIndex[self.index] = self.ver_ex_bar_dia.SelectedIndex
        VerBarSpaEx[self.index] = self.ver_ex_spa.Text
        HorBarDiaIntIndex[self.index] = self.hor_int_bar_dia.SelectedIndex
        HorBarSpaInt[self.index] = self.hor_int_spa.Text
        HorBarDiaExIndex[self.index] = self.hor_ex_bar_dia.SelectedIndex
        HorBarSpaEx[self.index] = self.hor_ex_spa.Text
        CorBarDiaIndex[self.index] = self.cor_bar_dia.SelectedIndex
        ConTypeIndex[self.index] = self.con_type.SelectedIndex

    # Append value for the list
    def _list_append(self):
        VerBarDiaIntIndex.append(self.ver_int_bar_dia.SelectedIndex)
        VerBarSpaInt.append(self.ver_int_spa.Text)
        VerBarDiaExIndex.append(self.ver_ex_bar_dia.SelectedIndex)
        VerBarSpaEx.append(self.ver_ex_spa.Text)
        HorBarDiaIntIndex.append(self.hor_int_bar_dia.SelectedIndex)
        HorBarSpaInt.append(self.hor_int_spa.Text)
        HorBarDiaExIndex.append(self.hor_ex_bar_dia.SelectedIndex)
        HorBarSpaEx.append(self.hor_ex_spa.Text)
        CorBarDiaIndex.append(self.cor_bar_dia.SelectedIndex)
        ConTypeIndex.append(self.con_type.SelectedIndex)

    # Check if the parameters exist in the project. If not, add this parameters
    def _check_parameters_existence(self, wall):
        if not wall.LookupParameter('Vertical Bar Diameter Interior'):
            add_parameters("Vertical Bar Diameter Interior")
        if not wall.LookupParameter('Vertical Rebar Spacing Interior'):
            add_parameters("Vertical Rebar Spacing Interior")
        if not wall.LookupParameter('Vertical Bar Diameter Exterior'):
            add_parameters("Vertical Bar Diameter Exterior")
        if not wall.LookupParameter('Vertical Rebar Spacing Exterior'):
            add_parameters("Vertical Rebar Spacing Exterior")
        if not wall.LookupParameter('Horizontal Bar Diameter Interior'):
            add_parameters("Horizontal Bar Diameter Interior")
        if not wall.LookupParameter('Horizontal Rebar Spacing Interior'):
            add_parameters("Horizontal Rebar Spacing Interior")
        if not wall.LookupParameter('Horizontal Bar Diameter Exterior'):
            add_parameters("Horizontal Bar Diameter Exterior")
        if not wall.LookupParameter('Horizontal Rebar Spacing Exterior'):
            add_parameters("Horizontal Rebar Spacing Exterior")
        if not wall.LookupParameter('Corner Bar Diameter'):
            add_parameters("Corner Bar Diameter")
        if not wall.LookupParameter('Wall Zone'):
            add_parameters("Wall Zone")
        if not wall.LookupParameter('Rebar Height (Floors)'):
            add_parameters("Rebar Height (Floors)")
        # Delete old 'Concrete Type' parameter
        try:
            myGuid = System.Guid("be916b57-f7e6-46e5-853e-7c417f7cfb60")
            sharedParam = SharedParameterElement.Lookup(doc, myGuid)
            doc.Delete(sharedParam.Id)
        except Exception, ex:
            self
        if not wall.LookupParameter('Concrete Type'):
            add_parameters("Concrete Type")

    # Create new drafting view for the table
    def _create_drafting_view(self, ZoneNumber):
        # get all views and collect names
        all_graphviews = revit.query.get_all_views(doc=revit.doc)
        all_drafting_names = [revit.query.get_name(x)
                              for x in all_graphviews
                              if x.ViewType == DB.ViewType.DraftingView]
        # get the first style for Drafting views.
        # This will act as the default style
        for view_type in DB.FilteredElementCollector(revit.doc)\
                           .OfClass(DB.ViewFamilyType):
            if view_type.ViewFamily == DB.ViewFamily.Drafting:
                drafting_view_type = view_type
                break
        dest_view = DB.ViewDrafting.Create(revit.doc, drafting_view_type.Id)
        try:
            new_name = "Zone " + str(ZoneNumber) + " - " + "Wall Type Table"
            revit.update.set_name(dest_view, new_name)
            nameFlag = True
        except Exception, ex:
            nameFlag = False
        nameIndex = 1
        while nameFlag == False:
            try:
                new_name = "Zone " + str(ZoneNumber) + " - " + "Wall Type Table " + str(nameIndex)
                revit.update.set_name(dest_view, new_name)
                nameFlag = True
            except Exception, ex:
                nameFlag = False
                nameIndex = nameIndex + 1
        dest_view.Scale = 50
        return dest_view

    # Create vertical lines for the table
    def _createVlines(self, hline, vline, fieldColWidth, colCount, colWidth, dest_view):
        cArray = DB.CurveArray()
        vn = direction(hline)
        xFormValues = [0]
        
        [xFormValues.append(j) for j in list(frange(fieldColWidth, colCount+1, colWidth))]
        
        for i in xFormValues:
            cArray.Append(vline.CreateTransformed(DB.Transform.CreateTranslation(vn*i)))

        revit.doc.Create.NewDetailCurveArray(dest_view, cArray) 

    # Create horizontal lines for the table
    def _createHlines(self, vline, hline, rowCount, rowHeight, fieldRowCount, fieldRowHeight, dest_view):
        cArray = DB.CurveArray()
        vn = direction(vline)
        xFormValues = []

        for i in list(frange(0, rowCount, rowHeight)):
            [xFormValues.append(j) for j in list(frange(i, fieldRowCount+1, fieldRowHeight))]
        
        for i in xFormValues:
            cArray.Append(hline.CreateTransformed(DB.Transform.CreateTranslation(vn*i)))

        revit.doc.Create.NewDetailCurveArray(dest_view, cArray) 

    # Check user values
    def _checkualueszones(self):
        j = 0
        for ZoneLevel in ZoneLevelIndex:
            try:
                if ZoneLevel == 0:
                    i = j+1
                    while i < len(ZoneLevelIndex):
                        if not ZoneLevelIndex[i] == 0:
                            return True
                        i = i+1
                elif j%2 == 0:
                    if ZoneLevel >= ZoneLevelIndex[j+1]:
                        return True
                else:
                    if ZoneLevelIndex[j+1] == 0:
                        i = j+2
                        while i < len(ZoneLevelIndex):
                            if not ZoneLevelIndex[i] == 0:
                                return True
                            i = i+1
                    elif ZoneLevel > ZoneLevelIndex[j+1]:
                        return True
            except Exception, ex:
                self
            j = j+1
        return False

    # Check user values
    def _checkualues(self):
        self.checkvalue = False
        try:
			float(self.ver_int_spa.Text)
			float(self.ver_ex_spa.Text)
			float(self.hor_int_spa.Text)
			float(self.hor_ex_spa.Text)
        except Exception:
            self.checkvalue = True
        if self.checkvalue == False:
            if float(self.ver_int_spa.Text) <= 0 or float(self.ver_ex_spa.Text) <= 0 or float(self.hor_int_spa.Text) <= 0 or float(self.hor_ex_spa.Text) <= 0:
                return True
            else:
                return False
        else:
            return True

    def _table_text(self, footToCm, height, width, dest_view, opts, horEqualFlag, verEqualFlag):
        DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(width - (75/footToCm), height - (15/footToCm), 0), 'טיפוס קיר', opts)
        DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(width - (225/footToCm), height - (15/footToCm), 0), 'סוג בטון', opts)
        DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ((75/footToCm), height - (3/footToCm), 0), 'N ברזל פינה\n(C)', opts)
        if horEqualFlag == True:
            DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(225/footToCm, height - (1/footToCm), 0), 'ברזל אופקי (B)', opts)
            DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(225/footToCm, height - (27/footToCm), 0), 'פנים/חוץ', opts)
            horWidth = 225/footToCm
        else:
            DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(225/footToCm, height - (1/footToCm), 0), 'ברזל אופקי (B)', opts)
            DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(225/footToCm, height - (27/footToCm), 0), 'חוץ', opts)
            DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(375/footToCm, height - (1/footToCm), 0), 'ברזל אופקי (B)', opts)
            DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(375/footToCm, height - (27/footToCm), 0), 'פנים', opts)
            horWidth = 375/footToCm
        if verEqualFlag == True:
            DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(horWidth + (150/footToCm), height - (1/footToCm), 0), 'ברזל אנכי (A)', opts)
            DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(horWidth + (150/footToCm), height - (27/footToCm), 0), 'פנים/חוץ', opts)
        else:
            DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(horWidth + (150/footToCm), height - (1/footToCm), 0), 'ברזל אנכי (A)', opts)
            DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(horWidth + (150/footToCm), height - (27/footToCm), 0), 'חוץ', opts)
            DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(horWidth + (300/footToCm), height - (1/footToCm), 0), 'ברזל אנכי (A)', opts)
            DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(horWidth + (300/footToCm), height - (27/footToCm), 0), 'פנים', opts)

    def _load_rebar_symbol_family(self):
        projectFamilies = FilteredElementCollector(doc).OfClass(Family)
        familyFlag = False
        for family in projectFamilies:
            if family.Name == "DENG_Rebar Symbol":
                rebarSymbolFamily = family
                familyFlag = True
                break
        if familyFlag == False:
            rebarSymbolFamily = doc.LoadFamily(sys.path[0]+"\DENG_Rebar Symbol.rfa")
            projectFamilies = FilteredElementCollector(doc).OfClass(Family)
            for family in projectFamilies:
                if family.Name == "DENG_Rebar Symbol":
                    rebarSymbolFamily = family
                    break
        symbols = rebarSymbolFamily.GetFamilySymbolIds().GetEnumerator()
        symbols.MoveNext()
        symbol1 = doc.GetElement(symbols.Current)
        if not symbol1.IsActive: symbol1.Activate()
        return symbol1

    def _rebar_symbol(self, footToCm, height, width, dest_view, opts, corbardiameterslist, symbol1, horEqualFlag, verEqualFlag, bardiameterslist, ZoneIndexStart, ZoneIndexEnd, ZoneValue):
        index = ZoneIndexStart
        splitZoneValue = ZoneValue.split()
        if len(splitZoneValue) == 5:
            fromValue = splitZoneValue[2]
            fromValueList = fromValue.split("_")
            if len(fromValueList) == 1:
                fromValueList = fromValue.split("-")
            toValue = splitZoneValue[4]
            toValueList = toValue.split("_")
            if len(toValueList) == 1:
                toValueList = toValue.split("-")
            if len(fromValueList) == 2 and len(toValueList) == 2:
                try:
                    fromValueLevel = int(fromValueList[1]) - 100
                    toValueLevel = int(toValueList[1]) - 100
                    ZoneValue = str(fromValueLevel) + " - " + str(toValueLevel) + "  קומות"
                except Exception, ex:
                    self
        DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(width/2, height + 35/footToCm, 0), ZoneValue, opts)
        walltypeHeight = height - (15/footToCm)
        contypelistHebrew = ["ב-30", "ב-40", "ב-50", "ב-60", "ב-80", "ב-100", "לפי תכנית"]
        while index <= ZoneIndexEnd:
            walltypeHeight = walltypeHeight - (50/footToCm)
            walltypeHeightSymbol = walltypeHeight - (23/footToCm)
            DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(width - (75/footToCm), walltypeHeight, 0), self.walltypecollector[index], opts)
            DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(width - (225/footToCm), walltypeHeight, 0), contypelistHebrew[ConTypeIndex[index]], opts)
            if corbardiameterslist[CorBarDiaIndex[index]] == "Other":
                DB.TextNote.Create(revit.doc, dest_view.Id, DB.XYZ(75/footToCm, walltypeHeight, 0), "לפי תכנית", opts)
            else:
                rebarSymbol = doc.Create.NewFamilyInstance(XYZ(70/footToCm, walltypeHeightSymbol, 0), symbol1, dest_view)
                rebarSymbol.LookupParameter('Right Text').Set(str(corbardiameterslist[CorBarDiaIndex[index]]))
                rebarSymbol.LookupParameter('Line Visible').Set(False)
            if horEqualFlag == True:
                rebarSymbol = doc.Create.NewFamilyInstance(XYZ(205/footToCm, walltypeHeightSymbol, 0), symbol1, dest_view)
                rebarSymbol.LookupParameter('Left Text').Set("2")
                rebarSymbol.LookupParameter('Right Text').Set(str(bardiameterslist[HorBarDiaIntIndex[index]]) + "@" + HorBarSpaInt[index])
                rebarSymbol.LookupParameter('Line Visible').Set(False)
                horWidth = 205/footToCm
            else:
                rebarSymbol = doc.Create.NewFamilyInstance(XYZ(200/footToCm, walltypeHeightSymbol, 0), symbol1, dest_view)
                rebarSymbol.LookupParameter('Right Text').Set(str(bardiameterslist[HorBarDiaIntIndex[index]]) + "@" + HorBarSpaInt[index])
                rebarSymbol.LookupParameter('Line Visible').Set(False)
                rebarSymbol = doc.Create.NewFamilyInstance(XYZ(350/footToCm, walltypeHeightSymbol, 0), symbol1, dest_view)
                rebarSymbol.LookupParameter('Right Text').Set(str(bardiameterslist[HorBarDiaExIndex[index]]) + "@" + HorBarSpaEx[index])
                rebarSymbol.LookupParameter('Line Visible').Set(False)
                horWidth = 350/footToCm
            if verEqualFlag == True:
                rebarSymbol = doc.Create.NewFamilyInstance(XYZ(horWidth + (150/footToCm), walltypeHeightSymbol, 0), symbol1, dest_view)
                rebarSymbol.LookupParameter('Left Text').Set("2")
                rebarSymbol.LookupParameter('Right Text').Set(str(bardiameterslist[VerBarDiaIntIndex[index]]) + "@" + VerBarSpaInt[index])
                rebarSymbol.LookupParameter('Line Visible').Set(False)
            else:
                rebarSymbol = doc.Create.NewFamilyInstance(XYZ(horWidth + (150/footToCm), walltypeHeightSymbol, 0), symbol1, dest_view)
                rebarSymbol.LookupParameter('Right Text').Set(str(bardiameterslist[VerBarDiaIntIndex[index]]) + "@" + VerBarSpaInt[index])
                rebarSymbol.LookupParameter('Line Visible').Set(False)
                rebarSymbol = doc.Create.NewFamilyInstance(XYZ(horWidth + (300/footToCm), walltypeHeightSymbol, 0), symbol1, dest_view)
                rebarSymbol.LookupParameter('Right Text').Set(str(bardiameterslist[VerBarDiaExIndex[index]]) + "@" + VerBarSpaEx[index])
                rebarSymbol.LookupParameter('Line Visible').Set(False)
            index = index + 1

    # Text Type
    def _textType(self):
        # Creating collectior instance and collecting all the text types from the model
        textTypesCollector = FilteredElementCollector(doc).OfClass(TextNoteType)
        textType = None
        for types in textTypesCollector:
            if types.get_Parameter(BuiltInParameter.SYMBOL_NAME_PARAM).AsString() == "2.5mm Arial":
                textType = types
                break
        if not textType:
            for types in textTypesCollector:
                if types.get_Parameter(BuiltInParameter.SYMBOL_NAME_PARAM).AsString() != "":
                    duptype = types
                    break
            textType = duptype.Duplicate("2.5mm Arial")
            textType.get_Parameter(BuiltInParameter.TEXT_SIZE).Set(2.5/304.8)
            # Creating collectior instance and collecting all the text types from the model
            textType.get_Parameter(BuiltInParameter.LINE_COLOR).Set(0)
            textType.get_Parameter(BuiltInParameter.TEXT_BACKGROUND).Set(1)
            textType.get_Parameter(BuiltInParameter.LEADER_ARROWHEAD).Set(ElementId(432))
            textType.get_Parameter(BuiltInParameter.TEXT_FONT).Set('Arial')
        return textType.Id

    # Previous window
    def previousToZoneWindow(self, sender, args):
        try:
            self._list_change()
        except Exception, ex:
            self._list_append()
        # Close the Window Form (Xaml)
        self.Close()
        WallTypeWindow('WallTypeZonesWindow.xaml', index=0, walltypelist=self.walltypecollector, zoneslist=self.zonescollector).show(modal=True)

    def WallTopBase (self, wall, footToCm):
        p = wall.get_Parameter(BuiltInParameter.WALL_BASE_CONSTRAINT)
        base = doc.GetElement(p.AsElementId())
        baseConstraintElevation = base.Elevation*footToCm
        p = wall.get_Parameter(BuiltInParameter.WALL_HEIGHT_TYPE)
        if not p.AsValueString() == 'Unconnected':
            top = doc.GetElement(p.AsElementId())
            topConstraintElevation = top.Elevation*footToCm
        else:
            UnconnectedHeight = wall.get_Parameter(BuiltInParameter.WALL_USER_HEIGHT_PARAM)
            topElevation = baseConstraintElevation + UnconnectedHeight.AsDouble()*footToCm
            i = 2
            while i < len(sorted_levels_elevations_list):
                if sorted_levels_elevations_list[i]*footToCm == topElevation:
                    topConstraintElevation = sorted_levels_elevations_list[i]*footToCm
                    break
                elif sorted_levels_elevations_list[i]*footToCm > topElevation:
                    above = sorted_levels_elevations_list[i]*footToCm - topElevation
                    below = topElevation - sorted_levels_elevations_list[i-1]*footToCm
                    if above <= below:
                        topConstraintElevation = sorted_levels_elevations_list[i]*footToCm
                    else:
                        topConstraintElevation = sorted_levels_elevations_list[i-1]*footToCm
                    break
                i = i+1
            if i == len(sorted_levels_elevations_list) and sorted_levels_elevations_list[i-1]*footToCm < topElevation:
                topConstraintElevation = sorted_levels_elevations_list[i-1]*footToCm
        return baseConstraintElevation, topConstraintElevation

    # Collectiong the Zones and Wall Types
    def _ZonesAndWallTypeCollector(self):
        footToCm = 30.48
        if ZoneLevelIndex[0] == 0:
            walltypecollector = []
            zonescollector = []
            for wall in wall_selection:
                param = wall.LookupParameter('Wall Type').AsString()
                if param:
                    typeflag = False
                    for walltype in walltypecollector:
                        if walltype == wall.LookupParameter('Wall Type').AsString():
                            typeflag = True
                            break
                    if typeflag == False:
                        walltypecollector.append(wall.LookupParameter('Wall Type').AsString())
                        zonescollector.append("Not Specified")
            return sorted(walltypecollector), zonescollector
        else:
            walltypecollector = []
            zonescollector = []
            j = 0
            walltypecollectorTempNotSpecifiedZone = []
            while j < len(ZoneLevelIndex):
                if not ZoneLevelIndex[j] == 0:
                    ZoneNum = (j+2)/2
                    WallZoneValue = sorted_levels_names_list[ZoneLevelIndex[j]] + " to " + sorted_levels_names_list[ZoneLevelIndex[j+1]]
                    walltypecollectorTemp = []
                    for wall in wall_selection:
                        try:
                            baseConstraintElevation, topConstraintElevation = self.WallTopBase(wall, footToCm)
                            param = wall.LookupParameter('Wall Type').AsString()
                            if param and baseConstraintElevation >= sorted_levels_elevations_list[ZoneLevelIndex[j]]*footToCm and topConstraintElevation <= sorted_levels_elevations_list[ZoneLevelIndex[j+1]]*footToCm:
                                typeflag = False
                                for walltype in walltypecollectorTemp:
                                    if walltype == wall.LookupParameter('Wall Type').AsString():
                                        typeflag = True
                                        break
                                if typeflag == False:
                                    walltypecollectorTemp.append(wall.LookupParameter('Wall Type').AsString())
                                    zonescollector.append("Zone " + str(ZoneNum) + "  " + sorted_levels_names_list[ZoneLevelIndex[j]] + " to " + sorted_levels_names_list[ZoneLevelIndex[j+1]])
                        except Exception, ex:
                            self
                    if j + 2 < len(ZoneLevelIndex):
                        if ZoneLevelIndex[j+2] == 0:
                            for wall in wall_selection:
                                try:
                                    baseConstraintElevation, topConstraintElevation = self.WallTopBase(wall, footToCm)
                                    param = wall.LookupParameter('Wall Type').AsString()
                                    if param:
                                        k = 0
                                        typeflag = False
                                        for walltype in walltypecollectorTempNotSpecifiedZone:
                                            if walltype == wall.LookupParameter('Wall Type').AsString():
                                                typeflag = True
                                                break
                                        if not typeflag == True:
                                            while k <= j:
                                                if baseConstraintElevation >= sorted_levels_elevations_list[ZoneLevelIndex[k]]*footToCm and topConstraintElevation <= sorted_levels_elevations_list[ZoneLevelIndex[k+1]]*footToCm:
                                                    typeflag = True
                                                    break
                                                k = k+2
                                        if typeflag == False:
                                            walltypecollectorTempNotSpecifiedZone.append(wall.LookupParameter('Wall Type').AsString())
                                            zonescollector.append("Not Specified")
                                except Exception, ex:
                                    self
                    if j == 6:
                        for wall in wall_selection:
                            try:
                                baseConstraintElevation, topConstraintElevation = self.WallTopBase(wall, footToCm)
                                param = wall.LookupParameter('Wall Type').AsString()
                                if param:
                                    k = 0
                                    typeflag = False
                                    for walltype in walltypecollectorTempNotSpecifiedZone:
                                        if walltype == wall.LookupParameter('Wall Type').AsString():
                                            typeflag = True
                                            break
                                    if not typeflag == True:
                                        while k <= j:
                                            if baseConstraintElevation >= sorted_levels_elevations_list[ZoneLevelIndex[k]]*footToCm and topConstraintElevation <= sorted_levels_elevations_list[ZoneLevelIndex[k+1]]*footToCm:
                                                typeflag = True
                                                break
                                            k = k+2
                                    if typeflag == False:
                                        walltypecollectorTempNotSpecifiedZone.append(wall.LookupParameter('Wall Type').AsString())
                                        zonescollector.append("Not Specified")
                            except Exception, ex:
                                self
                    walltypecollector.extend(sorted(walltypecollectorTemp))  
                j = j+2
            if walltypecollectorTempNotSpecifiedZone:
                walltypecollector.extend(sorted(walltypecollectorTempNotSpecifiedZone)) 
            return walltypecollector, zonescollector

    # Next window
    def nextZoneWindow(self, sender, args):
        try:
            self._list_change_zones()
        except Exception, ex:
            self._list_append_zones()
        self.checkvaluezones = self._checkualueszones()
        if self.checkvaluezones == False:
            # Close the Window Form (Xaml)
            self.Close()
            walltypecollector, zonescollector = self._ZonesAndWallTypeCollector()
            if len(walltypecollector) == 1:
                WallTypeWindow('WallTypeDetailingOneTypeWindow.xaml', index=0, walltypelist=walltypecollector, zoneslist=zonescollector).show(modal=True)
            else:
                WallTypeWindow('WallTypeDetailingFirstWindow.xaml', index=0, walltypelist=walltypecollector, zoneslist=zonescollector).show(modal=True)
		# If there is invalid zones show Error window
        else:
            # Error if the zone range value is invalid 
			ErrorWindow(errortext="You entered an invalid zone range").ShowDialog()

    # Next window
    def next(self, sender, args):
        self.checkvalue = self._checkualues()
		# If all the Bar Spacing are valid  		
        if self.checkvalue == False:
            try:
                self._list_change()
            except Exception, ex:
                self._list_append()
            # Close the Window Form (Xaml)
            self.Close()
            if self.index + 2 < len(self.walltypecollector):
                WallTypeWindow('WallTypeDetailingWindow.xaml', index=self.index+1, walltypelist=self.walltypecollector, zoneslist=self.zonescollector).show(modal=True)
            else :
                WallTypeWindow('WallTypeDetailingFinalWindow.xaml', index=self.index+1, walltypelist=self.walltypecollector, zoneslist=self.zonescollector).show(modal=True)
		# If there is invalid Bar Spacing show Error window
        else:
            # Error if the Load value is invalid (the value must to be float() and greater than 0)  
			ErrorWindow(errortext="You entered an invalid value").ShowDialog()

    # Previous window
    def previous(self, sender, args):
        try:
            self._list_change()
        except Exception, ex:
            self._list_append()
        # Close the Window Form (Xaml)
        self.Close()
        if self.index == 1:
            WallTypeWindow('WallTypeDetailingFirstWindow.xaml', index=0, walltypelist=self.walltypecollector, zoneslist=self.zonescollector).show(modal=True)
        else :
            WallTypeWindow('WallTypeDetailingWindow.xaml', index=self.index-1, walltypelist=self.walltypecollector, zoneslist=self.zonescollector).show(modal=True)

    # Add value for the Wall Zone parameter
    def _wallZoneParameterValue(self, wall, footToCm):
        baseConstraintElevation, topConstraintElevation = self.WallTopBase(wall, footToCm)
        i = 0
        try:
            while i < len(ZoneLevelIndex):
                if baseConstraintElevation >= sorted_levels_elevations_list[ZoneLevelIndex[i]]*footToCm and topConstraintElevation <= sorted_levels_elevations_list[ZoneLevelIndex[i+1]]*footToCm:
                    value_name = sorted_levels_names_list[ZoneLevelIndex[i]] + " to " + sorted_levels_names_list[ZoneLevelIndex[i+1]]
                    wall.LookupParameter('Wall Zone').Set(value_name)
                    break
                else:
                    i = i+2
        except Exception, ex:
            self

    # run the script and create NewDimension, set parameters and generate table.
    def make_wall_type_detailing(self, sender, args):
        try:
            self._list_change()
        except Exception, ex:
            self._list_append()
        bardiameterslist = [8, 10, 12, 14, 16, 18, 20, 22, 25, 28, 32, 36]
        corbardiameterslist = bardiameterslist
        corbardiameterslist.append("Other")
        contypelist = ["B30", "B40", "B50", "B60", "B80", "B100", "Other"]
        rebarHeightList = [1, 2]
        self.checkvalue = self._checkualues()
		# If all the Bar Spacing are valid  		
        if self.checkvalue == False:
            self.Close()
            AddParametersFlag = False
            footToCm = 30.48
            footToMm = 304.8
            with revit.Transaction('Wall Type Detailing', log_errors=False):
                for wall in wall_selection:
                    try:
                        index = 0
                        param = wall.LookupParameter('Wall Type').AsString()
                        if AddParametersFlag == False:
                            self._check_parameters_existence(wall)
                            AddParametersFlag = True
                        if param:
                            baseConstraintElevation, topConstraintElevation = self.WallTopBase(wall, footToCm)
                            while index < len(self.walltypecollector):
                                walltypevalue = self.walltypecollector[index]
                                zonevalue = self.zonescollector[index]
                                i = 0
                                FromCheck = False
                                ToCheck = False
                                while i < len(ZoneLevelIndex):
                                    FromCheck = False
                                    ToCheck = False
                                    if sorted_levels_names_list[ZoneLevelIndex[i]] != "":
                                        FromCheck = sorted_levels_names_list[ZoneLevelIndex[i]] in self.zonescollector[index]
                                        ToCheck = sorted_levels_names_list[ZoneLevelIndex[i+1]] in self.zonescollector[index]
                                        if FromCheck == True and ToCheck == True:
                                            break
                                        else:
                                            i = i+2
                                    else:
                                        break 
                                continueFlag = False
                                if walltypevalue == param and FromCheck == True and ToCheck == True:
                                    if baseConstraintElevation >= sorted_levels_elevations_list[ZoneLevelIndex[i]]*footToCm and topConstraintElevation <= sorted_levels_elevations_list[ZoneLevelIndex[i+1]]*footToCm:
                                        zone_value_name = sorted_levels_names_list[ZoneLevelIndex[i]] + " to " + sorted_levels_names_list[ZoneLevelIndex[i+1]]
                                        continueFlag = True
                                        break
                                elif walltypevalue == param and FromCheck == False and ToCheck == False:
                                    zone_value_name = ""
                                    continueFlag = True
                                    break
                                index = index + 1
                            wall.LookupParameter('Wall Zone').Set(zone_value_name)
                            wall.LookupParameter('Vertical Bar Diameter Interior').Set(bardiameterslist[VerBarDiaIntIndex[index]]/footToMm)
                            wall.LookupParameter('Vertical Rebar Spacing Interior').Set(int(VerBarSpaInt[index])/footToCm)
                            wall.LookupParameter('Vertical Bar Diameter Exterior').Set(bardiameterslist[VerBarDiaExIndex[index]]/footToMm)
                            wall.LookupParameter('Vertical Rebar Spacing Exterior').Set(int(VerBarSpaEx[index])/footToCm)
                            wall.LookupParameter('Horizontal Bar Diameter Interior').Set(bardiameterslist[HorBarDiaIntIndex[index]]/footToMm)
                            wall.LookupParameter('Horizontal Rebar Spacing Interior').Set(int(HorBarSpaInt[index])/footToCm)
                            wall.LookupParameter('Horizontal Bar Diameter Exterior').Set(bardiameterslist[HorBarDiaExIndex[index]]/footToMm)
                            wall.LookupParameter('Horizontal Rebar Spacing Exterior').Set(int(HorBarSpaEx[index])/footToCm)
                            if corbardiameterslist[CorBarDiaIndex[index]] != "Other":
                                wall.LookupParameter('Corner Bar Diameter').Set(corbardiameterslist[CorBarDiaIndex[index]]/footToMm)
                            if contypelist[ConTypeIndex[index]] != "Other":
                                wall.LookupParameter('Concrete Type').Set(contypelist[ConTypeIndex[index]])
                            wall.LookupParameter('Rebar Height (Floors)').Set(rebarHeightList[RebarHeightIndex[0]])
                    except Exception, ex:
                        self
                ZoneCheckValue = self.zonescollector[0]
                ZonesSetCollector = []
                ZonesSetCollector.append(ZoneCheckValue)
                ZonesIndexCollector = []
                ZonesIndex = 0
                ZonesIndexCollector.append(ZonesIndex)
                for ZoneValue in self.zonescollector:
                    if ZoneCheckValue != ZoneValue:
                        ZonesSetCollector.append(ZoneValue)
                        ZoneCheckValue = ZoneValue
                        ZonesIndexCollector.append(ZonesIndex - 1)
                        ZonesIndexCollector.append(ZonesIndex)
                    ZonesIndex = ZonesIndex + 1
                ZonesIndexCollector.append(ZonesIndex - 1)
                ZoneValueIndex = 0
                for ZoneValue in ZonesSetCollector:
                    if not ZoneValue == "Not Specified":
                        ZoneNumber = (ZoneValueIndex+2)/2
                    else:
                        ZoneNumber = "Not Specified"
                    dest_view = self._create_drafting_view(ZoneNumber)
                    ZoneRangeLength = ZonesIndexCollector[ZoneValueIndex+1] - ZonesIndexCollector[ZoneValueIndex] + 1
                    width = (150*5)/footToCm
                    height = (50*(ZoneRangeLength + 1))/footToCm
                    colCount = 5
                    rowCount = ZoneRangeLength + 2
                    rowHeight = 50/footToCm
                    fieldColWidth = 0
                    fieldRowCount = 0
                    fieldRowHeight = 0
                    colWidth = (width-fieldColWidth)/colCount
                    index = ZonesIndexCollector[ZoneValueIndex]
                    verEqualFlag = True
                    horEqualFlag = True
                    while index <= ZonesIndexCollector[ZoneValueIndex + 1]:
                        if VerBarDiaIntIndex[index] != VerBarDiaExIndex[index] or VerBarSpaInt[index] != VerBarSpaEx[index]:
                            width = width + (150/footToCm)
                            colCount = colCount + 1
                            verEqualFlag = False
                            break
                        index = index + 1
                    index = ZonesIndexCollector[ZoneValueIndex]
                    while index <= ZonesIndexCollector[ZoneValueIndex + 1]:
                        if HorBarDiaIntIndex[index] != HorBarDiaExIndex[index] or HorBarSpaInt[index] != HorBarSpaEx[index]:
                            width = width + (150/footToCm)
                            colCount = colCount + 1
                            horEqualFlag = False
                            break
                        index = index + 1
                    colCountMiddel = colCount - 2
                    origin = DB.XYZ()
                    vline = DB.Line.CreateBound(DB.XYZ(origin.X + 150/footToCm, origin.Y, origin.Z), DB.XYZ(origin.X + 150/footToCm, origin.Y + height, origin.Z))
                    hline = DB.Line.CreateBound(DB.XYZ(origin.X, origin.Y, origin.Z), DB.XYZ(origin.X + width, origin.Y, origin.Z))
                    self._createVlines(hline, vline, fieldColWidth, colCountMiddel, colWidth, dest_view)
                    self._createHlines(vline, hline, rowCount+1, rowHeight, fieldRowCount, fieldRowHeight, dest_view)

                    widthFrame = width
                    heightFrame = height + 50/footToCm
                    colCount = 1
                    rowCount = 2
                    rowHeight = heightFrame
                    colWidth = widthFrame
                    vline = DB.Line.CreateBound(DB.XYZ(origin.X, origin.Y, origin.Z), DB.XYZ(origin.X, origin.Y + heightFrame, origin.Z))
                    hline = DB.Line.CreateBound(DB.XYZ(origin.X, origin.Y, origin.Z), DB.XYZ(origin.X + width, origin.Y, origin.Z))
                    self._createVlines(hline, vline, fieldColWidth, colCount, colWidth, dest_view)
                    self._createHlines(vline, hline, rowCount, rowHeight, fieldRowCount, fieldRowHeight, dest_view)

                    widthFrame = width + 20/footToCm
                    heightFrame = height + 70/footToCm
                    colCount = 1
                    rowCount = 2
                    rowHeight = heightFrame
                    fieldColWidth = 0
                    fieldRowCount = 0
                    fieldRowHeight = 0
                    colWidth = widthFrame
                    vline = DB.Line.CreateBound(DB.XYZ(origin.X - 10/footToCm, origin.Y - 10/footToCm, origin.Z), DB.XYZ(origin.X - 10/footToCm, origin.Y - 10/footToCm + heightFrame, origin.Z))
                    hline = DB.Line.CreateBound(DB.XYZ(origin.X - 10/footToCm, origin.Y - 10/footToCm, origin.Z), DB.XYZ(origin.X - 10/footToCm + widthFrame, origin.Y - 10/footToCm, origin.Z))
                    self._createVlines(hline, vline, fieldColWidth, colCount, colWidth, dest_view)
                    self._createHlines(vline, hline, rowCount, rowHeight, fieldRowCount, fieldRowHeight, dest_view)

                    widthCellDivision = width - 450/footToCm
                    rowCount = 1
                    rowHeight = 0
                    fieldRowCount = 0
                    fieldRowHeight = 0
                    hline = DB.Line.CreateBound(DB.XYZ(origin.X + 150/footToCm, origin.Y + height - 25/footToCm, origin.Z), DB.XYZ(origin.X + 150/footToCm + widthCellDivision, origin.Y + height - 25/footToCm, origin.Z))
                    self._createHlines(vline, hline, rowCount, rowHeight, fieldRowCount, fieldRowHeight, dest_view)

                    typeId = self._textType()
                    opts = TextNoteOptions(typeId)
                    opts.HorizontalAlignment = HorizontalTextAlignment.Center
                    self._table_text(footToCm, height, width, dest_view, opts, horEqualFlag, verEqualFlag)

                    symbol1 = self._load_rebar_symbol_family()
                    ZoneIndexStart = ZonesIndexCollector[ZoneValueIndex]
                    ZoneIndexEnd = ZonesIndexCollector[ZoneValueIndex + 1]
                    self._rebar_symbol(footToCm, height, width, dest_view, opts, corbardiameterslist, symbol1, horEqualFlag, verEqualFlag, bardiameterslist, ZoneIndexStart, ZoneIndexEnd, ZoneValue)

                    ZoneValueIndex = ZoneValueIndex + 2

                WallOverrideColor(ZonesSetCollector)

                SuccessWindow(messagetext="Drafting Views with tables were successfully created. Please add it to your Sheet.").ShowDialog()
		# If there is invalid Bar Spacing show Error window
        else:
            # Error if the Load value is invalid (the value must to be float() and greater than 0)  
			ErrorWindow(errortext="You entered an invalid value").ShowDialog()

# Define class to filter elements of category to select 
class CustomSelectionFilter(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, self.category).Id:
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

# Show Error Window Form
class ErrorWindow(Windows.Window):
    def __init__(self, errortext):
        self.errortext=errortext
        wpf.LoadComponent(self, xamlfileError)
        self.error.Text = "{}".format(self.errortext)

    def ok(self, sender, args):
        self.Close()

# Show Success Window Form
class SuccessWindow(Windows.Window):
    def __init__(self, messagetext):
        self.messagetext=messagetext
        wpf.LoadComponent(self, xamlfileSuccess)
        self.message.Text = "{}".format(self.messagetext)

    def ok(self, sender, args):
        self.Close()

# Override the walls with colors
def zonesDetailing():
    ZonesSetCollector = []
    for wall in wall_selection:
        paramWallZone = wall.LookupParameter('Wall Zone').AsString()
        if paramWallZone:
            if not paramWallZone in ZonesSetCollector:
                ZonesSetCollector.append(paramWallZone)
    ZonesNumber = len(ZonesSetCollector)
    detailingFlag = True
    if ZonesNumber > 4:
        ErrorWindow(errortext="You must have maximum four zones.").ShowDialog()
        detailingFlag = False
    LevelsList = []
    i = 0
    if detailingFlag == True:
        level_list_for_sort = []
        for zone in ZonesSetCollector:
            splitZoneValue = zone.split(" to ")
            if len(splitZoneValue) == 2:
                q = 0
                while q < len(sorted_levels_names_list):
                    if splitZoneValue[0] == sorted_levels_names_list[q]:
                        level_list_for_sort.append(sorted_levels_elevations_list[q])
                        break
                    else:
                        q = q + 1
                        if q == len(sorted_levels_names_list):
                            detailingFlag = False
                            ErrorWindow(errortext="You have an invalid values on the 'Wall Zone' parameter.").ShowDialog()
            else:
                ErrorWindow(errortext="You have an invalid values on the 'Wall Zone' parameter.").ShowDialog()
                detailingFlag = False
                break
        zipped_zones_lists = zip(level_list_for_sort, ZonesSetCollector)
        sorted_zipped_zones_lists = sorted(zipped_zones_lists)
        sorted_zones_names_list = [element for _, element in sorted_zipped_zones_lists]
        while i < ZonesNumber:
            splitZoneValue = sorted_zones_names_list[i].split(" to ")
            if len(splitZoneValue) == 2:
                LevelsList.append(splitZoneValue[0])
                LevelsList.append(splitZoneValue[1])
            else:
                ErrorWindow(errortext="You have an invalid values on the 'Wall Zone' parameter.").ShowDialog()
                detailingFlag = False
                break
            i = i+1
        if i == ZonesNumber:
            first = 0
            second = first + 1
            counter = 0
            firstCounter = 0
            firstLevelName = LevelsList[first]
            while firstCounter < len(sorted_levels_names_list):
                if firstLevelName == sorted_levels_names_list[firstCounter]:
                    firstLevelElevation = sorted_levels_elevations_list[firstCounter]
                    break
                else:
                    firstCounter = firstCounter+1
                    if firstCounter == len(sorted_levels_names_list):
                        detailingFlag = False
                        ErrorWindow(errortext="You have an invalid values on the 'Wall Zone' parameter.").ShowDialog()
            if detailingFlag == True:
                while counter < len(LevelsList)-1:
                    secondLevelName = LevelsList[second]
                    secondCounter = 0
                    while secondCounter < len(sorted_levels_names_list):
                        if secondLevelName == sorted_levels_names_list[secondCounter]:
                            secondLevelElevation = sorted_levels_elevations_list[secondCounter]
                            break
                        else:
                            secondCounter = secondCounter+1
                            if secondCounter == len(sorted_levels_names_list):
                                detailingFlag = False
                                ErrorWindow(errortext="You have an invalid values on the 'Wall Zone' parameter.").ShowDialog()
                                counter = len(LevelsList)
                    if detailingFlag == True:
                        if firstLevelElevation > secondLevelElevation:
                            detailingFlag = False
                            ErrorWindow(errortext="You have an invalid values on the 'Wall Zone' parameter.").ShowDialog()
                            break
                        else:
                            firstLevelElevation = secondLevelElevation
                            second = second + 1
                            counter = counter + 1
    if detailingFlag == True:
        with revit.Transaction('Wall Zones Detailing', log_errors=False):
            WallOverrideColor(sorted_zones_names_list)

# Set colors for the walls
def WallOverrideColor(ZonesSetCollector):
    colorW = WallColors(0, 120, 120, 0, 45, 45)
    colorM = WallColors(255, 50, 0, 0, 45, 0)
    colorG = WallColors(0, 120, 0, 0, 45, 0)
    colorWT = WallColors(120, 0, 120, 45, 0, 45)
    ogs = DB.OverrideGraphicSettings()
    solid_fpattern = find_solid_fillpat()
    if solid_fpattern:
        if HOST_APP.is_newer_than(2018):
            ogs.SetCutForegroundPatternId(solid_fpattern.Id)
            ogs.SetSurfaceForegroundPatternId(solid_fpattern.Id)
        else:
            ogs.SetProjectionFillPatternId(solid_fpattern.Id)
            ogs.SetCutFillPatternId(solid_fpattern.Id)
    else:
        ErrorWindow(errortext="Can not find solid fill pattern in model to assign as projection/cut pattern.").ShowDialog()
    for wall in wall_selection:
        paramWallType = wall.LookupParameter('Wall Type').AsString()
        paramWallZone = wall.LookupParameter('Wall Zone').AsString()
        if paramWallType and paramWallZone:
            i = 0
            for ZoneValue in ZonesSetCollector:
                if paramWallZone in ZoneValue:
                    break
                i = i + 1
            if "M" in paramWallType:
                colorValue = colorM[i]
            elif "G" in paramWallType:
                colorValue = colorG[i]
            elif "WT" in paramWallType:
                colorValue = colorWT[i]
            elif "W" in paramWallType:
                colorValue = colorW[i]
            if HOST_APP.is_newer_than(2018):
                ogs.SetSurfaceForegroundPatternColor(colorValue)
                ogs.SetCutForegroundPatternColor(colorValue)
            else:
                ogs.SetProjectionFillColor(colorValue)
                ogs.SetCutFillColor(colorValue)
            revit.doc.ActiveView.SetElementOverrides(wall.Id, ogs)

# Build list of walls colors
def WallColors(r, g, b, rUpdate, gUpdate, bUpdate):
    colorList = []
    i = 0
    while i < 4:
        color = DB.Color(r, g, b)
        colorList.append(color)
        r = r + rUpdate
        g = g + gUpdate
        b = b + bUpdate
        i = i + 1
    return colorList

def add_parameters(parameter_name):
    # Get the parameter file
    originalFile = app.SharedParametersFilename
    app.SharedParametersFilename = sys.path[0]+"\TempDavidEngSharedParameters.txt"
    sharedParameterFile = app.OpenSharedParameterFile()
    # Add Categories to Category Set. 
    wallCat = doc.Settings.Categories.get_Item(BuiltInCategory.OST_Walls);
    catset = app.Create.NewCategorySet()
    catset.Insert(wallCat);
    # Txt group name 
    GroupName = sharedParameterFile.Groups.get_Item("Rebar")
    # Txt parameter name
    externalDefinition = GroupName.Definitions.get_Item(parameter_name) 
    # Create the new shared parameter 
    newInstanceBinding = app.Create.NewInstanceBinding(catset)
    # Insert the new parameter into your project.
    doc.ParameterBindings.Insert(externalDefinition, newInstanceBinding, BuiltInParameterGroup.PG_STRUCTURAL)
    # Set "Values can vary by group instance"
    try:
        param = query.get_project_parameter(parameter_name, doc=doc)
        param.param_def.SetAllowVaryBetweenGroups(doc, True)
    except Exception, ex:
        parameter_name
    app.SharedParametersFilename = originalFile

# draw line for the dimension
def LineElement(element):
    footToCm = 30.48
    if round(element.Location.Curve.GetEndPoint(0).Y, 3) == round(element.Location.Curve.GetEndPoint(1).Y, 3):
        offDistX = (element.Width/2) + 30/footToCm
        offDistY = (element.Width/2) + 30/footToCm
    elif round(element.Location.Curve.GetEndPoint(0).X, 3) == round(element.Location.Curve.GetEndPoint(1).X, 3):
        offDistX = - (element.Width/2) - 30/footToCm
        offDistY = - (element.Width/2) - 30/footToCm
    else:
        offDistX = (element.Width/2) - 15/footToCm
        offDistY = (element.Width/2) + 60/footToCm
    pt1 = XYZ(element.Location.Curve.GetEndPoint(0).X+offDistX, element.Location.Curve.GetEndPoint(0).Y+offDistY, 0)
    pt2 = XYZ(element.Location.Curve.GetEndPoint(1).X+offDistX, element.Location.Curve.GetEndPoint(1).Y+offDistY, 0)
    line = Line.CreateBound(pt1,pt2)
    return line

# Collectiong the Wall Types
def WallTypeCollector(wall_selection):
    walltypecollector = []
    for wall in wall_selection:
        param = wall.LookupParameter('Wall Type').AsString()
        if param:
            typeflag = False
            for walltype in walltypecollector:
                if walltype == wall.LookupParameter('Wall Type').AsString():
                    typeflag = True
                    break
            if typeflag == False:
                walltypecollector.append(wall.LookupParameter('Wall Type').AsString())
    return sorted(walltypecollector)

# find crossing wall faces and return true or false
def isParallel(v1,v2):
	return v1.CrossProduct(v2).IsAlmostEqualTo(XYZ(0,0,0))

def frange(start, count, step=1.0):
    ''' "range()" like function which accept float type''' 
    i = start
    for c in range(count):
        yield i
        i += step

def direction(line):
    p = line.GetEndPoint(0)
    q = line.GetEndPoint(1)
    v = q-p
    vn = v.Normalize()
    return vn

def find_solid_fillpat():
    existing_pats = DB.FilteredElementCollector(revit.doc)\
                      .OfClass(DB.FillPatternElement)\
                      .ToElements()
    for pat in existing_pats:
        fpat = pat.GetFillPattern()
        if fpat.IsSolidFill and fpat.Target == DB.FillPatternTarget.Drafting:
            return pat

if (DavidEng.checkPermission()):
    if doc.ActiveView.LookupParameter("Type").AsValueString() == "3D View":
        # collecting all the starting walls that selected by the user
        if not selection:
            customFilter = CustomSelectionFilter(DB.BuiltInCategory.OST_Walls)
            try:
                wall_selection = [doc.GetElement(reference) for reference in uidoc.Selection.PickObjects(
                    ObjectType.Element, customFilter, "Select Walls")]
            except Exceptions.OperationCanceledException:
                import sys
                sys.exit()
        else:
            wall_selection = []
            for element in selection:
                if element.Category.Name == 'Walls':
                    wall_selection.append(element)

        for wall in wall_selection:
            if wall.LookupParameter('Wall Type'):
                walltypecollector = WallTypeCollector(wall_selection)
                if not len(walltypecollector) == 0:
                    ChooseOptionWindow('SelectOption.xaml').show(modal=True)
                    break
                else:
                    ErrorWindow(errortext="No value was added to the\n'Wall Type' parameter of the selected walls").ShowDialog()
                    break
            else:
                with revit.Transaction('Add Wall Type Parameter', log_errors=False):
                    # Get the parameter file
                    originalFile = app.SharedParametersFilename
                    app.SharedParametersFilename = sys.path[0]+"\TempDavidEngSharedParameters.txt"
                    sharedParameterFile = app.OpenSharedParameterFile()
                    # Add Categories to Category Set. 
                    wallCat = doc.Settings.Categories.get_Item(BuiltInCategory.OST_Walls);
                    catset = app.Create.NewCategorySet()
                    catset.Insert(wallCat);
                    # Txt group name 
                    GroupName = sharedParameterFile.Groups.get_Item("Rebar")
                    # Txt parameter name
                    externalDefinition = GroupName.Definitions.get_Item("Wall Type") 
                    # Create the new shared parameter 
                    newInstanceBinding = app.Create.NewInstanceBinding(catset)
                    # Insert the new parameter into your project.
                    doc.ParameterBindings.Insert(externalDefinition, newInstanceBinding, BuiltInParameterGroup.PG_STRUCTURAL)
                    # Set "Values can vary by group instance"
                    param = query.get_project_parameter("Wall Type", doc=doc)
                    param.param_def.SetAllowVaryBetweenGroups(doc, True)
                    app.SharedParametersFilename = originalFile
                ErrorWindow(errortext="No value was added to the\n'Wall Type' parameter of the selected walls").ShowDialog()
                break
    else:
        ErrorWindow(errortext="You can only run this add-in on\n3D View").ShowDialog()
else:
    DavidEng.ErrorWindow(errortext="You don't have permission\nto use the tool").ShowDialog()