# # -*- coding: utf-8 -*-

"""Version: 1.5.9
Rebar Quantities Tool collects all the visible rebar families from the current view and calculates the quantities."""

# App, Author Name and Version
__title__ = 'Rebar\nQuantities'
__authors__ = 'Yuval Dolin'
__version__ = '1.5.9'

import sys
import os
sys.path.append(os.path.dirname(__file__))
from core import main
from pyrevit import script

# import WPF creator and base Window
import wpf
import DavidEng
from System import Windows

# find the path of RebarQuantitiesWindow.xaml
barQuantitiesXamlFile = script.get_bundle_file('RebarQuantitiesWindow.xaml')

# Identify the current Revit file
doc = __revit__.ActiveUIDocument.Document

class RebarQuantitiesWindow(Windows.Window):
    def __init__(self, quantityToString):
        self.quantitiesAsString = quantityToString
        wpf.LoadComponent(self, barQuantitiesXamlFile)
        self.view_name.Text = doc.ActiveView.Name
        self.rebar_quantities.Text = self.quantitiesAsString

    def close(self, sender, args):
        self.Close()

def num(s):
    """ 3.0 -> 3, 3.001000 -> 3.001 otherwise return s """
    s = str(s)
    try:
        int(float(s))
        return s.rstrip('0').rstrip('.')
    except ValueError:
        return s

def CalcQuantitiesByDiameter():
    global totalQuantity
    quantitiesByDiameter = []
    dia = quantitiesArray[1][1]
    weight = quantitiesArray[1][4]
    quantitiesByDiameter.append([dia, weight])
    totalQuantity += weight
    rowIndex = 2
    while rowIndex < len(quantitiesArray):
        if quantitiesArray[rowIndex][1] != quantitiesByDiameter[len(quantitiesByDiameter)-1][0]:
            dia = quantitiesArray[rowIndex][1]
            weight = quantitiesArray[rowIndex][4]
            quantitiesByDiameter.append([dia, weight])
            totalQuantity += weight
        else:
            weight = quantitiesArray[rowIndex][4]
            quantitiesByDiameter[len(quantitiesByDiameter)-1][1] += weight
            totalQuantity += weight
        rowIndex += 1
    return quantitiesByDiameter

def QuantityToString(calcQuantitiesByDiameter):
    quantityString = ""
    for diaRow in calcQuantitiesByDiameter:
        quantityString += "Ø" + num(str(diaRow[0])) + " - " +  str(round(diaRow[1], 2)) + " ton" + "\n"
    return quantityString

def CalcQuantitiesByMeshType():
    global totalQuantity
    quantitiesByMeshType = []
    meshType = fabricSheetQuantitiesArray[1][0]
    weight = fabricSheetQuantitiesArray[1][7]
    quantitiesByMeshType.append([meshType, weight])
    totalQuantity += weight
    rowIndex = 2
    while rowIndex < len(fabricSheetQuantitiesArray):
        if fabricSheetQuantitiesArray[rowIndex][0] != quantitiesByMeshType[len(quantitiesByMeshType)-1][0]:
            meshType = fabricSheetQuantitiesArray[rowIndex][0]
            weight = fabricSheetQuantitiesArray[rowIndex][7]
            quantitiesByMeshType.append([meshType, weight])
            totalQuantity += weight
        else:
            weight = fabricSheetQuantitiesArray[rowIndex][7]
            quantitiesByMeshType[len(quantitiesByMeshType)-1][1] += weight
            totalQuantity += weight
        rowIndex += 1
    return quantitiesByMeshType

def QuantityToStringMesh(calcQuantitiesByDiameter):
    quantityString = ""
    for diaRow in calcQuantitiesByDiameter:
        quantityString += "Mesh Type " + num(str(diaRow[0])) + " - " +  str(round(diaRow[1], 2)) + " ton" + "\n"
    return quantityString

if (DavidEng.checkPermission()):
    
    quantitiesArray, fabricSheetQuantitiesArray, continueFlag, rebarShapesFlag, fabricSheetFlag = main()

    if continueFlag == True:
        quantityToString = ""
        global totalQuantity
        totalQuantity = 0
        if rebarShapesFlag == True:
            calcQuantitiesByDiameter = CalcQuantitiesByDiameter()
            quantityToString = QuantityToString(calcQuantitiesByDiameter)
        if fabricSheetFlag == True:
            calcQuantitiesByMeshType = CalcQuantitiesByMeshType()
            quantityToStringMesh = QuantityToStringMesh(calcQuantitiesByMeshType)
            quantityToString += quantityToStringMesh
        quantityToString += "\n" + "Total: " + str(round(totalQuantity, 2)) + " ton" + "\n"
        RebarQuantitiesWindow(quantityToString).ShowDialog()
else:
    DavidEng.ErrorWindow(errortext="You don't have permission\nto use the tool").ShowDialog()