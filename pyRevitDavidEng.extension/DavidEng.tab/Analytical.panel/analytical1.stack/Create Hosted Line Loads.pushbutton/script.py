"""The script will load the selected floors edges with the user load input (Fz force)."""
# pylint: disable=E0401,W0703,W0613
from pyrevit import coreutils, UI, revit, DB, forms, script
from pyrevit.framework import Math
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, BuiltInParameter, Transaction, XYZ, ElementId, Options, Solid, Line, Plane, SketchPlane, Arc, Transform, TransactionGroup
from Autodesk.Revit.DB.Structure import LineLoad
from Autodesk.Revit.UI.Selection import ObjectType, ISelectionFilter
from Autodesk.Revit import Exceptions
from pyrevit import HOST_APP

# dependencies
import clr
clr.AddReference('System.Windows.Forms')
import DavidEng

# find the path of ErrorWindow.xaml
xamlfile = script.get_bundle_file('ErrorWindow.xaml')

# find the path of CreateHostedLineLoadsWindow.xaml
UserInputXamlFile = script.get_bundle_file('CreateHostedLineLoadsWindow.xaml')

# import WPF creator and base Window
import wpf
from System import Windows

# App and Author name
__title__ = 'Create Hosted\nLine Loads'
__authors__ = 'Yuval Dolin'

# Identify the current Revit file
doc = __revit__.ActiveUIDocument.Document
uidoc = __revit__.ActiveUIDocument

# Creating collectior instance and collecting all the Load Cases from the model
loadcases = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_LoadCases)

if HOST_APP.is_newer_than(2022):
    Analytical_Floor_Collector = []
    # Creating collectior instance and collecting all the analytical panels from the model
    analyticalPanelsCollector = FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_AnalyticalPanel) \
                                                .WhereElementIsNotElementType()
    for analyticalPanel in analyticalPanelsCollector:
        if analyticalPanel.StructuralRole.ToString() == 'StructuralRoleFloor':
            Analytical_Floor_Collector.append(analyticalPanel)
else:
    # Creating collectior instance and collecting all the floors from the model
    Analytical_Floor_Collector = FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_FloorAnalytical) \
                                                .WhereElementIsNotElementType()

# Define class to filter elements of category to select 
class CustomSelectionFilter(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, DB.BuiltInCategory.OST_Lines).Id:
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

# Show Error Window Form
class ErrorWindow(Windows.Window):
    def __init__(self, errortext):
        self.errortext=errortext
        wpf.LoadComponent(self, xamlfile)
        self.error.Text = "{}".format(self.errortext)

    def ok(self, sender, args):
        self.Close()

class SettingsWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name):
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)
        self._setup_loadcases()

    def cancel(self, sender, args):
        self.Close()
        t = Transaction(doc, 'Delete Temporary Model Lines')
        t.Start()
        for TempLine in ListTempModelLines:
            doc.Delete(TempLine.Id)
        t.Commit()

    def apply_new_values(self, sender, args):
		# Check if all the loads are valid (the value must to be float())  
        self.checkvalue = True
        try:
            floatloadvalue=float(self.load_format_tb.Text)
        except Exception:
            self.checkvalue = False
		# If all the loads are valid  		
        if self.checkvalue == True:
			# Close the Window Form (Xaml)
            self.Close()
			# Create Area Loads with the inserted values in one transaction (one Undo to erase the loads)
            t = Transaction(doc, 'Place Hosted Line Loads')
            t.Start()
            param = self.selected_loadcase
            for load in loadcases:
                if load.Name == param:
                    loadid=load.Id.IntegerValue
            fz=float(self.load_format_tb.Text)
            force=XYZ(0,0,fz*9806.65)
            moment=XYZ(0,0,0)
            if param:
                for FloorModelLine in Model_Lines_Selection:
                    LoadCheck = False
                    i = 0
                    for Analytical_Floor in Analytical_Floor_Collector:
                        i = 0
                        if HOST_APP.is_newer_than(2022):
                            curves = Analytical_Floor.GetOuterContour()
                        else:
                            curves = Analytical_Floor.GetCurves(0)
                        for Curve in curves:
                            if Curve.IsCyclic == False:
                                try:
                                    if round(Curve.Origin.X, 0) == round(FloorModelLine.Location.Curve.Origin.X, 0) \
                                    and round(Curve.Origin.Y, 0) == round(FloorModelLine.Location.Curve.Origin.Y, 0) \
                                    and round(Curve.Origin.Z, 0) == round(FloorModelLine.Location.Curve.Origin.Z, 0) \
                                    and round(Curve.Direction.X, 0) == round(FloorModelLine.Location.Curve.Direction.X, 0) \
                                    and round(Curve.Direction.Y, 0) == round(FloorModelLine.Location.Curve.Direction.Y, 0) \
                                    and round(Curve.Direction.Z, 0) == round(FloorModelLine.Location.Curve.Direction.Z, 0) \
                                    and round(Curve.Length, 0) == round(FloorModelLine.Location.Curve.Length, 0):
                                        if HOST_APP.is_newer_than(2022):
                                            createload=LineLoad.Create(doc,Analytical_Floor.Id,i,force,moment,None)
                                        else:
                                            createload=LineLoad.Create(doc,Analytical_Floor,i,force,moment,None)
                                        setloadcase=createload.get_Parameter(BuiltInParameter.LOAD_CASE_ID).Set(ElementId(loadid))
                                        LoadCheck = True
                                        break
                                    else:
                                        i = i+1
                                except Exception as ex:
                                    i = i+1
                            else:
                                try:
                                    if round(Curve.Center.X, 0) == round(FloorModelLine.Location.Curve.Center.X, 0) \
                                    and round(Curve.Center.Y, 0) == round(FloorModelLine.Location.Curve.Center.Y, 0) \
                                    and round(Curve.Center.Z, 0) == round(FloorModelLine.Location.Curve.Center.Z, 0) \
                                    and round(Curve.Radius, 0) == round(FloorModelLine.Location.Curve.Radius, 0) \
                                    and round(Curve.Length, 0) == round(FloorModelLine.Location.Curve.Length, 0):
                                        if HOST_APP.is_newer_than(2022):
                                            createload=LineLoad.Create(doc,Analytical_Floor.Id,i,force,moment,None)
                                        else:
                                            createload=LineLoad.Create(doc,Analytical_Floor,i,force,moment,None)
                                        setloadcase=createload.get_Parameter(BuiltInParameter.LOAD_CASE_ID).Set(ElementId(loadid))
                                        LoadCheck = True
                                        break
                                    else:
                                        i = i+1
                                except Exception as ex:
                                    i = i+1
                        if LoadCheck == True:
                            break
            for TempLine in ListTempModelLines:
                doc.Delete(TempLine.Id)
            t.Commit()
            tg.Assimilate()    
		# If there is invalid Load show Error window
        else:
            # Error if the Load value is invalid (the value must to be float())  
			ErrorWindow(errortext="You entered an invalid value: '{}'".format(self.load_format_tb.Text)).ShowDialog()

    @property
    def selected_loadcase(self):
        return self.loadcases_cb.SelectedItem

    @property
    def load_format(self):
        return self.load_format_tb.Text

# Setup the Load Cases for the ComboBox (list of Load Cases)
    def _setup_loadcases(self):
        loadnamelist = set()
        for load in loadcases:
            if not load.Name == "SW":
                loadnamelist.add(load.Name)
        loadcasenamelist = []
        loadcasenamelist.extend(sorted(list(loadnamelist)))
        self.loadcases_cb.ItemsSource = loadcasenamelist
        self.loadcases_cb.SelectedIndex = 0

def AnalyticalFloorCollector(floor_collector_all):
    # Collecting all the floors from the model that have value in the 'Category of Use' and 'Enable Analytical Model' is checked
    List_Analytical_Floor = set()
    # Flag to show the warning window only once
    analyticalcheckwindow = True
    for floor in floor_collector_all:
        analyticalcheck = 'No'
        try:
            analyticalcheck = floor.get_Parameter(BuiltInParameter.STRUCTURAL_ANALYTICAL_MODEL).AsValueString()
        except Exception, ex:
            analyticalcheck = 'No'
        if analyticalcheck=='Yes':
            List_Analytical_Floor.add(floor.GetAnalyticalModel())
    return List_Analytical_Floor

#Create temporary model lines for the selection of the floor edges
def TempModelLines(Analytical_Floor_Collector):
    opts = Options()
    opts.ComputeReferences = True
    ListTempLines = []
    t = Transaction(doc, 'Temporary Model Lines')
    t.Start()
    for AnalyticalFloor in Analytical_Floor_Collector:
        FloorGeo = AnalyticalFloor.get_Geometry(opts)
        for obj in FloorGeo:
            if isinstance(obj,Solid):
                for Face in obj.Faces:
                    EdgeArray = Face.EdgeLoops
                    break
            break
        for Edge in EdgeArray:
            for FloorLine in Edge:
                try:
                    StartPoint = FloorLine.AsCurve().GetEndPoint(0)
                    EndPoint = FloorLine.AsCurve().GetEndPoint(1)
                    p1 = XYZ(StartPoint.X, StartPoint.Y, StartPoint.Z)
                    p2 = XYZ(EndPoint.X, EndPoint.Y, EndPoint.Z)
                    pl = Plane.CreateByNormalAndOrigin(Face.FaceNormal, Face.Origin)
                    if FloorLine.AsCurve().IsCyclic == False:
                        line = Line.CreateBound(p1, p2)
                    else:
                        radius = FloorLine.AsCurve().Radius
                        center = FloorLine.AsCurve().Center
                        sagitta = radius - Math.Sqrt(Math.Pow(radius, 2.0) - Math.Pow((p2 - p1).GetLength() / 2.0, 2.0))
                        midPointOfChord = (p1 + p2) / 2.0
                        midPointOfArc = midPointOfChord + Transform.CreateRotation(XYZ.BasisZ, Math.PI / 2.0).OfVector((p2 - p1).Normalize().Multiply(sagitta))
                        if round((midPointOfArc - center).GetLength(), 5) == round(radius, 5):
                            line = Arc.Create(p1, p2, midPointOfArc)
                        else:
                            midPointOfArc = midPointOfArc - 2 * Transform.CreateRotation(XYZ.BasisZ, Math.PI / 2.0).OfVector((p2 - p1).Normalize().Multiply(sagitta))
                            line = Arc.Create(p1, p2, midPointOfArc)
                    sk = SketchPlane.Create(doc, pl)
                    mc = doc.Create.NewModelCurve(line, sk)
                    ListTempLines.append(mc)
                    ogs = DB.OverrideGraphicSettings()
                    ogs.SetProjectionLineColor(DB.Color(0, 255, 0))
                    ogs.SetProjectionLineWeight(10)
                    revit.doc.ActiveView.SetElementOverrides(mc.Id, ogs)
                except:
                    pass
    t.Commit()
    return ListTempLines

if (DavidEng.checkPermission()):
    if HOST_APP.is_newer_than(2022):
        analyticalFloorsCount = len(Analytical_Floor_Collector)
    else:
        analyticalFloorsCount = Analytical_Floor_Collector.GetElementCount()
    if analyticalFloorsCount != 0:
        tg = TransactionGroup(doc, 'Creare Hosted Line Loads')
        tg.Start()
        ListTempModelLines = TempModelLines(Analytical_Floor_Collector) 
        customFilter = CustomSelectionFilter(DB.BuiltInCategory)
        Model_Lines_Selection = None
        try:
            Model_Lines_Selection = [doc.GetElement(reference) for reference in uidoc.Selection.PickObjects(
                ObjectType.Element, customFilter, "Select Floors Edges")]
        except Exception, ex:
            Model_Lines_Selection = False
        if Model_Lines_Selection:
            # Run the Xaml
            SettingsWindow(UserInputXamlFile).show(modal=True)
        elif Model_Lines_Selection != False:
            ErrorWindow(errortext="You have not selected any floor edge.").ShowDialog()
            t = Transaction(doc, 'Delete Temporary Model Lines')
            t.Start()
            for TempLine in ListTempModelLines:
                doc.Delete(TempLine.Id)
            t.Commit()
            tg.RollBack()
        else:
            tg.RollBack()
    else:
        ErrorWindow(errortext="You don't have any visiable analytical floor in your active view.").ShowDialog()
else:
    DavidEng.ErrorWindow(errortext="You don't have permission\nto use the tool").ShowDialog()