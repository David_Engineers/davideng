"""Generate wind loads according to the values that the user enter and in accordance with the Israeli standard (Code according to: SI 414 - 2008)."""
# pylint: disable=E0401,W0703,W0613
from pyrevit import coreutils, UI, revit, DB, forms, script
from pyrevit.framework import Math
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, BuiltInParameter, Transaction, XYZ, ElementId, Options, Solid, Line, Plane, SketchPlane, Arc, Transform, TransactionGroup, Level
from Autodesk.Revit.DB.Structure import LineLoad
from Autodesk.Revit.UI.Selection import ObjectType, ISelectionFilter
from Autodesk.Revit import Exceptions
from pyrevit import HOST_APP

# dependencies
import clr
clr.AddReference('System.Windows.Forms')
import DavidEng

# find the path of ErrorWindow.xaml
xamlfile = script.get_bundle_file('ErrorWindow.xaml')

# find the path of SelectZeroLevel.xaml
SelectZeroLevelXamlFile = script.get_bundle_file('SelectZeroLevel.xaml')

# find the path of SelectLevels.xaml
SelectLevelsXamlFile = script.get_bundle_file('SelectLevels.xaml')

# find the path of GenerateWindLoadsWindow.xaml
GenerateWindLoadsXamlFile = script.get_bundle_file('GenerateWindLoadsWindow.xaml')

# import WPF creator and base Window
import wpf
from System import Windows

# App and Author name
__title__ = 'Generate\nWind Loads'
__authors__ = 'Yuval Dolin'

# Identify the current Revit file
doc = __revit__.ActiveUIDocument.Document
uidoc = __revit__.ActiveUIDocument

# Creating collectior instance and collecting all the Load Cases from the model
loadcases = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_LoadCases)

# Creating collectior instance and collecting all the Load Cases from the model
LevelsCollector = FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_Levels)

if HOST_APP.is_newer_than(2022):
    Analytical_Floor_Collector = []
    # Creating collectior instance and collecting all the Analytical Panels from the model
    analyticalPanelsCollector = FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_AnalyticalPanel)
    for analyticalPanel in analyticalPanelsCollector:
        if analyticalPanel.StructuralRole.ToString() == 'StructuralRoleFloor':
            Analytical_Floor_Collector.append(analyticalPanel)
else:
    # Creating collectior instance and collecting all the floors from the model
    Analytical_Floor_Collector = FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_FloorAnalytical) \
                                                .WhereElementIsNotElementType()

# Define class to filter elements of category to select 
class CustomSelectionFilter(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, DB.BuiltInCategory.OST_Lines).Id:
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

# Define class to filter elements of category to select 
class CustomSelectionFilterLevel(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, DB.BuiltInCategory.OST_Levels).Id:
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

# Show Error Window Form
class ErrorWindow(Windows.Window):
    def __init__(self, errortext):
        self.errortext=errortext
        wpf.LoadComponent(self, xamlfile)
        self.error.Text = "{}".format(self.errortext)

    def ok(self, sender, args):
        self.Close()

class ChooseLevelsWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name):
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)

    # user select all levels
    def all_levels(self, sender, args):
        self.Close()
        levels_collector_all = FilteredElementCollector(doc).OfClass(Level).WhereElementIsNotElementType().ToElements()
        SelectZeroLevel(SelectZeroLevelXamlFile, levels=levels_collector_all).show(modal=True)

    # user select specific levels
    def select_levels(self, sender, args):
        self.Close()
        customFilter = CustomSelectionFilterLevel(DB.BuiltInCategory.OST_Levels)
        try:
            level_selection = [doc.GetElement(reference) for reference in uidoc.Selection.PickObjects(
                ObjectType.Element, customFilter, "Select Levels")]
        except Exceptions.OperationCanceledException:
            import sys
            sys.exit()
        if not level_selection:
			ErrorWindow(errortext="You have not selected any Level").ShowDialog()
			ChooseLevelsWindow(SelectLevelsXamlFile).show(modal=True)
        else:
			SelectZeroLevel(SelectZeroLevelXamlFile, levels=level_selection).show(modal=True)

class SelectZeroLevel(forms.WPFWindow):
    def __init__(self, xaml_file_name, levels):
        self.levels = levels
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)

    # user select all columns
    def select_level(self, sender, args):
        self.Close()
        customFilter = CustomSelectionFilterLevel(DB.BuiltInCategory)
        try:
            Level_Selection = uidoc.Selection.PickObject(ObjectType.Element, customFilter, "Select +0.00 Level")
        except Exception, ex:
            Level_Selection = None
        if Level_Selection:
            Selected_Level = doc.GetElement(Level_Selection)
            Highest_Level = self.building_height()
            # Run the Xaml
            SettingsWindow(GenerateWindLoadsXamlFile, Top_Elev=Highest_Level, Zero_Elev=Selected_Level.ProjectElevation*30.48, levels=self.levels).show(modal=True)
        else:
            tg.RollBack()

    def building_height(self):
        Top_Level = 0
        for Level in LevelsCollector:
            if Level.ProjectElevation > Top_Level:
                Top_Level = Level.ProjectElevation
        return Top_Level*30.48

class SettingsWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name, Top_Elev, Zero_Elev, levels):
        self.top_elev = Top_Elev
        self.zero_elev = Zero_Elev
        self.levels = levels
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)
        self._setup_loadcases()
        self._setup_terrain_type()
        self._setup_wind_direction()
        self.height_value.Text = str(round(self.top_elev-self.zero_elev, 0))
        Bot_Width = self.building_width()

    def cancel(self, sender, args):
        self.Close()
        t = Transaction(doc, 'Delete Temporary Model Lines')
        t.Start()
        for TempLine in ListTempModelLines:
            doc.Delete(TempLine.Id)
        t.Commit()

    def apply(self, sender, args):
		# Check if all the loads are valid (the value must to be float())  
        self.checkvalue = True
        try:
            floatloadvalue=float(self.height_value.Text)
            floatloadvalue=float(self.width_value.Text)
            if float(self.vb_value.Text) < 0:
                self.checkvalue = False
            if float(self.cpe_value.Text) < 0:
                self.checkvalue = False
        except Exception:
            self.checkvalue = False
		# If all the loads are valid  		
        if self.checkvalue == True:
            # Close the Window Form (Xaml)
            self.Close()
            levels_sorted_list = self.sort_levels()
            height_zones = self.loads_zones(levels_sorted_list)
            floor_edges_lines = self.sort_floor_edges()
            level_loads = self.levels_loads_values(height_zones, levels_sorted_list)
            t = Transaction(doc, 'Generate Wind Loads')
            t.Start()
            self.apply_loads(levels_sorted_list, floor_edges_lines, level_loads)
            t.Commit()
            t = Transaction(doc, 'Delete Temporary Model Lines')
            t.Start()
            for TempLine in ListTempModelLines:
                doc.Delete(TempLine.Id)
            t.Commit()
            tg.Assimilate()

		# If there is invalid Load show Error window
        else:
            # Error if the Load value is invalid (the value must to be float())  
			ErrorWindow(errortext="You entered an invalid value.").ShowDialog()

    def apply_loads(self, levels_list, edges_lines, loads):
        i = 0
        for line in edges_lines:
            line_elev = float(round(line.Location.Curve.GetEndPoint(0).Z*30.48, 2))-self.zero_elev
            while i < len(levels_list):
                if line_elev == (round(levels_list[i].ProjectElevation*30.48, 2)-self.zero_elev):
                    self.create_load(loads[i], line)
                    break
                elif i == 0 and line_elev < (round(levels_list[i].ProjectElevation*30.48, 2)-self.zero_elev):
                    if (round(levels_list[i].ProjectElevation*30.48, 2)-self.zero_elev)-line_elev < line_elev:
                        self.create_load(loads[i], line)
                        break
                    else:
                        break
                elif i+1 == len(levels_list):
                    if (round(levels_list[i].ProjectElevation*30.48, 2)-self.zero_elev)-line_elev <= line_elev-(round(levels_list[i-1].ProjectElevation*30.48, 2)-self.zero_elev):
                        self.create_load(loads[i], line)
                        break
                    else:
                        self.create_load(loads[i-1], line)
                        break
                elif line_elev < (round(levels_list[i].ProjectElevation*30.48, 2)-self.zero_elev) and line_elev > (round(levels_list[i-1].ProjectElevation*30.48, 2)-self.zero_elev):
                    if (round(levels_list[i].ProjectElevation*30.48, 2)-self.zero_elev)-line_elev <= line_elev-(round(levels_list[i-1].ProjectElevation*30.48, 2)-self.zero_elev):
                        self.create_load(loads[i], line)
                        break
                    else:
                        self.create_load(loads[i-1], line)
                        break
                else:
                    i = i+1

    def create_load(self, load_value, ModelLine):
        param = self.selected_loadcase
        for load in loadcases:
            if load.Name == param:
                loadid=load.Id.IntegerValue
        if self.wind_direction.SelectedIndex == 0:
            force=XYZ(load_value*9806.65,0,0)
        elif self.wind_direction.SelectedIndex == 1:
            force=XYZ(load_value*9806.65*-1,0,0)
        elif self.wind_direction.SelectedIndex == 2:
            force=XYZ(0,load_value*9806.65,0)
        else:
            force=XYZ(0,load_value*9806.65*-1,0)
        moment=XYZ(0,0,0)
        if param:
            LoadCheck = False
            i = 0
            for Analytical_Floor in Analytical_Floor_Collector:
                i = 0
                if HOST_APP.is_newer_than(2022):
                    curves = Analytical_Floor.GetOuterContour()
                else:
                    curves = Analytical_Floor.GetCurves(0)
                for Curve in curves:
                    if Curve.IsCyclic == False:
                        try:
                            if round(Curve.Origin.X, 0) == round(ModelLine.Location.Curve.Origin.X, 0) \
                            and round(Curve.Origin.Y, 0) == round(ModelLine.Location.Curve.Origin.Y, 0) \
                            and round(Curve.Origin.Z, 0) == round(ModelLine.Location.Curve.Origin.Z, 0) \
                            and round(Curve.Direction.X, 0) == round(ModelLine.Location.Curve.Direction.X, 0) \
                            and round(Curve.Direction.Y, 0) == round(ModelLine.Location.Curve.Direction.Y, 0) \
                            and round(Curve.Direction.Z, 0) == round(ModelLine.Location.Curve.Direction.Z, 0) \
                            and round(Curve.Length, 0) == round(ModelLine.Location.Curve.Length, 0):
                                if HOST_APP.is_newer_than(2022):
                                    createload=LineLoad.Create(doc,Analytical_Floor.Id,i,force,moment,None)
                                else:
                                    createload=LineLoad.Create(doc,Analytical_Floor,i,force,moment,None)
                                setloadcase=createload.get_Parameter(BuiltInParameter.LOAD_CASE_ID).Set(ElementId(loadid))
                                LoadCheck = True
                                break
                            else:
                                i = i+1
                        except Exception as ex:
                            i = i+1
                    else:
                        try:
                            if round(Curve.Center.X, 0) == round(ModelLine.Location.Curve.Center.X, 0) \
                            and round(Curve.Center.Y, 0) == round(ModelLine.Location.Curve.Center.Y, 0) \
                            and round(Curve.Center.Z, 0) == round(ModelLine.Location.Curve.Center.Z, 0) \
                            and round(Curve.Radius, 0) == round(ModelLine.Location.Curve.Radius, 0) \
                            and round(Curve.Length, 0) == round(ModelLine.Location.Curve.Length, 0):
                                if HOST_APP.is_newer_than(2022):
                                    createload=LineLoad.Create(doc,Analytical_Floor.Id,i,force,moment,None)
                                else:
                                    createload=LineLoad.Create(doc,Analytical_Floor,i,force,moment,None)
                                setloadcase=createload.get_Parameter(BuiltInParameter.LOAD_CASE_ID).Set(ElementId(loadid))
                                LoadCheck = True
                                break
                            else:
                                i = i+1
                        except Exception as ex:
                            i = i+1
                if LoadCheck == True:
                    break

    def levels_loads_values(self, height_zones_values, sorted_levels):
        list_loads_values = []
        i = 1
        k = 0
        while i < len(height_zones_values):
            while height_zones_values[i] > round(sorted_levels[k].ProjectElevation*30.48, 0)-self.zero_elev and k+1 != len(sorted_levels):
                below_height = 0
                above_height = 0
                if k == 0:
                    below_height = (round(sorted_levels[k].ProjectElevation*30.48, 2)-self.zero_elev)/2
                else:
                    below_height = (round(sorted_levels[k].ProjectElevation*30.48, 2) - round(sorted_levels[k-1].ProjectElevation*30.48, 2))/2
                above_height = (round(sorted_levels[k+1].ProjectElevation*30.48, 2) - round(sorted_levels[k].ProjectElevation*30.48, 2))/2
                total_height = (below_height+above_height)/100
                line_load = self.calculate_load(height_zones_values[i], total_height)
                list_loads_values.append(line_load)
                k = k+1
            if k+1 != len(sorted_levels):
                below_height = 0
                above_height = 0
                if k == 0:
                    below_height = (round(sorted_levels[k].ProjectElevation*30.48, 2)-self.zero_elev)/2
                else:
                    below_height = (round(sorted_levels[k].ProjectElevation*30.48, 2) - round(sorted_levels[k-1].ProjectElevation*30.48, 2))/2
                below_line_load = self.calculate_load(height_zones_values[i], below_height/100)
                above_height = (round(sorted_levels[k+1].ProjectElevation*30.48, 2) - round(sorted_levels[k].ProjectElevation*30.48, 2))/2
                above_line_load = self.calculate_load(height_zones_values[i+1], above_height/100)
                list_loads_values.append(below_line_load+above_line_load)
                k = k+1
            else:
                below_height = 0
                above_height = 0
                below_height = (round(sorted_levels[k].ProjectElevation*30.48, 2) - round(sorted_levels[k-1].ProjectElevation*30.48, 2))/2
                above_height = round(float(self.height_value.Text), 2) - round(sorted_levels[k].ProjectElevation*30.48, 2)+self.zero_elev
                total_height = (below_height+above_height)/100
                line_load = self.calculate_load(height_zones_values[i], total_height)
                list_loads_values.append(line_load)
                break
            i = i+1
        return list_loads_values

    def calculate_load(self, z_value, height):
        Z0 = [0.003, 0.01, 0.05, 0.3, 1]
        Kr = [0.156, 0.17, 0.19, 0.215, 0.234]
        selected_index = self.terrain_type.SelectedIndex
        C0 = 1
        Kp = 3.5
        Kt = 1
        Vb = float(self.vb_value.Text)
        Cpe = float(self.cpe_value.Text)
        qb = (Vb*Vb)/1.6/10000
        IvZ = Kt/(C0*Math.Log((z_value/100)/Z0[selected_index]))
        CrZ = Kr[selected_index]*Math.Log((z_value/100)/Z0[selected_index])
        CeZ = CrZ*CrZ*C0*C0*(1+2*Kp*IvZ)
        We = qb*CeZ*Cpe
        We_line = We*height
        return We_line

    def loads_zones(self, sorted_levels):
        height_loads_zones = []
        height_loads_zones.append(0)
        if float(self.height_value.Text) <= float(self.width_value.Text):
            height_loads_zones.append(round(float(self.height_value.Text), 2))
        elif float(self.height_value.Text) > float(self.width_value.Text) and float(self.height_value.Text) <= 2*float(self.width_value.Text):
            i = 0
            while i < len(sorted_levels):
                level_hight = round(sorted_levels[i].ProjectElevation*30.48, 2)-self.zero_elev
                if level_hight > round(float(self.width_value.Text), 2):
                    level_hight = round(sorted_levels[i-1].ProjectElevation*30.48, 2)-self.zero_elev
                    height_loads_zones.append(level_hight)
                    if level_hight != round(float(self.height_value.Text), 2):
                        height_loads_zones.append(round(float(self.height_value.Text), 2))
                    break
                i = i+1
        else:
            i = 0
            while i < len(sorted_levels):
                level_hight_bot = round(sorted_levels[i].ProjectElevation*30.48, 2)-self.zero_elev
                if level_hight_bot > round(float(self.width_value.Text), 2):
                    level_hight_bot = round(sorted_levels[i-1].ProjectElevation*30.48, 2)-self.zero_elev
                    height_loads_zones.append(level_hight_bot)
                    break
                i = i+1
            k = len(sorted_levels)-1
            while k > 0:
                level_hight_top = float(self.height_value.Text) - round(sorted_levels[k].ProjectElevation*30.48, 2)
                if level_hight_top > round(float(self.width_value.Text), 2):
                    level_hight_top = round(sorted_levels[k+1].ProjectElevation*30.48, 2)
                    break
                k = k-1
            divide_count = (level_hight_top-level_hight_bot)/float(self.width_value.Text)
            height_divide = level_hight_bot
            while divide_count > 0:
                if divide_count >= 1:
                    height_divide = height_divide + float(self.width_value.Text)
                    while i < len(sorted_levels):
                        level_hight = round(sorted_levels[i].ProjectElevation*30.48, 2)-self.zero_elev
                        if level_hight > height_divide:
                            level_hight = round(sorted_levels[i-1].ProjectElevation*30.48, 2)-self.zero_elev
                            height_loads_zones.append(level_hight)
                            break
                        i = i+1
                divide_count = divide_count - 1  
            try:
                if round(level_hight, 0) != round(level_hight_top, 0):
                    height_loads_zones.append(level_hight_top)
            except Exception, ex:
                height_loads_zones.append(level_hight_top)
            height_loads_zones.append(round(float(self.height_value.Text), 2))
        return height_loads_zones

    def sort_levels(self):
        levels_list = []
        elevations_list = []
        for level in self.levels:
            levels_list.append(level)
            elevations_list.append(level.Elevation)
        zipped_lists = zip(elevations_list, levels_list)
        sorted_zipped_lists = sorted(zipped_lists)
        sorted_levels_list = [element for _, element in sorted_zipped_lists]
        clean_levels_list = []
        for level in sorted_levels_list:
            if round(level.ProjectElevation*30.48, 2)-self.zero_elev > 0 and round(level.ProjectElevation*30.48, 2)-self.zero_elev <= float(self.height_value.Text):
                clean_levels_list.append(level)
        return clean_levels_list

    def sort_floor_edges(self):
        model_lines_list = []
        lines_elevation_list = []
        for line in Model_Lines_Selection:
            model_lines_list.append(line)
            lines_elevation_list.append(line.Location.Curve.GetEndPoint(0).Z)
        zipped_lists = zip(lines_elevation_list, model_lines_list)
        sorted_zipped_lists = sorted(zipped_lists)
        sorted_lines_list = [element for _, element in sorted_zipped_lists]
        clean_lines_list = []
        for line in sorted_lines_list:
            if float(round(line.Location.Curve.GetEndPoint(0).Z*30.48, 2))-self.zero_elev > 0 and \
            float(round(line.Location.Curve.GetEndPoint(0).Z*30.48, 2))-self.zero_elev <= float(self.height_value.Text):
                clean_lines_list.append(line)
        return clean_lines_list

    def building_width(self):
        i = 0
        while round(Model_Lines_Selection[i].Location.Curve.GetEndPoint(0).Z*30.48, 2) != round(Model_Lines_Selection[i].Location.Curve.GetEndPoint(1).Z*30.48, 2) and i<len(Model_Lines_Selection):
            i = i+1
        Z_Bot_Line = Model_Lines_Selection[i].Location.Curve.GetEndPoint(0).Z*30.48
        i = i+1
        while i<len(Model_Lines_Selection):
            if round(Model_Lines_Selection[i].Location.Curve.GetEndPoint(0).Z*30.48, 2) < round(Z_Bot_Line, 2):
                Z_Bot_Line = Model_Lines_Selection[i].Location.Curve.GetEndPoint(0).Z*30.48
                i = i+1
            else:
                i = i+1
        bottom_width = 0
        for line in Model_Lines_Selection:
            if round(line.Location.Curve.GetEndPoint(0).Z*30.48, 2) == round(Z_Bot_Line, 2):
                bottom_width = bottom_width + line.Location.Curve.Length
        self.width_value.Text = str(round(bottom_width*30.48, 1))

    @property
    def selected_loadcase(self):
        return self.loadcases_cb.SelectedItem

# Setup the Load Cases for the ComboBox (list of Load Cases)
    def _setup_loadcases(self):
        loadnamelist = set()
        for load in loadcases:
            if not load.Name == "SW":
                loadnamelist.add(load.Name)
        loadcasenamelist = []
        loadcasenamelist.extend(sorted(list(loadnamelist)))
        self.loadcases_cb.ItemsSource = loadcasenamelist
        self.loadcases_cb.SelectedIndex = 0

    @property
    def selected_terrain_type(self):
        return self.terrain_type.SelectedItem

# Setup the Terrain Type for the ComboBox (list of Terrain Types)
    def _setup_terrain_type(self):
        sorted_terrain_type_list = ["0 - Open to Sea", "I - Rough, Without Obstacles", "II - Farmland", "III - Suburban or Industrial", "IV - Urban Areas"]
        self.terrain_type.ItemsSource = sorted_terrain_type_list
        self.terrain_type.SelectedIndex = 0

    @property
    def selected_wind_direction(self):
        return self.wind_direction.SelectedItem

# Setup the Wind Direction for the ComboBox (list of Wind Directions)
    def _setup_wind_direction(self):
        sorted_wind_direction_list = ["+X", "-X", "+Y", "-Y"]
        self.wind_direction.ItemsSource = sorted_wind_direction_list
        self.wind_direction.SelectedIndex = 0

#Create temporary model lines for the selection of the floor edges
def TempModelLines(Analytical_Floor_Collector):
    opts = Options()
    opts.ComputeReferences = True
    ListTempLines = []
    t = Transaction(doc, 'Temporary Model Lines')
    t.Start()
    for AnalyticalFloor in Analytical_Floor_Collector:
        FloorGeo = AnalyticalFloor.get_Geometry(opts)
        for obj in FloorGeo:
            if isinstance(obj,Solid):
                for Face in obj.Faces:
                    EdgeArray = Face.EdgeLoops
                    break
            break
        for Edge in EdgeArray:
            for FloorLine in Edge:
                StartPoint = FloorLine.AsCurve().GetEndPoint(0)
                EndPoint = FloorLine.AsCurve().GetEndPoint(1)
                p1 = XYZ(StartPoint.X, StartPoint.Y, StartPoint.Z)
                p2 = XYZ(EndPoint.X, EndPoint.Y, EndPoint.Z)
                pl = Plane.CreateByNormalAndOrigin(Face.FaceNormal, Face.Origin)
                if FloorLine.AsCurve().IsCyclic == False:
                    line = Line.CreateBound(p1, p2)
                else:
                    radius = FloorLine.AsCurve().Radius
                    center = FloorLine.AsCurve().Center
                    sagitta = radius - Math.Sqrt(Math.Pow(radius, 2.0) - Math.Pow((p2 - p1).GetLength() / 2.0, 2.0))
                    midPointOfChord = (p1 + p2) / 2.0
                    midPointOfArc = midPointOfChord + Transform.CreateRotation(XYZ.BasisZ, Math.PI / 2.0).OfVector((p2 - p1).Normalize().Multiply(sagitta))
                    if round((midPointOfArc - center).GetLength(), 5) == round(radius, 5):
                        line = Arc.Create(p1, p2, midPointOfArc)
                    else:
                        midPointOfArc = midPointOfArc - 2 * Transform.CreateRotation(XYZ.BasisZ, Math.PI / 2.0).OfVector((p2 - p1).Normalize().Multiply(sagitta))
                        line = Arc.Create(p1, p2, midPointOfArc)
                sk = SketchPlane.Create(doc, pl)
                mc = doc.Create.NewModelCurve(line, sk)
                ListTempLines.append(mc)
                ogs = DB.OverrideGraphicSettings()
                ogs.SetProjectionLineColor(DB.Color(0, 255, 0))
                ogs.SetProjectionLineWeight(10)
                revit.doc.ActiveView.SetElementOverrides(mc.Id, ogs)
    t.Commit()
    return ListTempLines

if (DavidEng.checkPermission()):
    if doc.ActiveView.LookupParameter("Type").AsValueString() == "3D View":
        if LevelsCollector.GetElementCount() != 0:
            if HOST_APP.is_newer_than(2022):
                analyticalFloorsCount = len(Analytical_Floor_Collector)
            else:
                analyticalFloorsCount = Analytical_Floor_Collector.GetElementCount()
            if analyticalFloorsCount != 0:
                tg = TransactionGroup(doc, 'Generate Wind Loads')
                tg.Start()
                ListTempModelLines = TempModelLines(Analytical_Floor_Collector) 
                customFilter = CustomSelectionFilter(DB.BuiltInCategory)
                Model_Lines_Selection = None
                try:
                    Model_Lines_Selection = [doc.GetElement(reference) for reference in uidoc.Selection.PickObjects(
                        ObjectType.Element, customFilter, "Select Floors Edges")]
                except Exception, ex:
                    Model_Lines_Selection = False

                if Model_Lines_Selection:
                    # Run the Xaml
                    ChooseLevelsWindow(SelectLevelsXamlFile).show(modal=True)
                elif Model_Lines_Selection != False:
                    ErrorWindow(errortext="You have not selected any floor edge.").ShowDialog()
                    t = Transaction(doc, 'Delete Temporary Model Lines')
                    t.Start()
                    for TempLine in ListTempModelLines:
                        doc.Delete(TempLine.Id)
                    t.Commit()
                    tg.RollBack()
            else:
                ErrorWindow(errortext="You don't have any visiable analytical floor in your active view.").ShowDialog()
        else:
            ErrorWindow(errortext="Please turn on the visibility of the levels on your 3D View").ShowDialog()
    else:
        ErrorWindow(errortext="You can only run this add-in on\n3D View").ShowDialog()
else:
    DavidEng.ErrorWindow(errortext="You don't have permission\nto use the tool").ShowDialog()