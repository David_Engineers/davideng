"""Creates hosted area loads on analytical panels. The parameter 'Category of Use' must be entered for each analytical panel before running this plugin.
Only analytical panels which are visible in the current view will be shown by the plugin."""
# pylint: disable=E0401,W0703,W0613
from pyrevit import coreutils, revit, forms, script
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, BuiltInParameter, XYZ, ElementId
from Autodesk.Revit.DB.Structure import AreaLoad

# dependencies
import clr
clr.AddReference('System.Windows.Forms')

# find the path of ErrorWindow.xaml
xamlfile = script.get_bundle_file('ErrorWindow.xaml')

# import WPF creator and base Window
import wpf
from System import Windows
import DavidEng

# App and Author name
__title__ = 'Create Hosted\nArea Loads'
__authors__ = 'Yuval Dolin'

# Identify the current Revit file
doc = __revit__.ActiveUIDocument.Document

# Creating collectior instance and collecting all the Load Cases from the model
loadcases = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_LoadCases)

# Creating collectior instance and collecting all the floors from the model
analytical_panel_collector = FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_AnalyticalPanel) \
                                               .WhereElementIsNotElementType()
		
class ReValueItem(object):
    def __init__(self, eid, catofuse, final=False, level=0):
        self.eid = eid
        self.floorlevel = level
        self.catofuse = catofuse
        self.loadvalue = ''
        self.final = final
        self.tooltip = ''

    def format_value(self, catofuse_format, load_format):
        try:
            if catofuse_format:
                # Write the user input on the 'Load' column
                self.loadvalue = coreutils.reformat_string(self.catofuse, catofuse_format, load_format)
                # Show tool tip for the user by pause the mouse over a row
                self.tooltip = 'Fz force: {}(t/m^2)'.format(self.loadvalue)
            elif not self.loadvalue:
                self.tooltip = 'No Load Specified'            
            else:
                self.tooltip = 'Fz force: {}(t/m^2)'.format(self.loadvalue)
        # The rest of the rows that don't get self.load_format_tb.Text stay with their own values
        except Exception:
            self

class ReValueWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name):
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)
        # Generator the table with the initial values and parameters
        self._target_elements = analytical_panel_collector
        self._reset_preview()
        for element in self._target_elements:
            catofuse_value = ''
            param = element.LookupParameter('Category of Use')
            if param:
                catofuse_value = param.AsString()
            levelofelement = element.get_Parameter(BuiltInParameter.HIGHEST_ASSOCIATED_LEVEL).AsValueString()
            row_values = ReValueItem(eid=element.Id, catofuse=catofuse_value, level=levelofelement)
            row_values.format_value(self.catofuse_format, self.load_format)
            self._revalue_items.append(row_values)
        self._refresh_preview()
        self._setup_loadcases()
        self.loadflag = True

    @property
    def selected_loadcase(self):
        return self.loadcases_cb.SelectedItem

    @property
    def catofuse_format(self):
        return self.orig_format_tb.Text

    @catofuse_format.setter
    def catofuse_format(self, value):
        self.orig_format_tb.Text = value

    @property
    def load_format(self):
        return self.load_format_tb.Text

    @property
    def preview_items(self):
        return self.preview_dg.ItemsSource

    @property
    def selected_preview_items(self):
        return self.preview_dg.SelectedItems

# Setup the Load Cases for the ComboBox (list of Load Cases)
    def _setup_loadcases(self):
        loadnamelist = set()
        for load in loadcases:
            if not load.Name == "SW":
                loadnamelist.add(load.Name)
        loadcasenamelist = []
        loadcasenamelist.extend(sorted(list(loadnamelist)))
        self.loadcases_cb.ItemsSource = loadcasenamelist
        self.loadcases_cb.SelectedIndex = 0

    def _reset_preview(self):
        self._revalue_items = []
        self.preview_dg.ItemsSource = self._revalue_items

    def _refresh_preview(self):
        self.preview_dg.Items.Refresh()

    def on_format_change(self, sender, args):
        # Don't run this when coming from the function 'on_selection_change'
        if not self.loadflag == True:
            for item in self._revalue_items:
                if not item.final:
                    item.format_value(self.catofuse_format,self.load_format)
        self.loadflag = False
        self._refresh_preview()

    def on_selection_change(self, sender, args):
        # Find the value of the load in the row to show for the user
        for item in self._revalue_items:
            if item.catofuse == self.preview_dg.SelectedItem.catofuse and item.loadvalue == self.preview_dg.SelectedItem.loadvalue:
                self.setloadvalue = item.loadvalue
                break
        # Don't run (flag) 'on_format_change' when changing here the 'load_format_tb' and 'catofuse_format'
        self.loadflag = True
        self.load_format_tb.Text = self.setloadvalue
        self.loadflag = True
        self.catofuse_format = self.preview_dg.SelectedItem.catofuse
        
# Mark selected rows as final and lock them
    def mark_as_final(self, sender, args):
        selected_names = [x.eid for x in self.selected_preview_items]
        for item in self._revalue_items:
            if item.eid in selected_names:
                item.final = True
        self._refresh_preview()

# Cancel mark selected rows as final and unlock them        
    def cancel_mark_as_final(self, sender, args):
        selected_names = [x.eid for x in self.selected_preview_items]
        for item in self._revalue_items:
            if item.eid in selected_names:
                item.final = False
        self._refresh_preview()

    def apply_new_values(self, sender, args):
		# Check if all the loads are valid (the value must to be float())  
        self.checkvalue = True
        try:
			for item in self._revalue_items:
				if item.loadvalue:
					strloadvalue=item.loadvalue
					floatloadvalue=float(item.loadvalue)
        except Exception:
            self.checkvalue = False
		# If all the loads are valid  		
        if self.checkvalue == True:
			# Close the Window Form (Xaml)
			self.Close()
			#Create Area Loads with the inserted values in one transaction (one Undo to erase the loads)
			try:
				with revit.Transaction('Place Hosted Area Loads', log_errors=False):
					for item in self._revalue_items:
						if item.loadvalue and item.loadvalue!='0' and item.loadvalue!='-0':
							param = self.selected_loadcase
							for load in loadcases:
								if load.Name == param:
									loadid=load.Id.IntegerValue
							if param:
								fz=float(item.loadvalue)
								force=XYZ(0,0,fz*2989.06692)
								createload=AreaLoad.Create(doc,item.eid,force,None)
								createload.get_Parameter(BuiltInParameter.LOAD_CASE_ID).Set(ElementId(loadid))
			except Exception as ex:
				forms.alert(str(ex), title='Error')
		# If there is invalid Load show Error window
        else:
            # Error if the Load value is invalid (the value must to be float())  
			ErrorWindow(errortext="You entered an invalid value: '{}'".format(strloadvalue)).ShowDialog()

# Show Error Window Form
class ErrorWindow(Windows.Window):
    def __init__(self, errortext):
        self.errortext=errortext
        wpf.LoadComponent(self, xamlfile)
        self.error.Text = "{}".format(self.errortext)

    def ok(self, sender, args):
        self.Close()

if (DavidEng.checkPermission()):
    # Run the Xaml
    for analytical_panel in analytical_panel_collector:
        if analytical_panel.LookupParameter('Category of Use'):
            ReValueWindow('CreateHostedAreaLoads.xaml').show(modal=True)
            break
        else:
            # Error if parameter 'Category of Use' does not exist on floors  
            ErrorWindow(errortext="Parameter 'Category of Use' does not exist in Analytical Panels").ShowDialog()
            break
else:
    DavidEng.ErrorWindow(errortext="You don't have permission\nto use the tool").ShowDialog()