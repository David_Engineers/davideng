"""Align Analytical Nodes by selecting the target analytical node and the source analytical nodes."""
# pylint: disable=E0401,W0703,W0613
from pyrevit import revit, DB
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, BuiltInParameter, Transaction, XYZ, TransactionGroup, \
                              View, Outline, BoundingBoxIntersectsFilter, Line
from Autodesk.Revit.DB.Structure import AnalyticalElementSelector
from Autodesk.Revit.UI.Selection import ObjectType, ISelectionFilter
import DavidEng
from pyrevit import HOST_APP

from System.Collections.Generic import List

# dependencies
import clr
clr.AddReference('System.Windows.Forms')

# import WPF creator and base Window
import wpf
from System import Windows

# App and Author name
__title__ = 'Align Analytical\nNodes'
__authors__ = 'Yuval Dolin'

# Identify the current Revit file
doc = __revit__.ActiveUIDocument.Document
uidoc = __revit__.ActiveUIDocument

if HOST_APP.is_newer_than(2022):
    # Creating collectior instance and collecting all the analytical members from the model
    Analytical_Lines_Collector = FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_AnalyticalMember).WhereElementIsNotElementType()
else:
    CatFilter = DB.ElementMulticategoryFilter(List[DB.BuiltInCategory]([DB.BuiltInCategory.OST_BeamAnalytical, DB.BuiltInCategory.OST_ColumnAnalytical]))
    # Creating collectior instance and collecting all the analytical beams and columns from the model
    Analytical_Lines_Collector = FilteredElementCollector(doc, doc.ActiveView.Id).WherePasses(CatFilter).WhereElementIsNotElementType()

# Define class to filter elements of category to select 
class CustomSelectionFilter(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, DB.BuiltInCategory.OST_AnalyticalNodes).Id and e.Id != sourceNodeId:
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

def BoundingBox(selectedNode):
    BoxMax = selectedNode.BoundingBox[doc.ActiveView].Max
    BoxMin = selectedNode.BoundingBox[doc.ActiveView].Min
    NewBoxMax = XYZ(BoxMax.X + 0.1, BoxMax.Y + 0.1, BoxMax.Z + 0.1)
    NewBoxMin = XYZ(BoxMin.X - 0.1, BoxMin.Y - 0.1, BoxMin.Z - 0.1)
    outline = Outline(NewBoxMin, NewBoxMax)
    bbfilter = BoundingBoxIntersectsFilter(outline)
    if HOST_APP.is_newer_than(2022):
        # Creating collectior instance and collecting all the analytical members from the node bounding box
        bb_collector = FilteredElementCollector(doc, doc.ActiveView.Id).WhereElementIsNotElementType() \
                                                    .WherePasses(bbfilter).OfCategory(BuiltInCategory.OST_AnalyticalMember)
    else:
        CatFilter = DB.ElementMulticategoryFilter(List[DB.BuiltInCategory]([DB.BuiltInCategory.OST_BeamAnalytical, DB.BuiltInCategory.OST_ColumnAnalytical]))
        # Creating collectior instance and collecting all the analytical beams and columns from the node bounding box
        bb_collector = FilteredElementCollector(doc, doc.ActiveView.Id).WhereElementIsNotElementType() \
                                                    .WherePasses(bbfilter).WherePasses(CatFilter)
    return bb_collector

def setOffset(sideLocation, analyticalElement, elemLocation, startOrEnd):
    if sideLocation == "start":
        getOffset = analyticalElement.GetOffset(AnalyticalElementSelector.StartOrBase)
    elif sideLocation == "end":
        getOffset = analyticalElement.GetOffset(AnalyticalElementSelector.EndOrTop)
    pointX = sourceNodeCenter.X - elemLocation.X + getOffset.X
    pointY = sourceNodeCenter.Y - elemLocation.Y + getOffset.Y
    pointZ = sourceNodeCenter.Z - elemLocation.Z + getOffset.Z
    offsetXYZ = XYZ(pointX, pointY, pointZ)
    if sideLocation == "start":
        analyticalElement.SetOffset(AnalyticalElementSelector.StartOrBase, offsetXYZ)
    elif sideLocation == "end":
        analyticalElement.SetOffset(AnalyticalElementSelector.EndOrTop, offsetXYZ)

def AdjustNodes():
    t = Transaction(doc, 'Adjust Analytical Nodes')
    t.Start()
    for selectedNode in Source_Node_Selection:
        nodePosition = selectedNode.Position
        bb_collector = BoundingBox(selectedNode)
        for analyticalElement in bb_collector:
            StartEndFlag = None
            elemStart = analyticalElement.GetCurve().GetEndPoint(0)
            elemEnd = analyticalElement.GetCurve().GetEndPoint(1)
            if round(elemStart.X, 10) == round(nodePosition.X, 10) and round(elemStart.Y, 10) == round(nodePosition.Y, 10) and round(elemStart.Z, 10) == round(nodePosition.Z, 10):
                StartEndFlag = "start"
                if HOST_APP.is_newer_than(2022):
                    newAnalyticalLine = Line.CreateBound(sourceNodeCenter, elemEnd)
                    analyticalElement.SetCurve(newAnalyticalLine)
                else:
                    setOffset(StartEndFlag, analyticalElement, elemStart, 0)
            else:
                if round(elemEnd.X, 10) == round(nodePosition.X, 10) and round(elemEnd.Y, 10) == round(nodePosition.Y, 10) and round(elemEnd.Z, 10) == round(nodePosition.Z, 10):
                    StartEndFlag = "end"
                    if HOST_APP.is_newer_than(2022):
                        newAnalyticalLine = Line.CreateBound(elemStart, sourceNodeCenter)
                        analyticalElement.SetCurve(newAnalyticalLine)
                    else:
                        setOffset(StartEndFlag, analyticalElement, elemEnd, 1)

            doc.Regenerate()

            elemStart = analyticalElement.GetCurve().GetEndPoint(0)
            elemEnd = analyticalElement.GetCurve().GetEndPoint(1)
            if StartEndFlag == "start":
                if HOST_APP.is_newer_than(2022):
                    newAnalyticalLine = Line.CreateBound(sourceNodeCenter, elemEnd)
                    analyticalElement.SetCurve(newAnalyticalLine)
                else:
                    setOffset(StartEndFlag, analyticalElement, elemStart, 0)
            elif StartEndFlag == "end":
                if HOST_APP.is_newer_than(2022):
                    newAnalyticalLine = Line.CreateBound(elemStart, sourceNodeCenter)
                    analyticalElement.SetCurve(newAnalyticalLine)
                else:
                    setOffset(StartEndFlag, analyticalElement, elemEnd, 1)

    t.Commit()

if (DavidEng.checkPermission()):
    if Analytical_Lines_Collector.GetElementCount() != 0:
        tg = TransactionGroup(doc, 'Adjust Analytical Nodes')
        tg.Start()

        t = Transaction(doc, 'Temporary Analytical Nodes')
        t.Start()

        ActiveViewTemplateId = doc.ActiveView.ViewTemplateId
        if not ActiveViewTemplateId.IntegerValue < 0:
            viewTemplateParam = doc.ActiveView.get_Parameter(BuiltInParameter.VIEW_TEMPLATE)
            viewTemplateParam.Set(DB.ElementId.InvalidElementId)
        
        analyticalNodesCategory = DB.Category.GetCategory(doc, DB.BuiltInCategory.OST_AnalyticalNodes)
        checkCatVisibility = View.GetCategoryHidden(doc.ActiveView, analyticalNodesCategory.Id)
        
        if checkCatVisibility == True:
            View.SetCategoryHidden(doc.ActiveView, analyticalNodesCategory.Id, False)

        ogs = DB.OverrideGraphicSettings()
        ogs.SetProjectionLineColor(DB.Color(0, 255, 0))

        # Creating collectior instance and collecting all the analytical nodes from the model
        analyticalNodesCollector = FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_AnalyticalNodes) \
                                                    .WhereElementIsNotElementType()

        for node in analyticalNodesCollector:                                            
            revit.doc.ActiveView.SetElementOverrides(node.Id, ogs)

        t.Commit()

        customFilter = CustomSelectionFilter(DB.BuiltInCategory)
        Target_Node_Selection = None
        sourceNodeId = 0
        try:
            Target_Node_Selection = uidoc.Selection.PickObject(ObjectType.Element, customFilter, "Please pick the target Analytical Node")
            sourceNode = doc.GetElement(Target_Node_Selection)
            sourceNodeCenter = sourceNode.Position
            sourceNodeId = sourceNode.Id
        except Exception, ex:
            pass

        Source_Node_Selection = None
        if Target_Node_Selection:
            try:
                Source_Node_Selection = [doc.GetElement(reference) for reference in uidoc.Selection.PickObjects(
                    ObjectType.Element, customFilter, "Please pick the source Analytical Nodes")]
            except Exception, ex:
                pass
            
        if Target_Node_Selection and Source_Node_Selection:
            AdjustNodes()
            
            t = Transaction(doc, 'Cancel Temporary Analytical Nodes')
            t.Start()
            newOgs = DB.OverrideGraphicSettings()
            for node in analyticalNodesCollector:                                            
                doc.ActiveView.SetElementOverrides(node.Id, newOgs)
            if checkCatVisibility == True:
                View.SetCategoryHidden(doc.ActiveView, analyticalNodesCategory.Id, True)
            if not ActiveViewTemplateId.IntegerValue < 0:
                viewTemplateParam.Set(ActiveViewTemplateId)
            t.Commit()
            tg.Assimilate() 
        else:
            tg.RollBack()

    else:
        DavidEng.ErrorWindow(errortext="You don't have any visiable analytical beam or column in your active view.").ShowDialog()
else:
    DavidEng.ErrorWindow(errortext="You don't have permission\nto use the tool").ShowDialog()