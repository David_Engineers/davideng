"""Align the selected Analytical Floors to the nearest selected levels."""
# pylint: disable=E0401,W0703,W0613
from pyrevit import coreutils, UI, revit, DB, forms, script
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, BuiltInParameter, Transaction, XYZ, ElementId, Level, View3D, ViewSection, CurveLoop, Line, Curve
from Autodesk.Revit.UI.Selection import ObjectType, ISelectionFilter
from Autodesk.Revit import Exceptions
import DavidEng
from pyrevit import HOST_APP

from System.Collections.Generic import List

# dependencies
import clr
clr.AddReference('System.Windows.Forms')

# find the path of SelectLevels.xaml
SelectLevelsXamlFile = script.get_bundle_file('SelectLevels.xaml')

# import WPF creator and base Window
import wpf
from System import Windows

# App and Author name
__title__ = 'Analytical Floors'
__authors__ = 'Yuval Dolin'

# Identify the current Revit file
doc = __revit__.ActiveUIDocument.Document
uidoc = __revit__.ActiveUIDocument

# if the user selected elements before runing the add-in
selection = [doc.GetElement(id) for id in uidoc.Selection.GetElementIds()]

if HOST_APP.is_newer_than(2022):
    Analytical_Floors_Collector = []
    # Creating collectior instance and collecting all the Analytical Panels from the model
    analyticalPanelsCollector = FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_AnalyticalPanel)
    for analyticalPanel in analyticalPanelsCollector:
        if analyticalPanel.StructuralRole.ToString() == 'StructuralRoleFloor':
            Analytical_Floors_Collector.append(analyticalPanel)
else:
    # Creating collectior instance and collecting all the Analytical Floors from the model
    Analytical_Floors_Collector = FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_FloorAnalytical)

# Define class to filter elements of category to select 
class CustomSelectionFilterNewAPI(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, self.category).Id and e.StructuralRole.ToString() == 'StructuralRoleFloor':
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

# Define class to filter elements of category to select 
class CustomSelectionFilterOldAPI(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, self.category).Id:
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

# Define class to filter elements of category to select 
class CustomSelectionFilterLevel(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, DB.BuiltInCategory.OST_Levels).Id:
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

class ChooseLevelsWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name):
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)

    # user select all levels
    def all_levels(self, sender, args):
        self.Close()
        levels_collector_all = FilteredElementCollector(doc).OfClass(Level).WhereElementIsNotElementType().ToElements()
        AlignAnalyticalFloors(levels_collector_all)

    # user select specific levels
    def select_levels(self, sender, args):
        self.Close()
        try:
            selected_levels = forms.select_levels()
        except Exceptions.OperationCanceledException:
            import sys
            sys.exit()
        if selected_levels == []:
			DavidEng.ErrorWindow(errortext="You have not selected any Level").ShowDialog()
			ChooseLevelsWindow(SelectLevelsXamlFile).show(modal=True)
        else:
			AlignAnalyticalFloors(selected_levels)

def AlignAnalyticalFloors(levels_collector_all):
    t = Transaction(doc, "Align Analytical Floors to Nearest Levels")
    t.Start()
    groupFlag = False
    for AnalyticalFloor in AnalyticalFloors_selection:
        if AnalyticalFloor.GroupId.IntegerValue < 0:
            if HOST_APP.is_newer_than(2022):
                newCurveList = List[Curve]()
                panelContour = AnalyticalFloor.GetOuterContour()
                # flag that verify that all curves are lines
                linesValidationFlag = True
                # flag that verify that the panel is flat and not sloped
                flatFlag = True
                for contourLine in panelContour:
                    if type(contourLine) != Line:
                        linesValidationFlag = False
                    lineStartPoint = contourLine.GetEndPoint(0)
                    startProjectionLevel = ProjectionLevel(lineStartPoint, levels_collector_all)
                    startProjectionElevation = startProjectionLevel.ProjectElevation
                    newStartAnalyticalBeam = XYZ(lineStartPoint.X, lineStartPoint.Y, startProjectionElevation)
                    lineEndPoint = contourLine.GetEndPoint(1)
                    endProjectionLevel = ProjectionLevel(lineEndPoint, levels_collector_all)
                    endProjectionElevation = endProjectionLevel.ProjectElevation
                    newEndAnalyticalBeam = XYZ(lineEndPoint.X, lineEndPoint.Y, endProjectionElevation)
                    newAnalyticalLine = Line.CreateBound(newStartAnalyticalBeam, newEndAnalyticalBeam)
                    newCurveList.Add(newAnalyticalLine)
                    if round(lineStartPoint.Z, 5) != round(lineEndPoint.Z, 5):
                        flatFlag = False
                    
                if linesValidationFlag == True:
                    newCurveLoop = CurveLoop.Create(newCurveList)
                    AnalyticalFloor.SetOuterContour(newCurveLoop)
                # if the CurveLoop not including only lines but the CurveLoop is flat
                elif flatFlag == True:
                    offsetDis = startProjectionElevation - lineStartPoint.Z
                    newCurveLoop = CurveLoop.CreateViaTransform(panelContour, DB.Transform.CreateTranslation(XYZ(0,0,offsetDis)))
                    AnalyticalFloor.SetOuterContour(newCurveLoop)
            else:
                startPoint = AnalyticalFloor.get_BoundingBox(doc.ActiveView).Min
                endPoint = AnalyticalFloor.get_BoundingBox(doc.ActiveView).Max
                startProjectionLevel = ProjectionLevel(startPoint, levels_collector_all)
                endProjectionLevel = ProjectionLevel(endPoint, levels_collector_all)

                if startProjectionLevel.ProjectElevation == endProjectionLevel.ProjectElevation:
                    AnalyticalFloor.get_Parameter(BuiltInParameter.ANALYTICAL_MODEL_FLOOR_ALIGNMENT_METHOD).Set(1)
                    AnalyticalFloor.get_Parameter(BuiltInParameter.ANALYTICAL_MODEL_FLOOR_PROJECTION).Set(startProjectionLevel.Id)
        else:
            groupFlag = True
    t.Commit()
    if groupFlag == True:
        DavidEng.ErrorWindow(errortext="We could not run the plugin on Analytical Floors that are in groups. Please set them manually.").ShowDialog()

def ProjectionLevel(point, levels_collector_all):
    projectionLevel = None
    belowElevation = None
    aboveElevation = None
    for level in levels_collector_all:
        if level.ProjectElevation == point.Z:
            projectionLevel = level
            break
        elif level.ProjectElevation < point.Z:
            if belowElevation == None:
                belowElevation = level
            elif level.ProjectElevation > belowElevation.ProjectElevation:
                belowElevation = level
        else:
            if aboveElevation == None:
                aboveElevation = level
            elif level.ProjectElevation < aboveElevation.ProjectElevation:
                aboveElevation = level
    if projectionLevel == None and belowElevation != None and aboveElevation != None:
        belowDifference = point.Z - belowElevation.ProjectElevation
        aboveDifference = aboveElevation.ProjectElevation - point.Z
        if belowDifference < aboveDifference:
            projectionLevel = belowElevation
        else:
            projectionLevel = aboveElevation
    elif projectionLevel == None and belowElevation == None and aboveElevation != None:
        projectionLevel = aboveElevation
    elif projectionLevel == None and belowElevation != None and aboveElevation == None:
        projectionLevel = belowElevation
    return projectionLevel

if type(doc.ActiveView) == View3D or type(doc.ActiveView) == ViewSection:
    if HOST_APP.is_newer_than(2022):
        analyticalFloorsCount = len(Analytical_Floors_Collector)
    else:
        analyticalFloorsCount = Analytical_Floors_Collector.GetElementCount()
    if analyticalFloorsCount != 0:
        if not selection:
            if HOST_APP.is_newer_than(2022):
                customFilter = CustomSelectionFilterNewAPI(DB.BuiltInCategory.OST_AnalyticalPanel)
            else:
                customFilter = CustomSelectionFilterOldAPI(DB.BuiltInCategory.OST_FloorAnalytical)
            try:
                AnalyticalFloors_selection = [doc.GetElement(reference) for reference in uidoc.Selection.PickObjects(
                    ObjectType.Element, customFilter, "Select Analytical Floors")]
            except Exceptions.OperationCanceledException:
                import sys
                sys.exit()
            if AnalyticalFloors_selection == []:
                DavidEng.ErrorWindow(errortext="You have not selected any Analytical Floor").ShowDialog()
        else:
            AnalyticalFloors_selection = []
            if HOST_APP.is_newer_than(2022):
                for element in selection:
                    if element.Category.Name == 'Analytical Panels' and element.StructuralRole.ToString() == 'StructuralRoleFloor':
                        AnalyticalFloors_selection.append(element)
            else:
                for element in selection:
                    if element.Category.Name == 'Analytical Floors':
                        AnalyticalFloors_selection.append(element)
            if AnalyticalFloors_selection == []:
                DavidEng.ErrorWindow(errortext="You have not selected any Analytical Floor").ShowDialog()

        if not AnalyticalFloors_selection == []:
            ChooseLevelsWindow(SelectLevelsXamlFile).show(modal=True)
    else:
        DavidEng.ErrorWindow(errortext="You don't have any visiable Analytical Floor in your active view").ShowDialog()
else:
    DavidEng.ErrorWindow(errortext="You can only run this add-in on\n3D Views or Section Views").ShowDialog()