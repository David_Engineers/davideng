"""Align the selected Analytical Beams to the nearest selected levels."""
# pylint: disable=E0401,W0703,W0613
from pyrevit import coreutils, UI, revit, DB, forms, script
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, BuiltInParameter, Transaction, XYZ, ElementId, Level, View3D, ViewSection, Line, Arc
from Autodesk.Revit.UI.Selection import ObjectType, ISelectionFilter
from Autodesk.Revit import Exceptions
import DavidEng
from pyrevit import HOST_APP

# dependencies
import clr
clr.AddReference('System.Windows.Forms')

# find the path of SelectLevels.xaml
SelectLevelsXamlFile = script.get_bundle_file('SelectLevels.xaml')

# import WPF creator and base Window
import wpf
from System import Windows

# App and Author name
__title__ = 'Analytical Beams'
__authors__ = 'Yuval Dolin'

# Identify the current Revit file
doc = __revit__.ActiveUIDocument.Document
uidoc = __revit__.ActiveUIDocument

# if the user selected elements before runing the add-in
selection = [doc.GetElement(id) for id in uidoc.Selection.GetElementIds()]

if HOST_APP.is_newer_than(2022):
    Analytical_Beams_Collector = []
    # Creating collectior instance and collecting all the Analytical Members from the model
    analyticalMembersCollector = FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_AnalyticalMember)
    for analyticalMember in analyticalMembersCollector:
        if analyticalMember.StructuralRole.ToString() != 'StructuralRoleColumn':
            Analytical_Beams_Collector.append(analyticalMember)
else:
    # Creating collectior instance and collecting all the Analytical Beams from the model
    Analytical_Beams_Collector = FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_BeamAnalytical)

# Define class to filter elements of category to select 
class CustomSelectionFilterNewAPI(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, self.category).Id and e.StructuralRole.ToString() != 'StructuralRoleColumn':
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

# Define class to filter elements of category to select 
class CustomSelectionFilterOldAPI(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, self.category).Id:
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

# Define class to filter elements of category to select 
class CustomSelectionFilterLevel(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, DB.BuiltInCategory.OST_Levels).Id:
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

class ChooseLevelsWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name):
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)

    # user select all levels
    def all_levels(self, sender, args):
        self.Close()
        levels_collector_all = FilteredElementCollector(doc).OfClass(Level).WhereElementIsNotElementType().ToElements()
        AlignAnalyticalBeams(levels_collector_all)

    # user select specific levels
    def select_levels(self, sender, args):
        self.Close()
        try:
            selected_levels = forms.select_levels()
        except Exceptions.OperationCanceledException:
            import sys
            sys.exit()
        if selected_levels == []:
			DavidEng.ErrorWindow(errortext="You have not selected any Level").ShowDialog()
			ChooseLevelsWindow(SelectLevelsXamlFile).show(modal=True)
        else:
			AlignAnalyticalBeams(selected_levels)

def AlignAnalyticalBeams(levels_collector_all):
    t = Transaction(doc, "Align Analytical Beams to Nearest Levels")
    t.Start()
    groupFlag = False
    for AnalyticalBeam in AnalyticalBeams_selection:
        if AnalyticalBeam.GroupId.IntegerValue < 0:
            startPoint = AnalyticalBeam.get_BoundingBox(doc.ActiveView).Min
            endPoint = AnalyticalBeam.get_BoundingBox(doc.ActiveView).Max
            startProjectionLevel = ProjectionLevel(startPoint, levels_collector_all)
            endProjectionLevel = ProjectionLevel(endPoint, levels_collector_all)

            if startProjectionLevel.ProjectElevation == endProjectionLevel.ProjectElevation:
                if HOST_APP.is_newer_than(2022):
                    startProjectionElevation = startProjectionLevel.ProjectElevation
                    endProjectionElevation = endProjectionLevel.ProjectElevation
                    startAnalyticalBeam = AnalyticalBeam.GetCurve().GetEndPoint(0)
                    endAnalyticalBeam = AnalyticalBeam.GetCurve().GetEndPoint(1)
                    newStartAnalyticalBeam = XYZ(startAnalyticalBeam.X, startAnalyticalBeam.Y, startProjectionElevation)
                    newEndAnalyticalBeam = XYZ(endAnalyticalBeam.X, endAnalyticalBeam.Y, endProjectionElevation)
                    if AnalyticalBeam.GetCurve().GetType() == Line:
                        newAnalyticalLine = Line.CreateBound(newStartAnalyticalBeam, newEndAnalyticalBeam)
                        AnalyticalBeam.SetCurve(newAnalyticalLine)
                    elif AnalyticalBeam.GetCurve().GetType() == Arc:
                        midAnalyticalBeam = AnalyticalBeam.GetCurve().Evaluate(0.5, True)
                        newMidAnalyticalBeam = XYZ(midAnalyticalBeam.X, midAnalyticalBeam.Y, startProjectionElevation)
                        newAnalyticalLine = Arc.Create(newStartAnalyticalBeam, newEndAnalyticalBeam, newMidAnalyticalBeam)
                        AnalyticalBeam.SetCurve(newAnalyticalLine)
                else:
                    AnalyticalBeam.get_Parameter(BuiltInParameter.ANALYTICAL_MODEL_START_ALIGNMENT_METHOD).Set(1)
                    AnalyticalBeam.get_Parameter(BuiltInParameter.ANALYTICAL_MODEL_START_Z_PROJECTION).Set(startProjectionLevel.Id)
                    AnalyticalBeam.get_Parameter(BuiltInParameter.ANALYTICAL_MODEL_END_ALIGNMENT_METHOD).Set(1)
                    AnalyticalBeam.get_Parameter(BuiltInParameter.ANALYTICAL_MODEL_END_Z_PROJECTION).Set(endProjectionLevel.Id)
        else:
            groupFlag = True
    t.Commit()
    if groupFlag == True:
        DavidEng.ErrorWindow(errortext="We could not run the plugin on Analytical Beams that are in groups. Please set them manually.").ShowDialog()

def ProjectionLevel(point, levels_collector_all):
    projectionLevel = None
    belowElevation = None
    aboveElevation = None
    for level in levels_collector_all:
        if level.ProjectElevation == point.Z:
            projectionLevel = level
            break
        elif level.ProjectElevation < point.Z:
            if belowElevation == None:
                belowElevation = level
            elif level.ProjectElevation > belowElevation.ProjectElevation:
                belowElevation = level
        else:
            if aboveElevation == None:
                aboveElevation = level
            elif level.ProjectElevation < aboveElevation.ProjectElevation:
                aboveElevation = level
    if projectionLevel == None and belowElevation != None and aboveElevation != None:
        belowDifference = point.Z - belowElevation.ProjectElevation
        aboveDifference = aboveElevation.ProjectElevation - point.Z
        if belowDifference < aboveDifference:
            projectionLevel = belowElevation
        else:
            projectionLevel = aboveElevation
    elif projectionLevel == None and belowElevation == None and aboveElevation != None:
        projectionLevel = aboveElevation
    elif projectionLevel == None and belowElevation != None and aboveElevation == None:
        projectionLevel = belowElevation
    return projectionLevel

if type(doc.ActiveView) == View3D or type(doc.ActiveView) == ViewSection:
    if HOST_APP.is_newer_than(2022):
        analyticalBeamsCount = len(Analytical_Beams_Collector)
    else:
        analyticalBeamsCount = Analytical_Beams_Collector.GetElementCount()
    if analyticalBeamsCount != 0:
        if not selection:
            if HOST_APP.is_newer_than(2022):
                customFilter = CustomSelectionFilterNewAPI(DB.BuiltInCategory.OST_AnalyticalMember)
            else:
                customFilter = CustomSelectionFilterOldAPI(DB.BuiltInCategory.OST_BeamAnalytical)
            try:
                AnalyticalBeams_selection = [doc.GetElement(reference) for reference in uidoc.Selection.PickObjects(
                    ObjectType.Element, customFilter, "Select Analytical Beams")]
            except Exceptions.OperationCanceledException:
                import sys
                sys.exit()
            if AnalyticalBeams_selection == []:
                DavidEng.ErrorWindow(errortext="You have not selected any Analytical Beam").ShowDialog()
        else:
            AnalyticalBeams_selection = []
            if HOST_APP.is_newer_than(2022):
                for element in selection:
                    if element.Category.Name == 'Analytical Members' and element.StructuralRole.ToString() != 'StructuralRoleColumn':
                        AnalyticalBeams_selection.append(element)
            else:
                for element in selection:
                    if element.Category.Name == 'Analytical Beams':
                        AnalyticalBeams_selection.append(element)
            if AnalyticalBeams_selection == []:
                DavidEng.ErrorWindow(errortext="You have not selected any Analytical Beam").ShowDialog()

        if not AnalyticalBeams_selection == []:
            ChooseLevelsWindow(SelectLevelsXamlFile).show(modal=True)
    else:
        DavidEng.ErrorWindow(errortext="You don't have any visiable Analytical Beam in your active view").ShowDialog()
else:
    DavidEng.ErrorWindow(errortext="You can only run this add-in on\n3D Views or Section Views").ShowDialog()