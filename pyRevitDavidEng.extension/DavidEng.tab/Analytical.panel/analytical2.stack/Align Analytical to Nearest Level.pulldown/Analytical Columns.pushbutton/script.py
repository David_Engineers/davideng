"""Align the selected Analytical Columns to the nearest selected levels."""
# pylint: disable=E0401,W0703,W0613
from pyrevit import coreutils, UI, revit, DB, forms, script
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, BuiltInParameter, Transaction, XYZ, ElementId, Level, View3D, ViewSection, Line
from Autodesk.Revit.UI.Selection import ObjectType, ISelectionFilter
from Autodesk.Revit import Exceptions
import DavidEng
from pyrevit import HOST_APP

# dependencies
import clr
clr.AddReference('System.Windows.Forms')

# find the path of SelectLevels.xaml
SelectLevelsXamlFile = script.get_bundle_file('SelectLevels.xaml')

# find the path of SelectPointsToAlign.xaml
SelectColumnPointsXamlFile = script.get_bundle_file('SelectPointsToAlign.xaml')

# import WPF creator and base Window
import wpf
from System import Windows

# App and Author name
__title__ = 'Analytical Columns'
__authors__ = 'Yuval Dolin'

# Identify the current Revit file
doc = __revit__.ActiveUIDocument.Document
uidoc = __revit__.ActiveUIDocument

# if the user selected elements before runing the add-in
selection = [doc.GetElement(id) for id in uidoc.Selection.GetElementIds()]

if HOST_APP.is_newer_than(2022):
    Analytical_Columns_Collector = []
    # Creating collectior instance and collecting all the Analytical Members from the model
    analyticalMembersCollector = FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_AnalyticalMember)
    for analyticalMember in analyticalMembersCollector:
        if analyticalMember.StructuralRole.ToString() != 'StructuralRoleBeam':
            Analytical_Columns_Collector.append(analyticalMember)
else:
    # Creating collectior instance and collecting all the Analytical Columns from the model
    Analytical_Columns_Collector = FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_ColumnAnalytical)

# Define class to filter elements of category to select 
class CustomSelectionFilterNewAPI(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, self.category).Id and e.StructuralRole.ToString() != 'StructuralRoleBeam':
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

# Define class to filter elements of category to select 
class CustomSelectionFilterOldAPI(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, self.category).Id:
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

# Define class to filter elements of category to select 
class CustomSelectionFilterLevel(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, DB.BuiltInCategory.OST_Levels).Id:
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

class ChooseLevelsWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name):
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)

    # user select all levels
    def all_levels(self, sender, args):
        self.Close()
        levels_collector_all = FilteredElementCollector(doc).OfClass(Level).WhereElementIsNotElementType().ToElements()
        ChooseColumnPoints(SelectColumnPointsXamlFile, levels_collector_all).show(modal=True)

    # user select specific levels
    def select_levels(self, sender, args):
        self.Close()
        try:
            selected_levels = forms.select_levels()
        except Exceptions.OperationCanceledException:
            import sys
            sys.exit()
        if selected_levels == []:
			DavidEng.ErrorWindow(errortext="You have not selected any Level").ShowDialog()
			ChooseLevelsWindow(SelectLevelsXamlFile).show(modal=True)
        else:
            ChooseColumnPoints(SelectColumnPointsXamlFile, selected_levels).show(modal=True)

class ChooseColumnPoints(forms.WPFWindow):
    def __init__(self, xaml_file_name, selected_levels):
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)
        self.selected_levels = selected_levels

    def top_point(self, sender, args):
        self.Close()
        AlignAnalyticalColumns(self.selected_levels, True, False)

    def bottom_point(self, sender, args):
        self.Close()
        AlignAnalyticalColumns(self.selected_levels, False, True)

    def both_points(self, sender, args):
        self.Close()
        AlignAnalyticalColumns(self.selected_levels, True, True)

def calcNewPoint(startPoint, endPoint, zValue):
    x = (endPoint[0] - startPoint[0]) / (endPoint[2] - startPoint[2]) * (zValue - startPoint[2]) + startPoint[0]
    y = (endPoint[1] - startPoint[1]) / (endPoint[2] - startPoint[2]) * (zValue - startPoint[2]) + startPoint[1]
    return XYZ(x, y, zValue)

def AlignAnalyticalColumns(levels_collector_all, align_top, align_bottom):
    t = Transaction(doc, "Align Analytical Columns to Nearest Levels")
    t.Start()
    groupFlag = False
    for AnalyticalColumn in AnalyticalColumns_selection:
        if AnalyticalColumn.GroupId.IntegerValue < 0:
            basePoint = AnalyticalColumn.GetCurve().GetEndPoint(0)
            topPoint = AnalyticalColumn.GetCurve().GetEndPoint(1)
            baseProjectionLevel = ProjectionLevel(basePoint, levels_collector_all)
            topProjectionLevel = ProjectionLevel(topPoint, levels_collector_all)

            if align_top and align_bottom and baseProjectionLevel.ProjectElevation == topProjectionLevel.ProjectElevation:
                continue
            else:
                if HOST_APP.is_newer_than(2022):
                    baseProjectionElevation = baseProjectionLevel.ProjectElevation
                    if (align_bottom):
                        newBasePoint = calcNewPoint(basePoint, topPoint, baseProjectionElevation)
                    else:
                        newBasePoint = basePoint
                    topProjectionElevation = topProjectionLevel.ProjectElevation
                    if (align_top):
                        newTopPoint = calcNewPoint(basePoint, topPoint, topProjectionElevation)
                    else:
                        newTopPoint = topPoint
                    newAnalyticalLine = Line.CreateBound(newBasePoint, newTopPoint)
                    AnalyticalColumn.SetCurve(newAnalyticalLine)
                else:
                    if (align_bottom):
                        AnalyticalColumn.get_Parameter(BuiltInParameter.ANALYTICAL_MODEL_BASE_EXTENSION_METHOD).Set(1)
                        AnalyticalColumn.get_Parameter(BuiltInParameter.ANALYTICAL_MODEL_COLUMN_BASE_EXTENSION).Set(baseProjectionLevel.Id)
                    if (align_top):
                        AnalyticalColumn.get_Parameter(BuiltInParameter.ANALYTICAL_MODEL_TOP_EXTENSION_METHOD).Set(1)
                        AnalyticalColumn.get_Parameter(BuiltInParameter.ANALYTICAL_MODEL_COLUMN_TOP_EXTENSION).Set(topProjectionLevel.Id)
        else:
            groupFlag = True
    t.Commit()
    if groupFlag == True:
        DavidEng.ErrorWindow(errortext="We could not run the plugin on Analytical Walls that are in groups. Please set them manually.").ShowDialog()

def ProjectionLevel(point, levels_collector_all):
    projectionLevel = None
    belowElevation = None
    aboveElevation = None
    for level in levels_collector_all:
        if level.ProjectElevation == point.Z:
            projectionLevel = level
            break
        elif level.ProjectElevation < point.Z:
            if belowElevation == None:
                belowElevation = level
            elif level.ProjectElevation > belowElevation.ProjectElevation:
                belowElevation = level
        else:
            if aboveElevation == None:
                aboveElevation = level
            elif level.ProjectElevation < aboveElevation.ProjectElevation:
                aboveElevation = level
    if projectionLevel == None and belowElevation != None and aboveElevation != None:
        belowDifference = point.Z - belowElevation.ProjectElevation
        aboveDifference = aboveElevation.ProjectElevation - point.Z
        if belowDifference < aboveDifference:
            projectionLevel = belowElevation
        else:
            projectionLevel = aboveElevation
    elif projectionLevel == None and belowElevation == None and aboveElevation != None:
        projectionLevel = aboveElevation
    elif projectionLevel == None and belowElevation != None and aboveElevation == None:
        projectionLevel = belowElevation
    return projectionLevel

if type(doc.ActiveView) == View3D or type(doc.ActiveView) == ViewSection:
    if HOST_APP.is_newer_than(2022):
        analyticalColumnsCount = len(Analytical_Columns_Collector)
    else:
        analyticalColumnsCount = Analytical_Columns_Collector.GetElementCount()
    if analyticalColumnsCount != 0:
        if not selection:
            if HOST_APP.is_newer_than(2022):
                customFilter = CustomSelectionFilterNewAPI(DB.BuiltInCategory.OST_AnalyticalMember)
            else:
                customFilter = CustomSelectionFilterOldAPI(DB.BuiltInCategory.OST_ColumnAnalytical)
            try:
                AnalyticalColumns_selection = [doc.GetElement(reference) for reference in uidoc.Selection.PickObjects(
                    ObjectType.Element, customFilter, "Select Analytical Columns")]
            except Exceptions.OperationCanceledException:
                import sys
                sys.exit()
            if AnalyticalColumns_selection == []:
                DavidEng.ErrorWindow(errortext="You have not selected any Analytical Column").ShowDialog()
        else:
            AnalyticalColumns_selection = []
            if HOST_APP.is_newer_than(2022):
                for element in selection:
                    if element.Category.Name == 'Analytical Members' and element.StructuralRole.ToString() != 'StructuralRoleBeam':
                        AnalyticalColumns_selection.append(element)
            else:
                for element in selection:
                    if element.Category.Name == 'Analytical Columns':
                        AnalyticalColumns_selection.append(element)
            if AnalyticalColumns_selection == []:
                DavidEng.ErrorWindow(errortext="You have not selected any Analytical Column").ShowDialog()

        if not AnalyticalColumns_selection == []:
            ChooseLevelsWindow(SelectLevelsXamlFile).show(modal=True)
    else:
        DavidEng.ErrorWindow(errortext="You don't have any visiable Analytical Column in your active view").ShowDialog()
else:
    DavidEng.ErrorWindow(errortext="You can only run this add-in on\n3D Views or Section Views").ShowDialog()