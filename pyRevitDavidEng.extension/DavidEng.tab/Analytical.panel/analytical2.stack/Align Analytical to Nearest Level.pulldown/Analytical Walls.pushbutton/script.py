"""Align the selected Analytical Walls to the nearest selected levels."""
# pylint: disable=E0401,W0703,W0613
from pyrevit import coreutils, UI, revit, DB, forms, script
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, BuiltInParameter, Transaction, XYZ, ElementId, Level, View3D, ViewSection, CurveLoop, Line, Curve
from Autodesk.Revit.UI.Selection import ObjectType, ISelectionFilter
from Autodesk.Revit import Exceptions
import DavidEng
from pyrevit import HOST_APP

from System.Collections.Generic import List

# dependencies
import clr
clr.AddReference('System.Windows.Forms')

# find the path of SelectLevels.xaml
SelectLevelsXamlFile = script.get_bundle_file('SelectLevels.xaml')

# import WPF creator and base Window
import wpf
from System import Windows

# App and Author name
__title__ = 'Analytical Walls'
__authors__ = 'Yuval Dolin'

# Identify the current Revit file
doc = __revit__.ActiveUIDocument.Document
uidoc = __revit__.ActiveUIDocument

# if the user selected elements before runing the add-in
selection = [doc.GetElement(id) for id in uidoc.Selection.GetElementIds()]

if HOST_APP.is_newer_than(2022):
    Analytical_Walls_Collector = []
    # Creating collectior instance and collecting all the Analytical Panels from the model
    analyticalPanelsCollector = FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_AnalyticalPanel)
    for analyticalPanel in analyticalPanelsCollector:
        if analyticalPanel.StructuralRole.ToString() == 'StructuralRoleWall':
            Analytical_Walls_Collector.append(analyticalPanel)
else:
    # Creating collectior instance and collecting all the Analytical Walls from the model
    Analytical_Walls_Collector = FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_WallAnalytical)

# Define class to filter elements of category to select 
class CustomSelectionFilterNewAPI(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, self.category).Id and e.StructuralRole.ToString() == 'StructuralRoleWall':
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

# Define class to filter elements of category to select 
class CustomSelectionFilterOldAPI(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, self.category).Id:
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

# Define class to filter elements of category to select 
class CustomSelectionFilterLevel(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, DB.BuiltInCategory.OST_Levels).Id:
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

class ChooseLevelsWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name):
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)

    # user select all levels
    def all_levels(self, sender, args):
        self.Close()
        levels_collector_all = FilteredElementCollector(doc).OfClass(Level).WhereElementIsNotElementType().ToElements()
        AlignAnalyticalWalls(levels_collector_all)

    # user select specific levels
    def select_levels(self, sender, args):
        self.Close()
        try:
            selected_levels = forms.select_levels()
        except Exceptions.OperationCanceledException:
            import sys
            sys.exit()
        if selected_levels == []:
			DavidEng.ErrorWindow(errortext="You have not selected any Level").ShowDialog()
			ChooseLevelsWindow(SelectLevelsXamlFile).show(modal=True)
        else:
			AlignAnalyticalWalls(selected_levels)

def AlignAnalyticalWalls(levels_collector_all):
    t = Transaction(doc, "Align Analytical Walls to Nearest Levels")
    t.Start()
    groupFlag = False
    for AnalyticalWall in AnalyticalWalls_selection:
        if AnalyticalWall.GroupId.IntegerValue < 0:
            if HOST_APP.is_newer_than(2022):
                newCurveList = List[Curve]()
                panelContour = AnalyticalWall.GetOuterContour()
                # flag that verify that all curves are lines
                linesValidationFlag = True
                for contourLine in panelContour:
                    if type(contourLine) != Line:
                        linesValidationFlag = False
                        break
                    lineStartPoint = contourLine.GetEndPoint(0)
                    startProjectionLevel = ProjectionLevel(lineStartPoint, levels_collector_all)
                    startProjectionElevation = startProjectionLevel.ProjectElevation
                    newStartAnalyticalBeam = XYZ(lineStartPoint.X, lineStartPoint.Y, startProjectionElevation)
                    lineEndPoint = contourLine.GetEndPoint(1)
                    endProjectionLevel = ProjectionLevel(lineEndPoint, levels_collector_all)
                    endProjectionElevation = endProjectionLevel.ProjectElevation
                    newEndAnalyticalBeam = XYZ(lineEndPoint.X, lineEndPoint.Y, endProjectionElevation)
                    newAnalyticalLine = Line.CreateBound(newStartAnalyticalBeam, newEndAnalyticalBeam)
                    newCurveList.Add(newAnalyticalLine)
                    
                if linesValidationFlag == True:
                    newCurveLoop = CurveLoop.Create(newCurveList)
                    AnalyticalWall.SetOuterContour(newCurveLoop)
            else:
                basePoint = AnalyticalWall.get_BoundingBox(doc.ActiveView).Min
                topPoint = AnalyticalWall.get_BoundingBox(doc.ActiveView).Max
                baseProjectionLevel = ProjectionLevel(basePoint, levels_collector_all)
                topProjectionLevel = ProjectionLevel(topPoint, levels_collector_all)

                if not baseProjectionLevel.ProjectElevation == topProjectionLevel.ProjectElevation:
                    AnalyticalWall.get_Parameter(BuiltInParameter.ANALYTICAL_MODEL_BASE_EXTENSION_METHOD).Set(1)
                    AnalyticalWall.get_Parameter(BuiltInParameter.ANALYTICAL_MODEL_WALL_BASE_PROJECTION).Set(baseProjectionLevel.Id)
                    AnalyticalWall.get_Parameter(BuiltInParameter.ANALYTICAL_MODEL_TOP_EXTENSION_METHOD).Set(1)
                    AnalyticalWall.get_Parameter(BuiltInParameter.ANALYTICAL_MODEL_WALL_TOP_PROJECTION).Set(topProjectionLevel.Id)
        else:
            groupFlag = True
    t.Commit()
    if groupFlag == True:
        DavidEng.ErrorWindow(errortext="We could not run the plugin on Analytical Walls that are in groups. Please set them manually.").ShowDialog()

def ProjectionLevel(point, levels_collector_all):
    projectionLevel = None
    belowElevation = None
    aboveElevation = None
    for level in levels_collector_all:
        if level.ProjectElevation == point.Z:
            projectionLevel = level
            break
        elif level.ProjectElevation < point.Z:
            if belowElevation == None:
                belowElevation = level
            elif level.ProjectElevation > belowElevation.ProjectElevation:
                belowElevation = level
        else:
            if aboveElevation == None:
                aboveElevation = level
            elif level.ProjectElevation < aboveElevation.ProjectElevation:
                aboveElevation = level
    if projectionLevel == None and belowElevation != None and aboveElevation != None:
        belowDifference = point.Z - belowElevation.ProjectElevation
        aboveDifference = aboveElevation.ProjectElevation - point.Z
        if belowDifference < aboveDifference:
            projectionLevel = belowElevation
        else:
            projectionLevel = aboveElevation
    elif projectionLevel == None and belowElevation == None and aboveElevation != None:
        projectionLevel = aboveElevation
    elif projectionLevel == None and belowElevation != None and aboveElevation == None:
        projectionLevel = belowElevation
    return projectionLevel

if type(doc.ActiveView) == View3D or type(doc.ActiveView) == ViewSection:
    if HOST_APP.is_newer_than(2022):
        analyticalWallsCount = len(Analytical_Walls_Collector)
    else:
        analyticalWallsCount = Analytical_Walls_Collector.GetElementCount()
    if analyticalWallsCount != 0:
        if not selection:
            if HOST_APP.is_newer_than(2022):
                customFilter = CustomSelectionFilterNewAPI(DB.BuiltInCategory.OST_AnalyticalPanel)
            else:
                customFilter = CustomSelectionFilterOldAPI(DB.BuiltInCategory.OST_WallAnalytical)
            try:
                AnalyticalWalls_selection = [doc.GetElement(reference) for reference in uidoc.Selection.PickObjects(
                    ObjectType.Element, customFilter, "Select Analytical Walls")]
            except Exceptions.OperationCanceledException:
                import sys
                sys.exit()
            if AnalyticalWalls_selection == []:
                DavidEng.ErrorWindow(errortext="You have not selected any Analytical Wall").ShowDialog()
        else:
            AnalyticalWalls_selection = []
            if HOST_APP.is_newer_than(2022):
                for element in selection:
                    if element.Category.Name == 'Analytical Panels' and element.StructuralRole.ToString() == 'StructuralRoleWall':
                        AnalyticalWalls_selection.append(element)
            else:
                for element in selection:
                    if element.Category.Name == 'Analytical Walls':
                        AnalyticalWalls_selection.append(element)
            if AnalyticalWalls_selection == []:
                DavidEng.ErrorWindow(errortext="You have not selected any Analytical Wall").ShowDialog()

        if not AnalyticalWalls_selection == []:
            ChooseLevelsWindow(SelectLevelsXamlFile).show(modal=True)
    else:
        DavidEng.ErrorWindow(errortext="You don't have any visiable Analytical Wall in your active view").ShowDialog()
else:
    DavidEng.ErrorWindow(errortext="You can only run this add-in on\n3D Views or Section Views").ShowDialog()