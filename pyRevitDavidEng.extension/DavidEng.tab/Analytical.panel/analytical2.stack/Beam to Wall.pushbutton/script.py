"""Create walls instead of beams for the analysis of the Coupling Beams. This add-in doesn't delete the existing beams but only creates new walls and disable \
the analytical model from the existing beams. For filtering the walls from the main model we will use Design Options.
In order to use this add-in, you must to disable manually the analytical model of the beams that are in groups."""
# pylint: disable=E0401,W0703,W0613
from pyrevit import coreutils, UI, revit, DB, forms, script
from pyrevit.framework import Math
from pyrevit.forms import ProgressBar
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, BuiltInParameter, Transaction, XYZ, ElementId, Options, Solid, WallType, Wall, IFailuresPreprocessor, \
                              FailureProcessingResult, Outline, BoundingBoxIntersectsFilter, ElementMulticategoryFilter, ElementTransformUtils, CompoundStructureLayer, \
                              JoinGeometryUtils
from Autodesk.Revit.UI.Selection import ObjectType, ISelectionFilter
from Autodesk.Revit.UI import UIApplication, RevitCommandId, PostableCommand
from Autodesk.Revit import Exceptions
import DavidEng

from System.Collections.Generic import List

# dependencies
import clr
clr.AddReference('System.Windows.Forms')

# find the path of ErrorWindow.xaml
xamlfile = script.get_bundle_file('ErrorWindow.xaml')

# find the path of InstructionsWindow.xaml
InstructionsXamlFile = script.get_bundle_file('InstructionsWindow.xaml')

# import WPF creator and base Window
import wpf
from System import Windows

# App and Author name
__title__ = 'Beam to\nWall'
__authors__ = 'Yuval Dolin'

# Identify the current Revit file
doc = __revit__.ActiveUIDocument.Document
uidoc = __revit__.ActiveUIDocument
uiapp = UIApplication(doc.Application)

options = Options(ComputeReferences=True, IncludeNonVisibleObjects=True)

# if the user selected elements before runing the add-in
selection = [doc.GetElement(id) for id in uidoc.Selection.GetElementIds()]

# Creating collectior instance and collecting all the text types from the model
WallTypesCollector = FilteredElementCollector(doc).OfClass(WallType)

# Define class to filter elements of category to select 
class CustomSelectionFilter(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, self.category).Id:
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

class ViewConverterFailurePreProcessor(IFailuresPreprocessor):
    def __init__(self):
        self

    def PreprocessFailures(self, failures_accessor):
        fmas = list(failures_accessor.GetFailureMessages())

        if len(fmas) == 0:
            return FailureProcessingResult.Continue

        else:
            # DeleteWarning mimics clicking 'Ok' button
            for fma in fmas:
                failures_accessor.DeleteWarning(fma)

            return FailureProcessingResult.ProceedWithCommit

        return FailureProcessingResult.Continue

class ChooseWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name):
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)

    # user select to close the window and isolate the relevant beams
    def isolate_beams(self, sender, args):
        self.Close()
        curview = doc.ActiveView
        # reset temporary hide/isolate before filtering elements
        curview.DisableTemporaryViewMode(DB.TemporaryViewMode.TemporaryHideIsolate)
        # isolate them in the active view
        curview.IsolateElementsTemporary(beam_in_group)

    # user select to close the window
    def close(self, sender, args):
        self.Close()

class InstructionsWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name):
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)

    def close(self, sender, args):
        self.Close()

    def design_options(self, sender, args):
        self.Close()
        curview = doc.ActiveView
        # reset temporary hide/isolate before filtering elements
        curview.DisableTemporaryViewMode(DB.TemporaryViewMode.TemporaryHideIsolate)
        # isolate them in the active view
        curview.IsolateElementsTemporary(new_walls)
        uiapp.PostCommand(RevitCommandId.LookupPostableCommandId(PostableCommand.DesignOptions)) 

class NewWallTypwWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name, Beam_Width, Material_Name):
        self.beam_width = Beam_Width
        self.material_name = Material_Name
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)
        self.width_value.Text = str(round(self.beam_width, 0))
        self.material_value.Text = self.material_name

    def click_ok(self, sender, args):
        if not self.name_value.Text.lstrip() == '':
            CheckEqualFlag = False
            for wallType in WallTypesCollector:
                if wallType.get_Parameter(BuiltInParameter.SYMBOL_NAME_PARAM).AsString() == self.name_value.Text:
                    CheckEqualFlag = True
                    break
            if CheckEqualFlag == False:
                self.Close()
                global WallTypeNameValue
                WallTypeNameValue = self.name_value.Text
            else:
                DavidEng.ErrorWindow(errortext="The name '{}' is already in use.".format(self.name_value.Text)).ShowDialog()
        else:
            DavidEng.ErrorWindow(errortext="You can't leave the wall type name empty.").ShowDialog()

    def cancel(self, sender, args):
        self.Close()

def CleanModelInPlace(Beams_selection):
    beams_collector = []
    for Beam in Beams_selection:
        analyticalcheck = 'No'
        try:
            Beam.get_Parameter(BuiltInParameter.STRUCTURAL_ANALYTICAL_MODEL).AsValueString()
            analyticalcheck = 'Yes'
        except Exception, ex:
            analyticalcheck = 'No'
        if analyticalcheck == 'Yes':
            beams_collector.append(Beam)
    return beams_collector

def BeamFaces(TempBeam):
    BeamDirection = TempBeam.Location.Curve.Direction
    BeamTop = BeamBot = BeamStart = BeamEnd = BeamBack = BeamFront = False
    geoElement = TempBeam.get_Geometry(options)
    #get beam faces
    for obj in geoElement:
        if isinstance(obj,Solid):
            if obj.Faces.Size == 6:
                for f in obj.Faces:
                    if round(f.FaceNormal.Z, 5) == 1:
                        BeamTop = f
                    elif round(f.FaceNormal.Z, 5) == -1:
                        BeamBot = f
                    elif f.FaceNormal.X == BeamDirection.X and f.FaceNormal.Y == BeamDirection.Y:
                        BeamStart = f
                    elif -(f.FaceNormal.X) == BeamDirection.X and -(f.FaceNormal.Y) == BeamDirection.Y:
                        BeamEnd = f
                    elif round(f.FaceNormal.X, 5) == round(BeamDirection.Y, 5) and round(-(f.FaceNormal.Y), 5) == round(BeamDirection.X, 5):
                        BeamBack = f
                    elif round(-(f.FaceNormal.X), 5) == round(BeamDirection.Y, 5) and round(f.FaceNormal.Y, 5) == round(BeamDirection.X, 5):
                        BeamFront = f
    return BeamTop, BeamBot, BeamStart, BeamEnd, BeamBack, BeamFront

def NewWallType(BeamMaterialId, BeamWidth):
    BeamMaterial = doc.GetElement(BeamMaterialId)
    BeamMaterialName = BeamMaterial.Name
    global WallTypeNameValue
    WallTypeNameValue = ''
    NewWallTypwWindow('CreateNewWallTypeWindow.xaml', Beam_Width = BeamWidth * 30.48, Material_Name = BeamMaterialName).show(modal=True)
    if not WallTypeNameValue.lstrip() == '':
        for wallType in WallTypesCollector:
            if wallType.get_Parameter(BuiltInParameter.SYMBOL_NAME_PARAM).AsString() != "" and str(wallType.FamilyName) == "Basic Wall":
                duptype = wallType
                break
        wallType = duptype.Duplicate(WallTypeNameValue)
        StructureLayer = List[CompoundStructureLayer]()
        CompoundStructure = wallType.GetCompoundStructure()
        Layers = CompoundStructure.GetLayers()
        for layer in Layers:
            Function = layer.Function
            LayerId = layer.LayerId
            if str(Function) == "Structure":
                StructureLayer.Add(layer)
                break
        CompoundStructure.SetLayers(StructureLayer)
        wallType.SetCompoundStructure(CompoundStructure)
        CompoundStructure.SetLayerWidth(0, BeamWidth)
        CompoundStructure.SetMaterialId(0, BeamMaterialId)
        CompoundStructure.StructuralMaterialIndex = 0
        wallType.SetCompoundStructure(CompoundStructure)
        return wallType.Id
    else:
        return -1

def CreateWall(TempBeam, Beam, BeamTop, BeamBot, BeamStart, BeamEnd, BeamBack, BeamFront):
    StopFlag = False
    if BeamTop != False and BeamBot != False:
        BeamCurve = TempBeam.Location.Curve
        BeamWidth = Math.Sqrt(Math.Pow((BeamFront.Origin.X - BeamBack.Origin.X), 2.0) + Math.Pow((BeamFront.Origin.Y - BeamBack.Origin.Y), 2.0))
        BeamMaterialId = TempBeam.StructuralMaterialId
        if BeamMaterialId.IntegerValue < 0:
            BeamType = doc.GetElement(TempBeam.GetTypeId())
            BeamMaterialId = BeamType.get_Parameter(BuiltInParameter.STRUCTURAL_MATERIAL_PARAM).AsElementId()
        if not BeamMaterialId.IntegerValue < 0:
            WallTypeId = -1
            for wallType in WallTypesCollector:
                try:
                    TypeMaterialId = wallType.get_Parameter(BuiltInParameter.STRUCTURAL_MATERIAL_PARAM).AsElementId()
                    if TypeMaterialId == BeamMaterialId and round(wallType.Width, 5) == round(BeamWidth, 5):
                        WallTypeId = wallType.Id
                        break
                except:
                    pass
            if WallTypeId == -1:
                WallTypeId = NewWallType(BeamMaterialId, BeamWidth)
            try:
                ReferenceLevelId = TempBeam.get_Parameter(BuiltInParameter.INSTANCE_REFERENCE_LEVEL_PARAM).AsElementId()
                BeamHeight = BeamTop.Origin.Z - BeamBot.Origin.Z
                ReferenceLevel = doc.GetElement(ReferenceLevelId)
                ReferenceElevation = ReferenceLevel.ProjectElevation
                BeamBotOffset = BeamBot.Origin.Z - ReferenceElevation
                NewWall = Wall.Create(doc, BeamCurve, WallTypeId, ReferenceLevelId, BeamHeight, BeamBotOffset, False, True)
                new_walls.Add(NewWall.Id)
                new_walls.Add(NewWall.GetAnalyticalModelId())
                if Beam.GroupId.IntegerValue < 0:
                    EnableAnalyticalParam = Beam.get_Parameter(BuiltInParameter.STRUCTURAL_ANALYTICAL_MODEL)
                    EnableAnalyticalParam.Set(0)
                return StopFlag
            except:
                StopFlag = True
                return StopFlag
        else:
            DavidEng.ErrorWindow(errortext="In order to use this add-in, you must to define Structural Material for all the selected beams.").ShowDialog()
            StopFlag = True
            return StopFlag
    else:
        return StopFlag

def Unjoin(TempBeam):
    BoxMax = TempBeam.BoundingBox[doc.ActiveView].Max
    BoxMin = TempBeam.BoundingBox[doc.ActiveView].Min
    NewBoxMax = XYZ(BoxMax.X, BoxMax.Y, BoxMax.Z)
    NewBoxMin = XYZ(BoxMin.X, BoxMin.Y, BoxMin.Z)
    outline = Outline(NewBoxMin, NewBoxMax)
    bbfilter = BoundingBoxIntersectsFilter(outline)
    CatFilter = DB.ElementMulticategoryFilter(List[DB.BuiltInCategory]([DB.BuiltInCategory.OST_Walls, DB.BuiltInCategory.OST_Floors, \
                                                                        DB.BuiltInCategory.OST_StructuralFraming, DB.BuiltInCategory.OST_StructuralColumns]))
    # Creating collectior instance and collecting all the structural elements from the beam bounding box
    bb_collector = FilteredElementCollector(doc).WhereElementIsNotElementType() \
                                                   .WherePasses(bbfilter).WherePasses(CatFilter)
    for element in bb_collector:
        try:
            JoinGeometryUtils.JoinGeometry(doc,TempBeam,element)
        except:
            pass
    for element in bb_collector:
        try:
            JoinGeometryUtils.UnjoinGeometry(doc,TempBeam,element)
        except:
            pass

if (DavidEng.checkPermission()):
    if not selection:
        customFilter = CustomSelectionFilter(DB.BuiltInCategory.OST_StructuralFraming)
        try:
            Beams_selection = [doc.GetElement(reference) for reference in uidoc.Selection.PickObjects(
                ObjectType.Element, customFilter, "Select Beams")]
        except Exceptions.OperationCanceledException:
            import sys
            sys.exit()
        if Beams_selection == []:
            DavidEng.ErrorWindow(errortext="You have not selected any Beam").ShowDialog()
    else:
        Beams_selection = []
        for element in selection:
            if element.Category.Name == 'Structural Framing':
                Beams_selection.append(element)
        if Beams_selection == []:
            DavidEng.ErrorWindow(errortext="You have not selected any Beam").ShowDialog()

    if not Beams_selection == []:
        beams_collector = CleanModelInPlace(Beams_selection)
        t = Transaction(doc, "Replace Beams for Walls")
        t.Start()
        failure_ops = t.GetFailureHandlingOptions()
        failure_ops.SetFailuresPreprocessor(ViewConverterFailurePreProcessor())
        t.SetFailureHandlingOptions(failure_ops)
        max_value = len(beams_collector)
        counter = 0
        beam_in_group = List[DB.ElementId]()
        new_walls = List[DB.ElementId]()
        for Beam in beams_collector:
            analyticalcheck = Beam.get_Parameter(BuiltInParameter.STRUCTURAL_ANALYTICAL_MODEL).AsValueString()
            if not Beam.GroupId.IntegerValue < 0 and analyticalcheck == 'Yes':
                beam_in_group.Add(Beam.Id)
                try:
                    beam_in_group.Add(Beam.GetAnalyticalModelId())
                except:
                    pass
        if len(beam_in_group) == 0:
            with DavidEng.ProgressBar(title='Create Walls ... ({value} of {max_value})', cancellable=True) as pb:
                for Beam in beams_collector:
                    if pb.cancelled:
                        t.RollBack()
                        import sys
                        sys.exit()
                    ListTempBeamWeak = ElementTransformUtils.CopyElement(doc, Beam.Id, DB.XYZ(0, 0, 0))
                    TempBeamWeak = doc.GetElement(ListTempBeamWeak[0])
                    DB.Structure.StructuralFramingUtils.DisallowJoinAtEnd(TempBeamWeak, 0)
                    DB.Structure.StructuralFramingUtils.DisallowJoinAtEnd(TempBeamWeak, 1)
                    ListTempBeam = ElementTransformUtils.CopyElement(doc, TempBeamWeak.Id, DB.XYZ(0, 0, 0))
                    doc.Delete(TempBeamWeak.Id)
                    TempBeam = doc.GetElement(ListTempBeam[0])
                    Unjoin(TempBeam)
                    doc.Regenerate()
                    BeamTop, BeamBot, BeamStart, BeamEnd, BeamBack, BeamFront = BeamFaces(TempBeam)
                    StopFlag = CreateWall(TempBeam, Beam, BeamTop, BeamBot, BeamStart, BeamEnd, BeamBack, BeamFront)
                    if StopFlag == True:
                        t.RollBack()
                        break
                    doc.Delete(TempBeam.Id)
                    counter += 1
                    pb.update_progress(counter, max_value)
                    revit.uidoc.RefreshActiveView()
            if not str(t.GetStatus ()) == "RolledBack":
                InstructionsWindow(InstructionsXamlFile).show(modal=True)
        else:
            ChooseWindow('SelectWindow.xaml').show(modal=True)
        if not str(t.GetStatus ()) == "RolledBack":
            t.Commit()
else:
    DavidEng.ErrorWindow(errortext="You don't have permission\nto use the tool").ShowDialog()