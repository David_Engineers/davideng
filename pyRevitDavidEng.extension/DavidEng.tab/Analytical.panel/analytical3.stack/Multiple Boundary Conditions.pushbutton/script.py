"""Defines point, line, and area boundary conditions in an analytical model for multiple elements.

Verify that the boundary condition family types are loaded and assigned in the Structural Setting menu.
"""
# pylint: disable=E0401,W0703,W0613
from pyrevit import coreutils, UI, revit, DB, forms, script
from Autodesk.Revit.DB import BuiltInCategory, Transaction, Reference
from Autodesk.Revit.DB.Structure import AnalyticalModelSelector, TranslationRotationValue, AnalyticalCurveSelector, AnalyticalLoopType
from Autodesk.Revit.UI.Selection import ObjectType, ISelectionFilter
from Autodesk.Revit import Exceptions
from pyrevit import HOST_APP

# dependencies
import clr
clr.AddReference('System.Windows.Forms')
import DavidEng

# find the path of ErrorWindow.xaml
xamlfile = script.get_bundle_file('ErrorWindow.xaml')

# find the path of SelectWallBoundarySide.xaml
SelectWallBoundarySideXamlFile = script.get_bundle_file('SelectWallBoundarySide.xaml')

# find the path of SelectColumnBoundarySide.xaml
SelectColumnBoundarySideXamlFile = script.get_bundle_file('SelectColumnBoundarySide.xaml')

# import WPF creator and base Window
import wpf
from System import Windows

# App and Author name
__title__ = 'Multiple Boundary\nConditions'
__authors__ = 'Yuval Dolin'

# Identify the current Revit file
doc = __revit__.ActiveUIDocument.Document
uidoc = __revit__.ActiveUIDocument

# if the user selected elements before runing the add-in
selection = [doc.GetElement(id) for id in uidoc.Selection.GetElementIds()]

# Define class to filter elements of category to select 
class CustomSelectionFilterOldAPI(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, DB.BuiltInCategory.OST_ColumnAnalytical).Id or e.Category.Id == DB.Category.GetCategory(doc, DB.BuiltInCategory.OST_WallAnalytical).Id \
            or e.Category.Id == DB.Category.GetCategory(doc, DB.BuiltInCategory.OST_FloorAnalytical).Id or e.Category.Id == DB.Category.GetCategory(doc, DB.BuiltInCategory.OST_BeamAnalytical).Id \
            or e.Category.Id == DB.Category.GetCategory(doc, DB.BuiltInCategory.OST_IsolatedFoundationAnalytical).Id \
            or e.Category.Id == DB.Category.GetCategory(doc, DB.BuiltInCategory.OST_WallFoundationAnalytical).Id \
            or e.Category.Id == DB.Category.GetCategory(doc, DB.BuiltInCategory.OST_FoundationSlabAnalytical).Id:
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

# Define class to filter elements of category to select 
class CustomSelectionFilterNewAPI(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, DB.BuiltInCategory.OST_AnalyticalMember).Id \
            or e.Category.Id == DB.Category.GetCategory(doc, DB.BuiltInCategory.OST_AnalyticalPanel).Id:
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

# Show Error Window Form
class ErrorWindow(Windows.Window):
    def __init__(self, errortext):
        self.errortext=errortext
        wpf.LoadComponent(self, xamlfile)
        self.error.Text = "{}".format(self.errortext)

    def ok(self, sender, args):
        self.Close()

class ChooseColumnBoundarySideWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name):
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)

    # user select to place the boundary conditions at the bottom of the analytical columns
    def select_bottom_column(self, sender, args):
        self.Close()
        if not AnalyticalWalls_selection == []:
            ChooseWallBoundarySideWindow(SelectWallBoundarySideXamlFile, boundaryPoint='StartPoint').show(modal=True)
        else:
            NewBoundaryConditions('StartPoint', 3)

    # user select to place the boundary conditions at the top of the analytical columns
    def select_top_column(self, sender, args):
        self.Close()
        if not AnalyticalWalls_selection == []:
            ChooseWallBoundarySideWindow(SelectWallBoundarySideXamlFile, boundaryPoint='EndPoint').show(modal=True)
        else:
            NewBoundaryConditions('EndPoint', 3)

class ChooseWallBoundarySideWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name, boundaryPoint):
        self.boundaryPoint = boundaryPoint
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)

    # user select to place the boundary conditions at the bottom of the analytical walls
    def select_bottom_wall(self, sender, args):
        self.Close()
        NewBoundaryConditions(self.boundaryPoint, 3)

    # user select to place the boundary conditions at the top of the analytical walls
    def select_top_wall(self, sender, args):
        self.Close()
        NewBoundaryConditions(self.boundaryPoint, 0)

def NewBoundaryConditions(boundaryPoint, boundaryLine):
    F = TranslationRotationValue.Fixed
    R = TranslationRotationValue.Release
    WallCurvesFlag = True
    with revit.Transaction('Boundary Conditions', log_errors=False):
        for AnalyticalColumn in AnalyticalColumns_selection:
            curve = AnalyticalColumn.GetCurve()
            if boundaryPoint == 'StartPoint':
                CurveSelector = AnalyticalModelSelector(curve, AnalyticalCurveSelector.StartPoint)
            else:
                CurveSelector = AnalyticalModelSelector(curve, AnalyticalCurveSelector.EndPoint)
            startPointRef = AnalyticalColumn.GetReference(CurveSelector)
            try:
                newPointColumn = doc.Create.NewPointBoundaryConditions(startPointRef, F, 0, F, 0, F, 0, R, 0, R, 0, R, 0 )
            except Exception:
                a = 0
        for AnalyticalFloor in AnalyticalFloors_selection:
            try:
                newAreaFloor = doc.Create.NewAreaBoundaryConditions(AnalyticalFloor, F, 0, F, 0, F, 0 )
            except Exception:
                a = 0
        for AnalyticalWall in AnalyticalWalls_selection:
            if HOST_APP.is_newer_than(2022):
                curveLoops = AnalyticalWall.GetOuterContour()
                CurveLoopIterator = curveLoops.GetCurveLoopIterator()
                curves = curveLoops.NumberOfCurves()
            else:
                curveLoops = AnalyticalWall.GetLoops(AnalyticalLoopType.External)
                CurveLoopIterator = curveLoops[0].GetCurveLoopIterator()
                curves = len(AnalyticalWall.GetCurves(0))
            StartPoint = CurveLoopIterator.Current.GetEndPoint(0)
            EndPoint = CurveLoopIterator.Current.GetEndPoint(1)
            CurveList = []
            CurveList.append(0)
            if HOST_APP.is_newer_than(2022):
                curvesCounter = curves + 1
            else:
                curvesCounter = curves
            if boundaryLine == 3:
                MinZPoint = (round(StartPoint.Z, 0) + round(EndPoint.Z, 0)) / 2
                i = 1
                while i < curvesCounter:
                    CurveLoopIterator.MoveNext()
                    try:
                        StartPoint = CurveLoopIterator.Current.GetEndPoint(0)
                        EndPoint = CurveLoopIterator.Current.GetEndPoint(1)
                        zPoint = (round(StartPoint.Z, 0) + round(EndPoint.Z, 0)) / 2
                        if zPoint < MinZPoint:
                            MinZPoint = zPoint
                            CurveList = []
                            CurveList.append(i)
                        elif zPoint == MinZPoint:
                            CurveList.append(i)
                    except Exception:
                        a = 0
                    i = i+1
            else:
                MaxZPoint = (round(StartPoint.Z, 0) + round(EndPoint.Z, 0)) / 2
                i = 1
                while i < curvesCounter:
                    CurveLoopIterator.MoveNext()
                    try:
                        StartPoint = CurveLoopIterator.Current.GetEndPoint(0)
                        EndPoint = CurveLoopIterator.Current.GetEndPoint(1)
                        zPoint = (round(StartPoint.Z, 0) + round(EndPoint.Z, 0)) / 2
                        if zPoint > MaxZPoint:
                            MaxZPoint = zPoint
                            CurveList = []
                            CurveList.append(i)
                        elif zPoint == MaxZPoint:
                            CurveList.append(i)
                    except Exception:
                        a = 0
                    i = i+1
            if not curves < 4:
                for CurveNum in CurveList:
                    if HOST_APP.is_newer_than(2022):
                        curveLoops = AnalyticalWall.GetOuterContour()
                        CurveLoopIterator = curveLoops.GetCurveLoopIterator()
                    else:
                        curveLoops = AnalyticalWall.GetLoops(AnalyticalLoopType.External)
                        CurveLoopIterator = curveLoops[0].GetCurveLoopIterator()
                    if CurveNum == 0:
                        CurveSelector = AnalyticalModelSelector(CurveLoopIterator.Current)
                        wholeCurveRef = AnalyticalWall.GetReference(CurveSelector)
                        try:
                            newLineWall = doc.Create.NewLineBoundaryConditions(wholeCurveRef, F, 0, F, 0, F, 0, R, 0 )
                        except Exception:
                            a = 0
                    else:
                        i = 0
                        while i < CurveNum:
                            CurveLoopIterator.MoveNext()
                            i = i+1
                        CurveSelector = AnalyticalModelSelector(CurveLoopIterator.Current)
                        wholeCurveRef = AnalyticalWall.GetReference(CurveSelector)
                        try:
                            newLineWall = doc.Create.NewLineBoundaryConditions(wholeCurveRef, F, 0, F, 0, F, 0, R, 0 )
                        except Exception:
                            a = 0
            else:
                WallCurvesFlag = False
        for AnalyticalBeam in AnalyticalBeams_selection:
            try:
                newLineBeam = doc.Create.NewLineBoundaryConditions(AnalyticalBeam, F, 0, F, 0, F, 0, R, 0 )
            except Exception:
                a = 0
        for AnalyticalIsolatedFoundation in AnalyticalIsolatedFoundations_selection:
            curve = AnalyticalIsolatedFoundation.GetCurve()
            CurveSelector = AnalyticalModelSelector(curve)
            PointRef = AnalyticalIsolatedFoundation.GetReference(CurveSelector)
            try:
                newPointIsolatedFoundation = doc.Create.NewPointBoundaryConditions(PointRef, F, 0, F, 0, F, 0, R, 0, R, 0, R, 0 )
            except Exception:
                a = 0
        for AnalyticalWallFoundation in AnalyticalWallFoundations_selection:
            try:
                newLineWallFoundation = doc.Create.NewLineBoundaryConditions(AnalyticalWallFoundation, F, 0, F, 0, F, 0, R, 0 )
            except Exception:
                a = 0
        for AnalyticalFoundationSlab in AnalyticalFoundationSlab_selection:
            try:
                newAreaFoundationSlab = doc.Create.NewAreaBoundaryConditions(AnalyticalFoundationSlab, F, 0, F, 0, F, 0 )
            except Exception:
                a = 0
    if WallCurvesFlag == False:
        ErrorWindow(errortext="There is one or more walls which we couldn't place Boundary Conditions. Please place manually Boundary Conditions for these walls.").ShowDialog()

# Split selections for categories
def AnalyticalOrder(Analytical_selection):
    AnalyticalColumns_selection = []
    AnalyticalWalls_selection = []
    AnalyticalFloors_selection = []
    AnalyticalBeams_selection = []
    AnalyticalIsolatedFoundations_selection = []
    AnalyticalWallFoundations_selection = []
    AnalyticalFoundationSlab_selection = []
    for element in Analytical_selection:
        if element.Category.Name == 'Analytical Columns':
            AnalyticalColumns_selection.append(element)
        elif element.Category.Name == 'Analytical Walls':
            AnalyticalWalls_selection.append(element)
        elif element.Category.Name == 'Analytical Floors':
            AnalyticalFloors_selection.append(element)
        elif element.Category.Name == 'Analytical Beams':
            AnalyticalBeams_selection.append(element)
        elif element.Category.Name == 'Analytical Isolated Foundations':
            AnalyticalIsolatedFoundations_selection.append(element)
        elif element.Category.Name == 'Analytical Wall Foundations':
            AnalyticalWallFoundations_selection.append(element)
        elif element.Category.Name == 'Analytical Foundation Slabs':
            AnalyticalFoundationSlab_selection.append(element)
        elif HOST_APP.is_newer_than(2022):
            if element.Category.Name == 'Analytical Members' and element.StructuralRole.ToString() == 'StructuralRoleColumn':
                AnalyticalColumns_selection.append(element)
            elif element.Category.Name == 'Analytical Members' and element.StructuralRole.ToString() == 'StructuralRoleBeam':
                AnalyticalBeams_selection.append(element)
            elif element.Category.Name == 'Analytical Panels' and element.StructuralRole.ToString() == 'StructuralRoleWall':
                AnalyticalWalls_selection.append(element)
            elif element.Category.Name == 'Analytical Panels' and element.StructuralRole.ToString() == 'StructuralRoleFloor':
                AnalyticalFloors_selection.append(element)
    if AnalyticalColumns_selection == [] and AnalyticalWalls_selection == [] and AnalyticalFloors_selection == [] and AnalyticalBeams_selection == [] and AnalyticalIsolatedFoundations_selection == [] \
        and AnalyticalWallFoundations_selection == [] and AnalyticalFoundationSlab_selection == []:
        ErrorWindow(errortext="You have not selected any Analytical Element").ShowDialog()
    return AnalyticalColumns_selection, AnalyticalWalls_selection, AnalyticalFloors_selection, AnalyticalBeams_selection, AnalyticalIsolatedFoundations_selection, AnalyticalWallFoundations_selection, \
        AnalyticalFoundationSlab_selection

if (DavidEng.checkPermission()):
    if not selection:
        if HOST_APP.is_newer_than(2022):
            customFilter = CustomSelectionFilterNewAPI(DB.BuiltInCategory)
        else:
            customFilter = CustomSelectionFilterOldAPI(DB.BuiltInCategory)
        try:
            Analytical_selection = [doc.GetElement(reference) for reference in uidoc.Selection.PickObjects(
                ObjectType.Element, customFilter, "Select Analytical Elements")]
        except Exceptions.OperationCanceledException:
            import sys
            sys.exit()
        AnalyticalColumns_selection, AnalyticalWalls_selection, AnalyticalFloors_selection, AnalyticalBeams_selection, AnalyticalIsolatedFoundations_selection, AnalyticalWallFoundations_selection, \
        AnalyticalFoundationSlab_selection = AnalyticalOrder(Analytical_selection)
    else:
        AnalyticalColumns_selection, AnalyticalWalls_selection, AnalyticalFloors_selection, AnalyticalBeams_selection, AnalyticalIsolatedFoundations_selection, AnalyticalWallFoundations_selection, \
        AnalyticalFoundationSlab_selection = AnalyticalOrder(selection)

    if not AnalyticalColumns_selection == []:
        ChooseColumnBoundarySideWindow(SelectColumnBoundarySideXamlFile).show(modal=True)
    elif not AnalyticalWalls_selection == []:
        ChooseWallBoundarySideWindow(SelectWallBoundarySideXamlFile, boundaryPoint='StartPoint').show(modal=True)
    else:
        NewBoundaryConditions('StartPoint', 3)
else:
    DavidEng.ErrorWindow(errortext="You don't have permission\nto use the tool").ShowDialog()