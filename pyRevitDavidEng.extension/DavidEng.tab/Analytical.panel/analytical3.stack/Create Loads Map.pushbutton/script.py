"""Create Loads Map for specified Floors. """
# pylint: disable=E0401,W0703,W0613
import System
from pyrevit import UI, revit, DB, forms, script
from pyrevit.framework import Math
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, BuiltInParameter, Transaction, XYZ, Line, Options, Solid, UV, Arc, Transform, TransactionGroup, IFailuresPreprocessor, FailureProcessingResult
from Autodesk.Revit.UI.Selection import ObjectType, ISelectionFilter
from Autodesk.Revit import Exceptions

# dependencies
import clr
clr.AddReference('System.Windows.Forms')
import DavidEng

# find the path of ErrorWindow.xaml
xamlfile = script.get_bundle_file('ErrorWindow.xaml')

# find the path of SuccessWindow.xaml
xamlfileSuccess = script.get_bundle_file('SuccessWindow.xaml')

# import WPF creator and base Window
import wpf
from System import Windows

# App and Author name
__title__ = 'Create\nLoads Map'
__authors__ = 'Yuval Dolin'

# Identify the current Revit file
doc = __revit__.ActiveUIDocument.Document
uidoc = __revit__.ActiveUIDocument

# if the user selected elements before runing the add-in
selection = [doc.GetElement(id) for id in uidoc.Selection.GetElementIds()]

# Creating collectior instance and collecting all the area loads from the model
AreaLoadsCollector = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_AreaLoads) \
                                               .WhereElementIsNotElementType()

# Define class to filter elements of category to select 
class CustomSelectionFilter(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, self.category).Id:
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

# Show Error Window Form
class ErrorWindow(Windows.Window):
    def __init__(self, errortext):
        self.errortext=errortext
        wpf.LoadComponent(self, xamlfile)
        self.error.Text = "{}".format(self.errortext)

    def ok(self, sender, args):
        self.Close()


class ViewConverterFailurePreProcessor(IFailuresPreprocessor):
    def __init__(self):
        self

    def PreprocessFailures(self, failures_accessor):
        fmas = list(failures_accessor.GetFailureMessages())

        if len(fmas) == 0:
            return FailureProcessingResult.Continue

        else:
            # DeleteWarning mimics clicking 'Ok' button
            for fma in fmas:
                failures_accessor.DeleteWarning(fma)

            return FailureProcessingResult.ProceedWithCommit

        return FailureProcessingResult.Continue

# Show Success Window Form
class SuccessWindow(Windows.Window):
    def __init__(self, messagetext):
        self.messagetext=messagetext
        wpf.LoadComponent(self, xamlfileSuccess)
        self.message.Text = "{}".format(self.messagetext)

    def ok(self, sender, args):
        self.Close()

def Main(Floors_selection):
    tg = TransactionGroup(doc, 'Create Loads Map')
    tg.Start()
    t = Transaction(doc, 'Create Loads Map')
    t.Start()
    failure_ops = t.GetFailureHandlingOptions()
    failure_ops.SetFailuresPreprocessor(ViewConverterFailurePreProcessor())
    t.SetFailureHandlingOptions(failure_ops)
    opts = Options()
    opts.ComputeReferences = True
    Area_List = []
    Area_UV_List = []
    for Floor in Floors_selection:
        FloorGeo = Floor.get_Geometry(opts)
        for obj in FloorGeo:
            if isinstance(obj,Solid):
                for Face in obj.Faces:
                    EdgeArray = Face.EdgeLoops
                    break
            break
        PointsCounter = 0
        PointXCount = 0
        PointYCount = 0
        for Edge in EdgeArray:
            for FloorLine in Edge:
                StartPoint = FloorLine.AsCurve().GetEndPoint(0)
                EndPoint = FloorLine.AsCurve().GetEndPoint(1)
                p1 = XYZ(StartPoint.X, StartPoint.Y, 0)
                p2 = XYZ(EndPoint.X, EndPoint.Y, 0)
                try:
                    if FloorLine.AsCurve().IsCyclic == False:
                        line = Line.CreateBound(p1, p2)
                    else:
                        radius = FloorLine.AsCurve().Radius
                        center = FloorLine.AsCurve().Center
                        sagitta = radius - Math.Sqrt(Math.Pow(radius, 2.0) - Math.Pow((p2 - p1).GetLength() / 2.0, 2.0))
                        midPointOfChord = (p1 + p2) / 2.0
                        midPointOfArc = midPointOfChord + Transform.CreateRotation(XYZ.BasisZ, Math.PI / 2.0).OfVector((p2 - p1).Normalize().Multiply(sagitta))
                        if round((midPointOfArc - center).GetLength(), 5) == round(radius, 5):
                            line = Arc.Create(p1, p2, midPointOfArc)
                        else:
                            midPointOfArc = midPointOfArc - 2 * Transform.CreateRotation(XYZ.BasisZ, Math.PI / 2.0).OfVector((p2 - p1).Normalize().Multiply(sagitta))
                            line = Arc.Create(p1, p2, midPointOfArc)
                    sp = doc.ActiveView.SketchPlane
                    modelLine = doc.Create.NewAreaBoundaryLine(sp, line, doc.ActiveView)
                except Exception, ex:
                    abc = 0
                PointsCounter = PointsCounter + 1
                PointXCount = PointXCount + StartPoint.X
                PointYCount = PointYCount + StartPoint.Y
        AreaUV = UV(PointXCount/PointsCounter, PointYCount/PointsCounter)
        Area_UV_List.append(AreaUV)
        AddArea = doc.Create.NewArea(doc.ActiveView, AreaUV)
        Area_List.append(AddArea)
    t.Commit()
    i = 0
    while i < len(Area_List):
        FloorArea = Floors_selection[i].get_Parameter(BuiltInParameter.HOST_AREA_COMPUTED).AsDouble()
        if not FloorArea < Area_List[i].Area + 10 and FloorArea > Area_List[i].Area - 10:
            AddValue = 5
            Count = 0
            AddArea = Area_List[i]
            DeleteArea(AddArea)
            while Count < 4 and AddValue <= 100:
                if Count == 0:
                    AreaUV = UV(Area_UV_List[i].U - AddValue, Area_UV_List[i].V)
                    AddArea = LocateNewArea(AreaUV)
                    if FloorArea < AddArea.Area + 10 and FloorArea > AddArea.Area - 10:
                        Area_UV_List[i] = AreaUV
                        Area_List[i] = AddArea
                        break
                    else:
                        DeleteArea(AddArea)
                        Count = Count + 1
                if Count == 1:
                    AreaUV = UV(Area_UV_List[i].U + AddValue, Area_UV_List[i].V)
                    AddArea = LocateNewArea(AreaUV)
                    if FloorArea < AddArea.Area + 10 and FloorArea > AddArea.Area - 10:
                        Area_UV_List[i] = AreaUV
                        Area_List[i] = AddArea
                        break
                    else:
                        DeleteArea(AddArea)
                        Count = Count + 1
                if Count == 2:
                    AreaUV = UV(Area_UV_List[i].U, Area_UV_List[i].V - AddValue)
                    AddArea = LocateNewArea(AreaUV)
                    if FloorArea < AddArea.Area + 10 and FloorArea > AddArea.Area - 10:
                        Area_UV_List[i] = AreaUV
                        Area_List[i] = AddArea
                        break
                    else:
                        DeleteArea(AddArea)
                        Count = Count + 1
                if Count == 3:
                    AreaUV = UV(Area_UV_List[i].U, Area_UV_List[i].V + AddValue)
                    AddArea = LocateNewArea(AreaUV)
                    if FloorArea < AddArea.Area + 10 and FloorArea > AddArea.Area - 10:
                        Area_UV_List[i] = AreaUV
                        Area_List[i] = AddArea
                        break
                    else:
                        DeleteArea(AddArea)
                        Count = 0
                        AddValue = AddValue + 5
            if AddValue == 105:
                AreaUV = Area_UV_List[i]
                t = Transaction(doc, 'Create Loads Map')
                t.Start()
                AddArea = doc.Create.NewArea(doc.ActiveView, AreaUV)
                t.Commit()
                Area_List[i] = AddArea
        i = i + 1
    i = 0
    t = Transaction(doc, 'Create Loads Map')
    t.Start()
    while i < len(Floors_selection):
        try:
            AddAreaTag = doc.Create.NewAreaTag(doc.ActiveView, Area_List[i], Area_UV_List[i])
            AddAreaTag.HasLeader = False
        except:
            pass
        try:
            SetValues(Floors_selection[i], Area_List[i])
        except:
            pass
        i = i + 1
    t.Commit()
    tg.Assimilate() 
    SuccessWindow(messagetext="Please add 'Color Fill Legend' to your Area Plan View").ShowDialog()

def LocateNewArea(AreaUV):
    t = Transaction(doc, 'Create Loads Map')
    t.Start()
    failure_ops = t.GetFailureHandlingOptions()
    failure_ops.SetFailuresPreprocessor(ViewConverterFailurePreProcessor())
    t.SetFailureHandlingOptions(failure_ops)
    AddArea = doc.Create.NewArea(doc.ActiveView, AreaUV)
    t.Commit()
    return AddArea

def DeleteArea(AddArea):
    t = Transaction(doc, 'Create Loads Map')
    t.Start()
    doc.Delete(AddArea.Id)
    t.Commit()

def CreatExtensionStorage(area, DeadLoadFlag, AreaDeadLoadId, LiveLoadFlag, AreaLiveLoadId):
    schemaBuilder=DB.ExtensibleStorage.SchemaBuilder(System.Guid("d64e3bab-7b93-415a-a20a-4d09ec106aab"))
    schemaBuilder.SetReadAccessLevel(DB.ExtensibleStorage.AccessLevel.Public)
    schemaBuilder.SetWriteAccessLevel(DB.ExtensibleStorage.AccessLevel.Public)
    schemaBuilder.SetVendorId("YuvalDolin")
    schemaBuilder.SetSchemaName("DavidEng_Schema_CreateLoadsMap")
    fieldBuilder = schemaBuilder.AddSimpleField("Id_Area_Load_DL", area.Id.GetType())
    fieldBuilder.SetDocumentation("The Id of the associated dead load")
    fieldBuilder = schemaBuilder.AddSimpleField("Id_Area_Load_LL", area.Id.GetType())
    fieldBuilder.SetDocumentation("The Id of the associated live load")
    schema = schemaBuilder.Finish()
    
    entity =DB.ExtensibleStorage.Entity(schema)
    if DeadLoadFlag == True:
        fieldSpliceLocation = schema.GetField("Id_Area_Load_DL")
        entity.Set(fieldSpliceLocation, AreaDeadLoadId)
    if LiveLoadFlag == True:
        fieldSpliceLocation = schema.GetField("Id_Area_Load_LL")
        entity.Set(fieldSpliceLocation, AreaLiveLoadId)
    area.SetEntity(entity)

def SetValues(Floor, AddArea):
    Analytical = Floor.GetAnalyticalModel()
    CatOfUse = Floor.LookupParameter('Category of Use').AsString()
    if CatOfUse != None:
        AreaCatOfUse = AddArea.LookupParameter('Category of Use').Set(CatOfUse)
    DeadLoadFlag = False
    LiveLoadFlag = False
    AreaDeadLoadId = None
    AreaLiveLoadId = None
    for AreaLoad in AreaLoadsCollector:
        LoadCaseValue = AreaLoad.get_Parameter(BuiltInParameter.LOAD_CASE_ID).AsValueString()
        if Analytical.Id == AreaLoad.HostElement.Id and "DL" in LoadCaseValue:
            DeadLoadValue = AreaLoad.get_Parameter(BuiltInParameter.LOAD_AREA_FORCE_FZ1).AsDouble()
            DeadLoad = AddArea.LookupParameter('Dead Load').Set(DeadLoadValue)
            AreaDeadLoadId = AreaLoad.Id
            DeadLoadFlag = True
        elif Analytical.Id == AreaLoad.HostElement.Id and "LL" in LoadCaseValue:
            LiveLoadValue = AreaLoad.get_Parameter(BuiltInParameter.LOAD_AREA_FORCE_FZ1).AsDouble()
            LiveLoad = AddArea.LookupParameter('Live Load').Set(LiveLoadValue)
            AreaLiveLoadId = AreaLoad.Id
            LiveLoadFlag = True
        if DeadLoadFlag == True and LiveLoadFlag == True:
            break
    if DeadLoadFlag == True or LiveLoadFlag == True:
        CreatExtensionStorage(AddArea, DeadLoadFlag, AreaDeadLoadId, LiveLoadFlag, AreaLiveLoadId)

if (DavidEng.checkPermission()):
    if doc.ActiveView.LookupParameter("Family").AsValueString() == "Area Plan":
        if not selection:
            customFilter = CustomSelectionFilter(DB.BuiltInCategory.OST_Floors)
            try:
                Floors_selection = [doc.GetElement(reference) for reference in uidoc.Selection.PickObjects(
                    ObjectType.Element, customFilter, "Select Floors")]
            except Exceptions.OperationCanceledException:
                import sys
                sys.exit()
            if Floors_selection == []:
                ErrorWindow(errortext="You have not selected any Assembly").ShowDialog()
            else:
                Main(Floors_selection)
        else:
            Floors_selection = []
            for element in selection:
                if element.Category.Name == 'Floors':
                    Floors_selection.append(element)
            if Floors_selection == []:
                ErrorWindow(errortext="You have not selected any Floor").ShowDialog()
            else:
                Main(Floors_selection)
    else:
        ErrorWindow(errortext="You can only run this add-in on Area Plan view").ShowDialog()
else:
    DavidEng.ErrorWindow(errortext="You don't have permission\nto use the tool").ShowDialog()