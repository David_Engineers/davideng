"""The add-in calculates the self-weight of the element and converts it to a point/line load."""
# pylint: disable=E0401,W0703,W0613
from pyrevit import coreutils, UI, revit, DB, forms, script
from pyrevit.framework import Math
from pyrevit.forms import ProgressBar
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, BuiltInParameter, Transaction, XYZ, ElementId, ViewFamilyType, ViewFamily, View3D, ElementClassFilter, Floor, Options, Solid, Plane, SketchPlane
from Autodesk.Revit.DB.Structure import LineLoad, PointLoad
from Autodesk.Revit.UI.Selection import ObjectType, ISelectionFilter
from Autodesk.Revit import Exceptions
import DavidEng

# dependencies
import clr
clr.AddReference('System.Windows.Forms')

# find the path of AnalyticalModel.xaml
AnalyticalModelXamlFile = script.get_bundle_file('AnalyticalModel.xaml')

# find the path of DefineDensity.xaml
DefineDensityXamlFile = script.get_bundle_file('DefineDensity.xaml')

# import WPF creator and base Window
import wpf
from System import Windows

# App and Author name
__title__ = 'Self Weight\nto Load'
__authors__ = 'Yuval Dolin'

# Identify the current Revit file
doc = __revit__.ActiveUIDocument.Document
uidoc = __revit__.ActiveUIDocument

# Creating collectior instance and collecting all the Load Cases from the model
loadcases = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_LoadCases)

# if the user selected elements before runing the add-in
selection = [doc.GetElement(id) for id in uidoc.Selection.GetElementIds()]

disableAnalytical = None
elementInGroupCheck = False
floorFilter = ElementClassFilter(Floor)

# Define class to filter elements of category to select 
class CustomSelectionFilter(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, DB.BuiltInCategory.OST_Walls).Id \
            or e.Category.Id == DB.Category.GetCategory(doc, DB.BuiltInCategory.OST_StructuralFraming).Id \
            or e.Category.Id == DB.Category.GetCategory(doc, DB.BuiltInCategory.OST_StructuralColumns).Id:
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

class DisableAnalytical(forms.WPFWindow):
    def __init__(self, xaml_file_name):
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)

    # user select all levels
    def clickNo(self, sender, args):
        self.Close()
        global disableAnalytical
        disableAnalytical = False
        DefineDensity(DefineDensityXamlFile).show(modal=True)

    # user select specific levels
    def clickYes(self, sender, args):
        self.Close()
        global disableAnalytical
        disableAnalytical = True
        DefineDensity(DefineDensityXamlFile).show(modal=True)

class DefineDensity(forms.WPFWindow):
    def __init__(self, xaml_file_name):
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)
        self._setup_loadcases()

    # user select all levels
    def cancel(self, sender, args):
        self.Close()

    @property
    def selected_loadcase(self):
        return self.loadcases_cb.SelectedItem

# Setup the Load Cases for the ComboBox (list of Load Cases)
    def _setup_loadcases(self):
        loadnamelist = set()
        for load in loadcases:
            if not load.Name == "SW":
                loadnamelist.add(load.Name)
        loadcasenamelist = []
        loadcasenamelist.extend(sorted(list(loadnamelist)))
        self.loadcases_cb.ItemsSource = loadcasenamelist
        self.loadcases_cb.SelectedIndex = 0

    # user select specific levels
    def click_ok(self, sender, args):
		# Check if the density value is valid (the value must to be int())  
        self.checkvalue = True
        try:
            floatDensityValue = float(self.value.Text)
            if floatDensityValue <= 0:
                self.checkvalue = False
        except:
            self.checkvalue = False

        if self.checkvalue == True:
            self.Close()
            max_value = len(element_selection)
            counter = 0
            t = Transaction(doc, "Self Weight to Load")
            t.Start()
            view3D = self.CreateTempView3D()
            view3D.AreAnalyticalModelCategoriesHidden = False
            with DavidEng.ProgressBar(title='Calculation and Conversion of Self Weight to Line Load ... ({value} of {max_value})', cancellable=True) as pb:
                for element in element_selection:
                    if pb.cancelled:
                        t.RollBack()
                        import sys
                        sys.exit()

                    if element.Category.Name == 'Walls' or element.Category.Name == 'Structural Framing':
                        isInPlaceCheck = False
                        try:
                            if element.Symbol.Family.IsInPlace == True:
                                isInPlaceCheck = True
                        except:
                            pass
                        if isInPlaceCheck == False:
                            self.lineElement(element, view3D, floatDensityValue)
                    if element.Category.Name == 'Structural Columns':
                        if element.Symbol.Family.IsInPlace == False:
                            self.pointElement(element, view3D, floatDensityValue)

                    if element.GroupId.IntegerValue < 0 and disableAnalytical == True:
                        EnableAnalyticalParam = element.get_Parameter(BuiltInParameter.STRUCTURAL_ANALYTICAL_MODEL)
                        try:
                            EnableAnalyticalParam.Set(0)
                        except:
                            pass
                    elif disableAnalytical == True and elementInGroupCheck == False:
                        global elementInGroupCheck
                        elementInGroupCheck = True
                        
                    counter += 1
                    pb.update_progress(counter, max_value)
                    revit.uidoc.RefreshActiveView()
                    doc.Regenerate()
            doc.Delete(view3D.Id)
            t.Commit()
            if elementInGroupCheck == True:
                DavidEng.ErrorWindow(errortext="We couldn't disable the Analytical Model of elements that are in Groups. Please do it manually.").ShowDialog()
        else:
            # Error if the density value is invalid (the value must to be float() and bigger than 0)  
			DavidEng.ErrorWindow(errortext="You entered an invalid value: '{}'".format(self.value.Text)).ShowDialog()

    def CreateTempView3D(self):
        viewTypeCollector = FilteredElementCollector(doc).OfClass(ViewFamilyType).ToElements()
        for i in viewTypeCollector:
            if i.ViewFamily == ViewFamily.ThreeDimensional:
                viewType = i
                break
        view3D = View3D.CreateIsometric(doc, viewType.Id)
        
        return view3D

    def StartEndPoint(self, point, referenceIntersectorFloor):
        ReferenceWithContextUp = referenceIntersectorFloor.Find(point, DB.XYZ(0, 0, 1))
        try:
            return ReferenceWithContextUp[0].GetReference().GlobalPoint
        except:
            pass
        try:
            ReferenceWithContextDown = referenceIntersectorFloor.Find(point, DB.XYZ(0, 0, -1))
            return ReferenceWithContextDown[0].GetReference().GlobalPoint
        except:
            pass

    def refWithContext(self, elementPoint, baseOffset, view3D):
        elementSide = XYZ(elementPoint.X, elementPoint.Y, elementPoint.Z + baseOffset)
        referenceIntersector = DB.ReferenceIntersector(floorFilter, DB.FindReferenceTarget.Face, view3D)
        ReferenceWithContextUp = referenceIntersector.FindNearest(elementSide, DB.XYZ(0, 0, 1))
        ReferenceWithContextDown = referenceIntersector.FindNearest(elementSide, DB.XYZ(0, 0, -1))
        return ReferenceWithContextUp, ReferenceWithContextDown

    def intPointUpDown(self, ReferenceWithContextUp, ReferenceWithContextDown):
        intersectPointUp = None
        intersectPointDown = None

        try:
            intersectPointUp = [ReferenceWithContextUp.GetReference().GlobalPoint, ReferenceWithContextUp.GetReference().ElementId]
        except:
            pass
        try:
            intersectPointDown = [ReferenceWithContextDown.GetReference().GlobalPoint, ReferenceWithContextDown.GetReference().ElementId]
        except:
            pass

        return intersectPointUp, intersectPointDown

    def intPoint(self, intersectPointUp, intersectPointDown, elementPoint, baseOffset):
        intersectPoint = None
        if intersectPointUp != None and intersectPointDown != None:
            if intersectPointUp[0].Z - (elementPoint.Z + baseOffset) < (elementPoint.Z + baseOffset) - intersectPointDown[0].Z:
                if intersectPointUp[0].Z - (elementPoint.Z + baseOffset) < (50/30.48):
                    intersectPoint = intersectPointUp
            else:
                if (elementPoint.Z + baseOffset) - intersectPointDown[0].Z < (50/30.48):
                    intersectPoint = intersectPointDown
        elif intersectPointUp == None and intersectPointDown == None:
            pass
        elif intersectPointUp == None:
            if (elementPoint.Z + baseOffset) - intersectPointDown[0].Z < (50/30.48):
                intersectPoint = intersectPointDown
        elif intersectPointDown == None:
             if intersectPointUp[0].Z - (elementPoint.Z + baseOffset) < (50/30.48):
                intersectPoint = intersectPointUp
        
        return intersectPoint

    def CalculatePoint(self, startIntersectPoint, endIntersectPoint, elementStart, elementEnd, view3D, equalZflag):
        floorStart = doc.GetElement(startIntersectPoint[1])
        floorEnd = doc.GetElement(endIntersectPoint[1])
        analyticalcheckStart = floorStart.get_Parameter(BuiltInParameter.STRUCTURAL_ANALYTICAL_MODEL).AsValueString()
        analyticalcheckEnd = floorEnd.get_Parameter(BuiltInParameter.STRUCTURAL_ANALYTICAL_MODEL).AsValueString()
        if analyticalcheckStart == 'Yes' and analyticalcheckEnd == 'Yes':
            analyticalFloorStart = floorStart.GetAnalyticalModelId()
            referenceIntersectorFloorStart = DB.ReferenceIntersector(analyticalFloorStart, DB.FindReferenceTarget.Face, view3D)
            startPoint = self.StartEndPoint(elementStart, referenceIntersectorFloorStart)
            analyticalFloorEnd = floorEnd.GetAnalyticalModelId()
            referenceIntersectorFloorEnd = DB.ReferenceIntersector(analyticalFloorEnd, DB.FindReferenceTarget.Face, view3D)
            endPoint = self.StartEndPoint(elementEnd, referenceIntersectorFloorEnd)
            return startPoint, endPoint, equalZflag
        else:
            return None, None, equalZflag

    def createLoadParams(self, elementVolume, floatDensityValue, elementLength, intersectPoint):
        param = self.selected_loadcase
        for load in loadcases:
            if load.Name == param:
                loadid=load.Id.IntegerValue

        zForce = -(elementVolume*floatDensityValue)/elementLength
        force=XYZ(0,0,zForce*9806.65)
        moment=XYZ(0,0,0)

        opts = Options()
        opts.ComputeReferences = True
        floor = doc.GetElement(intersectPoint[1])
        analyticalFloor = floor.GetAnalyticalModelId()
        AnalyticalFloorGeo = doc.GetElement(analyticalFloor).get_Geometry(opts)
        for obj in AnalyticalFloorGeo:
            if isinstance(obj,Solid):
                for Face in obj.Faces:
                    EdgeArray = Face.EdgeLoops
                    break
        pl = Plane.CreateByNormalAndOrigin(Face.FaceNormal, Face.Origin)
        sk = SketchPlane.Create(doc, pl)

        return force, moment, pl, sk, loadid

    def lineElement(self, element, view3D, floatDensityValue):
        elementCurve = element.Location.Curve
        elementStart = elementCurve.GetEndPoint(0)
        elementEnd = elementCurve.GetEndPoint(1)
        if element.Category.Name == 'Walls':
            baseOffset = element.get_Parameter(BuiltInParameter.WALL_BASE_OFFSET).AsDouble()
        else:
            baseOffset = 0

        startRefUp, startRefDown = self.refWithContext(elementStart, baseOffset, view3D)
        endRefUp, endRefDown = self.refWithContext(elementEnd, baseOffset, view3D)

        startIntersectPointUp, startIntersectPointDown = self.intPointUpDown(startRefUp, startRefDown)
        endIntersectPointUp, endIntersectPointDown = self.intPointUpDown(endRefUp, endRefDown)

        startIntersectPoint = self.intPoint(startIntersectPointUp, startIntersectPointDown, elementStart, baseOffset)
        endIntersectPoint = self.intPoint(endIntersectPointUp, endIntersectPointDown, elementEnd, baseOffset)

        startPoint = None
        endPoint = None
        equalZflag = None
        if startIntersectPoint and endIntersectPoint:
            if startIntersectPoint[1] == endIntersectPoint[1]:
                startPoint, endPoint, equalZflag = self.CalculatePoint(startIntersectPoint, endIntersectPoint, elementStart, elementEnd, view3D, False)
            else:
                startPoint, endPoint, equalZflag = self.CalculatePoint(startIntersectPoint, endIntersectPoint, elementStart, elementEnd, view3D, True)

        continueFlag = None
        if startPoint and endPoint and equalZflag == True:
            if startPoint.Z == endPoint.Z:
                continueFlag = True
            else:
                continueFlag = False
        elif startPoint and endPoint and equalZflag == False:
            continueFlag = True
        else:
            continueFlag = False

        if continueFlag == True:
            elementVolume = element.get_Parameter(BuiltInParameter.HOST_VOLUME_COMPUTED).AsDouble()*Math.Pow(0.3048, 3)
            elementLength = element.get_Parameter(BuiltInParameter.CURVE_ELEM_LENGTH).AsDouble()*0.3048

            force, moment, pl, sk, loadid = self.createLoadParams(elementVolume, floatDensityValue, elementLength, startIntersectPoint)
            
            createload=LineLoad.Create(doc, startPoint, endPoint, force, moment, None, sk)
            setloadcase=createload.get_Parameter(BuiltInParameter.LOAD_CASE_ID).Set(ElementId(loadid))

    def pointElement(self, element, view3D, floatDensityValue):
        fs = element.Symbol
        fm = fs.Family	
        if not fm.IsInPlace:
            if element.IsSlantedColumn:
                crv = element.Location.Curve
                elementStart = crv.GetEndPoint(0)
            else:
                elementPoint = element.Location.Point
                bLev = element.Document.GetElement(element.get_Parameter(BuiltInParameter.FAMILY_BASE_LEVEL_PARAM).AsElementId()).ProjectElevation + element.get_Parameter(BuiltInParameter.FAMILY_BASE_LEVEL_OFFSET_PARAM).AsDouble()
                elementStart = XYZ(elementPoint.X, elementPoint.Y, bLev)

        startRefUp, startRefDown = self.refWithContext(elementStart, 0, view3D)

        startIntersectPointUp, startIntersectPointDown = self.intPointUpDown(startRefUp, startRefDown)

        startIntersectPoint = self.intPoint(startIntersectPointUp, startIntersectPointDown, elementStart, 0)

        if startIntersectPoint:
            elementVolume = element.get_Parameter(BuiltInParameter.HOST_VOLUME_COMPUTED).AsDouble()*Math.Pow(0.3048, 3)

            force, moment, pl, sk, loadid = self.createLoadParams(elementVolume, floatDensityValue, 0.3048, startIntersectPoint)

            createload=PointLoad.Create(doc, elementStart, force, moment, None, sk)
            setloadcase=createload.get_Parameter(BuiltInParameter.LOAD_CASE_ID).Set(ElementId(loadid))

if (DavidEng.checkPermission()):
    if not selection:
        customFilter = CustomSelectionFilter(DB.BuiltInCategory)
        try:
            element_selection = [doc.GetElement(reference) for reference in uidoc.Selection.PickObjects(
                ObjectType.Element, customFilter, "Select Walls and Beams")]
        except Exceptions.OperationCanceledException:
            import sys
            sys.exit()
        if element_selection == []:
            DavidEng.ErrorWindow(errortext="You have not selected any Dimension").ShowDialog()
    else:
        element_selection = []
        for element in selection:
            if element.Category.Name == 'Walls' or element.Category.Name == 'Structural Framing' or element.Category.Name == 'Structural Columns':
                element_selection.append(element)
        if element_selection == []:
            DavidEng.ErrorWindow(errortext="You have not selected any Wall, Beam or Column.").ShowDialog()

    if not element_selection == []:
        DisableAnalytical(AnalyticalModelXamlFile).show(modal=True)
else:
    DavidEng.ErrorWindow(errortext="You don't have permission\nto use the tool").ShowDialog()