"""Calculate column's Angle 3D and write it to 'DavidEng Angle 3D' parameter."""
# pylint: disable=E0401,W0703,W0613
from pyrevit import coreutils, UI, revit, DB, forms, script
from pyrevit.revit.db import query
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, BuiltInParameter, Transaction, XYZ, ElementId, BuiltInParameterGroup
from Autodesk.Revit.UI.Selection import ObjectType, ISelectionFilter
from Autodesk.Revit import Exceptions
import DavidEng

import sys

# dependencies
import clr
clr.AddReference('System.Windows.Forms')

# import WPF creator and base Window
import math 
import wpf
from System import Windows

# App and Author name
__title__ = 'Angle 3D'
__authors__ = 'Yuval Dolin'

# Identify the current Revit file
doc = __revit__.ActiveUIDocument.Document
uidoc = __revit__.ActiveUIDocument
app = doc.Application

# if the user selected elements before runing the add-in
selection = [doc.GetElement(id) for id in uidoc.Selection.GetElementIds()]

# Define class to filter elements of category to select 
class CustomSelectionFilter(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, self.category).Id:
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

# function for finding the angle  
def angle_triangle(x1, x2, x3, y1, y2, y3, z1, z2, z3):  
  
    num = (x2-x1)*(x3-x1)+(y2-y1)*(y3-y1)+(z2-z1)*(z3-z1)  
  
    den = math.sqrt((x2-x1)**2+(y2-y1)**2+(z2-z1)**2)*math.sqrt((x3-x1)**2+(y3-y1)**2+(z3-z1)**2)  
  
    angle = math.degrees(math.acos(num / den))  
  
    return round(angle, 3)  

if (DavidEng.checkPermission()):
    if not selection:
        customFilter = CustomSelectionFilter(DB.BuiltInCategory.OST_StructuralColumns)
        try:
            Columns_selection = [doc.GetElement(reference) for reference in uidoc.Selection.PickObjects(
                ObjectType.Element, customFilter, "Select Columns")]
        except Exceptions.OperationCanceledException:
            import sys
            sys.exit()
        if Columns_selection == []:
            DavidEng.ErrorWindow(errortext="You have not selected any Column").ShowDialog()
    else:
        Columns_selection = []
        for element in selection:
            if element.Category.Name == 'Structural Columns':
                Columns_selection.append(element)
        if Columns_selection == []:
            DavidEng.ErrorWindow(errortext="You have not selected any Column").ShowDialog()

    if not Columns_selection == []:
        with revit.Transaction('Calculate Angle 3D', log_errors=False):
            for column in Columns_selection:
                crv = None
                fs = column.Symbol
                fm = fs.Family
                if not fm.IsInPlace:
                    if column.IsSlantedColumn:
                        try:
                            crv = column.Location.Curve
                            columnStartPoint = crv.GetEndPoint(0)
                            columnEndPoint = crv.GetEndPoint(1)    
                            x1 = columnStartPoint.X
                            y1 = columnStartPoint.Y
                            z1 = columnStartPoint.Z
                            x2 = columnEndPoint.X
                            y2 = columnEndPoint.Y
                            z2 = columnEndPoint.Z
                            x3 = columnEndPoint.X
                            y3 = columnEndPoint.Y
                            z3 = columnStartPoint.Z
                            angle_A = angle_triangle(x1, x2, x3, y1, y2, y3, z1, z2, z3)  
                            if column.LookupParameter('DavidEng Angle 3D'):
                                column.LookupParameter('DavidEng Angle 3D').SetValueString(str(angle_A))
                            else:
                                # Get the parameter file
                                originalFile = app.SharedParametersFilename
                                app.SharedParametersFilename = sys.path[0]+"\TempDavidEngSharedParameters.txt"
                                sharedParameterFile = app.OpenSharedParameterFile()
                                # Add Categories to Category Set. 
                                columnCat = doc.Settings.Categories.get_Item(BuiltInCategory.OST_StructuralColumns);
                                catset = app.Create.NewCategorySet()
                                catset.Insert(columnCat);
                                # Txt group name 
                                GroupName = sharedParameterFile.Groups.get_Item("Structural")
                                # Txt parameter name
                                externalDefinition = GroupName.Definitions.get_Item("DavidEng Angle 3D") 
                                # Create the new shared parameter 
                                newInstanceBinding = app.Create.NewInstanceBinding(catset)
                                # Insert the new parameter into your project.
                                doc.ParameterBindings.Insert(externalDefinition, newInstanceBinding, BuiltInParameterGroup.PG_GEOMETRY)
                                # Set "Values can vary by group instance"
                                param = query.get_project_parameter("DavidEng Angle 3D", doc=doc)
                                app.SharedParametersFilename = originalFile
                                column.LookupParameter('DavidEng Angle 3D').SetValueString(str(angle_A))
                        except:
                            pass
else:
    DavidEng.ErrorWindow(errortext="You don't have permission\nto use the tool").ShowDialog()