"""Calculate the total Mass (kg) of the assembly including the mass of the concrete, exterior layer, interior layer, rebars and fabric sheets."""
# pylint: disable=E0401,W0703,W0613
from pyrevit import coreutils, UI, revit, DB, forms, script
from Autodesk.Revit.DB import BuiltInCategory, BuiltInParameter, Transaction
from Autodesk.Revit.UI.Selection import ObjectType, ISelectionFilter
from Autodesk.Revit import Exceptions

# dependencies
import clr
import DavidEng
clr.AddReference('System.Windows.Forms')

# find the path of ErrorWindow.xaml
xamlfile = script.get_bundle_file('ErrorWindow.xaml')

# import WPF creator and base Window
import wpf
from System import Windows

# App and Author name
__title__ = 'Calculate\nAssembly Mass'
__authors__ = 'Yuval Dolin'

# Identify the current Revit file
doc = __revit__.ActiveUIDocument.Document
uidoc = __revit__.ActiveUIDocument

# if the user selected elements before runing the add-in
selection = [doc.GetElement(id) for id in uidoc.Selection.GetElementIds()]

# Define class to filter elements of category to select 
class CustomSelectionFilter(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, self.category).Id:
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

# Show Error Window Form
class ErrorWindow(Windows.Window):
    def __init__(self, errortext):
        self.errortext=errortext
        wpf.LoadComponent(self, xamlfile)
        self.error.Text = "{}".format(self.errortext)

    def ok(self, sender, args):
        self.Close()

if (DavidEng.checkPermission()):
    if not selection:
        customFilter = CustomSelectionFilter(DB.BuiltInCategory.OST_Assemblies)
        try:
            Assemblies_selection = [doc.GetElement(reference) for reference in uidoc.Selection.PickObjects(
                ObjectType.Element, customFilter, "Select Assemblies")]
        except Exceptions.OperationCanceledException:
            import sys
            sys.exit()
        if Assemblies_selection == []:
            ErrorWindow(errortext="You have not selected any Assembly").ShowDialog()

    else:
        Assemblies_selection = []
        for element in selection:
            if element.Category.Name == 'Assemblies':
                Assemblies_selection.append(element)
        if Assemblies_selection == []:
            ErrorWindow(errortext="You have not selected any Assembly").ShowDialog()

    footToCm = 30.48
    OunceToKg = 35.314666
    with revit.Transaction('Calculate Assembly Mass', log_errors=False):
        for Assembly in Assemblies_selection:
            AssemblyMass = 0
            AssemblyMembers = Assembly.GetMemberIds()
            for Member in AssemblyMembers:
                MemberElement = revit.doc.GetElement(Member)
                if MemberElement.Name == 'Part':
                    MaterialId = MemberElement.get_Parameter(BuiltInParameter.MATERIAL_ID_PARAM).AsElementId()
                    Volume = MemberElement.get_Parameter(BuiltInParameter.DPART_VOLUME_COMPUTED).AsDouble()/OunceToKg
                    Material = doc.GetElement(MaterialId)
                    StructuralAsset = doc.GetElement(Material.StructuralAssetId)
                    if not StructuralAsset == None:
                        Density = StructuralAsset.get_Parameter(BuiltInParameter.PHY_MATERIAL_PARAM_STRUCTURAL_DENSITY).AsDouble()*OunceToKg
                        AssemblyMass = AssemblyMass + Volume * Density
                elif MemberElement.Category.Name == 'Structural Rebar':
                    try:
                        BarLength = MemberElement.LookupParameter('SOFiSTiK_Bar_Length').AsDouble()*footToCm/100
                        Quantity = MemberElement.LookupParameter('SOFiSTiK_Quantity').AsInteger()
                        RebarType = revit.doc.GetElement(MemberElement.GetTypeId())
                        MassPerLength = RebarType.LookupParameter('SOFiSTiK_Mass_per_Length').AsDouble()*3.280839
                        AssemblyMass = AssemblyMass + BarLength * MassPerLength * Quantity
                    except Exception:
                        a = 0
                elif MemberElement.Category.Name == 'Structural Fabric Reinforcement':
                    CutSheetMass = MemberElement.LookupParameter('Cut Sheet Mass').AsDouble()
                    AssemblyMass = AssemblyMass + CutSheetMass
                elif MemberElement.Category.Name == 'Generic Models':
                    try:
                        CageMass = MemberElement.LookupParameter('Cage Mass').AsDouble()
                        AssemblyMass = AssemblyMass + CageMass
                    except Exception:
                        a = 0
            try:
                Assembly.LookupParameter('DavidEng Assembly Mass').Set(AssemblyMass)
            except Exception:
                ErrorWindow(errortext="You don't have the 'DavidEng Assembly Mass' parameter").ShowDialog()
                break
else:
    DavidEng.ErrorWindow(errortext="You don't have permission\nto use the tool").ShowDialog()