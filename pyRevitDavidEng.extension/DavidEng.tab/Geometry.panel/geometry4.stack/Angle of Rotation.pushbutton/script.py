"""Calculate column's Angle of Rotation and write it to 'DavidEng Angle of Rotation' parameter."""
# pylint: disable=E0401,W0703,W0613
from pyrevit import coreutils, UI, revit, DB, forms, script
from pyrevit.revit.db import query
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, BuiltInParameter, Transaction, XYZ, ElementId, BuiltInParameterGroup
from Autodesk.Revit.UI.Selection import ObjectType, ISelectionFilter
from Autodesk.Revit import Exceptions
import DavidEng

import sys

# dependencies
import clr
clr.AddReference('System.Windows.Forms')

# import WPF creator and base Window
import math 
import wpf
from System import Windows

# App and Author name
__title__ = 'Angle of\nRotation'
__authors__ = 'Yuval Dolin'

# Identify the current Revit file
doc = __revit__.ActiveUIDocument.Document
uidoc = __revit__.ActiveUIDocument
app = doc.Application

# if the user selected elements before runing the add-in
selection = [doc.GetElement(id) for id in uidoc.Selection.GetElementIds()]

# Define class to filter elements of category to select 
class CustomSelectionFilter(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, self.category).Id:
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

# returns square of distance b/w two points  
def lengthSquare(X, Y):  
    xDiff = X[0] - Y[0]  
    yDiff = X[1] - Y[1]  
    return xDiff * xDiff + yDiff * yDiff 
      
def calculateAngle(A, B, C):  
      
    # Square of lengths be a2, b2, c2  
    a2 = lengthSquare(B, C)  
    b2 = lengthSquare(A, C)  
    c2 = lengthSquare(A, B)  
  
    # length of sides be a, b, c  
    a = math.sqrt(a2);  
    b = math.sqrt(b2);  
    c = math.sqrt(c2);  
  
    # From Cosine law  
    alpha = math.acos((b2 + c2 - a2) /
                         (2 * b * c));  
    betta = math.acos((a2 + c2 - b2) / 
                         (2 * a * c));  
    gamma = math.acos((a2 + b2 - c2) / 
                         (2 * a * b));  
  
    # Converting to degree  
    alpha = alpha * 180 / math.pi;  
    betta = betta * 180 / math.pi;  
    gamma = gamma * 180 / math.pi;  

    return betta

if (DavidEng.checkPermission()):
    if not selection:
        customFilter = CustomSelectionFilter(DB.BuiltInCategory.OST_StructuralColumns)
        try:
            Columns_selection = [doc.GetElement(reference) for reference in uidoc.Selection.PickObjects(
                ObjectType.Element, customFilter, "Select Columns")]
        except Exceptions.OperationCanceledException:
            import sys
            sys.exit()
        if Columns_selection == []:
            DavidEng.ErrorWindow(errortext="You have not selected any Column").ShowDialog()
    else:
        Columns_selection = []
        for element in selection:
            if element.Category.Name == 'Structural Columns':
                Columns_selection.append(element)
        if Columns_selection == []:
            DavidEng.ErrorWindow(errortext="You have not selected any Column").ShowDialog()

    if not Columns_selection == []:
        with revit.Transaction('Calculate Angle of Rotation', log_errors=False):
            for column in Columns_selection:
                crv = None
                fs = column.Symbol
                fm = fs.Family
                if not fm.IsInPlace:
                    if column.IsSlantedColumn:
                        try:
                            crv = column.Location.Curve
                            columnStartPoint = crv.GetEndPoint(0)
                            columnEndPoint = crv.GetEndPoint(1)
                            A = (columnStartPoint.X, columnStartPoint.Y)
                            B = (columnEndPoint.X, columnEndPoint.Y)
                            C = (columnStartPoint.X, columnEndPoint.Y)
                            betta = calculateAngle(A, B, C);
                            if column.LookupParameter('DavidEng Angle of Rotation'):
                                column.LookupParameter('DavidEng Angle of Rotation').SetValueString(str(betta))
                            else:
                                # Get the parameter file
                                originalFile = app.SharedParametersFilename
                                app.SharedParametersFilename = sys.path[0]+"\TempDavidEngSharedParameters.txt"
                                sharedParameterFile = app.OpenSharedParameterFile()
                                # Add Categories to Category Set. 
                                columnCat = doc.Settings.Categories.get_Item(BuiltInCategory.OST_StructuralColumns);
                                catset = app.Create.NewCategorySet()
                                catset.Insert(columnCat);
                                # Txt group name 
                                GroupName = sharedParameterFile.Groups.get_Item("Structural")
                                # Txt parameter name
                                externalDefinition = GroupName.Definitions.get_Item("DavidEng Angle of Rotation") 
                                # Create the new shared parameter 
                                newInstanceBinding = app.Create.NewInstanceBinding(catset)
                                # Insert the new parameter into your project.
                                doc.ParameterBindings.Insert(externalDefinition, newInstanceBinding, BuiltInParameterGroup.PG_GEOMETRY)
                                # Set "Values can vary by group instance"
                                param = query.get_project_parameter("DavidEng Angle of Rotation", doc=doc)
                                app.SharedParametersFilename = originalFile
                                column.LookupParameter('DavidEng Angle of Rotation').SetValueString(str(betta))
                        except:
                            pass
else:
    DavidEng.ErrorWindow(errortext="You don't have permission\nto use the tool").ShowDialog()