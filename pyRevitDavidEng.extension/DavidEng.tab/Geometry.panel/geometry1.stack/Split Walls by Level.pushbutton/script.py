"""Split the walls by level in three simple steps - choose the walls, choose the levels and choose offset to split the walls relative to the level."""
from pyrevit import revit, forms, script, DB
from pyrevit.forms import ProgressBar
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, Level, Category, BuiltInParameter, ElementTransformUtils, XYZ, Transaction
from Autodesk.Revit.UI.Selection import ObjectType, ISelectionFilter
from Autodesk.Revit import Exceptions
import DavidEng

# import WPF creator and base Window
import wpf
from System import Windows

# App and Author name
__title__ = 'Split Walls\nby Level'
__authors__ = 'Yuval Dolin'

# Identify the current Revit file
doc = __revit__.ActiveUIDocument.Document
uidoc = __revit__.ActiveUIDocument

# Define class to filter elements of category to select 
class CustomSelectionFilter(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, self.category).Id:
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

class ChooseWallsWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name):
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)

    # user select all walls
    def all_walls(self, sender, args):
        self.Close()
        walls_collector_all = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Walls) \
                                               .WhereElementIsNotElementType().ToElements()
        ChooseLevelsWindow('SelectLevels.xaml', walls=walls_collector_all).show(modal=True)

    # user select specific walls
    def select_walls(self, sender, args):
        self.Close()
        customFilter = CustomSelectionFilter(DB.BuiltInCategory.OST_Walls)
        try:
            wall_selection = [doc.GetElement(reference) for reference in uidoc.Selection.PickObjects(
                ObjectType.Element, customFilter, "Select Walls")]
        except Exceptions.OperationCanceledException:
            import sys
            sys.exit()
        if not wall_selection:
			DavidEng.ErrorWindow(errortext="You have not selected any Wall").ShowDialog()
			ChooseWallsWindow('SelectWalls.xaml').show(modal=True)
        else:
			ChooseLevelsWindow('SelectLevels.xaml', walls=wall_selection).show(modal=True)

class ChooseLevelsWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name, walls):
        self.walls = walls
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)

    # user select all levels
    def all_levels(self, sender, args):
        self.Close()
        levels_collector_all = FilteredElementCollector(doc).OfClass(Level).WhereElementIsNotElementType().ToElements()
        ChooseOffsetWindow('Offset.xaml', walls=self.walls, levels=levels_collector_all).show(modal=True)

    # user select specific levels
    def select_levels(self, sender, args):
        self.Close()
        customFilter = CustomSelectionFilter(DB.BuiltInCategory.OST_Levels)
        try:
            level_selection = [doc.GetElement(reference) for reference in uidoc.Selection.PickObjects(
                ObjectType.Element, customFilter, "Select Levels")]
        except Exceptions.OperationCanceledException:
            import sys
            sys.exit()
        if not level_selection:
			DavidEng.ErrorWindow(errortext="You have not selected any Level").ShowDialog()
			ChooseLevelsWindow('SelectLevels.xaml', walls=self.walls).show(modal=True)
        else:
			ChooseOffsetWindow('Offset.xaml', walls=self.walls, levels=level_selection).show(modal=True)

class ChooseOffsetWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name, walls, levels):
        self.walls = walls
        self.levels = levels
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)

    def split_walls(self, sender, args):
        self.Close()
		# Check if the offset value is valid (the value must to be float())  
        FloatFlag = True
        try:
            if float(self.offset.Text):
                floatoffsetvalue=float(self.offset.Text)
            elif self.offset.Text == '0' or self.offset.Text == '-0':
                floatoffsetvalue=0
            FloatFlag = True
        except Exception:
			FloatFlag = False
        if FloatFlag == True:
            try:
                SplitWalls(walls=self.walls, levels=self.levels, offset=floatoffsetvalue)
            except:
                pass
        else:
            DavidEng.ErrorWindow(errortext="You entered an invalid value: '{}'".format(self.offset.Text)).ShowDialog()
            ChooseOffsetWindow('Offset.xaml', walls=self.walls, levels=self.levels).show(modal=True)	

    def cancel(self, sender, args):
        self.Close()

class SplitWalls(object):
    def __init__(self, walls, levels, offset):
        self.walls = walls
        self.levels = levels
        self.offset = offset
        outList = []
        footToCm = 30.48
        levels_list = []
        elevations_list = []
        for level in self.levels:
            levels_list.append(level)
            elevations_list.append(level.Elevation)
        zipped_lists = zip(elevations_list, levels_list)
        sorted_zipped_lists = sorted(zipped_lists)
        sorted_levels_list = [element for _, element in sorted_zipped_lists]
        max_value = len(self.walls)
        counter = 0
        t = Transaction(doc, "Split Walls")
        t.Start()
        with DavidEng.ProgressBar(title='Split Walls ... ({value} of {max_value})', cancellable=True) as pb:
            for w in self.walls:
                if pb.cancelled:
                    t.RollBack()
                    import sys
                    sys.exit()
                baseElevation, topElevation, continueFlag = self._WallElevation(w, footToCm)
                # Test whether walls Base and Top levels are NOT the same, if they are we will skip this wall, if they are not then we will get the Index of the Level in the sorted list of Levels we collected earlier for both the Base and Top of the wall...
                if continueFlag == True:
                    # Note: we are setting the bounds of the Loop below with the Indices of the found Levels so we will only loop through the Levels in between the Base and Top Levels...            
                    i = 0
                    RelevantLevels = []
                    RelevantLevels = self._FindLevels(sorted_levels_list, baseElevation, topElevation, footToCm)
                    j = len(RelevantLevels) - 1
                    
                    # Loop through the Levels between the Base and Top Levels copying the original wall for each iteration and stepping up one Level...
                    BaseFlag = False
                    TopFlag = True
                    try:
                        self._CopyWallByLevel(w, 0, RelevantLevels[i], BaseFlag, TopFlag, wallOffset = self.offset, topElevation=topElevation)
                        BaseFlag = True
                        while i < j:
                            self._CopyWallByLevel(w, RelevantLevels[i], RelevantLevels[i+1], BaseFlag, TopFlag, wallOffset = self.offset, topElevation=topElevation) 
                            i = i+1
                        TopFlag = False
                        self._CopyWallByLevel(w, RelevantLevels[i], 0, BaseFlag, TopFlag, wallOffset = self.offset, topElevation=topElevation)
                        # Delete original Wall as this has now been split by Level...
                        doc.Delete(w.Id)
                    except Exception, ex:
                        self
                counter += 1
                pb.update_progress(counter, max_value)
        t.Commit()

    # Returns the list of relevant Levels
    def _WallElevation(self, w, footToCm):
        # Get Base and Top Constraints as Levels...
        p = w.get_Parameter(BuiltInParameter.WALL_BASE_CONSTRAINT)
        base = doc.GetElement(p.AsElementId())
        baseConstraintElevation = base.Elevation*footToCm
        baseOffset = w.get_Parameter(BuiltInParameter.WALL_BASE_OFFSET).AsDouble()*footToCm
        baseElevation = baseConstraintElevation + baseOffset
        p = w.get_Parameter(BuiltInParameter.WALL_HEIGHT_TYPE)
        continueFlag = None
        if not p.AsValueString() == 'Unconnected':
            top = doc.GetElement(p.AsElementId())
            topConstraintElevation = top.Elevation*footToCm
            topOffset = w.get_Parameter(BuiltInParameter.WALL_TOP_OFFSET).AsDouble()*footToCm
            topElevation = topConstraintElevation + topOffset
            if not base.Id.IntegerValue == top.Id.IntegerValue:
                continueFlag = True
            else:
                continueFlag = False
        else:
            UnconnectedHeight = w.get_Parameter(BuiltInParameter.WALL_USER_HEIGHT_PARAM)
            topElevation = baseConstraintElevation + UnconnectedHeight.AsDouble()*footToCm
            continueFlag = True
        return baseElevation, topElevation, continueFlag

    # Returns the list of relevant Levels
    def _FindLevels(self, levels, baseElevation, topElevation, footToCm):
        LevelsList = []
        for l in levels:
            if l.Elevation*footToCm+self.offset > baseElevation and l.Elevation*footToCm+self.offset < topElevation:
                LevelsList.append(l)
        return LevelsList

    # Copy the original wall and set it's levels using the Built-In Parameters for the Base and Top Constraints...
    def _CopyWallByLevel(self, wall, b, t, bFlag, tFlag, wallOffset, topElevation):
        # Copy the Original Wall with a transformation vector of 0,0,0...
        w = ElementTransformUtils.CopyElement(doc,wall.Id,XYZ(0,0,0))
        # Since the CopyElements method returns the ElementId of the new wall we need to get this Element from the Document...
        w = doc.GetElement(w[0])
        # Update the Base and Top constraints Parameters using the Built-In Parameters.
        # Note: I have explicitly chosen the Overload as I was getting flaky behaviour where the wrong overload was being used...
        if bFlag == True:
            WallBaseConstraint = w.get_Parameter(BuiltInParameter.WALL_BASE_CONSTRAINT)
            WallBaseConstraint.Set.Overloads.Functions[2](b.Id)
            baseOffset = w.get_Parameter(BuiltInParameter.WALL_BASE_OFFSET).Set(wallOffset/30.48)
        if tFlag == True:
            WallTopConstraint = w.get_Parameter(BuiltInParameter.WALL_HEIGHT_TYPE)
            WallTopConstraint.Set.Overloads.Functions[2](t.Id)
            topOffset = w.get_Parameter(BuiltInParameter.WALL_TOP_OFFSET).Set(wallOffset/30.48)
        else:
            WallTopConstraint = w.get_Parameter(BuiltInParameter.WALL_HEIGHT_TYPE)
            if WallTopConstraint.AsValueString() == 'Unconnected':
                base = doc.GetElement(WallBaseConstraint.AsElementId())
                baseConstraintElevation = base.Elevation
                UnconnectedValue = topElevation/30.48 - baseConstraintElevation
                UserHeight = w.get_Parameter(BuiltInParameter.WALL_USER_HEIGHT_PARAM).Set(UnconnectedValue)

if (DavidEng.checkPermission()):
    # Run the Xaml
    ChooseWallsWindow('SelectWalls.xaml').show(modal=True)
else:
    DavidEng.ErrorWindow(errortext="You don't have permission\nto use the tool").ShowDialog()