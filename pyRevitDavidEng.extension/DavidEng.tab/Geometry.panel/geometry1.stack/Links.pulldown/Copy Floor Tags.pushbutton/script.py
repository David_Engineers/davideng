"""Copies selected Floor Tags from a linked project to the host project."""
# pylint: disable=E0401,W0703,W0613
from pyrevit import coreutils, UI, revit, DB, forms, script
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, BuiltInParameter, Transaction, XYZ, ElementId, RevitLinkInstance, ElementTransformUtils, IndependentTag, TagMode
from Autodesk.Revit.UI.Selection import ObjectType, ISelectionFilter
from Autodesk.Revit.UI import UIApplication, RevitCommandId, PostableCommand
from Autodesk.Revit import Exceptions
import DavidEng

from System.Collections.Generic import List

# dependencies
import clr
clr.AddReference('System.Windows.Forms')

# find the path of InstructionsWindow.xaml
InstructionsXamlFile = script.get_bundle_file('InstructionsWindow.xaml')

# import WPF creator and base Window
import wpf
from System import Windows

# App and Author name
__title__ = 'Copy Floor Tags'
__authors__ = 'Yuval Dolin'

# Identify the current Revit file
doc = __revit__.ActiveUIDocument.Document
uidoc = __revit__.ActiveUIDocument
uiapp = UIApplication(doc.Application)

# collect linked revit documents 
linkedDoc = FilteredElementCollector(doc, doc.ActiveView.Id).OfClass(RevitLinkInstance)

# if the user selected elements before runing the add-in
selection = [doc.GetElement(id) for id in uidoc.Selection.GetElementIds()]

# Define class to filter elements of category to select 
class CustomSelectionFilter(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, self.category).Id:
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

# Define class to filter elements of category to select 
class LinkCustomSelectionFilter(ISelectionFilter):
	def AllowElement(self, e):
		if e.Id == Link_selection.Id:
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		revitlinkinstance = doc.GetElement(ref)
		docLink = revitlinkinstance.GetLinkDocument()
		eScopeBoxLink = docLink.GetElement(ref.LinkedElementId)
		if (eScopeBoxLink.Category.Name == "Floor Tags"):
			return True

class InstructionsWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name):
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)

    def close(self, sender, args):
        self.Close()

    def settings(self, sender, args):
        self.Close()
        uiapp.PostCommand(RevitCommandId.LookupPostableCommandId(PostableCommand.VisibilityOrGraphics )) 

def SelectLink():
    if not selection:
        customFilter = CustomSelectionFilter(DB.BuiltInCategory.OST_RvtLinks)
        try:
            Link_selection_reference = uidoc.Selection.PickObject(ObjectType.Element, customFilter, "Select Link")
            Link_selection = doc.GetElement(Link_selection_reference)
            return Link_selection
        except Exceptions.OperationCanceledException:
            import sys
            sys.exit()
    else:
        Link_selection_list = []
        Link_selection = None
        for element in selection:
            if element.Category.Name == 'RVT Links':
                Link_selection_list.append(element)
        if Link_selection_list == []:
            DavidEng.ErrorWindow(errortext="You have not selected any Link").ShowDialog()
        elif len(Link_selection_list) > 1:
            DavidEng.ErrorWindow(errortext="You have to select only one Link").ShowDialog()
        else:
            Link_selection = Link_selection_list[0]
            return Link_selection

# create IndependentTag
def CreateNewTags(set_FloorTag_selection):
    FloorTagsCollector = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_FloorTags).WhereElementIsElementType()
    for Tag in set_FloorTag_selection:
        TagFamilyName = Tag.get_Parameter(BuiltInParameter.ELEM_FAMILY_PARAM).AsValueString()
        TagTypeName = Tag.get_Parameter(BuiltInParameter.ELEM_TYPE_PARAM).AsValueString()
        checkFlag = False
        for TagType in FloorTagsCollector:
            if TagType.FamilyName == TagFamilyName and TagType.get_Parameter(BuiltInParameter.SYMBOL_NAME_PARAM).AsString() == TagTypeName:
                ChangeTagType = TagType.Id
                checkFlag = True
                break
        if checkFlag == False:
            CopyTypeList = List[ElementId]()
            CopyTypeId = Tag.get_Parameter(BuiltInParameter.ELEM_TYPE_PARAM).AsElementId()
            CopyTypeList.Add(CopyTypeId)
            copy_3D = ElementTransformUtils.CopyElements(linkDoc, CopyTypeList, doc, None, None)
            ChangeTagType = copy_3D[0]
        BasisXX = transform.BasisX.X
        BasisXY = transform.BasisX.Y
        BasisYY = transform.BasisY.Y
        BasisYX = transform.BasisY.X
        Determinant = transform.Determinant
        linkActiveView = Tag.OwnerViewId
        TagReference = Tag.GetTaggedReference().CreateLinkReference(Link_selection)
        Orientation = Tag.TagOrientation
        TagXYZ = Tag.TagHeadPosition
        NewTagX = TransOrigin.X + (TagXYZ.X * BasisXX) + ((TagXYZ.Y * BasisXY) * -Determinant)
        NewTagY = TransOrigin.Y + (TagXYZ.Y * BasisYY) + ((TagXYZ.X * BasisYX) * -Determinant)
        NewTagXYZ = XYZ(NewTagX, NewTagY, 0)
        CreateTag = IndependentTag.Create(doc, doc.ActiveView.Id, TagReference, False, TagMode.TM_ADDBY_CATEGORY, Orientation, NewTagXYZ)
        CreateTag.ChangeTypeId(ChangeTagType)

# Define that V/G Overrides RVT Links won't be include in the view template
def TemplateLinkNotInclude():
    ActiveViewTemplateId = doc.ActiveView.ViewTemplateId
    if not ActiveViewTemplateId.IntegerValue < 0:
        ActiveViewTemplate = doc.GetElement(ActiveViewTemplateId)
        ActiveNotInclude = ActiveViewTemplate.GetNonControlledTemplateParameterIds()
        ParameterLinkId = doc.GetElement(ActiveViewTemplateId).LookupParameter("V/G Overrides RVT Links").Id
        ActiveNotInclude.Add(ParameterLinkId)
        ActiveViewTemplate.SetNonControlledTemplateParameterIds(ActiveNotInclude)

def CopyTags(FloorTag_selection):
    FloorTag_Id = []
    for FloorTag in FloorTag_selection:
        FloorTag_Id.append(FloorTag.Id)
    i = 0
    set_FloorTag_Id = []
    set_FloorTag_selection = []
    while i < len(FloorTag_Id):
        if not FloorTag_Id[i] in set_FloorTag_Id:
            set_FloorTag_Id.append(FloorTag_Id[i])
            set_FloorTag_selection.append(FloorTag_selection[i])
        i = i+1
    with revit.Transaction('Copy Floor Tags', log_errors=False):
        CreateNewTags(set_FloorTag_selection)
        TemplateLinkNotInclude()
    InstructionsWindow(InstructionsXamlFile).show(modal=True)

if (DavidEng.checkPermission()):
    if linkedDoc.GetElementCount() != 0:
        Link_selection = SelectLink()
        if not Link_selection == None:
            # get the linked document and transform 
            linkDoc = Link_selection.GetLinkDocument()
            transform = Link_selection.GetTotalTransform()
            TransOrigin = transform.Origin
            try:
                LinkCustomFilter = LinkCustomSelectionFilter()
                FloorTag_selection = [doc.GetElement(reference) and linkDoc.GetElement(reference.LinkedElementId) for reference in uidoc.Selection.PickObjects(
                    ObjectType.LinkedElement, LinkCustomFilter, "Select Floor Tags")]
            except Exception, ex:
                FloorTag_selection = None 
            if FloorTag_selection:
                CopyTags(FloorTag_selection)
            else:
                pass
else:
    DavidEng.ErrorWindow(errortext="You don't have permission\nto use the tool").ShowDialog()