"""Copies selected Dimensions from a linked project to the host project."""
# pylint: disable=E0401,W0703,W0613
import math
import System
from pyrevit import coreutils, UI, revit, DB, forms, script
from pyrevit.framework import List
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, BuiltInParameter, Transaction, XYZ, ElementId, RevitLinkInstance, ReferenceArray, DimensionType, Line, \
                              Reference, FilledRegion, ElementTransformUtils, ViewPlan
from Autodesk.Revit.UI.Selection import ObjectType, ISelectionFilter
from Autodesk.Revit.UI import UIApplication, RevitCommandId, PostableCommand
from Autodesk.Revit import Exceptions
import DavidEng

# dependencies
import clr
clr.AddReference('System.Windows.Forms')

# find the path of InstructionsWindow.xaml
InstructionsXamlFile = script.get_bundle_file('InstructionsWindow.xaml')

# import WPF creator and base Window
import wpf
from System import Windows

# App and Author name
__title__ = 'Copy Dimensions'
__authors__ = 'Yuval Dolin'

# Identify the current Revit file
doc = __revit__.ActiveUIDocument.Document
uidoc = __revit__.ActiveUIDocument
uiapp = UIApplication(doc.Application)

# collect linked revit documents 
linkedDoc = FilteredElementCollector(doc, doc.ActiveView.Id).OfClass(RevitLinkInstance)

# if the user selected elements before runing the add-in
selection = [doc.GetElement(id) for id in uidoc.Selection.GetElementIds()]

# Define class to filter elements of category to select 
class CustomSelectionFilter(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, self.category).Id:
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

# Define class to filter elements of category to select 
class LinkCustomSelectionFilter(ISelectionFilter):
    def AllowElement(self, e):
		if e.Id == Link_selection.Id:
			return True
    def AllowReference(self, ref, point):
        revitlinkinstance = doc.GetElement(ref)
        docLink = revitlinkinstance.GetLinkDocument()
        eScopeBoxLink = docLink.GetElement(ref.LinkedElementId)
        if (eScopeBoxLink.Category.Name == "Dimensions"):
            return True

class InstructionsWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name):
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)

    def close(self, sender, args):
        self.Close()

    def settings(self, sender, args):
        self.Close()
        uiapp.PostCommand(RevitCommandId.LookupPostableCommandId(PostableCommand.VisibilityOrGraphics )) 

def SelectLink():
    if not selection:
        customFilter = CustomSelectionFilter(DB.BuiltInCategory.OST_RvtLinks)
        try:
            Link_selection_reference = uidoc.Selection.PickObject(ObjectType.Element, customFilter, "Select Link")
            Link_selection = doc.GetElement(Link_selection_reference)
            return Link_selection
        except Exceptions.OperationCanceledException:
            import sys
            sys.exit()
    else:
        Link_selection_list = []
        Link_selection = None
        for element in selection:
            if element.Category.Name == 'RVT Links':
                Link_selection_list.append(element)
        if Link_selection_list == []:
            DavidEng.ErrorWindow(errortext="You have not selected any Link").ShowDialog()
        elif len(Link_selection_list) > 1:
            DavidEng.ErrorWindow(errortext="You have to select only one Link").ShowDialog()
        else:
            Link_selection = Link_selection_list[0]
            return Link_selection

def CreateXYZ(refPoint, AddValue, origin):
    refCheckLine = Line.CreateBound(origin, refPoint)
    refCheckLineLength = refCheckLine.Length
    refLine = refCheckLineLength + AddValue
    refLineX = ((refLine * (refPoint.X - origin.X))/refCheckLineLength)+origin.X
    refLineY = ((refLine * (refPoint.Y - origin.Y))/refCheckLineLength)+origin.Y
    refLineZ = ((refLine * (refPoint.Z - origin.Z))/refCheckLineLength)+origin.Z
    refLineXYZ = XYZ(refLineX, refLineY, refLineZ)
    return refLineXYZ

def DimType(Dimension, DimensionTypes):
    TagFamilyName = Dimension.get_Parameter(BuiltInParameter.ELEM_FAMILY_PARAM).AsValueString()
    TagTypeName = Dimension.get_Parameter(BuiltInParameter.ELEM_TYPE_PARAM).AsValueString()
    checkFlag = False
    for dimensiontype in DimensionTypes:
        if dimensiontype.FamilyName == TagFamilyName and dimensiontype.get_Parameter(BuiltInParameter.SYMBOL_NAME_PARAM).AsString() == TagTypeName:
            ChangeDimensionType = dimensiontype
            checkFlag = True
            break
    if checkFlag == False:
        CopyTypeList = List[ElementId]()
        CopyTypeId = Dimension.get_Parameter(BuiltInParameter.ELEM_TYPE_PARAM).AsElementId()
        CopyTypeList.Add(CopyTypeId)
        copy = ElementTransformUtils.CopyElements(linkDoc, CopyTypeList, doc, None, None)
        ChangeDimensionType = doc.GetElement(copy[0])
    return ChangeDimensionType

def DimReference(Dimension):
    DimensionOrigin = Dimension.Curve.Origin
    DimensionDirection = Dimension.Curve.Direction
    NewDimensionX = TransOrigin.X + (DimensionOrigin.X * BasisXX) + ((DimensionOrigin.Y * BasisXY) * -Determinant)
    NewDimensionY = TransOrigin.Y + (DimensionOrigin.Y * BasisYY) + ((DimensionOrigin.X * BasisYX) * -Determinant)
    NewDimensionXYZ = XYZ(NewDimensionX, NewDimensionY, 0)
    NewDirectionX = (DimensionDirection.X * BasisXX) + ((DimensionDirection.Y * BasisXY) * -Determinant)
    NewDirectionY = (DimensionDirection.Y * BasisYY) + ((DimensionDirection.X * BasisYX) * -Determinant)
    NewDirectionXYZ = XYZ(NewDirectionX, NewDirectionY, 0)
    refLine = Line.CreateUnbound(NewDimensionXYZ, NewDirectionXYZ)
    return NewDimensionXYZ, NewDirectionXYZ, refLine

def DimDetailItem(Dimension, Count, NewDirectionXYZ, NewDimensionXYZ, DetailLinesList):
    DimensionSegments = Dimension.Segments
    NewLineRef = None
    try:
        if Count >= 0:
            SegmentValue = DimensionSegments[Count].Value
            SegmentOrigin = DimensionSegments[Count].Origin
            SegmentX = TransOrigin.X + (SegmentOrigin.X * BasisXX) + ((SegmentOrigin.Y * BasisXY) * -Determinant)
            SegmentY = TransOrigin.Y + (SegmentOrigin.Y * BasisYY) + ((SegmentOrigin.X * BasisYX) * -Determinant)
            SegmentRefPointX = SegmentX + SegmentValue*(NewDirectionXYZ.X/2)
            SegmentRefPointY = SegmentY + SegmentValue*(NewDirectionXYZ.Y/2)
        else:
            SegmentValue = DimensionSegments[0].Value
            SegmentOrigin = DimensionSegments[0].Origin
            SegmentX = TransOrigin.X + (SegmentOrigin.X * BasisXX) + ((SegmentOrigin.Y * BasisXY) * -Determinant)
            SegmentY = TransOrigin.Y + (SegmentOrigin.Y * BasisYY) + ((SegmentOrigin.X * BasisYX) * -Determinant)
            SegmentRefPointX = SegmentX - SegmentValue*(NewDirectionXYZ.X/2)
            SegmentRefPointY = SegmentY - SegmentValue*(NewDirectionXYZ.Y/2)
        SegmentRefPointXYZ = XYZ(SegmentRefPointX, SegmentRefPointY, 0)
        refLinePlusXYZ = CreateXYZ(SegmentRefPointXYZ, 0.1, NewDimensionXYZ)
        refLineMinusXYZ = CreateXYZ(SegmentRefPointXYZ, -0.1, NewDimensionXYZ)
        NewLine = Line.CreateBound(refLinePlusXYZ, refLineMinusXYZ)
        CreateNewLine = doc.Create.NewDetailCurve(doc.ActiveView, NewLine)
        AxisLine = Line.CreateUnbound(SegmentRefPointXYZ, doc.ActiveView.CropBox.Transform.BasisZ)
        ElementTransformUtils.RotateElement(doc, CreateNewLine.Id, AxisLine, math.pi / 2)
        NewLineRef = CreateNewLine.GeometryCurve.Reference
        DetailLinesList.append(CreateNewLine)
    except:
        pass
    return NewLineRef, DetailLinesList

def StableRepresentation(ref):
    refString = ref.CreateLinkReference(Link_selection).ConvertToStableRepresentation(doc)
    ReplaceRVTLINK = refString.replace(":RVTLINK/", ":0:RVTLINK:")
    refObject = ref.CreateLinkReference(Link_selection).ParseFromStableRepresentation(doc, ReplaceRVTLINK)
    LinkTypeId = Link_selection.GetTypeId()
    refString = refObject.ConvertToStableRepresentation(doc)
    ReplaceLinkTypeId = refString.replace(str(LinkTypeId)+":", "")
    FinalRefObject = ref.CreateLinkReference(Link_selection).ParseFromStableRepresentation(doc, ReplaceLinkTypeId)
    return FinalRefObject

def CreatExtensionStorage(DetailLine, DimensionId):
    schemaBuilder=DB.ExtensibleStorage.SchemaBuilder(System.Guid("d64e3bab-7b93-415a-a20a-4d09ec106aad"))
    schemaBuilder.SetReadAccessLevel(DB.ExtensibleStorage.AccessLevel.Public)
    schemaBuilder.SetWriteAccessLevel(DB.ExtensibleStorage.AccessLevel.Public)
    schemaBuilder.SetVendorId("YuvalDolin")
    schemaBuilder.SetSchemaName("DavidEng_Schema_DimensionByLine")
    fieldBuilder = schemaBuilder.AddSimpleField("Id_Dimension", DetailLine.UniqueId.GetType())
    fieldBuilder.SetDocumentation("The Id of the associated dimension")
    schema = schemaBuilder.Finish()
    
    entity =DB.ExtensibleStorage.Entity(schema)
    fieldSpliceLocation = schema.GetField("Id_Dimension")
    entity.Set(fieldSpliceLocation, str(DimensionId))
    DetailLine.SetEntity(entity)

def CopyDimensions(Dimensions_selection):
    DimensionTypes = FilteredElementCollector(doc).OfClass(DimensionType)
    for Dimension in Dimensions_selection:
        refArray = ReferenceArray()
        ChangeDimensionType = DimType(Dimension, DimensionTypes)
        NewDimensionXYZ, NewDirectionXYZ, refLine = DimReference(Dimension)
        RefArray = Dimension.References
        Count = -1
        DetailLinesList = []
        for ref in RefArray:
            refId = ref.ElementId
            refElement = linkDoc.GetElement(refId)
            if refElement.Category.Name == "Detail Items" and type(refElement) != FilledRegion:
                NewLineRef, DetailLinesList = DimDetailItem(Dimension, Count, NewDirectionXYZ, NewDimensionXYZ, DetailLinesList)
                if NewLineRef != None:
                    refArray.Append(NewLineRef)
            elif refElement.Category.Name == "Model Groups":
                NewLineRef, DetailLinesList = DimDetailItem(Dimension, Count, NewDirectionXYZ, NewDimensionXYZ, DetailLinesList)
                if NewLineRef != None:
                    refArray.Append(NewLineRef)
            else:
                try:
                    FinalRefObject = StableRepresentation(ref)
                    refArray.Append(FinalRefObject)
                except:
                    pass
            Count += 1
        try:
            CreateDimension = doc.Create.NewDimension(doc.ActiveView, refLine, refArray, ChangeDimensionType)
            for DetailLine in DetailLinesList:
                CreatExtensionStorage(DetailLine, CreateDimension.Id)
        except:
            pass
    InstructionsWindow(InstructionsXamlFile).show(modal=True)

# Define that V/G Overrides RVT Links won't be include in the view template
def TemplateRvtLinkNotInclude():
    ActiveViewTemplateId = doc.ActiveView.ViewTemplateId
    if not ActiveViewTemplateId.IntegerValue < 0:
        ActiveViewTemplate = doc.GetElement(ActiveViewTemplateId)
        ActiveNotInclude = ActiveViewTemplate.GetNonControlledTemplateParameterIds()
        ParameterLinkId = doc.GetElement(ActiveViewTemplateId).LookupParameter("V/G Overrides RVT Links").Id
        ActiveNotInclude.Add(ParameterLinkId)
        ActiveViewTemplate.SetNonControlledTemplateParameterIds(ActiveNotInclude)

if (DavidEng.checkPermission()):
    if type(doc.ActiveView) == ViewPlan:
        if linkedDoc.GetElementCount() != 0:
            Link_selection = SelectLink()
            if not Link_selection == None:
                # get the linked document and transform 
                linkDoc = Link_selection.GetLinkDocument()
                transform = Link_selection.GetTotalTransform()
                TransOrigin = transform.Origin
                Determinant = transform.Determinant
                BasisXX = transform.BasisX.X
                BasisXY = transform.BasisX.Y
                BasisYY = transform.BasisY.Y
                BasisYX = transform.BasisY.X
                try:
                    LinkCustomFilter = LinkCustomSelectionFilter()
                    Dimensions_selection = [doc.GetElement(reference) and linkDoc.GetElement(reference.LinkedElementId) for reference in uidoc.Selection.PickObjects(
                        ObjectType.LinkedElement, LinkCustomFilter, "Select Dimensions")]
                except Exception, ex:
                    Dimensions_selection = None 
                if Dimensions_selection:
                    t = Transaction(doc, "Copy Dimensions from Linked Model")
                    t.Start()
                    TemplateRvtLinkNotInclude()
                    CopyDimensions(Dimensions_selection)
                    t.Commit()
                else:
                    pass
    else:
        DavidEng.ErrorWindow(errortext="You can only run this\nadd-in on Plan Views").ShowDialog()
else:
    DavidEng.ErrorWindow(errortext="You don't have permission\nto use the tool").ShowDialog()