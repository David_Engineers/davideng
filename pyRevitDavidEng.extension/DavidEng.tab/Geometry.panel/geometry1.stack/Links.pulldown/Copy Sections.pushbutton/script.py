# # -*- coding: utf-8 -*-

"""Version: 1.1.4
Copy sections from linked model to the current model."""
# pylint: disable=E0401,W0703,W0613
import imp
import math
import re
import System
import json
from System import String
from System.Collections.Generic import IDictionary, Dictionary
from pyrevit import revit, DB, forms, script
from pyrevit.revit.db import query
from pyrevit import framework
from pyrevit import PyRevitException
from pyrevit import HOST_APP
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, BuiltInParameter, Transaction, XYZ, ElementId, RevitLinkInstance, ViewSection, ElementTransformUtils, \
                              ViewPlan, ReferenceableViewUtils, SketchPlane
from Autodesk.Revit.UI.Selection import ObjectType, ISelectionFilter
from Autodesk.Revit import Exceptions
import DavidEng

from System.Collections.Generic import List

# dependencies
import clr
clr.AddReference('System.Windows.Forms')

# find the path of InstructionsWindow.xaml
InstructionsXamlFile = script.get_bundle_file('InstructionsWindow.xaml')

# find the path of SelectViewWindow.xaml
SelectViewXamlFile = script.get_bundle_file('SelectViewWindow.xaml')

# find the path of SelectLinkWindow.xaml
SelectLinkXamlFile = script.get_bundle_file('SelectLinkWindow.xaml')

# find the path of ErrorWindow.xaml
alertXamlFile = script.get_bundle_file("AlertWindowWithHebrew.xaml")

# import WPF creator and base Window
import wpf
from System import Windows

# App and Author name
__title__ = 'Copy Sections'
__authors__ = 'Yuval Dolin'

# Identify the current Revit file
doc = __revit__.ActiveUIDocument.Document
uidoc = __revit__.ActiveUIDocument

# collect linked revit documents 
linkedDoc = FilteredElementCollector(doc, doc.ActiveView.Id).OfClass(RevitLinkInstance)

# get the extensible storage schma
schemaGuid = "e64e3bab-7b93-415a-a20a-4d09ec106eaf"
schemaSections = DB.ExtensibleStorage.Schema.Lookup(System.Guid(schemaGuid))

viewSectionFilter = DB.ElementClassFilter(ViewSection)
sketchPlaneFilter = DB.ElementClassFilter(SketchPlane)

# Define class to filter elements of category to select 
class CustomSelectionFilter(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, self.category).Id:
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

# Show Alert Window Form
class AlertWindow(Windows.Window):
    def __init__(self, alertText, alertTextHebrew):
        # indicate if the user decided to continue anyway
        global continueToCopy
        continueToCopy = False
        self.alertText = alertText
        self.alertTextHebrew = alertTextHebrew
        wpf.LoadComponent(self, alertXamlFile)
        self.alert.Text = "{}".format(self.alertText)
        self.alertHebrew.Text = "{}".format(self.alertTextHebrew)

    def yes(self, sender, args):
        self.Close()
        # indicate if the user decided to continue anyway
        global continueToCopy
        continueToCopy = True
        
    def no(self, sender, args):
        self.Close()

class InstructionsWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name):
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)

    def close(self, sender, args):
        self.Close()

class CopyUseDestination(DB.IDuplicateTypeNamesHandler):
    def OnDuplicateTypeNamesFound(self, args):
        return DB.DuplicateTypeAction.UseDestinationTypes

class ChooseLinkModelWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name):
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)
        self._setup_linked_names()

    def cancel(self, sender, args):
        self.Close()

    def click_ok(self, sender, args):
        self.Close()
        
        global selectedLinkedModel
        selectedLinkedModel = self.selected_linkedmodel

    @property
    def selected_linkedmodel(self):
        selected_name = self.link_model.SelectedItem
        for linkedModel in linkedModelsList:
            if selected_name == linkedModel.get_Parameter(BuiltInParameter.ELEM_TYPE_PARAM).AsValueString():
                return linkedModel
        return None

    # Setup the Load Cases for the ComboBox (list of Load Cases)
    def _setup_linked_names(self):
        linkNamesList = []
        for linkedModel in linkedModelsList:
            linkName = linkedModel.get_Parameter(BuiltInParameter.ELEM_TYPE_PARAM).AsValueString()
            linkNamesList.append(linkName)
        self.link_model.ItemsSource = linkNamesList
        self.link_model.SelectedIndex = 0
        
        # Enable or disable OK button based on sourceLinkViews list
        self.ok_button.IsEnabled = bool(linkNamesList)
        
    def SearchTextBox_TextChanged(self, sender, args):
        search_text = self.searchTextBox.Text.lower()
        filtered_linked_models = [linkedModel for linkedModel in linkedModelsList if search_text in linkedModel.get_Parameter(BuiltInParameter.ELEM_TYPE_PARAM).AsValueString().lower()]
        linkNamesList = [linkedModel.get_Parameter(BuiltInParameter.ELEM_TYPE_PARAM).AsValueString() for linkedModel in filtered_linked_models]
        self.link_model.ItemsSource = linkNamesList
        self.link_model.SelectedIndex = 0
        
        # Enable or disable OK button based on filtered linked models list
        self.ok_button.IsEnabled = bool(linkNamesList)

class ChooseViewPlanWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name):
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)
        self._setup_viewplans()

    def cancel(self, sender, args):
        self.Close()

    def click_ok(self, sender, args):
        self.Close()
        
        LinkViewsCollectSections = sectionsCollector(linkDoc, self.selected_viewplan.Id)
        
        if LinkViewsCollectSections:
            SectionsToCopy = []
            sectionName = []
            sectionDependentName = []
            linkDependentSection = []
            SectionsList = []
            depSectionsUniqueId = []
            linkDepSectionsUniqueId = []
            for section in LinkViewsCollectSections:
                # Continue only if the section is not reference to other view
                if section.OwnerViewId.IntegerValue == -1:
                    dependencyValue = section.LookupParameter("Dependency").AsString()
                    # Check if the section is depentent or not (the user can "Duplicate as a Dependent" the section)
                    if not dependencyValue.startswith('Dependent on '):
                        SectionsToCopy.append(section.Id)
                        SectionsList.append(section)
                    else:
                        sectionName.append(section.Name)
                        sectionDependentName.append(dependencyValue.replace('Dependent on ', '', 1))
                        linkDependentSection.append(section)
                        depSectionsUniqueId.append(section.UniqueId)
                        linkDepSectionsUniqueId.append(section.UniqueId)

            SectionsToCopySorted = List[ElementId]()
            zipped_lists = zip(SectionsToCopy, SectionsList)
            sorted_zipped_lists = sorted(zipped_lists)
            sorted_sections_list = [element for _, element in sorted_zipped_lists]
            sortedSectionsUniqueId = []
            linkOriginalSectionsUniqueId = []
            for secToCopy in sorted_sections_list:
                SectionsToCopySorted.Add(secToCopy.Id)
                sortedSectionsUniqueId.append(secToCopy.UniqueId)
                linkOriginalSectionsUniqueId.append(secToCopy.UniqueId.ToString())

            options = DB.CopyPasteOptions()
            options.SetDuplicateTypeNamesHandler(CopyUseDestination())
            CopySections = None
            
            # Transform for temporary section in order to be able to copy rotated reference sections
            t = DB.Transform.Identity
            t.Origin = XYZ(0, 0, 0)
            t.BasisX = XYZ(-1, 0, 0)
            t.BasisY = XYZ(0, 0, 1)
            t.BasisZ = XYZ(0, 1, 0)
            section_box = DB.BoundingBoxXYZ()
            section_box.Transform = t
            section_box.Min = DB.XYZ(-10, -10, -10)
            section_box.Max = DB.XYZ(10, 10, 10)

            # Split between reference section and original and dependent sections
            refSections = []
            origionalSections = []
            for linkSec in LinkViewsCollectSections:
                if linkSec.ViewSpecific == True:
                    refSections.append(linkSec)
                else:
                    origionalSections.append(linkSec)

            # collect sections from current model without the new ones
            CurrentModelSections = sectionsCollector(doc, None)
            
            AllOrigionalSections = []
            AllUniqueIds = []
            for sec in CurrentModelSections:
                if sec.ViewSpecific == False:
                    try:
                        retrievedEntity = sec.GetEntity(schemaSections)
                        fieldSection = schemaSections.GetField("CopySectionsData")
                        retrievedDataSection =retrievedEntity.Get[IDictionary[str, str]](fieldSection)
                        retrievedDataSectionID = retrievedDataSection["Link_ElementUniqueIdData"]
                        AllUniqueIds.append(retrievedDataSectionID)
                        AllOrigionalSections.append(sec)
                    except:
                        pass
            
            linkSecCropBoxMinDataList = []
            linkSecCropBoxMaxDataList = []
            linkSecFarClipOffsetList = []
            linkSecLocationList = []
            linkSecNameList = []
            linkSecOrientationList = []
            elementBBoxMaxDataList = []
            elementBBoxMinDataList = []
            linkElementBBoxMaxDataList = []
            linkElementBBoxMinDataList = []
            elementLocationDataList = []
            elementOrientationList = []
            
            # copy sections
            max_value = len(SectionsToCopySorted) + len(refSections)
            counter = 0
            t = Transaction(doc, 'Copy Sections from Linked Model')
            t.Start()
            with DavidEng.ProgressBar(title='Copy Sections ... ({value} of {max_value})', cancellable=True) as pb:
                copySectionsCounter = 0
                
                SetViewRange(self.selected_viewplan)
                    
                try:
                    CopySections = []
                    for sec in SectionsToCopySorted:
                        if pb.cancelled:
                            t.RollBack()
                            import sys
                            sys.exit()
                        sectionToCopy = List[ElementId]()
                        sectionToCopy.Add(sec)
                        
                        # Copy the sections that are not dependent or references to other views
                        transformForSec = Main_link_selection.GetTotalTransform()
                        CopySection = ElementTransformUtils.CopyElements(linkDoc, sectionToCopy, doc, transformForSec, options)
                        CopySections.append(CopySection[0])
                        
                        originalViewSectionList = linkDoc.GetElement(sec).GetDependentElements(viewSectionFilter)
                        originalViewSection = originalViewSectionList[0]
                        cropBoxMin = linkDoc.GetElement(originalViewSection).CropBox.Min
                        cropBoxMinRound = [round(cropBoxMin.X, 5), round(cropBoxMin.Y, 5),  round(cropBoxMin.Z, 5)]
                        linkSecCropBoxMinDataList.append(cropBoxMinRound)
                        
                        cropBoxMax = linkDoc.GetElement(originalViewSection).CropBox.Max
                        cropBoxMaxRound = [round(cropBoxMax.X, 5), round(cropBoxMax.Y, 5),  round(cropBoxMax.Z, 5)]
                        linkSecCropBoxMaxDataList.append(cropBoxMaxRound)
                        
                        farClipOffset = linkDoc.GetElement(sec).get_Parameter(BuiltInParameter.VIEWER_BOUND_OFFSET_FAR).AsDouble()*30.48
                        linkSecFarClipOffsetList.append(round(farClipOffset, 2))
                        
                        linkSecLocation = linkDoc.GetElement(sec).GetDependentElements(sketchPlaneFilter)
                        linkSecLocationPlane = linkDoc.GetElement(linkSecLocation[0]).GetPlane()
                        linkSecLocationOrigin = linkSecLocationPlane.Origin
                        linkSecLocationRound = [round(linkSecLocationOrigin.X, 5), round(linkSecLocationOrigin.Y, 5),  round(linkSecLocationOrigin.Z, 5)]
                        linkSecLocationList.append(linkSecLocationRound)
                        
                        linkSecNameList.append(linkDoc.GetElement(sec).Name)
                        
                        linkSecOrientationXVector = linkSecLocationPlane.XVec
                        linkSecOrientationRound = [round(linkSecOrientationXVector.X, 5), round(linkSecOrientationXVector.Y, 5),  round(linkSecOrientationXVector.Z, 5)]
                        linkSecOrientationList.append(linkSecOrientationRound)
                        
                        secElement = doc.GetElement(CopySection[0])
                        
                        # Set the 'Hide at scales coarser than' value to be the maximum
                        secElement.get_Parameter(BuiltInParameter.SECTION_COARSER_SCALE_PULLDOWN_METRIC).Set(5000)
                        
                        if not secElement.get_BoundingBox(doc.ActiveView) == None:
                            bboxMax = secElement.get_BoundingBox(doc.ActiveView).Max
                            bboxMaxRound = [round(bboxMax.X, 5), round(bboxMax.Y, 5),  round(bboxMax.Z, 5)]
                            elementBBoxMaxDataList.append(bboxMaxRound)

                            bboxMin = secElement.get_BoundingBox(doc.ActiveView).Min
                            bboxMinRound = [round(bboxMin.X, 5), round(bboxMin.Y, 5),  round(bboxMin.Z, 5)]
                            elementBBoxMinDataList.append(bboxMinRound)
                        else:
                            elementBBoxMaxDataList.append(None)
                            elementBBoxMinDataList.append(None)
                        
                        linkSecElement = linkDoc.GetElement(sec)
                        linkBboxMax = linkSecElement.get_BoundingBox(self.selected_viewplan).Max
                        linkBboxMaxRound = [round(linkBboxMax.X, 5), round(linkBboxMax.Y, 5),  round(linkBboxMax.Z, 5)]
                        linkElementBBoxMaxDataList.append(linkBboxMaxRound)

                        linkBboxMin = linkSecElement.get_BoundingBox(self.selected_viewplan).Min
                        linkBboxMinRound = [round(linkBboxMin.X, 5), round(linkBboxMin.Y, 5),  round(linkBboxMin.Z, 5)]
                        linkElementBBoxMinDataList.append(linkBboxMinRound)
                        
                        secLocation = secElement.GetDependentElements(sketchPlaneFilter)
                        secLocationPlane = doc.GetElement(secLocation[0]).GetPlane()
                        secLocationOrigin = secLocationPlane.Origin
                        secLocationRound = [round(secLocationOrigin.X, 5), round(secLocationOrigin.Y, 5),  round(secLocationOrigin.Z, 5)]
                        elementLocationDataList.append(secLocationRound)
                        
                        secOrientationXVector = secLocationPlane.XVec
                        secOrientationRound = [round(secOrientationXVector.X, 5), round(secOrientationXVector.Y, 5),  round(secOrientationXVector.Z, 5)]
                        elementOrientationList.append(secOrientationRound)
                        
                        counter += 1
                        pb.update_progress(counter, max_value)
                        revit.uidoc.RefreshActiveView()
                        doc.Regenerate()
                        copySectionsCounter += 1
                except:
                    sortedSectionsUniqueId = []
                
                if counter == len(SectionsToCopySorted):    
                    if copySectionsCounter == len(SectionsToCopySorted):
                        
                        # Change the name of the dependent sections if the name of the original one was changed
                        i1 = 0
                        if not (len(sectionDependentName) == 0):
                            while (i1 < len(CopySections)):
                                if (doc.GetElement(CopySections[i1]).Name != linkDoc.GetElement(SectionsToCopySorted[i1]).Name):
                                    i2 = 0
                                    while (i2 < len(sectionDependentName)):
                                        if (linkDoc.GetElement(SectionsToCopySorted[i1]).Name == sectionDependentName[i2]):
                                            sectionDependentName[i2] = doc.GetElement(CopySections[i1]).Name
                                        i2 += 1
                                i1 += 1
                            
                        dependentSectionsList, finalDepSectionsUniqueId, finalLinkDepSectionsUniqueId, depElementBBoxMaxDataList, depElementBBoxMinDataList = CopyDependentSections(CopySections, sectionName, sectionDependentName, depSectionsUniqueId, linkDepSectionsUniqueId)
                        
                        elementBBoxMaxDataList += depElementBBoxMaxDataList
                        elementBBoxMinDataList += depElementBBoxMinDataList
                        
                        i = 0
                        while (i < len(linkDependentSection)):                    
                            originalViewSectionList = linkDependentSection[i].GetDependentElements(viewSectionFilter)
                            originalViewSection = originalViewSectionList[0]
                            cropBoxMin = linkDoc.GetElement(originalViewSection).CropBox.Min
                            cropBoxMinRound = [round(cropBoxMin.X, 5), round(cropBoxMin.Y, 5),  round(cropBoxMin.Z, 5)]
                            linkSecCropBoxMinDataList.append(cropBoxMinRound)
                            cropBoxMax = linkDoc.GetElement(originalViewSection).CropBox.Max
                            cropBoxMaxRound = [round(cropBoxMax.X, 5), round(cropBoxMax.Y, 5),  round(cropBoxMax.Z, 5)]
                            linkSecCropBoxMaxDataList.append(cropBoxMaxRound)
                            linkSecFarClipOffsetList.append(None)
                            linkSecLocationList.append(None)
                            linkSecNameList.append(linkDependentSection[i].Name)
                            linkSecOrientationList.append(None)
                            
                            linkDepSecElement = linkDependentSection[i]
                            linkBboxMax = linkDepSecElement.get_BoundingBox(self.selected_viewplan).Max
                            linkBboxMaxRound = [round(linkBboxMax.X, 5), round(linkBboxMax.Y, 5),  round(linkBboxMax.Z, 5)]
                            linkElementBBoxMaxDataList.append(linkBboxMaxRound)

                            linkBboxMin = linkDepSecElement.get_BoundingBox(self.selected_viewplan).Min
                            linkBboxMinRound = [round(linkBboxMin.X, 5), round(linkBboxMin.Y, 5),  round(linkBboxMin.Z, 5)]
                            linkElementBBoxMinDataList.append(linkBboxMinRound)
                            
                            elementLocationDataList.append(None)
                            elementOrientationList.append(None)
                            
                            i += 1
                        
                        sortedSectionsUniqueId = sortedSectionsUniqueId + finalDepSectionsUniqueId
                        copiedSections = []
                        for copiedSec in CopySections:
                            copiedSections.append(doc.GetElement(copiedSec))
                        copiedSections = copiedSections + dependentSectionsList
                        
                        i = 0
                        for copiedSec in copiedSections:
                            # collect (add) only the new sections from current model
                            AllUniqueIds.append(sortedSectionsUniqueId[i])
                            AllOrigionalSections.append(copiedSec)
                            i += 1

                        # Create temporary section in order to be able to copy rotated reference sections
                        section_type = doc.GetDefaultElementTypeId(DB.ElementTypeGroup.ViewTypeSection)
                        tempSection = DB.ViewSection.CreateSection(doc, section_type, section_box)
                        
                        # Copy reference sections
                        # collect sections in linked model
                        LinkSections = sectionsCollector(linkDoc, None)

                        AllLinkOrigionalSections = []
                        for sec in LinkSections:
                            if sec.ViewSpecific == False:
                                AllLinkOrigionalSections.append(sec)
                        
                        linkRefSectionsUniqueId = []
                        
                        # Create temporary level so we will be able to copy the reference sections (relevant when the Origin.Z of the GetTotalTransform is not 0)
                        linkViewProjectElevation = self.selected_viewplan.GenLevel.ProjectElevation
                        tempLevel = DB.Level.Create(doc, linkViewProjectElevation)
                        
                        for refSec in refSections:
                            secToRefer = None
                            if pb.cancelled:
                                t.RollBack()
                                import sys
                                sys.exit()
                            refSecName = refSec.Name
                            for oriLinkSec in AllLinkOrigionalSections:
                                if refSecName == oriLinkSec.Name:
                                    refUniqueId = oriLinkSec.UniqueId
                                    break
                            i = 0
                            for oriUniqueId in AllUniqueIds:
                                if oriUniqueId == refUniqueId:
                                    secToRefer = AllOrigionalSections[i]
                                    break
                                i += 1
                            # if the section to refer exist in the current model
                            if secToRefer:
                                viewIdToReferenceList = secToRefer.GetDependentElements(viewSectionFilter)
                                viewIdToReference = viewIdToReferenceList[0]
                                LinkViewPlan = self.selected_viewplan
                                dependencyValue = LinkViewPlan.LookupParameter("Dependency").AsString()
                                if dependencyValue.startswith('Dependent on '):
                                    sectionDependentName = dependencyValue.replace('Dependent on ', '', 1)
                                    for view in LinkViews:
                                        if sectionDependentName == view.Name:
                                            LinkViewPlan = view
                                            break

                                ids_2D = List[ElementId]()
                                ids_2D.Add(refSec.Id)
                                copy_2D = ElementTransformUtils.CopyElements(LinkViewPlan, ids_2D, doc.ActiveView, transform_Z_2D, None)
                                ReferenceableViewUtils.ChangeReferencedView(doc, copy_2D[0], viewIdToReference)
                                linkRefSectionsUniqueId.append(refSec.UniqueId.ToString())
                                copiedSections.append(doc.GetElement(copy_2D[0]))
                                
                                linkSecCropBoxMinDataList.append(None)
                                linkSecCropBoxMaxDataList.append(None)
                                linkSecFarClipOffsetList.append(None)
                                linkSecLocationList.append(None)
                                linkSecNameList.append(doc.GetElement(copy_2D[0]).Name)
                                linkSecOrientationList.append(None)
                                
                                secElement = doc.GetElement(copy_2D[0])
                                if not secElement.get_BoundingBox(doc.ActiveView) == None:
                                    bboxMax = secElement.get_BoundingBox(doc.ActiveView).Max
                                    bboxMaxRound = [round(bboxMax.X, 5), round(bboxMax.Y, 5),  round(bboxMax.Z, 5)]
                                    elementBBoxMaxDataList.append(bboxMaxRound)

                                    bboxMin = secElement.get_BoundingBox(doc.ActiveView).Min
                                    bboxMinRound = [round(bboxMin.X, 5), round(bboxMin.Y, 5),  round(bboxMin.Z, 5)]
                                    elementBBoxMinDataList.append(bboxMinRound)
                                else:
                                    elementBBoxMaxDataList.append(None)
                                    elementBBoxMinDataList.append(None)
                                
                                linkBboxMax = refSec.get_BoundingBox(self.selected_viewplan).Max
                                linkBboxMaxRound = [round(linkBboxMax.X, 5), round(linkBboxMax.Y, 5),  round(linkBboxMax.Z, 5)]
                                linkElementBBoxMaxDataList.append(linkBboxMaxRound)

                                linkBboxMin = refSec.get_BoundingBox(self.selected_viewplan).Min
                                linkBboxMinRound = [round(linkBboxMin.X, 5), round(linkBboxMin.Y, 5),  round(linkBboxMin.Z, 5)]
                                linkElementBBoxMinDataList.append(linkBboxMinRound)
                                
                                elementLocationDataList.append(None)
                                elementOrientationList.append(None)
                                
                            counter += 1
                            pb.update_progress(counter, max_value)
                            revit.uidoc.RefreshActiveView()
                            doc.Regenerate()
                        
                        # Delete the temporary section
                        doc.Delete(tempSection.Id)
                        
                        # Delete the temporary level
                        doc.Delete(tempLevel.Id)
                        
                        linkNameData = Main_link_selection.get_Parameter(BuiltInParameter.ELEM_TYPE_PARAM).AsValueString()
                        linkElementUniqueIdDataList = linkOriginalSectionsUniqueId + finalLinkDepSectionsUniqueId + linkRefSectionsUniqueId
                        i = 0
                        for copiedSec in copiedSections:
                            CreateExtensionStorageSection(copiedSec, linkElementUniqueIdDataList[i], linkNameData, linkSecCropBoxMinDataList[i], linkSecCropBoxMaxDataList[i], linkSecFarClipOffsetList[i], linkSecLocationList[i], linkSecNameList[i], linkSecOrientationList[i], \
                                elementBBoxMaxDataList[i], elementBBoxMinDataList[i], linkElementBBoxMaxDataList[i], linkElementBBoxMinDataList[i], elementLocationDataList[i], elementOrientationList[i])
                            i += 1
                        
                        t.Commit()

                    else:
                        t.RollBack()
            
            if counter == max_value:
                InstructionsWindow(InstructionsXamlFile).show(modal=True)

    @property
    def selected_viewplan(self):
        # Use regular expression to search for text inside the last set of brackets
        bracket_matches = re.findall(r'\((.*?)\)', self.view_plan.SelectedItem)
        selected_view_type = bracket_matches[-1]

        # Use regular expressions to remove the last brackets and the text inside them
        selected_view_name = re.sub(r'\s*\([^)]*\)$', '', self.view_plan.SelectedItem)

        # Iterate through LinkViews
        for view in LinkViews:
            # Get the view type as a string
            view_type = view.get_Parameter(BuiltInParameter.ELEM_FAMILY_PARAM).AsValueString()

            # Check if the selected view name and type match with the current view
            if (selected_view_name == view.Name and selected_view_type == view_type):
                return view  # Return the matching view

        return None  # Return None if no matching view is found

    # Setup the Load Cases for the ComboBox (list of Load Cases)
    def _setup_viewplans(self):
        sourceLinkViews = []
        for view in LinkViews:
            viewType = view.get_Parameter(BuiltInParameter.ELEM_FAMILY_PARAM).AsValueString()
            showName = view.Name + " (" + viewType + ")"
            sourceLinkViews.append(showName)
        self.view_plan.ItemsSource = sourceLinkViews
        self.view_plan.SelectedIndex = 0

        # Enable or disable OK button based on sourceLinkViews list
        self.ok_button.IsEnabled = bool(sourceLinkViews)

    def SearchTextBox_TextChanged(self, sender, args):
        search_text = self.searchTextBox.Text.lower()
        filtered_views = [view for view in LinkViews if search_text in view.Name.lower()]
        sourceLinkViews = [view.Name + " (" + view.get_Parameter(BuiltInParameter.ELEM_FAMILY_PARAM).AsValueString() + ")" for view in filtered_views]
        self.view_plan.ItemsSource = sourceLinkViews
        self.view_plan.SelectedIndex = 0
        
        # Enable or disable OK button based on sourceLinkViews list
        self.ok_button.IsEnabled = bool(sourceLinkViews)

def SetViewRange(selected_viewplan):
    # Check if the view range is not include in the view template
    CanSetViewRange = False
    ActiveViewTemplateId = doc.ActiveView.ViewTemplateId
    
    # Check if the active view has a valid template
    if not ActiveViewTemplateId.IntegerValue < 0:
        ActiveViewTemplate = doc.GetElement(ActiveViewTemplateId)
        ActiveNotInclude = ActiveViewTemplate.GetNonControlledTemplateParameterIds()
        ViewRangeId = doc.GetElement(ActiveViewTemplateId).LookupParameter("View Range").Id
        
        # Check if the View Range parameter is not controlled by the template
        for activeNotInclude in ActiveNotInclude:
            if ViewRangeId == activeNotInclude:
                CanSetViewRange = True
                break
    else:
        # If no template is applied, allow setting the view range
        CanSetViewRange = True
    
    if not CanSetViewRange:
        return
    
    # Get the view ranges
    activeViewRange = doc.ActiveView.GetViewRange()
    linkViewRange = selected_viewplan.GetViewRange()
    
    # Check if the view ranges are different
    if (activeViewRange != linkViewRange):
        
        # Creating collectior instance and collecting all the Levels from the model
        levelsCollector = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Levels).WhereElementIsNotElementType()
        
        try:
            # Copy the offsets from the linked view to the active view
            for plane in [DB.PlanViewPlane.CutPlane, DB.PlanViewPlane.UnderlayBottom, DB.PlanViewPlane.TopClipPlane, DB.PlanViewPlane.BottomClipPlane, DB.PlanViewPlane.ViewDepthPlane]:
                activeViewRange.SetOffset(plane, linkViewRange.GetOffset(plane))
                linkLevelId = linkViewRange.GetLevelId(plane)
                if not linkLevelId.IntegerValue < 0:
                    linkLevelName = linkDoc.GetElement(linkLevelId).Name
                    
                    # Find the matching level in the active document and set it
                    for level in levelsCollector:
                        if level.Name == linkLevelName:
                            activeViewRange.SetLevelId(plane, level.Id)
                            break
                
            # Apply the modified view range to the active view
            doc.ActiveView.SetViewRange(activeViewRange)
        except:
            pass

def get_biparam_stringequals_filter(bip_paramvalue_dict):
    filters = []
    for bip, fvalue in bip_paramvalue_dict.items():
        bip_id = DB.ElementId(bip)
        bip_valueprovider = DB.ParameterValueProvider(bip_id)
        bip_valuerule = DB.FilterStringRule(bip_valueprovider,
                                            DB.FilterStringEquals(),
                                            fvalue)
        filters.append(bip_valuerule)

    if filters:
        return DB.ElementParameterFilter(
            framework.List[DB.FilterRule](filters)
            )
    else:
        raise PyRevitException('Error creating filters.')

def sectionsCollector(document, viewId):
    sectionsCollectorList = []
    if viewId == None:
        if HOST_APP.is_newer_than(2022):
            collectSections = FilteredElementCollector(document).OfCategory(BuiltInCategory.OST_Viewers) \
                                        .WherePasses(get_biparam_stringequals_filter({DB.BuiltInParameter.ELEM_FAMILY_PARAM: "Section"}))
            collectDetailViews = FilteredElementCollector(document).OfCategory(BuiltInCategory.OST_Viewers) \
                                        .WherePasses(get_biparam_stringequals_filter({DB.BuiltInParameter.ELEM_FAMILY_PARAM: "Detail View"}))
        else:
            collectSections = FilteredElementCollector(document).OfCategory(BuiltInCategory.OST_Viewers) \
                                        .WherePasses(query.get_biparam_stringequals_filter({DB.BuiltInParameter.ELEM_FAMILY_PARAM: "Section"}))
            collectDetailViews = FilteredElementCollector(document).OfCategory(BuiltInCategory.OST_Viewers) \
                                        .WherePasses(query.get_biparam_stringequals_filter({DB.BuiltInParameter.ELEM_FAMILY_PARAM: "Detail View"}))
    else:
        if HOST_APP.is_newer_than(2022):
            collectSections = FilteredElementCollector(document, viewId).OfCategory(BuiltInCategory.OST_Viewers) \
                                        .WherePasses(get_biparam_stringequals_filter({DB.BuiltInParameter.ELEM_FAMILY_PARAM: "Section"}))
            collectDetailViews = FilteredElementCollector(document, viewId).OfCategory(BuiltInCategory.OST_Viewers) \
                                        .WherePasses(get_biparam_stringequals_filter({DB.BuiltInParameter.ELEM_FAMILY_PARAM: "Detail View"}))
        else:
            collectSections = FilteredElementCollector(document, viewId).OfCategory(BuiltInCategory.OST_Viewers) \
                                        .WherePasses(query.get_biparam_stringequals_filter({DB.BuiltInParameter.ELEM_FAMILY_PARAM: "Section"}))
            collectDetailViews = FilteredElementCollector(document, viewId).OfCategory(BuiltInCategory.OST_Viewers) \
                                        .WherePasses(query.get_biparam_stringequals_filter({DB.BuiltInParameter.ELEM_FAMILY_PARAM: "Detail View"}))
    for section in collectSections:
        sectionsCollectorList.append(section)
    for detailView in collectDetailViews:
        sectionsCollectorList.append(detailView)
        
    return sectionsCollectorList

def CopyDependentSections(CopySections, sectionName, sectionDependentName, depSectionsUniqueId, linkDepSectionsUniqueId):
    i = 0
    dependentSectionsList = []
    finalDepSectionsUniqueId = []
    finalLinkDepSectionsUniqueId = []
    depElementBBoxMaxDataList = []
    depElementBBoxMinDataList = []
    
    # collect sections from current model (original and dependent) and it's names
    modelSections = sectionsCollector(doc, None)
    modelSectionsNames = [(j.Name) for j in modelSections]
    
    for secName in sectionName:
        # Change the name of the section if it's already exist in the model
        copyNumber = 1
        originalSecName = secName
        while (secName in modelSectionsNames):
            secName = originalSecName + " Copy " + str(copyNumber)
            copyNumber += 1
        
        dependentName = sectionDependentName[i]
        for sec in CopySections:
            section = doc.GetElement(sec)
            try:
                if dependentName == section.Name:
                    sectionViewList = section.GetDependentElements(viewSectionFilter)
                    sectionView = doc.GetElement(sectionViewList[0])
                    # Duplicate the section as a dependent
                    newViewId = sectionView.Duplicate(DB.ViewDuplicateOption.AsDependent)
                    newVieweElement = doc.GetElement(newViewId)
                    newSectionView = newVieweElement.GetDependentElements(DB.ElementCategoryFilter(DB.BuiltInCategory.OST_Viewers))
                    newView = doc.GetElement(newSectionView[0])
                    dependentSectionsList.append(newView)
                    # Change the name of the dependent view according to its name in the linked model
                    parameter = newView.get_Parameter(DB.BuiltInParameter.VIEW_NAME)
                    parameter.Set(secName)
                    finalDepSectionsUniqueId.append(depSectionsUniqueId[i])
                    finalLinkDepSectionsUniqueId.append(linkDepSectionsUniqueId[i].ToString())
                    
                    bboxMax = newView.get_BoundingBox(doc.ActiveView).Max
                    bboxMaxRound = [round(bboxMax.X, 5), round(bboxMax.Y, 5),  round(bboxMax.Z, 5)]
                    depElementBBoxMaxDataList.append(bboxMaxRound)

                    bboxMin = newView.get_BoundingBox(doc.ActiveView).Min
                    bboxMinRound = [round(bboxMin.X, 5), round(bboxMin.Y, 5),  round(bboxMin.Z, 5)]
                    depElementBBoxMinDataList.append(bboxMinRound)
                    break
            except:
                pass
        i += 1
    return dependentSectionsList, finalDepSectionsUniqueId, finalLinkDepSectionsUniqueId, depElementBBoxMaxDataList, depElementBBoxMinDataList

def CreateExtensionStorageSection(copiedSec, linkElementUniqueIdData, linkNameData, linkSecCropBoxMinData, linkSecCropBoxMaxData, linkSecFarClipOffset, linkSecLocation, linkSecName, linkSecOrientation, \
    elementBBoxMaxData, elementBBoxMinData, linkElementBBoxMaxData, linkElementBBoxMinData, elementLocationData, elementOrientation):
    schemaBuilder=DB.ExtensibleStorage.SchemaBuilder(System.Guid(schemaGuid))
    schemaBuilder.SetReadAccessLevel(DB.ExtensibleStorage.AccessLevel.Public)
    schemaBuilder.SetWriteAccessLevel(DB.ExtensibleStorage.AccessLevel.Public)
    schemaBuilder.SetVendorId("YuvalDolin")
    schemaBuilder.SetSchemaName("DavidEng_Schema_CopySectionsData")
    fieldBuilder = schemaBuilder.AddMapField("CopySectionsData", String, String)
    fieldBuilder.SetDocumentation("The Data of the sections from the current and linked models")
    schema = schemaBuilder.Finish()
    
    entity =DB.ExtensibleStorage.Entity(schema)
    dictionary = Dictionary[str, str]()
    dictionary.Add("ElementBBoxMaxData", json.dumps(elementBBoxMaxData))
    dictionary.Add("ElementBBoxMinData", json.dumps(elementBBoxMinData))
    dictionary.Add("ElementLocationData", json.dumps(elementLocationData))
    dictionary.Add("ElementNameData", str(copiedSec.Name))
    dictionary.Add("ElementOrientationData", json.dumps(elementOrientation))
    dictionary.Add("ElementUniqueIdData", str(copiedSec.UniqueId))
    dictionary.Add("Link_ElementUniqueIdData", linkElementUniqueIdData)
    dictionary.Add("Link_NameData", linkNameData)
    dictionary.Add("LinkSec_BBoxMaxData", json.dumps(linkElementBBoxMaxData))
    dictionary.Add("LinkSec_BBoxMinData", json.dumps(linkElementBBoxMinData))
    dictionary.Add("LinkSec_CropBoxMinData", json.dumps(linkSecCropBoxMinData))
    dictionary.Add("LinkSec_CropBoxMaxData", json.dumps(linkSecCropBoxMaxData))
    dictionary.Add("LinkSec_FarClipOffsetData", str(linkSecFarClipOffset))
    dictionary.Add("LinkSec_LocationData", json.dumps(linkSecLocation))
    dictionary.Add("LinkSec_NameData", str(linkSecName))
    dictionary.Add("LinkSec_OrientationData", json.dumps(linkSecOrientation))

    entity.Set[IDictionary[str, str]]("CopySectionsData", dictionary)
    copiedSec.SetEntity(entity)

# collect the sections from the active view before the user run the tool
sectionsInActiveView = sectionsCollector(doc, doc.ActiveView.Id)

if sectionsInActiveView:
    alertMessage = "In the active view there is at least one section\nthat is already modeled.\nIt is recommended to use the Compare Sections tool\nin order not to copy duplicated sections.\nWould you like to proceed anyway?"
    alertMessageHebrew = ".במבט הנוכחי יש לפחות חתך אחד שכבר ממודל\nCompare Sections מומלץ להשתמש בתוסף\n.כדי לא להעתיק חתכים כפולים\n?האם ברצונך להמשיך בכל זאת בפעולה"
    AlertWindow(alertText=alertMessage, alertTextHebrew=alertMessageHebrew).ShowDialog()
else:
    continueToCopy = True

if (DavidEng.checkPermission()):
    if continueToCopy:
        if linkedDoc.GetElementCount() != 0:
            checkIsTranslation = False
            global selectedLinkedModel
            selectedLinkedModel = None
            while checkIsTranslation == False:
                linkedModelsList = list(FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_RvtLinks).WhereElementIsNotElementType())
                ChooseLinkModelWindow(SelectLinkXamlFile).show(modal=True)
                Main_link_selection = selectedLinkedModel
                if Main_link_selection == None:
                    checkIsTranslation = True
                elif Main_link_selection.GetTotalTransform().HasReflection == False:
                    checkIsTranslation = True
                else:
                    DavidEng.ErrorWindow(errortext="You can't select linked model that is mirrored").ShowDialog()
            if not Main_link_selection == None:
                # get the linked document and transform 
                linkDoc = Main_link_selection.GetLinkDocument()
                transform = Main_link_selection.GetTotalTransform()
                TransOrigin = transform.Origin
                transform_Z_2D = transform
                if not TransOrigin.Z == 0:
                    transform_Z_2D.Origin = XYZ(TransOrigin.X, TransOrigin.Y, 0)
                Determinant = transform.Determinant
                BasisXX = transform.BasisX.X
                BasisXY = transform.BasisX.Y
                BasisYY = transform.BasisY.Y
                BasisYX = transform.BasisY.X
                
                # collect view plans in linked model
                LinkViewsAll = FilteredElementCollector(linkDoc).OfClass(ViewPlan)
                LinkViews = []
                # clean view templates from the ViewPlan list
                for view in LinkViewsAll:
                    if view.IsTemplate == False:
                        LinkViews.append(view)
                
                # Run the Xaml
                ChooseViewPlanWindow(SelectViewXamlFile).show(modal=True)
else:
    DavidEng.ErrorWindow(errortext="You don't have permission\nto use the tool").ShowDialog()