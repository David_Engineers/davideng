# # -*- coding: utf-8 -*-

"""Version: 1.0.4
Compare the sections between the selected view from the linked model and the active view of the current model."""
# pylint: disable=E0401,W0703,W0613
import System
import json
import re
from core import main
from System.Collections.Generic import IDictionary
from pyrevit import DB, script, forms
from pyrevit.revit.db import query
from pyrevit import framework
from pyrevit import PyRevitException
from pyrevit import HOST_APP
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, RevitLinkInstance, Transaction, TransactionGroup, ViewPlan, XYZ, BuiltInParameter, Outline, Options, \
                                SketchPlane, ElementTransformUtils, ViewSection
from Autodesk.Revit.UI.Selection import ObjectType, ISelectionFilter
from Autodesk.Revit import Exceptions
import DavidEng
# dependencies
import clr
clr.AddReference('System.Windows.Forms')

# find the path of SelectViewWindow.xaml
SelectViewXamlFile = script.get_bundle_file('SelectViewWindow.xaml')

# find the path of SelectLinkWindow.xaml
SelectLinkXamlFile = script.get_bundle_file('SelectLinkWindow.xaml')

# find the path of ErrorWindow.xaml
alertXamlFile = script.get_bundle_file("CompareAlertWindowWithHebrew.xaml")

# import WPF creator and base Window
import wpf
from System import Windows

# App and Author name
__title__ = 'Compare Sections'
__authors__ = 'Yuval Dolin'

# Identify the current Revit file
doc = __revit__.ActiveUIDocument.Document
uidoc = __revit__.ActiveUIDocument

options = Options(ComputeReferences=True, IncludeNonVisibleObjects=True)
options.View = doc.ActiveView

# collect linked revit documents 
linkedDoc = FilteredElementCollector(doc, doc.ActiveView.Id).OfClass(RevitLinkInstance)

# get the extensible storage schma
schemaGuid = "e64e3bab-7b93-415a-a20a-4d09ec106eaf"
schemaSections = DB.ExtensibleStorage.Schema.Lookup(System.Guid(schemaGuid))

viewSectionFilter = DB.ElementClassFilter(ViewSection)
sketchPlaneFilter = DB.ElementClassFilter(SketchPlane)


# Define class to filter elements of category to select 
class CustomSelectionFilter(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, self.category).Id:
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

# Show Alert Window Form
class AlertWindow(Windows.Window):
    def __init__(self, alertText, alertTextHebrew):
        # indicate if the user decided to modify the element or not
        global modifyElement
        modifyElement = False
        # indicate if the user decided to abort the process by clicking X button or to continue by clicking Yes/No
        global abortProcessFlag
        abortProcessFlag = True
        self.alertText = alertText
        self.alertTextHebrew = alertTextHebrew
        wpf.LoadComponent(self, alertXamlFile)
        self.alert.Text = "{}".format(self.alertText)
        self.alertHebrew.Text = "{}".format(self.alertTextHebrew)

    def yes(self, sender, args):
        self.Close()
        # indicate if the user decided to modify the element or not
        global modifyElement
        modifyElement = True
        global abortProcessFlag
        abortProcessFlag = False
        
    def no(self, sender, args):
        self.Close()
        global abortProcessFlag
        abortProcessFlag = False

class ChooseViewPlanWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name):
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)
        self._setup_viewplans()

    def cancel(self, sender, args):
        self.Close()

    def click_ok(self, sender, args):
        self.Close()
        
        global LinkViewsCollectSections
        global selectedViewPlan
        selectedViewPlan = self.selected_viewplan
        SetViewRange()
        LinkViewsCollectSections = sectionsCollector(linkDoc, selectedViewPlan.Id)

    @property
    def selected_viewplan(self):
        # Use regular expression to search for text inside the last set of brackets
        bracket_matches = re.findall(r'\((.*?)\)', self.view_plan.SelectedItem)
        selected_view_type = bracket_matches[-1]

        # Use regular expressions to remove the last brackets and the text inside them
        selected_view_name = re.sub(r'\s*\([^)]*\)$', '', self.view_plan.SelectedItem)

        # Iterate through LinkViews
        for view in LinkViews:
            # Get the view type as a string
            view_type = view.get_Parameter(BuiltInParameter.ELEM_FAMILY_PARAM).AsValueString()

            # Check if the selected view name and type match with the current view
            if (selected_view_name == view.Name and selected_view_type == view_type):
                return view  # Return the matching view

        return None  # Return None if no matching view is found

    # Setup the Load Cases for the ComboBox (list of Load Cases)
    def _setup_viewplans(self):
        sourceLinkViews = []
        for view in LinkViews:
            viewType = view.get_Parameter(BuiltInParameter.ELEM_FAMILY_PARAM).AsValueString()
            showName = view.Name + " (" + viewType + ")"
            sourceLinkViews.append(showName)
        self.view_plan.ItemsSource = sourceLinkViews
        self.view_plan.SelectedIndex = 0
        
        # Enable or disable OK button based on sourceLinkViews list
        self.ok_button.IsEnabled = bool(sourceLinkViews)
        
    def SearchTextBox_TextChanged(self, sender, args):
        search_text = self.searchTextBox.Text.lower()
        filtered_views = [view for view in LinkViews if search_text in view.Name.lower()]
        sourceLinkViews = [view.Name + " (" + view.get_Parameter(BuiltInParameter.ELEM_FAMILY_PARAM).AsValueString() + ")" for view in filtered_views]
        self.view_plan.ItemsSource = sourceLinkViews
        self.view_plan.SelectedIndex = 0
        
        # Enable or disable OK button based on sourceLinkViews list
        self.ok_button.IsEnabled = bool(sourceLinkViews)
        
class ChooseLinkModelWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name):
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)
        self._setup_linked_names()

    def cancel(self, sender, args):
        self.Close()

    def click_ok(self, sender, args):
        self.Close()
        
        global selectedLinkedModel
        selectedLinkedModel = self.selected_linkedmodel

    @property
    def selected_linkedmodel(self):
        selected_name = self.link_model.SelectedItem
        for linkedModel in linkedModelsList:
            if selected_name == linkedModel.get_Parameter(BuiltInParameter.ELEM_TYPE_PARAM).AsValueString():
                return linkedModel
        return None

    # Setup the Load Cases for the ComboBox (list of Load Cases)
    def _setup_linked_names(self):
        linkNamesList = []
        for linkedModel in linkedModelsList:
            linkName = linkedModel.get_Parameter(BuiltInParameter.ELEM_TYPE_PARAM).AsValueString()
            linkNamesList.append(linkName)
        self.link_model.ItemsSource = linkNamesList
        self.link_model.SelectedIndex = 0
        
        # Enable or disable OK button based on sourceLinkViews list
        self.ok_button.IsEnabled = bool(linkNamesList)
        
    def SearchTextBox_TextChanged(self, sender, args):
        search_text = self.searchTextBox.Text.lower()
        filtered_linked_models = [linkedModel for linkedModel in linkedModelsList if search_text in linkedModel.get_Parameter(BuiltInParameter.ELEM_TYPE_PARAM).AsValueString().lower()]
        linkNamesList = [linkedModel.get_Parameter(BuiltInParameter.ELEM_TYPE_PARAM).AsValueString() for linkedModel in filtered_linked_models]
        self.link_model.ItemsSource = linkNamesList
        self.link_model.SelectedIndex = 0
        
        # Enable or disable OK button based on filtered linked models list
        self.ok_button.IsEnabled = bool(linkNamesList)

def get_biparam_stringequals_filter(bip_paramvalue_dict):
    filters = []
    for bip, fvalue in bip_paramvalue_dict.items():
        bip_id = DB.ElementId(bip)
        bip_valueprovider = DB.ParameterValueProvider(bip_id)
        bip_valuerule = DB.FilterStringRule(bip_valueprovider,
                                            DB.FilterStringEquals(),
                                            fvalue)
        filters.append(bip_valuerule)

    if filters:
        return DB.ElementParameterFilter(
            framework.List[DB.FilterRule](filters)
            )
    else:
        raise PyRevitException('Error creating filters.')

def sectionsCollector(document, viewId):
    sectionsCollectorList = []
    if viewId == None:
        if HOST_APP.is_newer_than(2022):
            collectSections = FilteredElementCollector(document).OfCategory(BuiltInCategory.OST_Viewers) \
                                        .WherePasses(get_biparam_stringequals_filter({DB.BuiltInParameter.ELEM_FAMILY_PARAM: "Section"}))
            collectDetailViews = FilteredElementCollector(document).OfCategory(BuiltInCategory.OST_Viewers) \
                                        .WherePasses(get_biparam_stringequals_filter({DB.BuiltInParameter.ELEM_FAMILY_PARAM: "Detail View"}))
        else:
            collectSections = FilteredElementCollector(document).OfCategory(BuiltInCategory.OST_Viewers) \
                                        .WherePasses(query.get_biparam_stringequals_filter({DB.BuiltInParameter.ELEM_FAMILY_PARAM: "Section"}))
            collectDetailViews = FilteredElementCollector(document).OfCategory(BuiltInCategory.OST_Viewers) \
                                        .WherePasses(query.get_biparam_stringequals_filter({DB.BuiltInParameter.ELEM_FAMILY_PARAM: "Detail View"}))
    else:
        if HOST_APP.is_newer_than(2022):
            collectSections = FilteredElementCollector(document, viewId).OfCategory(BuiltInCategory.OST_Viewers) \
                                        .WherePasses(get_biparam_stringequals_filter({DB.BuiltInParameter.ELEM_FAMILY_PARAM: "Section"}))
            collectDetailViews = FilteredElementCollector(document, viewId).OfCategory(BuiltInCategory.OST_Viewers) \
                                        .WherePasses(get_biparam_stringequals_filter({DB.BuiltInParameter.ELEM_FAMILY_PARAM: "Detail View"}))
        else:
            collectSections = FilteredElementCollector(document, viewId).OfCategory(BuiltInCategory.OST_Viewers) \
                                        .WherePasses(query.get_biparam_stringequals_filter({DB.BuiltInParameter.ELEM_FAMILY_PARAM: "Section"}))
            collectDetailViews = FilteredElementCollector(document, viewId).OfCategory(BuiltInCategory.OST_Viewers) \
                                        .WherePasses(query.get_biparam_stringequals_filter({DB.BuiltInParameter.ELEM_FAMILY_PARAM: "Detail View"}))
    for section in collectSections:
        sectionsCollectorList.append(section)
    for detailView in collectDetailViews:
        sectionsCollectorList.append(detailView)
        
    return sectionsCollectorList

def ZoomToElement(elementToShow):
    # Getting the geometry and the bounding box of the rebar shape
    elementBBox = elementToShow.BoundingBox[doc.ActiveView]
    if not elementBBox == None:
        outlinElement = Outline(elementBBox.Min, elementBBox.Max) 
        active_ui_views = uidoc.GetOpenUIViews()
        # find current uiview
        current_ui_view_list = []
        for active_ui_view in active_ui_views:
            if active_ui_view.ViewId == doc.ActiveView.Id:
                current_ui_view_list.append(active_ui_view)
        for current_ui_view in current_ui_view_list:
            current_ui_view.ZoomAndCenterRectangle(outlinElement.MaximumPoint, outlinElement.MinimumPoint)

def ColorAndAlertElement(alertMessage, alertMessageHebrew, elementToShow):
    ogs = DB.OverrideGraphicSettings()
    ogs.SetProjectionLineColor(DB.Color(255, 0, 0))
    ogs.SetProjectionLineWeight(5)
    tg = TransactionGroup(doc, 'Color the invalid section')
    tg.Start()
    t = Transaction(doc, 'Color the invalid section')
    t.Start()
    doc.ActiveView.SetElementOverrides(elementToShow.Id, ogs)
    t.Commit()
    AlertWindow(alertText=alertMessage, alertTextHebrew=alertMessageHebrew).ShowDialog()
    tg.RollBack()

def DeletedSections():
    ZoomToElement(sectionElement)
    alertMessage = "Section '{0}' was deleted in the original model.\nWould you like to delete this section in the current model?".format(linkSecNameData)
    alertMessageHebrew = ".נמחק במודל המקורי '{0}' חתך\n?האם ברצונך למחוק אותו במודל הנוכחי".format(linkSecNameData)
    ColorAndAlertElement(alertMessage, alertMessageHebrew, sectionElement)
    if modifyElement:
        # add the element to the list and delete it later
        elementsToDelete.append(sectionElement)  

def MoveSections():
    ZoomToElement(sectionElement)
    alertMessage = "Section '{0}' moved in the original model.\nWould you like to move this section in the current model?".format(linkSecNameData)
    alertMessageHebrew = ".זז במודל המקורי '{0}' חתך\n?האם ברצונך להזיז אותו במודל הנוכחי".format(linkSecNameData)
    ColorAndAlertElement(alertMessage, alertMessageHebrew, sectionElement)
    if modifyElement:
        # update in the dictionary the location of the link section
        liveSection["LinkSec_LocationData"] = json.dumps([actualLinkSecLocationOriginX, actualLinkSecLocationOriginY, actualLinkSecLocationOriginZ])
        
        xDiffLink = actualLinkSecLocationOriginX - linkSecLocationOriginX
        yDiffLink = actualLinkSecLocationOriginY - linkSecLocationOriginY
        zDiffLink = actualLinkSecLocationOriginZ - linkSecLocationOriginZ
    
        # active model section location
        secLocation = sectionElement.GetDependentElements(sketchPlaneFilter)
        secLocationPlane = doc.GetElement(secLocation[0]).GetPlane()
        secLocationOrigin = secLocationPlane.Origin
        secLocationRound = [round(secLocationOrigin.X, 5), round(secLocationOrigin.Y, 5),  round(secLocationOrigin.Z, 5)]
        actualSecLocationOriginX = round(secLocationOrigin.X, 5)
        actualSecLocationOriginY = round(secLocationOrigin.Y, 5)
        actualSecLocationOriginZ = round(secLocationOrigin.Z, 5)
        secLocationOriginX = json.loads(elementLocationData)[0]
        secLocationOriginY = json.loads(elementLocationData)[1]
        secLocationOriginZ = json.loads(elementLocationData)[2]
        xDiffActive = 0
        yDiffActive = 0
        zDiffActive = 0

        if secLocationOriginX != actualSecLocationOriginX or \
                secLocationOriginY != actualSecLocationOriginY or \
                secLocationOriginZ != actualSecLocationOriginZ:
            xDiffActive = actualSecLocationOriginX - secLocationOriginX
            yDiffActive = actualSecLocationOriginY - secLocationOriginY
            zDiffActive = actualSecLocationOriginZ - secLocationOriginZ
            
        xDiff = xDiffLink - xDiffActive
        yDiff = yDiffLink - yDiffActive
        zDiff = zDiffLink - zDiffActive
        
        moveTranslation = XYZ(xDiff, yDiff, zDiff)
        
        # add the element and the dictionary to the list and move and update it later
        elementsToMove.append([sectionElement, moveTranslation, liveSection])  

def CropBoxSections():
    ZoomToElement(sectionElement)
    alertMessage = "The Crop Box of secion '{0}' was changed\nin the original model.\nWould you like to change this section in the current model?".format(linkSecNameData)
    alertMessageHebrew = ".השתנה במודל המקורי '{0}' של חתך Crop Box-ה\n?האם ברצונך לשנות אותו במודל הנוכחי".format(linkSecNameData)
    ColorAndAlertElement(alertMessage, alertMessageHebrew, sectionElement)
    if modifyElement:
        # update in the dictionary the location of the link section
        liveSection["LinkSec_CropBoxMinData"] = json.dumps([actualLinkCropBoxMinX, actualLinkCropBoxMinY, actualLinkCropBoxMinZ])
        liveSection["LinkSec_CropBoxMaxData"] = json.dumps([actualLinkCropBoxMaxX, actualLinkCropBoxMaxY, actualLinkCropBoxMaxZ])
        
        cropBoxMin = XYZ(actualLinkCropBoxMinX, actualLinkCropBoxMinY, actualLinkCropBoxMinZ)
        cropBoxMax = XYZ(actualLinkCropBoxMaxX, actualLinkCropBoxMaxY, actualLinkCropBoxMaxZ)
        # add the element and the dictionary to the list and change and update it later
        elementsToChangeCropBox.append([sectionElement, cropBoxMin, cropBoxMax, liveSection])  

def FarClipOffsetSections():
    ZoomToElement(sectionElement)
    alertMessage = "The value in the 'Far Clip Offset' parameter of section '{0}' was changed in the original model.\nWould you like to change this section in the current model?".format(linkSecNameData)
    alertMessageHebrew = "של חתך 'Far Clip Offset' הערך בפרמטר\n.השתנה במודל המקורי '{0}'\n?האם ברצונך לשנות אותו במודל הנוכחי".format(linkSecNameData)
    ColorAndAlertElement(alertMessage, alertMessageHebrew, sectionElement)
    if modifyElement:
        liveSection["LinkSec_FarClipOffsetData"] = str(actualLinkSecFarClipOffsetRound)
        elementsToChangeFarClip.append([sectionElement, actualLinkSecFarClipOffset, liveSection]) 
    
def NameSections():
    ZoomToElement(sectionElement)
    alertMessage = "The name of section '{0}' was changed in the original model.\nWould you like to change the name of this section in the current model?".format(linkSecNameData)
    alertMessageHebrew = "השם של החתך\n.השתנה במודל המקורי '{0}'\n?האם ברצונך לשנות אותו במודל הנוכחי".format(linkSecNameData)
    ColorAndAlertElement(alertMessage, alertMessageHebrew, sectionElement)
    if modifyElement:
        # update in the dictionary the name of the link section
        liveSection["LinkSec_NameData"] = str(linkViewsSection.Name)
        liveSection["ElementNameData"] = str(linkViewsSection.Name)
        
        elementsToRename.append([sectionElement, linkViewsSection.Name, liveSection]) 

# collect the data from the sections that were already copied
def CopySectionsData(liveSection):
    elementNameData = liveSection["ElementNameData"]
    elementUniqueIdData = liveSection["ElementUniqueIdData"]
    linkElementUniqueIdData = liveSection["Link_ElementUniqueIdData"]
    linkNameData = liveSection["Link_NameData"]
    linkSecCropBoxMaxData = liveSection["LinkSec_CropBoxMaxData"]
    linkSecCropBoxMinData = liveSection["LinkSec_CropBoxMinData"]
    linkSecFarClipOffsetData = liveSection["LinkSec_FarClipOffsetData"]
    linkSecLocationData = liveSection["LinkSec_LocationData"]
    linkSecNameData = liveSection["LinkSec_NameData"]
    linkSecOrientationData = liveSection["LinkSec_OrientationData"]
    elementBBoxMaxData = liveSection["ElementBBoxMaxData"]
    elementBBoxMinData = liveSection["ElementBBoxMinData"]
    elementLocationData = liveSection["ElementLocationData"]
    elementOrientationData = liveSection["ElementOrientationData"]
    linkSecBBoxMaxData = liveSection["LinkSec_BBoxMaxData"]
    linkSecBBoxMinData = liveSection["LinkSec_BBoxMinData"]
    
    return elementNameData, elementUniqueIdData, linkElementUniqueIdData, linkNameData, linkSecCropBoxMaxData, linkSecCropBoxMinData, linkSecFarClipOffsetData, \
           linkSecLocationData, linkSecNameData, linkSecOrientationData, elementBBoxMaxData, elementBBoxMinData, elementLocationData, elementOrientationData, \
            linkSecBBoxMaxData, linkSecBBoxMinData

def SetViewRange():
    # Check if the view range is not include in the view template
    CanSetViewRange = False
    ActiveViewTemplateId = doc.ActiveView.ViewTemplateId
    
    # Check if the active view has a valid template
    if not ActiveViewTemplateId.IntegerValue < 0:
        ActiveViewTemplate = doc.GetElement(ActiveViewTemplateId)
        ActiveNotInclude = ActiveViewTemplate.GetNonControlledTemplateParameterIds()
        ViewRangeId = doc.GetElement(ActiveViewTemplateId).LookupParameter("View Range").Id
        
        # Check if the View Range parameter is not controlled by the template
        for activeNotInclude in ActiveNotInclude:
            if ViewRangeId == activeNotInclude:
                CanSetViewRange = True
                break
    else:
        # If no template is applied, allow setting the view range
        CanSetViewRange = True
    
    if not CanSetViewRange:
        return
    
    # Get the view ranges
    activeViewRange = doc.ActiveView.GetViewRange()
    linkViewRange = selectedViewPlan.GetViewRange()
    
    # Check if the view ranges are different
    if (activeViewRange != linkViewRange):
        
        # Creating collectior instance and collecting all the Levels from the model
        levelsCollector = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Levels).WhereElementIsNotElementType()
        
        t = Transaction(doc, 'Set View Range')
        t.Start()
        
        try:
            # Copy the offsets from the linked view to the active view
            for plane in [DB.PlanViewPlane.CutPlane, DB.PlanViewPlane.UnderlayBottom, DB.PlanViewPlane.TopClipPlane, DB.PlanViewPlane.BottomClipPlane, DB.PlanViewPlane.ViewDepthPlane]:
                activeViewRange.SetOffset(plane, linkViewRange.GetOffset(plane))
                linkLevelId = linkViewRange.GetLevelId(plane)
                if not linkLevelId.IntegerValue < 0:
                    linkLevelName = linkDoc.GetElement(linkLevelId).Name
                    
                    # Find the matching level in the active document and set it
                    for level in levelsCollector:
                        if level.Name == linkLevelName:
                            activeViewRange.SetLevelId(plane, level.Id)
                            break
                
            # Apply the modified view range to the active view
            doc.ActiveView.SetViewRange(activeViewRange)
            t.Commit()
        except:
            t.RollBack()

if (DavidEng.checkPermission()):
    if type(doc.ActiveView) == ViewPlan:
        # collect all the sections from the active view
        activeViewCollectSections = sectionsCollector(doc, doc.ActiveView.Id)
        
        if len(activeViewCollectSections) != 0:
            if not schemaSections == None:
                tg = TransactionGroup(doc, 'Delete Data Schema')
                tg.Start()
                copiedSectionsList = []
                for viewSection in activeViewCollectSections:
                    retrievedEntity = viewSection.GetEntity(schemaSections)
                    fieldSection = schemaSections.GetField("CopySectionsData")
                    try:
                        retrievedDataSection = retrievedEntity.Get[IDictionary[str, str]](fieldSection)
                    except:
                        retrievedDataSection = None
                    if not retrievedDataSection == None:
                        retrievedDataSectionID = retrievedDataSection["ElementUniqueIdData"]
                        # if section with schema was copied manually, then delete the schema
                        if str(viewSection.UniqueId) != retrievedDataSectionID:
                            t = Transaction(doc, 'Delete Schema from section')
                            t.Start()
                            viewSection.DeleteEntity(schemaSections)
                            t.Commit()
                        else:
                            copiedSectionsList.append(retrievedDataSection)
                tg.Assimilate() 
                
                # if there are no sections with the relevant schema in the active view
                if len(copiedSectionsList) == 0:
                    DavidEng.ErrorWindow(errortext="We couldn't find any section in the active view that was copied with the 'Copy Sections' tool so we don't have any section to compare.").ShowDialog()
                elif linkedDoc.GetElementCount() != 0:
                    checkIsTranslation = False
                    global selectedLinkedModel
                    selectedLinkedModel = None
                    while checkIsTranslation == False:
                        linkedModelsList = list(FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_RvtLinks).WhereElementIsNotElementType())
                        ChooseLinkModelWindow(SelectLinkXamlFile).show(modal=True)
                        Main_link_selection = selectedLinkedModel
                        if Main_link_selection == None:
                            checkIsTranslation = True
                        elif Main_link_selection.GetTotalTransform().HasReflection == False:
                            checkIsTranslation = True
                        else:
                            DavidEng.ErrorWindow(errortext="You can't select linked model that is mirrored").ShowDialog()
                    if not Main_link_selection == None:
                        # get the linked document and transform 
                        linkDoc = Main_link_selection.GetLinkDocument()
                        transform = Main_link_selection.GetTotalTransform()
                        TransOrigin = transform.Origin
                        transform_Z_2D = transform
                        if not TransOrigin.Z == 0:
                            transform_Z_2D.Origin = XYZ(TransOrigin.X, TransOrigin.Y, 0)
                        Determinant = transform.Determinant
                        BasisXX = transform.BasisX.X
                        BasisXY = transform.BasisX.Y
                        BasisYY = transform.BasisY.Y
                        BasisYX = transform.BasisY.X
                        
                        # collect view plans in linked model
                        LinkViewsAll = FilteredElementCollector(linkDoc).OfClass(ViewPlan)
                        LinkViews = []
                        # clean view templates fro ViewPlan
                        for view in LinkViewsAll:
                            if view.IsTemplate == False:
                                LinkViews.append(view)
                        
                        global LinkViewsCollectSections
                        LinkViewsCollectSections = None
                        
                        linkReferencedSections = []
                        liveReferencedSections = []
                        
                        # Run the Xaml
                        ChooseViewPlanWindow(SelectViewXamlFile).show(modal=True)
                        
                        if LinkViewsCollectSections != None:
                            global abortProcessFlag
                            abortProcessFlag = False
                            # list of elements that the user choose to delete if they were deleted from the original model
                            global elementsToDelete 
                            elementsToDelete = []
                            global elementsToMove
                            elementsToMove = []
                            global elementsToRename
                            elementsToRename = []
                            global elementsToChangeFarClip
                            elementsToChangeFarClip = []
                            global elementsToChangeCropBox
                            
                            # We should collect the sections again after setting the view range
                            # collect all the sections from the active view
                            activeViewCollectSections = sectionsCollector(doc, doc.ActiveView.Id)
                            copiedSectionsList = []
                            for viewSection in activeViewCollectSections:
                                retrievedEntity = viewSection.GetEntity(schemaSections)
                                fieldSection = schemaSections.GetField("CopySectionsData")
                                try:
                                    retrievedDataSection = retrievedEntity.Get[IDictionary[str, str]](fieldSection)
                                except:
                                    retrievedDataSection = None
                                if not retrievedDataSection == None:
                                    retrievedDataSectionID = retrievedDataSection["ElementUniqueIdData"]
                                    copiedSectionsList.append(retrievedDataSection)
                            
                            elementsToChangeCropBox = []
                            for liveSection in copiedSectionsList:
                                elementNameData, elementUniqueIdData, linkElementUniqueIdData, linkNameData, linkSecCropBoxMaxData, \
                                linkSecCropBoxMinData, linkSecFarClipOffsetData, linkSecLocationData, linkSecNameData, linkSecOrientationData, \
                                elementBBoxMaxData, elementBBoxMinData, elementLocationData, elementOrientationData, linkSecBBoxMaxData, linkSecBBoxMinData = CopySectionsData(liveSection)
                                global sectionElement
                                sectionElement = doc.GetElement(elementUniqueIdData)
                                isMatchingSectionExist = False
                                # check if there is matching section in the original model
                                for linkViewsSection in LinkViewsCollectSections:
                                    if str(linkViewsSection.UniqueId) == linkElementUniqueIdData:
                                        isMatchingSectionExist = True
                                        # remove the link view section that is matching from the list
                                        LinkViewsCollectSections.remove(linkViewsSection)
                                        break
                                if abortProcessFlag == True:
                                    continue
                                # if there is no matching section in the original model
                                if isMatchingSectionExist == False:
                                    DeletedSections()
                                    if abortProcessFlag == True:
                                        continue
                                else:
                                    # check if the section is referenced section
                                    if linkViewsSection.OwnerViewId.IntegerValue != -1:
                                        linkReferencedSections.append(linkViewsSection)
                                        liveReferencedSections.append(sectionElement)
                                        continue

                                    # check if the location of the section in the linked model was changed
                                    if not linkSecLocationData == "null":
                                        linkSecLocation = linkViewsSection.GetDependentElements(sketchPlaneFilter)
                                        linkSecLocationPlane = linkDoc.GetElement(linkSecLocation[0]).GetPlane()
                                        linkSecLocationOrigin = linkSecLocationPlane.Origin
                                        linkSLocationRound = [round(linkSecLocationOrigin.X, 5), round(linkSecLocationOrigin.Y, 5),  round(linkSecLocationOrigin.Z, 5)]
                                        actualLinkSecLocationOriginX = round(linkSecLocationOrigin.X, 5)
                                        actualLinkSecLocationOriginY = round(linkSecLocationOrigin.Y, 5)
                                        actualLinkSecLocationOriginZ = round(linkSecLocationOrigin.Z, 5)
                                        linkSecLocationOriginX = json.loads(linkSecLocationData)[0]
                                        linkSecLocationOriginY = json.loads(linkSecLocationData)[1]
                                        linkSecLocationOriginZ = json.loads(linkSecLocationData)[2]
                                        if linkSecLocationOriginX != actualLinkSecLocationOriginX or \
                                                linkSecLocationOriginY != actualLinkSecLocationOriginY or \
                                                linkSecLocationOriginZ != actualLinkSecLocationOriginZ:
                                            MoveSections()
                                            if abortProcessFlag == True:
                                                continue

                                    # check if the name of the section in the linked model was changed
                                    # if the section is not reference to other view
                                    if linkViewsSection.OwnerViewId.IntegerValue == -1:
                                        if linkSecNameData != linkViewsSection.Name:
                                            NameSections()
                                            if abortProcessFlag == True:
                                                continue

                                    # check if the value of the far clip offset parameter of the section in the linked model was changed
                                    # if the section is not reference to other view
                                    if linkViewsSection.OwnerViewId.IntegerValue == -1:
                                        dependencyValue = linkViewsSection.LookupParameter("Dependency").AsString()
                                        # Check if the section is depentent or not (the user can "Duplicate as a Dependent" the section)
                                        if not dependencyValue.startswith('Dependent on '):
                                            actualLinkSecFarClipOffset = linkViewsSection.get_Parameter(BuiltInParameter.VIEWER_BOUND_OFFSET_FAR).AsDouble()
                                            actualLinkSecFarClipOffsetRound = round(actualLinkSecFarClipOffset*30.48, 2)
                                            linkSecFarClipOffset = json.loads(linkSecFarClipOffsetData)
                                            if linkSecFarClipOffset != actualLinkSecFarClipOffsetRound:
                                                FarClipOffsetSections()
                                                if abortProcessFlag == True:
                                                    continue

                                    # check if the crop box of the section in the linked model was changed
                                    # if the section is not reference to other view
                                    if linkViewsSection.OwnerViewId.IntegerValue == -1:
                                        linkViewSectionList = linkViewsSection.GetDependentElements(viewSectionFilter)
                                        linkViewSection = linkViewSectionList[0]
                                        actualLinkCropBoxMin = linkDoc.GetElement(linkViewSection).CropBox.Min
                                        
                                        actualLinkCropBoxMinX = round(actualLinkCropBoxMin.X, 5)
                                        actualLinkCropBoxMinY = round(actualLinkCropBoxMin.Y, 5)
                                        actualLinkCropBoxMinZ = round(actualLinkCropBoxMin.Z, 5)
                                        linkCropBoxMinX = json.loads(linkSecCropBoxMinData)[0]
                                        linkCropBoxMinY = json.loads(linkSecCropBoxMinData)[1]
                                        linkCropBoxMinZ = json.loads(linkSecCropBoxMinData)[2]

                                        actualLinkCropBoxMax = linkDoc.GetElement(linkViewSection).CropBox.Max
                                        actualLinkCropBoxMaxX = round(actualLinkCropBoxMax.X, 5)
                                        actualLinkCropBoxMaxY = round(actualLinkCropBoxMax.Y, 5)
                                        actualLinkCropBoxMaxZ = round(actualLinkCropBoxMax.Z, 5)
                                        linkCropBoxMaxX = json.loads(linkSecCropBoxMaxData)[0]
                                        linkCropBoxMaxY = json.loads(linkSecCropBoxMaxData)[1]
                                        linkCropBoxMaxZ = json.loads(linkSecCropBoxMaxData)[2]
                                        
                                        if linkCropBoxMinX != actualLinkCropBoxMinX or \
                                                linkCropBoxMinY != actualLinkCropBoxMinY or \
                                                linkCropBoxMinZ != actualLinkCropBoxMinZ or \
                                                linkCropBoxMaxX != actualLinkCropBoxMaxX or \
                                                linkCropBoxMaxY != actualLinkCropBoxMaxY or \
                                                linkCropBoxMaxZ != actualLinkCropBoxMaxZ:
                                            CropBoxSections()
                                            if abortProcessFlag == True:
                                                continue

                            tg = TransactionGroup(doc, 'Compare & Modify Sections')
                            tg.Start()
                            # delete the elements that the user decided to delete
                            if elementsToDelete:
                                t = Transaction(doc, 'Delete Sections')
                                t.Start()
                                for elementToDelete in elementsToDelete:
                                    try:
                                        doc.Delete(elementToDelete.Id)
                                    except:
                                        None
                                t.Commit()
                                
                            # delete the referenced sections from the model
                            if liveReferencedSections:
                                t = Transaction(doc, 'Delete Referenced Sections')
                                t.Start()
                                for liveReferencedSection in liveReferencedSections:
                                    try:
                                        doc.Delete(liveReferencedSection.Id)
                                    except:
                                        None
                                t.Commit()

                            # copy the referenced sections from the linked model that were deleted
                            if linkReferencedSections:
                                main(linkReferencedSections, selectedViewPlan, linkDoc, Main_link_selection, LinkViews, True)
                                
                            # rename the elements that the user decided to rename
                            if elementsToRename:
                                t = Transaction(doc, 'Delete Sections')
                                t.Start()
                                for elementToRename in elementsToRename:
                                    elementNameParam = elementToRename[0].get_Parameter(BuiltInParameter.VIEW_NAME)
                                    elementNameParam.Set(elementToRename[1])
                                    
                                    # update the schema with the updated dictionary
                                    retrievedEntity = elementToRename[0].GetEntity(schemaSections)
                                    fieldSection = schemaSections.GetField("CopySectionsData")
                                    retrievedDataSection = retrievedEntity.Get[IDictionary[str, str]](fieldSection)
                                    retrievedEntity.Set[IDictionary[str, str]]("CopySectionsData", elementToRename[2])
                                    elementToRename[0].SetEntity(retrievedEntity)
                                t.Commit()
                                
                            # move the elements that the user decided to move
                            if elementsToMove:
                                t = Transaction(doc, 'Move Sections')
                                t.Start()
                                for elementToMove in elementsToMove:
                                    isElementPinned = elementToMove[0].Pinned
                                    if (isElementPinned):
                                        elementToMove[0].Pinned = False
                                    # move the section
                                    ElementTransformUtils.MoveElement(doc, elementToMove[0].Id, elementToMove[1])
                                    if (isElementPinned):
                                        elementToMove[0].Pinned = True
                                    
                                    # get the new location of the live section and update the dictionary
                                    secLocation = elementToMove[0].GetDependentElements(sketchPlaneFilter)
                                    secLocationPlane = doc.GetElement(secLocation[0]).GetPlane()
                                    secLocationOrigin = secLocationPlane.Origin
                                    secLocationRound = [round(secLocationOrigin.X, 5), round(secLocationOrigin.Y, 5),  round(secLocationOrigin.Z, 5)]
                                    elementToMove[2]["ElementLocationData"] = json.dumps(secLocationRound)
                                    
                                    # update the schema with the updated dictionary
                                    retrievedEntity = elementToMove[0].GetEntity(schemaSections)
                                    fieldSection = schemaSections.GetField("CopySectionsData")
                                    retrievedDataSection = retrievedEntity.Get[IDictionary[str, str]](fieldSection)
                                    retrievedEntity.Set[IDictionary[str, str]]("CopySectionsData", elementToMove[2])
                                    elementToMove[0].SetEntity(retrievedEntity)
                                t.Commit()
                            
                            # change the far clip offset of elements that the user decided to change
                            if elementsToChangeFarClip:
                                t = Transaction(doc, 'Change Far Clip Offset of Sections')
                                t.Start()
                                for elementToChangeFarClip in elementsToChangeFarClip:
                                    elementToSetFarClipOffset = elementToChangeFarClip[0].get_Parameter(BuiltInParameter.VIEWER_BOUND_OFFSET_FAR)
                                    elementToSetFarClipOffset.Set(elementToChangeFarClip[1])
                                    
                                    # update the schema with the updated dictionary
                                    retrievedEntity = elementToChangeFarClip[0].GetEntity(schemaSections)
                                    fieldSection = schemaSections.GetField("CopySectionsData")
                                    retrievedDataSection = retrievedEntity.Get[IDictionary[str, str]](fieldSection)
                                    retrievedEntity.Set[IDictionary[str, str]]("CopySectionsData", elementToChangeFarClip[2])
                                    elementToChangeFarClip[0].SetEntity(retrievedEntity)
                                t.Commit()
                            
                            # change the crop box of elements that the user decided to change
                            if elementsToChangeCropBox:
                                t = Transaction(doc, 'Change Crop Box of Sections')
                                t.Start()
                                for elementToChangeCropBox in elementsToChangeCropBox:
                                    liveViewSectionList = elementToChangeCropBox[0].GetDependentElements(viewSectionFilter)
                                    liveViewSection = liveViewSectionList[0]
                                    CropBox = doc.GetElement(liveViewSection).CropBox
                                    CropBox.Min = elementToChangeCropBox[1]
                                    CropBox.Max = elementToChangeCropBox[2]
                                    doc.GetElement(liveViewSection).CropBox = CropBox
                                    
                                    # update the schema with the updated dictionary
                                    retrievedEntity = elementToChangeCropBox[0].GetEntity(schemaSections)
                                    fieldSection = schemaSections.GetField("CopySectionsData")
                                    retrievedDataSection = retrievedEntity.Get[IDictionary[str, str]](fieldSection)
                                    retrievedEntity.Set[IDictionary[str, str]]("CopySectionsData", elementToChangeCropBox[3])
                                    elementToChangeCropBox[0].SetEntity(retrievedEntity)
                                t.Commit()
                            
                            # if there are new sections in the selected view from the linked model
                            if LinkViewsCollectSections:
                                main(LinkViewsCollectSections, selectedViewPlan, linkDoc, Main_link_selection, LinkViews)
                            
                            tg.Assimilate()
                                    
            else:
                # if the schema doesn't exist at all in the model
                DavidEng.ErrorWindow(errortext="We couldn't find any section in the model that copied with the 'Copy Sections' tool so we don't have any section to compare.").ShowDialog()
        else:
            DavidEng.ErrorWindow(errortext="We couldn't find any section in the active view.").ShowDialog()
    else:
        DavidEng.ErrorWindow(errortext="In order to be able to run this tool your Active View must be a Plan View").ShowDialog()
else:
    DavidEng.ErrorWindow(errortext="You don't have permission\nto use the tool").ShowDialog()