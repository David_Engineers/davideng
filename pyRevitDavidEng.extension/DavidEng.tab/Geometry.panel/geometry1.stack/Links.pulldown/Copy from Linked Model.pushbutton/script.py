"""Copies selected elements from a linked project to the host project."""
# pylint: disable=E0401,W0703,W0613
from pyrevit import coreutils, UI, revit, DB, forms, script
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, Transaction, ElementId, RevitLinkInstance, ElementTransformUtils, XYZ
from Autodesk.Revit.UI.Selection import ObjectType, ISelectionFilter
from Autodesk.Revit import Exceptions
import DavidEng

from System.Collections.Generic import List

# dependencies
import clr
clr.AddReference('System.Windows.Forms')

# find the path of ErrorWindow.xaml
xamlfile = script.get_bundle_file('ErrorWindow.xaml')

# import WPF creator and base Window
import wpf
from System import Windows

# App and Author name
__title__ = 'Copy from\nLinked Model'
__authors__ = 'Yuval Dolin'

# Identify the current Revit file
doc = __revit__.ActiveUIDocument.Document
uidoc = __revit__.ActiveUIDocument

# collect linked revit documents 
linkedDoc = FilteredElementCollector(doc, doc.ActiveView.Id).OfClass(RevitLinkInstance)

# if the user selected elements before runing the add-in
selection = [doc.GetElement(id) for id in uidoc.Selection.GetElementIds()]

# Define class to filter elements of category to select 
class CustomSelectionFilter(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, self.category).Id:
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

# Define class to filter elements of category to select 
class LinkCustomSelectionFilter(ISelectionFilter):
	def AllowElement(self, e):
		if e.Id == Link_selection.Id:
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

# Show Error Window Form
class ErrorWindow(Windows.Window):
    def __init__(self, errortext):
        self.errortext=errortext
        wpf.LoadComponent(self, xamlfile)
        self.error.Text = "{}".format(self.errortext)

    def ok(self, sender, args):
        self.Close()

class CopyUseDestination(DB.IDuplicateTypeNamesHandler):
    def OnDuplicateTypeNamesFound(self, args):
        return DB.DuplicateTypeAction.UseDestinationTypes

if (DavidEng.checkPermission()):
    if linkedDoc.GetElementCount() != 0:
        if not selection:
            customFilter = CustomSelectionFilter(DB.BuiltInCategory.OST_RvtLinks)
            try:
                Link_selection_reference = uidoc.Selection.PickObject(ObjectType.Element, customFilter, "Select Link")
                Link_selection = doc.GetElement(Link_selection_reference)
            except Exceptions.OperationCanceledException:
                import sys
                sys.exit()
        else:
            Link_selection_list = []
            Link_selection = None
            for element in selection:
                if element.Category.Name == 'RVT Links':
                    Link_selection_list.append(element)
            if Link_selection_list == []:
                ErrorWindow(errortext="You have not selected any Link").ShowDialog()
            elif len(Link_selection_list) > 1:
                ErrorWindow(errortext="You have to select only one Link").ShowDialog()
            else:
                Link_selection = Link_selection_list[0]
        if not Link_selection == None:
            # get the linked document and transform 
            linkDoc = Link_selection.GetLinkDocument()
            transform = Link_selection.GetTotalTransform()
            TransOrigin = transform.Origin
            transform_Z_2D = transform
            if not TransOrigin.Z == 0:
                transform_Z_2D.Origin = XYZ(TransOrigin.X, TransOrigin.Y, 0)
            try:
                LinkCustomFilter = LinkCustomSelectionFilter()
                Elements_selection = [doc.GetElement(reference) and linkDoc.GetElement(reference.LinkedElementId) for reference in uidoc.Selection.PickObjects(
                    ObjectType.LinkedElement, LinkCustomFilter, "Select Elements")]
            except Exceptions.OperationCanceledException:
                import sys
                sys.exit()
            if not Elements_selection == []:
                # list of elements ids 
                ids_2D = List[ElementId]()
                ids_3D = List[ElementId]()
                # add all elements ids to list. separate between 2D elements and 3D elements.
                viewFlag = False
                viewId = None
                for i in Elements_selection:
                    if i.OwnerViewId.IntegerValue < 0:
                        try:
                            ids_3D.Add(i.Id)
                        except Exception:
                            a = 0
                    else:
                        try:
                            ids_2D.Add(i.Id)
                        except Exception:
                            a = 0
                        if viewFlag == False:
                            viewId = i.OwnerViewId
                            viewFlag = True
                # define the relevant view for 2D elements
                linkViews = FilteredElementCollector(linkDoc).OfCategory(BuiltInCategory.OST_Views)
                for view in linkViews:
                    if view.Id == viewId:
                        linkActiveView = view
                        break
                options = DB.CopyPasteOptions()
                options.SetDuplicateTypeNamesHandler(CopyUseDestination())
                # copy elements
                with revit.Transaction('Copy Elements from Linked Model', log_errors=False):
                    try:
                        copy_2D = ElementTransformUtils.CopyElements(linkActiveView, ids_2D, doc.ActiveView, transform_Z_2D, options)
                    except Exception:
                        a = 0
                    try:
                        copy_3D = ElementTransformUtils.CopyElements(linkDoc, ids_3D, doc, transform, options)
                    except Exception:
                        a = 0
            else:
                ErrorWindow(errortext="You have not selected any Element").ShowDialog()
    else:
        ErrorWindow(errortext="You don't have any visiable link on your active view").ShowDialog()
else:
    DavidEng.ErrorWindow(errortext="You don't have permission\nto use the tool").ShowDialog()