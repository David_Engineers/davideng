"""Gathers the values from the parameter 'Category of Use' (floor parameter that added by the user in Revit) and ask the user to define Dead Load and Live Load for each 'Category of Use' (Fz force).
The script will load the floors (that got value for the 'Category of Use' parameter) with the user inputs."""
# pylint: disable=E0401,W0703,W0613
from pyrevit import coreutils, UI, revit, DB, forms, script
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, BuiltInParameter, Transaction, XYZ, ElementId, RevitLinkInstance, ElementTransformUtils, IndependentTag, TagMode, SpotDimensionType
from Autodesk.Revit.UI.Selection import ObjectType, ISelectionFilter
from Autodesk.Revit.UI import UIApplication, RevitCommandId, PostableCommand
from Autodesk.Revit import Exceptions
import DavidEng

from System.Collections.Generic import List

# dependencies
import clr
clr.AddReference('System.Windows.Forms')

# find the path of InstructionsWindow.xaml
InstructionsXamlFile = script.get_bundle_file('InstructionsWindow.xaml')

# find the path of InstructionsWindow.xaml
InstructionsXamlFile = script.get_bundle_file('InstructionsWindow.xaml')

# import WPF creator and base Window
import wpf
from System import Windows

# App and Author name
__title__ = 'Copy Spot Elevations'
__authors__ = 'Yuval Dolin'

# Identify the current Revit file
doc = __revit__.ActiveUIDocument.Document
uidoc = __revit__.ActiveUIDocument
uiapp = UIApplication(doc.Application)

# collect linked revit documents 
linkedDoc = FilteredElementCollector(doc, doc.ActiveView.Id).OfClass(RevitLinkInstance)

# if the user selected elements before runing the add-in
selection = [doc.GetElement(id) for id in uidoc.Selection.GetElementIds()]

# Creating collectior instance and collecting all the Levels from the model
LevelsCollector = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Levels).WhereElementIsNotElementType()

# Define class to filter elements of category to select 
class CustomSelectionFilter(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, self.category).Id:
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

# Define class to filter elements of category to select 
class LinkCustomSelectionFilter(ISelectionFilter):
	def AllowElement(self, e):
		if e.Id == Link_selection.Id:
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		revitlinkinstance = doc.GetElement(ref)
		docLink = revitlinkinstance.GetLinkDocument()
		eScopeBoxLink = docLink.GetElement(ref.LinkedElementId)
		if (eScopeBoxLink.Category.Name == "Spot Elevations"):
			return True

class InstructionsWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name):
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)

    def close(self, sender, args):
        self.Close()

    def settings(self, sender, args):
        self.Close()
        uiapp.PostCommand(RevitCommandId.LookupPostableCommandId(PostableCommand.VisibilityOrGraphics )) 

def SelectLink():
    if not selection:
        customFilter = CustomSelectionFilter(DB.BuiltInCategory.OST_RvtLinks)
        try:
            Link_selection_reference = uidoc.Selection.PickObject(ObjectType.Element, customFilter, "Select Link")
            Link_selection = doc.GetElement(Link_selection_reference)
            return Link_selection
        except Exceptions.OperationCanceledException:
            import sys
            sys.exit()
    else:
        Link_selection_list = []
        Link_selection = None
        for element in selection:
            if element.Category.Name == 'RVT Links':
                Link_selection_list.append(element)
        if Link_selection_list == []:
            DavidEng.ErrorWindow(errortext="You have not selected any Link").ShowDialog()
        elif len(Link_selection_list) > 1:
            DavidEng.ErrorWindow(errortext="You have to select only one Link").ShowDialog()
        else:
            Link_selection = Link_selection_list[0]
            return Link_selection

def StableRepresentation(ref):
    refString = ref.CreateLinkReference(Link_selection).ConvertToStableRepresentation(doc)
    ReplaceRVTLINK = refString.replace(":RVTLINK/", ":0:RVTLINK:")
    refObject = ref.CreateLinkReference(Link_selection).ParseFromStableRepresentation(doc, ReplaceRVTLINK)
    LinkTypeId = Link_selection.GetTypeId()
    refString = refObject.ConvertToStableRepresentation(doc)
    ReplaceLinkTypeId = refString.replace(str(LinkTypeId)+":", "")
    FinalRefObject = ref.CreateLinkReference(Link_selection).ParseFromStableRepresentation(doc, ReplaceLinkTypeId)
    return FinalRefObject

def setDisplayElevations(spotElevationsList, displayElevationList):
    viewTypeId = doc.ActiveView.GetTypeId()
    viewType = doc.GetElement(viewTypeId)
    viewDirection = viewType.get_Parameter(BuiltInParameter.PLAN_VIEW_VIEW_DIR).AsValueString()
    if viewDirection == 'Up':
        try:
            tempViewType = viewType.Duplicate('Temp')
            nameFlag = True
        except:
            nameFlag = False
        nameIndex = 1
        while nameFlag == False:
            try:
                tempViewType = viewType.Duplicate('Temp ' + str(nameIndex))
                nameFlag = True
            except:
                nameFlag = False
                nameIndex = nameIndex + 1
        tempViewType.get_Parameter(BuiltInParameter.PLAN_VIEW_VIEW_DIR).Set(0)
        doc.ActiveView.get_Parameter(BuiltInParameter.ELEM_TYPE_PARAM).Set(tempViewType.Id)
        doc.Regenerate()
        i = 0
        while i < len(spotElevationsList):
            spotElevationsList[i].get_Parameter(BuiltInParameter.SPOT_ELEV_DISPLAY_ELEVATIONS).Set(displayElevationList[i])
            i += 1
        doc.ActiveView.get_Parameter(BuiltInParameter.ELEM_TYPE_PARAM).Set(viewType.Id)
        doc.Delete(tempViewType.Id)
        doc.Regenerate()
    else:
        i = 0
        while i < len(spotElevationsList):
            spotElevationsList[i].get_Parameter(BuiltInParameter.SPOT_ELEV_DISPLAY_ELEVATIONS).Set(displayElevationList[i])
            i += 1

def CopySpotElevations(SpotElevations_selection):
    SpotDimensionTypeCollector = FilteredElementCollector(doc).OfClass(SpotDimensionType)
    spotElevationsList = []
    displayElevationList = []
    for spotElevation in SpotElevations_selection:
        SpotFamilyName = spotElevation.get_Parameter(BuiltInParameter.ELEM_FAMILY_PARAM).AsValueString()
        SpotTypeName = spotElevation.get_Parameter(BuiltInParameter.ELEM_TYPE_PARAM).AsValueString()
        checkFlag = False
        for SpotType in SpotDimensionTypeCollector:
            if SpotType.FamilyName == SpotFamilyName and SpotType.get_Parameter(BuiltInParameter.SYMBOL_NAME_PARAM).AsString() == SpotTypeName:
                ChangeSpotType = SpotType.Id
                checkFlag = True
                break
        if checkFlag == False:
            CopyTypeList = List[ElementId]()
            CopyTypeId = spotElevation.get_Parameter(BuiltInParameter.ELEM_TYPE_PARAM).AsElementId()
            CopyTypeList.Add(CopyTypeId)
            copy_3D = ElementTransformUtils.CopyElements(linkDoc, CopyTypeList, doc, None, None)
            ChangeSpotType = copy_3D[0]
        DisplayElevation = spotElevation.get_Parameter(BuiltInParameter.SPOT_ELEV_DISPLAY_ELEVATIONS).AsValueString()
        spotElevPoint = spotElevation.Location.Point
        NewSpotX = TransOrigin.X + (spotElevPoint.X * BasisXX) + ((spotElevPoint.Y * BasisXY) * -Determinant)
        NewSpotY = TransOrigin.Y + (spotElevPoint.Y * BasisYY) + ((spotElevPoint.X * BasisYX) * -Determinant)
        NewSpotXYZ = XYZ(NewSpotX, NewSpotY, 0)
        refArray = spotElevation.References
        FinalRefObject = StableRepresentation(refArray[0])
        createSpotElevation = doc.Create.NewSpotElevation(doc.ActiveView, FinalRefObject, NewSpotXYZ, NewSpotXYZ, NewSpotXYZ, NewSpotXYZ, False)
        createSpotElevation.ChangeTypeId(ChangeSpotType)
        relativeBae = spotElevation.get_Parameter(BuiltInParameter.SPOT_ELEV_RELATIVE_BASE).AsValueString()
        for level in LevelsCollector:
            if level.Name == relativeBae:
                createSpotElevation.get_Parameter(BuiltInParameter.SPOT_ELEV_RELATIVE_BASE).Set(level.Id)
                break
        displayElevationList.append(spotElevation.get_Parameter(BuiltInParameter.SPOT_ELEV_DISPLAY_ELEVATIONS).AsInteger())
        spotElevationsList.append(createSpotElevation)
    
    setDisplayElevations(spotElevationsList, displayElevationList)

    InstructionsWindow(InstructionsXamlFile).show(modal=True)

if (DavidEng.checkPermission()):
    if linkedDoc.GetElementCount() != 0:
        Link_selection = SelectLink()
        if not Link_selection == None:
            # get the linked document and transform 
            linkDoc = Link_selection.GetLinkDocument()
            transform = Link_selection.GetTotalTransform()
            BasisXX = transform.BasisX.X
            BasisXY = transform.BasisX.Y
            BasisYY = transform.BasisY.Y
            BasisYX = transform.BasisY.X
            Determinant = transform.Determinant
            TransOrigin = transform.Origin
            try:
                LinkCustomFilter = LinkCustomSelectionFilter()
                SpotElevations_selection = [doc.GetElement(reference) and linkDoc.GetElement(reference.LinkedElementId) for reference in uidoc.Selection.PickObjects(
                    ObjectType.LinkedElement, LinkCustomFilter, "Select Spot Elevations")]
            except Exception, ex:
                SpotElevations_selection = None 
            if SpotElevations_selection:
                SpotElevations_Ids = []
                SpotElevations_list = []
                for spotElevation in SpotElevations_selection:
                    if not spotElevation.Id in SpotElevations_Ids:
                        SpotElevations_Ids.append(spotElevation.Id)
                        SpotElevations_list.append(spotElevation)
                with revit.Transaction('Copy Spot Elevations', log_errors=False):
                    CopySpotElevations(SpotElevations_list)
            else:
                pass
else:
    DavidEng.ErrorWindow(errortext="You don't have permission\nto use the tool").ShowDialog()