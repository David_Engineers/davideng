# # -*- coding: utf-8 -*-

import sys
import os
sys.path.append(os.path.dirname(__file__))

import System
import json
from System import String
from System.Collections.Generic import IDictionary, Dictionary
from pyrevit import DB, revit, script
from pyrevit.revit.db import query
from pyrevit import framework
from pyrevit import PyRevitException
from pyrevit import HOST_APP
from Autodesk.Revit.DB import XYZ
from Autodesk.Revit.DB import Transaction, ElementId, FilteredElementCollector, BuiltInCategory, ElementTransformUtils, ViewSection, SketchPlane, BuiltInParameter, ReferenceableViewUtils, Outline
import DavidEng

from System.Collections.Generic import List

# find the path of ErrorWindow.xaml
alertXamlFile = script.get_bundle_file('CompareAlertWindowWithHebrew.xaml')

# import WPF creator and base Window
import wpf
from System import Windows

doc = __revit__.ActiveUIDocument.Document
uidoc = __revit__.ActiveUIDocument

# get the extensible storage schma
schemaGuid = "e64e3bab-7b93-415a-a20a-4d09ec106eaf"
schemaSections = DB.ExtensibleStorage.Schema.Lookup(System.Guid(schemaGuid))

viewSectionFilter = DB.ElementClassFilter(ViewSection)
sketchPlaneFilter = DB.ElementClassFilter(SketchPlane)

class CopyUseDestination(DB.IDuplicateTypeNamesHandler):
    def OnDuplicateTypeNamesFound(self, args):
        return DB.DuplicateTypeAction.UseDestinationTypes

# Show Alert Window Form
class AlertWindow(Windows.Window):
    def __init__(self, alertText, alertTextHebrew):
        # indicate if the user decided to modify the element or not
        global modifyElement
        modifyElement = False
        # indicate if the user decided to abort the process by clicking X button or to continue by clicking Yes/No
        global abortProcessFlag
        abortProcessFlag = True
        self.alertText = alertText
        self.alertTextHebrew = alertTextHebrew
        wpf.LoadComponent(self, alertXamlFile)
        self.alert.Text = "{}".format(self.alertText)
        self.alertHebrew.Text = "{}".format(self.alertTextHebrew)

    def yes(self, sender, args):
        self.Close()
        # indicate if the user decided to modify the element or not
        global modifyElement
        modifyElement = True
        global abortProcessFlag
        abortProcessFlag = False
        
    def no(self, sender, args):
        self.Close()
        global abortProcessFlag
        abortProcessFlag = False

# Show Approve Window Form
class ApproveWindow(Windows.Window):
    def __init__(self, alertText, alertTextHebrew):
        # indicate if the user approve the modification of the element or not
        global approveElement
        approveElement = False
        # indicate if the user decided to abort the process by clicking X button or to continue by clicking Yes/No
        global abortProcessFlag
        abortProcessFlag = True
        self.alertText = alertText
        self.alertTextHebrew = alertTextHebrew
        wpf.LoadComponent(self, alertXamlFile)
        self.alert.Text = "{}".format(self.alertText)
        self.alertHebrew.Text = "{}".format(self.alertTextHebrew)

    def yes(self, sender, args):
        self.Close()
        # indicate if the user approve the modification of the element or not
        global approveElement
        approveElement = True
        global abortProcessFlag
        abortProcessFlag = False
        
    def no(self, sender, args):
        self.Close()
        global abortProcessFlag
        abortProcessFlag = False

def CopyDependentSections(CopySections, sectionName, sectionDependentName, depSectionsUniqueId, linkDepSectionsUniqueId):
    i = 0
    dependentSectionsList = []
    finalDepSectionsUniqueId = []
    finalLinkDepSectionsUniqueId = []
    depElementBBoxMaxDataList = []
    depElementBBoxMinDataList = []
    
    # collect sections from current model (original and dependent) and it's names
    modelSections = sectionsCollector(doc, None)
    modelSectionsNames = [(j.Name) for j in modelSections]
    
    for secName in sectionName:
        # Change the name of the section if it's already exist in the model
        copyNumber = 1
        originalSecName = secName
        while (secName in modelSectionsNames):
            secName = originalSecName + " Copy " + str(copyNumber)
            copyNumber += 1
        
        dependentName = sectionDependentName[i]
        for sec in CopySections:
            section = doc.GetElement(sec)
            try:
                if dependentName == section.Name:
                    sectionViewList = section.GetDependentElements(viewSectionFilter)
                    sectionView = doc.GetElement(sectionViewList[0])
                    # Duplicate the section as a dependent
                    newViewId = sectionView.Duplicate(DB.ViewDuplicateOption.AsDependent)
                    newVieweElement = doc.GetElement(newViewId)
                    newSectionView = newVieweElement.GetDependentElements(DB.ElementCategoryFilter(DB.BuiltInCategory.OST_Viewers))
                    newView = doc.GetElement(newSectionView[0])
                    dependentSectionsList.append(newView)
                    # Change the name of the dependent view according to its name in the linked model
                    parameter = newView.get_Parameter(DB.BuiltInParameter.VIEW_NAME)
                    parameter.Set(secName)
                    finalDepSectionsUniqueId.append(depSectionsUniqueId[i])
                    finalLinkDepSectionsUniqueId.append(linkDepSectionsUniqueId[i].ToString())
                    
                    bboxMax = newView.get_BoundingBox(doc.ActiveView).Max
                    bboxMaxRound = [round(bboxMax.X, 5), round(bboxMax.Y, 5),  round(bboxMax.Z, 5)]
                    depElementBBoxMaxDataList.append(bboxMaxRound)

                    bboxMin = newView.get_BoundingBox(doc.ActiveView).Min
                    bboxMinRound = [round(bboxMin.X, 5), round(bboxMin.Y, 5),  round(bboxMin.Z, 5)]
                    depElementBBoxMinDataList.append(bboxMinRound)
                    break
            except:
                pass
        i += 1
    return dependentSectionsList, finalDepSectionsUniqueId, finalLinkDepSectionsUniqueId, depElementBBoxMaxDataList, depElementBBoxMinDataList

def get_biparam_stringequals_filter(bip_paramvalue_dict):
    filters = []
    for bip, fvalue in bip_paramvalue_dict.items():
        bip_id = DB.ElementId(bip)
        bip_valueprovider = DB.ParameterValueProvider(bip_id)
        bip_valuerule = DB.FilterStringRule(bip_valueprovider,
                                            DB.FilterStringEquals(),
                                            fvalue)
        filters.append(bip_valuerule)

    if filters:
        return DB.ElementParameterFilter(
            framework.List[DB.FilterRule](filters)
            )
    else:
        raise PyRevitException('Error creating filters.')

def sectionsCollector(document, viewId):
    sectionsCollectorList = []
    if viewId == None:
        if HOST_APP.is_newer_than(2022):
            collectSections = FilteredElementCollector(document).OfCategory(BuiltInCategory.OST_Viewers) \
                                        .WherePasses(get_biparam_stringequals_filter({DB.BuiltInParameter.ELEM_FAMILY_PARAM: "Section"}))
            collectDetailViews = FilteredElementCollector(document).OfCategory(BuiltInCategory.OST_Viewers) \
                                        .WherePasses(get_biparam_stringequals_filter({DB.BuiltInParameter.ELEM_FAMILY_PARAM: "Detail View"}))
        else:
            collectSections = FilteredElementCollector(document).OfCategory(BuiltInCategory.OST_Viewers) \
                                        .WherePasses(query.get_biparam_stringequals_filter({DB.BuiltInParameter.ELEM_FAMILY_PARAM: "Section"}))
            collectDetailViews = FilteredElementCollector(document).OfCategory(BuiltInCategory.OST_Viewers) \
                                        .WherePasses(query.get_biparam_stringequals_filter({DB.BuiltInParameter.ELEM_FAMILY_PARAM: "Detail View"}))
    else:
        if HOST_APP.is_newer_than(2022):
            collectSections = FilteredElementCollector(document, viewId).OfCategory(BuiltInCategory.OST_Viewers) \
                                        .WherePasses(get_biparam_stringequals_filter({DB.BuiltInParameter.ELEM_FAMILY_PARAM: "Section"}))
            collectDetailViews = FilteredElementCollector(document, viewId).OfCategory(BuiltInCategory.OST_Viewers) \
                                        .WherePasses(get_biparam_stringequals_filter({DB.BuiltInParameter.ELEM_FAMILY_PARAM: "Detail View"}))
        else:
            collectSections = FilteredElementCollector(document, viewId).OfCategory(BuiltInCategory.OST_Viewers) \
                                        .WherePasses(query.get_biparam_stringequals_filter({DB.BuiltInParameter.ELEM_FAMILY_PARAM: "Section"}))
            collectDetailViews = FilteredElementCollector(document, viewId).OfCategory(BuiltInCategory.OST_Viewers) \
                                        .WherePasses(query.get_biparam_stringequals_filter({DB.BuiltInParameter.ELEM_FAMILY_PARAM: "Detail View"}))
    for section in collectSections:
        sectionsCollectorList.append(section)
    for detailView in collectDetailViews:
        sectionsCollectorList.append(detailView)
        
    return sectionsCollectorList

def CreateExtensionStorageSection(copiedSec, linkElementUniqueIdData, linkNameData, linkSecCropBoxMinData, linkSecCropBoxMaxData, linkSecFarClipOffset, linkSecLocation, linkSecName, linkSecOrientation, \
    elementBBoxMaxData, elementBBoxMinData, linkElementBBoxMaxData, linkElementBBoxMinData, elementLocationData, elementOrientation):
    schemaBuilder=DB.ExtensibleStorage.SchemaBuilder(System.Guid("e64e3bab-7b93-415a-a20a-4d09ec106eaf"))
    schemaBuilder.SetReadAccessLevel(DB.ExtensibleStorage.AccessLevel.Public)
    schemaBuilder.SetWriteAccessLevel(DB.ExtensibleStorage.AccessLevel.Public)
    schemaBuilder.SetVendorId("YuvalDolin")
    schemaBuilder.SetSchemaName("DavidEng_Schema_CopySectionsData")
    fieldBuilder = schemaBuilder.AddMapField("CopySectionsData", String, String)
    fieldBuilder.SetDocumentation("The Data of the sections from the current and linked models")
    schema = schemaBuilder.Finish()
    
    entity =DB.ExtensibleStorage.Entity(schema)
    dictionary = Dictionary[str, str]()
    dictionary.Add("ElementBBoxMaxData", json.dumps(elementBBoxMaxData))
    dictionary.Add("ElementBBoxMinData", json.dumps(elementBBoxMinData))
    dictionary.Add("ElementLocationData", json.dumps(elementLocationData))
    dictionary.Add("ElementNameData", str(copiedSec.Name))
    dictionary.Add("ElementOrientationData", json.dumps(elementOrientation))
    dictionary.Add("ElementUniqueIdData", str(copiedSec.UniqueId))
    dictionary.Add("Link_ElementUniqueIdData", linkElementUniqueIdData)
    dictionary.Add("Link_NameData", linkNameData)
    dictionary.Add("LinkSec_BBoxMaxData", json.dumps(linkElementBBoxMaxData))
    dictionary.Add("LinkSec_BBoxMinData", json.dumps(linkElementBBoxMinData))
    dictionary.Add("LinkSec_CropBoxMinData", json.dumps(linkSecCropBoxMinData))
    dictionary.Add("LinkSec_CropBoxMaxData", json.dumps(linkSecCropBoxMaxData))
    dictionary.Add("LinkSec_FarClipOffsetData", str(linkSecFarClipOffset))
    dictionary.Add("LinkSec_LocationData", json.dumps(linkSecLocation))
    dictionary.Add("LinkSec_NameData", str(linkSecName))
    dictionary.Add("LinkSec_OrientationData", json.dumps(linkSecOrientation))

    entity.Set[IDictionary[str, str]]("CopySectionsData", dictionary)
    copiedSec.SetEntity(entity)

def ZoomToElement(elementToShow):
    # Getting the geometry and the bounding box of the rebar shape
    elementBBox = elementToShow.BoundingBox[doc.ActiveView]
    if not elementBBox == None:
        outlinElement = Outline(elementBBox.Min, elementBBox.Max) 
        active_ui_views = uidoc.GetOpenUIViews()
        # find current uiview
        current_ui_view_list = []
        for active_ui_view in active_ui_views:
            if active_ui_view.ViewId == doc.ActiveView.Id:
                current_ui_view_list.append(active_ui_view)
        for current_ui_view in current_ui_view_list:
            current_ui_view.ZoomAndCenterRectangle(outlinElement.MaximumPoint, outlinElement.MinimumPoint)

def main(LinkViewsCollectSections, selectedViewPlan, linkDoc, Main_link_selection, linkViews, skip_windows = False):
    SectionsToCopy = []
    sectionName = []
    sectionDependentName = []
    linkDependentSection = []
    SectionsList = []
    depSectionsUniqueId = []
    linkDepSectionsUniqueId = []
    global abortProcessFlag
    abortProcessFlag = False
    
    transform = Main_link_selection.GetTotalTransform()
    TransOrigin = transform.Origin
    transform_Z_2D = transform
    if not TransOrigin.Z == 0:
        transform_Z_2D.Origin = XYZ(TransOrigin.X, TransOrigin.Y, 0)
    
    for section in LinkViewsCollectSections:
        # Continue only if the section is not reference to other view
        if section.OwnerViewId.IntegerValue == -1:
            dependencyValue = section.LookupParameter("Dependency").AsString()
            # Check if the section is depentent or not (the user can "Duplicate as a Dependent" the section)
            if not dependencyValue.startswith('Dependent on '):
                SectionsToCopy.append(section.Id)
                SectionsList.append(section)
            else:
                sectionName.append(section.Name)
                sectionDependentName.append(dependencyValue.replace('Dependent on ', '', 1))
                linkDependentSection.append(section)
                depSectionsUniqueId.append(section.UniqueId)
                linkDepSectionsUniqueId.append(section.UniqueId)

    SectionsToCopySorted = List[ElementId]()
    zipped_lists = zip(SectionsToCopy, SectionsList)
    sorted_zipped_lists = sorted(zipped_lists)
    sorted_sections_list = [element for _, element in sorted_zipped_lists]
    sortedSectionsUniqueId = []
    linkOriginalSectionsUniqueId = []
    for secToCopy in sorted_sections_list:
        SectionsToCopySorted.Add(secToCopy.Id)
        sortedSectionsUniqueId.append(secToCopy.UniqueId)
        linkOriginalSectionsUniqueId.append(secToCopy.UniqueId.ToString())

    options = DB.CopyPasteOptions()
    options.SetDuplicateTypeNamesHandler(CopyUseDestination())
    CopySections = None
    
    # Transform for temporary section in order to be able to copy rotated reference sections
    t = DB.Transform.Identity
    t.Origin = XYZ(0, 0, 0)
    t.BasisX = XYZ(-1, 0, 0)
    t.BasisY = XYZ(0, 0, 1)
    t.BasisZ = XYZ(0, 1, 0)
    section_box = DB.BoundingBoxXYZ()
    section_box.Transform = t
    section_box.Min = DB.XYZ(-10, -10, -10)
    section_box.Max = DB.XYZ(10, 10, 10)
    
    # Split between reference section and original and dependent sections
    refSections = []
    origionalSections = []
    for linkSec in LinkViewsCollectSections:
        if linkSec.ViewSpecific == True:
            refSections.append(linkSec)
        else:
            origionalSections.append(linkSec)

    # collect sections from current model without the new ones
    CurrentModelSections = sectionsCollector(doc, None)

    AllOrigionalSections = []
    AllUniqueIds = []
    for sec in CurrentModelSections:
        if sec.ViewSpecific == False:
            try:
                retrievedEntity = sec.GetEntity(schemaSections)
                fieldSection = schemaSections.GetField("CopySectionsData")
                retrievedDataSection =retrievedEntity.Get[IDictionary[str, str]](fieldSection)
                retrievedDataSectionID = retrievedDataSection["Link_ElementUniqueIdData"]
                AllUniqueIds.append(retrievedDataSectionID)
                AllOrigionalSections.append(sec)
            except:
                pass

    linkSecCropBoxMinDataList = []
    linkSecCropBoxMaxDataList = []
    linkSecFarClipOffsetList = []
    linkSecLocationList = []
    linkSecNameList = []
    linkSecOrientationList = []
    elementBBoxMaxDataList = []
    elementBBoxMinDataList = []
    linkElementBBoxMaxDataList = []
    linkElementBBoxMinDataList = []
    elementLocationDataList = []
    elementOrientationList = []
    
    # copy sections
    counter = 0
    copySectionsCounter = 0
    try:
        CopySections = []
        for sec in SectionsToCopySorted:
            if abortProcessFlag == False:
                t = Transaction(doc, 'Copy Section from Linked Model')
                t.Start()
                alertMessage = "Section '{0}' is missing in the active model.\nWould you like to copy this section?".format(linkDoc.GetElement(sec).Name)
                alertMessageHebrew = ".חסר במודל הנוכחי '{0}' חתך\n?האם ברצונך להעתיק אותו".format(linkDoc.GetElement(sec).Name)
                AlertWindow(alertText=alertMessage, alertTextHebrew=alertMessageHebrew).ShowDialog()
                if modifyElement and abortProcessFlag == False:
                    sectionToCopy = List[ElementId]()
                    sectionToCopy.Add(sec)
                    
                    # Copy the sections that are not dependent or references to other views
                    transformForSec = Main_link_selection.GetTotalTransform()
                    CopySection = ElementTransformUtils.CopyElements(linkDoc, sectionToCopy, doc, transformForSec, options)
                    
                    # Set the 'Hide at scales coarser than' value to be the maximum
                    doc.GetElement(CopySection[0]).get_Parameter(BuiltInParameter.SECTION_COARSER_SCALE_PULLDOWN_METRIC).Set(5000)
                    doc.Regenerate()
                    
                    ZoomToElement(doc.GetElement(CopySection[0]))
                    alertMessage = "Do you approve the copy of section '{0}'?\nIf you click no, it will revert the action.".format(linkDoc.GetElement(sec).Name)
                    alertMessageHebrew = " ?'{0}' האם אתה מאשר את העתקת חתך\n.במידה ותלחץ לא, החתך ימחק".format(linkDoc.GetElement(sec).Name)
                    ApproveWindow(alertText=alertMessage, alertTextHebrew=alertMessageHebrew).ShowDialog()
                    if approveElement:
                        CopySections.append(CopySection[0])
                        originalViewSectionList = linkDoc.GetElement(sec).GetDependentElements(viewSectionFilter)
                        originalViewSection = originalViewSectionList[0]
                        cropBoxMin = linkDoc.GetElement(originalViewSection).CropBox.Min
                        cropBoxMinRound = [round(cropBoxMin.X, 5), round(cropBoxMin.Y, 5),  round(cropBoxMin.Z, 5)]
                        linkSecCropBoxMinDataList.append(cropBoxMinRound)
                        
                        cropBoxMax = linkDoc.GetElement(originalViewSection).CropBox.Max
                        cropBoxMaxRound = [round(cropBoxMax.X, 5), round(cropBoxMax.Y, 5),  round(cropBoxMax.Z, 5)]
                        linkSecCropBoxMaxDataList.append(cropBoxMaxRound)
                        
                        farClipOffset = linkDoc.GetElement(sec).get_Parameter(BuiltInParameter.VIEWER_BOUND_OFFSET_FAR).AsDouble()*30.48
                        linkSecFarClipOffsetList.append(round(farClipOffset, 2))
                        
                        linkSecLocation = linkDoc.GetElement(sec).GetDependentElements(sketchPlaneFilter)
                        linkSecLocationPlane = linkDoc.GetElement(linkSecLocation[0]).GetPlane()
                        linkSecLocationOrigin = linkSecLocationPlane.Origin
                        linkSecLocationRound = [round(linkSecLocationOrigin.X, 5), round(linkSecLocationOrigin.Y, 5),  round(linkSecLocationOrigin.Z, 5)]
                        linkSecLocationList.append(linkSecLocationRound)
                        
                        linkSecNameList.append(linkDoc.GetElement(sec).Name)
                        
                        linkSecOrientationXVector = linkSecLocationPlane.XVec
                        linkSecOrientationRound = [round(linkSecOrientationXVector.X, 5), round(linkSecOrientationXVector.Y, 5),  round(linkSecOrientationXVector.Z, 5)]
                        linkSecOrientationList.append(linkSecOrientationRound)
                        
                        secElement = doc.GetElement(CopySection[0])
                        if not secElement.get_BoundingBox(doc.ActiveView) == None:
                            bboxMax = secElement.get_BoundingBox(doc.ActiveView).Max
                            bboxMaxRound = [round(bboxMax.X, 5), round(bboxMax.Y, 5),  round(bboxMax.Z, 5)]
                            elementBBoxMaxDataList.append(bboxMaxRound)

                            bboxMin = secElement.get_BoundingBox(doc.ActiveView).Min
                            bboxMinRound = [round(bboxMin.X, 5), round(bboxMin.Y, 5),  round(bboxMin.Z, 5)]
                            elementBBoxMinDataList.append(bboxMinRound)
                        else:
                            elementBBoxMaxDataList.append(None)
                            elementBBoxMinDataList.append(None)
                        
                        linkSecElement = linkDoc.GetElement(sec)
                        linkBboxMax = linkSecElement.get_BoundingBox(selectedViewPlan).Max
                        linkBboxMaxRound = [round(linkBboxMax.X, 5), round(linkBboxMax.Y, 5),  round(linkBboxMax.Z, 5)]
                        linkElementBBoxMaxDataList.append(linkBboxMaxRound)

                        linkBboxMin = linkSecElement.get_BoundingBox(selectedViewPlan).Min
                        linkBboxMinRound = [round(linkBboxMin.X, 5), round(linkBboxMin.Y, 5),  round(linkBboxMin.Z, 5)]
                        linkElementBBoxMinDataList.append(linkBboxMinRound)
                        
                        secLocation = secElement.GetDependentElements(sketchPlaneFilter)
                        secLocationPlane = doc.GetElement(secLocation[0]).GetPlane()
                        secLocationOrigin = secLocationPlane.Origin
                        secLocationRound = [round(secLocationOrigin.X, 5), round(secLocationOrigin.Y, 5),  round(secLocationOrigin.Z, 5)]
                        elementLocationDataList.append(secLocationRound)
                        
                        secOrientationXVector = secLocationPlane.XVec
                        secOrientationRound = [round(secOrientationXVector.X, 5), round(secOrientationXVector.Y, 5),  round(secOrientationXVector.Z, 5)]
                        elementOrientationList.append(secOrientationRound)
                        
                        counter += 1
                        doc.Regenerate()
                        copySectionsCounter += 1
                        t.Commit()
                    else:
                        t.RollBack()
                        counter += 1
                        copySectionsCounter += 1
                else:
                    t.RollBack()
                    counter += 1
            else:
                counter += 1
    except:
        sortedSectionsUniqueId = []

    if counter == len(SectionsToCopySorted):    
        # Change the name of the dependent sections if the name of the original one was changed
        i1 = 0
        if not (len(sectionDependentName) == 0):
            while (i1 < len(CopySections)):
                if (doc.GetElement(CopySections[i1]).Name != linkDoc.GetElement(SectionsToCopySorted[i1]).Name):
                    i2 = 0
                    while (i2 < len(sectionDependentName)):
                        if (linkDoc.GetElement(SectionsToCopySorted[i1]).Name == sectionDependentName[i2]):
                            sectionDependentName[i2] = doc.GetElement(CopySections[i1]).Name
                        i2 += 1
                i1 += 1
 
        t = Transaction(doc, 'Copy Sections from Linked Model')
        t.Start()  
         
        dependentSectionsList, finalDepSectionsUniqueId, finalLinkDepSectionsUniqueId, depElementBBoxMaxDataList, depElementBBoxMinDataList = CopyDependentSections(CopySections, sectionName, sectionDependentName, depSectionsUniqueId, linkDepSectionsUniqueId)
        
        t.Commit()
        
        elementBBoxMaxDataList += depElementBBoxMaxDataList
        elementBBoxMinDataList += depElementBBoxMinDataList
        
        i = 0
        while (i < len(linkDependentSection)):                    
            originalViewSectionList = linkDependentSection[i].GetDependentElements(viewSectionFilter)
            originalViewSection = originalViewSectionList[0]
            cropBoxMin = linkDoc.GetElement(originalViewSection).CropBox.Min
            cropBoxMinRound = [round(cropBoxMin.X, 5), round(cropBoxMin.Y, 5),  round(cropBoxMin.Z, 5)]
            linkSecCropBoxMinDataList.append(cropBoxMinRound)
            cropBoxMax = linkDoc.GetElement(originalViewSection).CropBox.Max
            cropBoxMaxRound = [round(cropBoxMax.X, 5), round(cropBoxMax.Y, 5),  round(cropBoxMax.Z, 5)]
            linkSecCropBoxMaxDataList.append(cropBoxMaxRound)
            linkSecFarClipOffsetList.append(None)
            linkSecLocationList.append(None)
            linkSecNameList.append(linkDependentSection[i].Name)
            linkSecOrientationList.append(None)
            
            linkDepSecElement = linkDependentSection[i]
            linkBboxMax = linkDepSecElement.get_BoundingBox(selectedViewPlan).Max
            linkBboxMaxRound = [round(linkBboxMax.X, 5), round(linkBboxMax.Y, 5),  round(linkBboxMax.Z, 5)]
            linkElementBBoxMaxDataList.append(linkBboxMaxRound)

            linkBboxMin = linkDepSecElement.get_BoundingBox(selectedViewPlan).Min
            linkBboxMinRound = [round(linkBboxMin.X, 5), round(linkBboxMin.Y, 5),  round(linkBboxMin.Z, 5)]
            linkElementBBoxMinDataList.append(linkBboxMinRound)
            
            elementLocationDataList.append(None)
            elementOrientationList.append(None)
            
            i += 1
        
        sortedSectionsUniqueId = sortedSectionsUniqueId + finalDepSectionsUniqueId
        copiedSections = []
        for copiedSec in CopySections:
            copiedSections.append(doc.GetElement(copiedSec))
        copiedSections = copiedSections + dependentSectionsList
        
        i = 0
        for copiedSec in copiedSections:
            # collect (add) only the new sections from current model
            AllUniqueIds.append(sortedSectionsUniqueId[i])
            AllOrigionalSections.append(copiedSec)
            i += 1
        
        t = Transaction(doc, 'Create Temporary Section')
        t.Start()  
        
        # Create temporary section in order to be able to copy rotated reference sections
        section_type = doc.GetDefaultElementTypeId(DB.ElementTypeGroup.ViewTypeSection)
        tempSection = DB.ViewSection.CreateSection(doc, section_type, section_box)
        
        t.Commit()
        
        # Copy reference sections
        # collect sections in linked model
        LinkSections = sectionsCollector(linkDoc, None)

        AllLinkOrigionalSections = []
        for sec in LinkSections:
            if sec.ViewSpecific == False:
                AllLinkOrigionalSections.append(sec)
        
        linkRefSectionsUniqueId = []

        t = Transaction(doc, 'Create Temporary Level')
        t.Start()  
        
        # Create temporary level so we will be able to copy the reference sections (relevant when the Origin.Z of the GetTotalTransform is not 0)
        linkViewProjectElevation = selectedViewPlan.GenLevel.ProjectElevation
        tempLevel = DB.Level.Create(doc, linkViewProjectElevation)
        
        t.Commit()
        
        if abortProcessFlag == False:
            for refSec in refSections:
                secToRefer = None
                refSecName = refSec.Name
                for oriLinkSec in AllLinkOrigionalSections:
                    if refSecName == oriLinkSec.Name:
                        refUniqueId = oriLinkSec.UniqueId
                        break
                i = 0
                for oriUniqueId in AllUniqueIds:
                    if oriUniqueId == refUniqueId:
                        secToRefer = AllOrigionalSections[i]
                        break
                    i += 1
                # if the section to refer exist in the current model
                if secToRefer and abortProcessFlag == False:
                    t = Transaction(doc, 'Copy Section from Linked Model')
                    t.Start()
                    if not skip_windows:
                        alertMessage = "Section '{0}' is missing in the active model.\nWould you like to copy this section?".format(refSecName)
                        alertMessageHebrew = ".חסר במודל הנוכחי '{0}' חתך\n?האם ברצונך להעתיק אותו".format(refSecName)
                        AlertWindow(alertText=alertMessage, alertTextHebrew=alertMessageHebrew).ShowDialog()
                    if skip_windows or (modifyElement and abortProcessFlag == False):
                        viewIdToReferenceList = secToRefer.GetDependentElements(viewSectionFilter)
                        viewIdToReference = viewIdToReferenceList[0]
                        LinkViewPlan = selectedViewPlan
                        dependencyValue = LinkViewPlan.LookupParameter("Dependency").AsString()
                        if dependencyValue.startswith('Dependent on '):
                            sectionDependentName = dependencyValue.replace('Dependent on ', '', 1)
                            for view in linkViews:
                                if sectionDependentName == view.Name:
                                    LinkViewPlan = view
                                    break

                        ids_2D = List[ElementId]()
                        ids_2D.Add(refSec.Id)
                        copy_2D = ElementTransformUtils.CopyElements(LinkViewPlan, ids_2D, doc.ActiveView, transform_Z_2D, None)
                        ReferenceableViewUtils.ChangeReferencedView(doc, copy_2D[0], viewIdToReference)
                        
                        if not skip_windows:
                            ZoomToElement(doc.GetElement(copy_2D[0]))
                            alertMessage = "Do you approve the copy of section '{0}'?\nIf you click no, it will revert the action.".format(refSecName)
                            alertMessageHebrew = " ?'{0}' האם אתה מאשר את העתקת חתך\n.במידה ותלחץ לא, החתך ימחק".format(refSecName)
                            ApproveWindow(alertText=alertMessage, alertTextHebrew=alertMessageHebrew).ShowDialog()
                        if skip_windows or approveElement:
                            linkRefSectionsUniqueId.append(refSec.UniqueId.ToString())
                            copiedSections.append(doc.GetElement(copy_2D[0]))
                            
                            linkSecCropBoxMinDataList.append(None)
                            linkSecCropBoxMaxDataList.append(None)
                            linkSecFarClipOffsetList.append(None)
                            linkSecLocationList.append(None)
                            linkSecNameList.append(doc.GetElement(copy_2D[0]).Name)
                            linkSecOrientationList.append(None)
                            
                            secElement = doc.GetElement(copy_2D[0])
                            if not secElement.get_BoundingBox(doc.ActiveView) == None:
                                bboxMax = secElement.get_BoundingBox(doc.ActiveView).Max
                                bboxMaxRound = [round(bboxMax.X, 5), round(bboxMax.Y, 5),  round(bboxMax.Z, 5)]
                                elementBBoxMaxDataList.append(bboxMaxRound)

                                bboxMin = secElement.get_BoundingBox(doc.ActiveView).Min
                                bboxMinRound = [round(bboxMin.X, 5), round(bboxMin.Y, 5),  round(bboxMin.Z, 5)]
                                elementBBoxMinDataList.append(bboxMinRound)
                            else:
                                elementBBoxMaxDataList.append(None)
                                elementBBoxMinDataList.append(None)
                            
                            linkBboxMax = refSec.get_BoundingBox(selectedViewPlan).Max
                            linkBboxMaxRound = [round(linkBboxMax.X, 5), round(linkBboxMax.Y, 5),  round(linkBboxMax.Z, 5)]
                            linkElementBBoxMaxDataList.append(linkBboxMaxRound)

                            linkBboxMin = refSec.get_BoundingBox(selectedViewPlan).Min
                            linkBboxMinRound = [round(linkBboxMin.X, 5), round(linkBboxMin.Y, 5),  round(linkBboxMin.Z, 5)]
                            linkElementBBoxMinDataList.append(linkBboxMinRound)
                            
                            elementLocationDataList.append(None)
                            elementOrientationList.append(None)
                            
                            doc.Regenerate()
                            t.Commit()
                        else:
                            t.RollBack()
                    else:
                        t.RollBack()
        
        t = Transaction(doc, 'Create Temporary Section and Level')
        t.Start()  
        
        # Delete the temporary section
        doc.Delete(tempSection.Id)
        
        # Delete the temporary level
        doc.Delete(tempLevel.Id)
        
        t.Commit()
        
        t = Transaction(doc, 'Write Schema to Sections')
        t.Start()  
        
        linkNameData = Main_link_selection.get_Parameter(BuiltInParameter.ELEM_TYPE_PARAM).AsValueString()
        linkElementUniqueIdDataList = linkOriginalSectionsUniqueId + finalLinkDepSectionsUniqueId + linkRefSectionsUniqueId
        i = 0
        for copiedSec in copiedSections:
            CreateExtensionStorageSection(copiedSec, linkElementUniqueIdDataList[i], linkNameData, linkSecCropBoxMinDataList[i], linkSecCropBoxMaxDataList[i], linkSecFarClipOffsetList[i], linkSecLocationList[i], linkSecNameList[i], linkSecOrientationList[i], \
                elementBBoxMaxDataList[i], elementBBoxMinDataList[i], linkElementBBoxMaxDataList[i], linkElementBBoxMinDataList[i], elementLocationDataList[i], elementOrientationList[i])
            i += 1
        
        t.Commit()