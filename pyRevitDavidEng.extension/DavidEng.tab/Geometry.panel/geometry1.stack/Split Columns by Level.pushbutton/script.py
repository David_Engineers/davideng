"""Split the columns by level in three simple steps - choose the columns, choose the levels and choose offset to split the columns relative to the level."""
from pyrevit import revit, forms, script, DB
from pyrevit.forms import ProgressBar
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, Level, Category, BuiltInParameter, Transaction
from Autodesk.Revit.UI.Selection import ObjectType, ISelectionFilter
from Autodesk.Revit import Exceptions
import DavidEng

# import WPF creator and base Window
import wpf
from System import Windows

# App and Author name
__title__ = 'Split Columns\nby Level'
__authors__ = 'Yuval Dolin'

# Identify the current Revit file
doc = __revit__.ActiveUIDocument.Document
uidoc = __revit__.ActiveUIDocument

# Define class to filter elements of category to select 
class CustomSelectionFilter(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, self.category).Id:
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

class ChooseColumnsWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name):
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)

    # user select all columns
    def all_columns(self, sender, args):
        self.Close()
        columns_collector_all = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_StructuralColumns) \
                                               .WhereElementIsNotElementType().ToElements()
        ChooseLevelsWindow('SelectLevels.xaml', columns=columns_collector_all).show(modal=True)

    # user select specific columns
    def select_columns(self, sender, args):
        self.Close()  
        customFilter = CustomSelectionFilter(DB.BuiltInCategory.OST_StructuralColumns)
        try:
            column_selection = [doc.GetElement(reference) for reference in uidoc.Selection.PickObjects(
                ObjectType.Element, customFilter, "Select Columns")]
        except Exceptions.OperationCanceledException:
            import sys
            sys.exit() 
        if not column_selection:
			DavidEng.ErrorWindow(errortext="You have not selected any Column").ShowDialog()
			ChooseColumnsWindow('SelectColumns.xaml').show(modal=True)
        else:
			ChooseLevelsWindow('SelectLevels.xaml', columns=column_selection).show(modal=True)

class ChooseLevelsWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name, columns):
        self.columns = columns
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)

    # user select all levels
    def all_levels(self, sender, args):
        self.Close()
        levels_collector_all = FilteredElementCollector(doc).OfClass(Level).WhereElementIsNotElementType().ToElements()
        ChooseOffsetWindow('Offset.xaml', columns=self.columns, levels=levels_collector_all).show(modal=True)

    # user select specific levels
    def select_levels(self, sender, args):
        self.Close()
        customFilter = CustomSelectionFilter(DB.BuiltInCategory.OST_Levels)
        try:
            level_selection = [doc.GetElement(reference) for reference in uidoc.Selection.PickObjects(
                ObjectType.Element, customFilter, "Select Levels")]
        except Exceptions.OperationCanceledException:
            import sys
            sys.exit()
        if not level_selection:
			DavidEng.ErrorWindow(errortext="You have not selected any Level").ShowDialog()
			ChooseLevelsWindow('SelectLevels.xaml', columns=self.columns).show(modal=True)
        else:
			ChooseOffsetWindow('Offset.xaml', columns=self.columns, levels=level_selection).show(modal=True)

class ChooseOffsetWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name, columns, levels):
        self.columns = columns
        self.levels = levels
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)

    def split_columns(self, sender, args):
        self.Close()
		# Check if the offset value is valid (the value must to be float())  
        FloatFlag = True
        try:
			if float(self.offset.Text):
				floatoffsetvalue=float(self.offset.Text)
			elif self.offset.Text == '0' or self.offset.Text == '-0':
				floatoffsetvalue=0
			FloatFlag = True
        except Exception:
			FloatFlag = False
        if FloatFlag == True:
            try:
                SplitColumns(columns=self.columns, levels=self.levels, offset=floatoffsetvalue)
            except:
                pass
        else:
            DavidEng.ErrorWindow(errortext="You entered an invalid value: '{}'".format(self.offset.Text)).ShowDialog()
            ChooseOffsetWindow('Offset.xaml', columns=self.columns, levels=self.levels).show(modal=True)

    def cancel(self, sender, args):
        self.Close()

class SplitColumns(object):
    def __init__(self, columns, levels, offset):
        self.columns = columns
        self.levels = levels
        self.offset = offset
        outList = []
        max_value = len(self.columns)
        counter = 0
        t = Transaction(doc, "Split Columns")
        t.Start()
        with DavidEng.ProgressBar(title='Split Columns ... ({value} of {max_value})', cancellable=True) as pb:
            for c in self.columns:
                if pb.cancelled:
                    t.RollBack()
                    import sys
                    sys.exit()
                # Ensure only Structural Columns are used (modify if Arch Columns category is required)
                if c.Category.Name == Category.GetCategory(doc,BuiltInCategory.OST_StructuralColumns).Name:
                    newCol = None
                    crv = []
                    crv = GetColumnCentreline(c)
                    levels_list = []
                    elevations_list = []
                    for level in self.levels:
                        levels_list.append(level)
                        elevations_list.append(level.ProjectElevation)
                    zipped_lists = zip(elevations_list, levels_list)
                    sorted_zipped_lists = sorted(zipped_lists)
                    sorted_levels_list = [element for _, element in sorted_zipped_lists]
                    bLvlIndex = sorted_levels_list.index(NearestLevel(crv[0],sorted_levels_list))
                    tLvlIndex = sorted_levels_list.index(NearestLevel(crv[1],sorted_levels_list))
                    i = bLvlIndex
                    while i <= tLvlIndex:
                        try:	
                            if not newCol:
                                p = [] 
                                p = CalculateSplitParameter(c,sorted_levels_list,self.offset)
                                newCol = doc.GetElement(c.Split(p[0]))
                                if c.IsSlantedColumn:
                                    newCol.get_Parameter(BuiltInParameter.FAMILY_BASE_LEVEL_PARAM).Set(p[1]) 
                                    c.get_Parameter(BuiltInParameter.FAMILY_TOP_LEVEL_PARAM).Set(p[1])  
                            else:
                                p = [] 
                                p = CalculateSplitParameter(newCol,sorted_levels_list,self.offset)
                                if c.IsSlantedColumn:
                                    newCol.get_Parameter(BuiltInParameter.FAMILY_TOP_LEVEL_PARAM).Set(p[1])
                                newCol = doc.GetElement(newCol.Split(p[0]))  
                                if c.IsSlantedColumn:
                                    newCol.get_Parameter(BuiltInParameter.FAMILY_BASE_LEVEL_PARAM).Set(p[1])
                        except Exception, ex:
                            self
                        i = i+1	
                counter += 1
                pb.update_progress(counter, max_value)
        t.Commit()

# Gets the centreline of the column...
def GetColumnCentreline(e):	
	crv = None	
	Lev = []
	fs = e.Symbol
	fm = fs.Family	
	if not fm.IsInPlace:
		if e.IsSlantedColumn:
			crv = e.Location.Curve
			Lev.append(crv.GetEndPoint(0).Z * 30.48)
			Lev.append(crv.GetEndPoint(1).Z * 30.48)
		else:
			bLev = (e.Document.GetElement(e.get_Parameter(BuiltInParameter.FAMILY_BASE_LEVEL_PARAM).AsElementId()).ProjectElevation + e.get_Parameter(BuiltInParameter.FAMILY_BASE_LEVEL_OFFSET_PARAM).AsDouble()) * 30.48
			tLev = (e.Document.GetElement(e.get_Parameter(BuiltInParameter.FAMILY_TOP_LEVEL_PARAM).AsElementId()).ProjectElevation + e.get_Parameter(BuiltInParameter.FAMILY_TOP_LEVEL_OFFSET_PARAM).AsDouble()) * 30.48
			Lev.append(bLev)
			Lev.append(tLev)
	return Lev

# Get the nearest Level above in list of Levels to gien Elevation...
def NearestLevelAbove(elev,lvls):
	lvlAbv = None
	for l in lvls:
		if round(l.ProjectElevation*30.48,0) > elev:
			lvlAbv = l
			break			
	return lvlAbv

# Get the nearest Level above in list of Levels to gien Elevation...
def NearestLevelAboveNegativeOffset(elev,lvls):
	lvlAbv = None
	flag = False
	for l in lvls:
		if flag==True:
			lvlAbv = l
			break			
		if round(l.ProjectElevation*30.48,0) > elev:
			flag=True
	return lvlAbv

# Get the nearest Level in list of Levels to gien Elevation...
def NearestLevel(elev, lvls):
	return min(lvls, key=lambda x:abs(round(x.ProjectElevation*30.48,0)-elev))

# Calculates the location to split column with parameterised column length (between 0 & 1)
def CalculateSplitParameter(col,lvls,offset1):
	if col:
		crv = []
		crv = GetColumnCentreline(col)
		elev = round(crv[0],0)	
		if offset1 >= 0:
			l = NearestLevelAbove(elev,lvls)		
		else:
			l = NearestLevelAboveNegativeOffset(elev,lvls)			
		splitloc = []
		x = ((((l.ProjectElevation * 30.48)+offset1)-crv[0])/(crv[1]-crv[0]))
		splitloc.append(x)
		splitloc.append(l.Id)
		if x:
			return splitloc
	return None

if (DavidEng.checkPermission()):	
    # Run the Xaml
    ChooseColumnsWindow('SelectColumns.xaml').show(modal=True)
else:
    DavidEng.ErrorWindow(errortext="You don't have permission\nto use the tool").ShowDialog()