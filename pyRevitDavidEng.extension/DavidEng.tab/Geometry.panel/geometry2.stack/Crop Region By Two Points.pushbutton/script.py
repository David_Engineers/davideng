"""Crop Region By Two Points is designed for quick crop view on 2D View or 3D View. The view area will quickly be fixed to the region defined by the drag mouse of the user."""
# pylint: disable=E0401,W0703,W0613
from pyrevit import coreutils, UI, revit, DB, forms, script
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, BuiltInParameter, Transaction, XYZ, ElementId, BoundingBoxXYZ, ViewPlan, ViewSection, Outline, View3D
import DavidEng

# dependencies
import clr
clr.AddReference('System.Windows.Forms')

# import WPF creator and base Window
import wpf
from System import Windows

# App and Author name
__title__ = 'Crop Region\nBy Two Points'
__authors__ = 'Yuval Dolin'

# Identify the current Revit file
doc = __revit__.ActiveUIDocument.Document
uidoc = __revit__.ActiveUIDocument

def PickBoxCorners(PickBox, T):
    sbmn = PickBox.Min
    sbmx = PickBox.Max
    Btm_LL = sbmn  # Lower Left
    Top_UR = sbmx # Upper Right
    Out = [Btm_LL, Top_UR]
    i = 0
    loopTo = len(Out) - 1
    while i <= loopTo:
        # Transform bounding box coords to view coords
        Out[i] = T.Inverse.OfPoint(Out[i])
        i += 1
    return Out

if (DavidEng.checkPermission()):
    if type(doc.ActiveView) == ViewPlan or type(doc.ActiveView) == ViewSection or type(doc.ActiveView) == View3D:
        PickBox = None
        try:
            PickBox = uidoc.Selection.PickBox(UI.Selection.PickBoxStyle.Enclosing ,"Click and drag to define two corners of the Crop Region. ESC to cancel.")
        except:
            pass

        if PickBox:
            CropBox = doc.ActiveView.CropBox
            T = CropBox.Transform
            Corners = PickBoxCorners(PickBox, T)
            MinX = min(i[0] for i in Corners)
            MinY = min(i[1] for i in Corners)
            MinZ = min(i[2] for i in Corners)
            MaxX = max(i[0] for i in Corners)
            MaxY = max(i[1] for i in Corners)
            MaxZ = max(i[2] for i in Corners)
            CropBox.Min = XYZ(MinX, MinY, MinZ)
            CropBox.Max = XYZ(MaxX, MaxY, MaxZ)
            with revit.Transaction('Crop Region By Two Points', log_errors=False):
                doc.ActiveView.CropBox = CropBox

    else:
        DavidEng.ErrorWindow(errortext="Crop Region By Two Points add-in can't run on this View Type.").ShowDialog()
else:
    DavidEng.ErrorWindow(errortext="You don't have permission\nto use the tool").ShowDialog()