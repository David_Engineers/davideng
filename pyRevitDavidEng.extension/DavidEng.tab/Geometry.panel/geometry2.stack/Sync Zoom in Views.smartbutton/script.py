"""Keep views synchronized. This means that as you pan and zoom
and switch between Plan and RCP views, this tool will keep the
views in the same zoomed area so you can keep working in the
same area without the need to zoom and pan again.
This tool works best when the views are maximized.
"""
#pylint: disable=import-error,invalid-name,broad-except,superfluous-parens
import math

from pyrevit import framework
from pyrevit import script, revit
from pyrevit import DB, UI
import time


from Autodesk.Revit.UI import TaskDialog
from Autodesk.Revit.UI.Events import IdlingEventArgs

# App and Author name
__title__ = 'Sync Zoom\nin Views'
__authors__ = 'Yuval Dolin'

logger = script.get_logger()


SYNC_VIEW_ENV_VAR = 'SYNCVIEWACTIVELIVE'

SUPPORTED_VIEW_TYPES = (
    DB.ViewPlan,
    DB.ViewSection,
    DB.View3D,
    DB.ViewSheet,
    DB.ViewDrafting
)

def is_close(a, b, rnd=5):
    """Determine if a is close enough to b"""
    return a == b or int(a*10**rnd) == int(b*10**rnd)

def zoomstate(sender, args):
    """Copy and apply zoom state"""
    global cornerlist
    # if syncinc is active, and current view is supported
    if script.get_envvar(SYNC_VIEW_ENV_VAR) and \
            isinstance(CurActiveView, SUPPORTED_VIEW_TYPES):
        event_uidoc = sender.ActiveUIDocument
        event_doc = sender.ActiveUIDocument.Document
        active_ui_views = event_uidoc.GetOpenUIViews()
        # find current uiview
        current_ui_view = None
        for active_ui_view in active_ui_views:
            if active_ui_view.ViewId == CurActiveView.Id:
                current_ui_view = active_ui_view
        if not current_ui_view:
            return
        
        if not cornerlist:
            # get zoom corners
            cornerlist = current_ui_view.GetZoomCorners()
        else:
            if current_ui_view.GetZoomCorners()[0].X != cornerlist[0].X or current_ui_view.GetZoomCorners()[0].Y != cornerlist[0].Y or \
                current_ui_view.GetZoomCorners()[0].Z != cornerlist[0].Z or current_ui_view.GetZoomCorners()[1].X != cornerlist[1].X or \
                current_ui_view.GetZoomCorners()[1].Y != cornerlist[1].Y or current_ui_view.GetZoomCorners()[1].Z != cornerlist[1].Z:
                cornerlist = current_ui_view.GetZoomCorners()
                for on_ui_view in active_ui_views:
                    if on_ui_view.ViewId != CurActiveView.Id and str(type(CurActiveView).__name__) in str(type(event_doc.GetElement(on_ui_view.ViewId))):

                        checkFlag = True
                        # Set ViewOrientation3D
                        if isinstance(CurActiveView, DB.View3D):
                            if event_doc.GetElement(on_ui_view.ViewId).IsLocked:
                                checkFlag = False
                            event_doc.GetElement(on_ui_view.ViewId).SetOrientation(CurActiveView.GetOrientation())
                        elif isinstance(CurActiveView, DB.ViewSection):
                            angle = event_doc.GetElement(on_ui_view.ViewId).ViewDirection.AngleTo(CurActiveView.ViewDirection)
                            if not is_close(angle, math.pi) and not is_close(angle, 0):
                                checkFlag = False

                        # apply zoom and center
                        if checkFlag == True:
                            on_ui_view.ZoomAndCenterRectangle(cornerlist[0], cornerlist[1])

def get_currentactiveview(sender, args):
    global CurActiveView
    try:
        CurActiveView = args.CurrentActiveView
    except Exception as ex:
        logger.dev_log("copy_zoomstate::", str(ex))

def toggle_state():
    """Toggle tool state"""
    new_state = not script.get_envvar(SYNC_VIEW_ENV_VAR)
    script.set_envvar(SYNC_VIEW_ENV_VAR, new_state)
    script.toggle_icon(new_state)

#pylint: disable=unused-argument
def __selfinit__(script_cmp, ui_button_cmp, __rvt__):
    """pyRevit smartbuttom init"""
    global cornerlist
    cornerlist = None
    try:
        __rvt__.ViewActivated += \
            framework.EventHandler[
                UI.Events.ViewActivatedEventArgs](get_currentactiveview)

        __rvt__.Idling += framework.EventHandler[IdlingEventArgs](zoomstate)

        return True
    except Exception:
            return False

if __name__ == '__main__':
    toggle_state()
