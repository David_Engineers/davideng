"""Join Multiple Elements."""
# pylint: disable=E0401,W0703,W0613
from pyrevit import coreutils, UI, revit, DB, forms, script
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, BuiltInParameter, Transaction, XYZ, ElementId, IFailuresPreprocessor, FailureProcessingResult, Outline, BoundingBoxIntersectsFilter, ElementMulticategoryFilter, JoinGeometryUtils, \
                              FailureSeverity, TransactionGroup
from Autodesk.Revit.UI.Selection import ObjectType, ISelectionFilter
from Autodesk.Revit import Exceptions
import DavidEng

from System.Collections.Generic import List

# dependencies
import clr
clr.AddReference('System.Windows.Forms')

# import WPF creator and base Window
import wpf
from System import Windows

# App and Author name
__title__ = 'Join Multiple\nElements'
__authors__ = 'Yuval Dolin'

# Identify the current Revit file
doc = __revit__.ActiveUIDocument.Document
uidoc = __revit__.ActiveUIDocument

# if the user selected elements before runing the add-in
selection = [doc.GetElement(id) for id in uidoc.Selection.GetElementIds()]

# Define class to filter elements of category to select 
class CustomSelectionFilter(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, DB.BuiltInCategory.OST_Walls).Id \
            or e.Category.Id == DB.Category.GetCategory(doc, DB.BuiltInCategory.OST_Floors).Id \
            or e.Category.Id == DB.Category.GetCategory(doc, DB.BuiltInCategory.OST_StructuralFraming).Id \
            or e.Category.Id == DB.Category.GetCategory(doc, DB.BuiltInCategory.OST_StructuralColumns).Id \
            or e.Category.Id == DB.Category.GetCategory(doc, DB.BuiltInCategory.OST_StructuralFoundation).Id :
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

class ViewConverterFailurePreProcessor(IFailuresPreprocessor):
    def __init__(self):
        self

    def PreprocessFailures(self, failures_accessor):
        fmas = list(failures_accessor.GetFailureMessages())

        if len(fmas) == 0:
            return FailureProcessingResult.Continue

        else:
            # DeleteWarning mimics clicking 'Ok' button and ResolveFailure
            for fma in fmas:
                fseverity = fma.GetSeverity()
                if fseverity == FailureSeverity.Warning:
                    failures_accessor.DeleteWarning(fma)
                elif fseverity == FailureSeverity.Error:
                    failures_accessor.ResolveFailure(fma)

            return FailureProcessingResult.ProceedWithCommit

        return FailureProcessingResult.Continue

def Join(elementToJoin):
    BoxMax = elementToJoin.BoundingBox[doc.ActiveView].Max
    BoxMin = elementToJoin.BoundingBox[doc.ActiveView].Min
    NewBoxMax = XYZ(BoxMax.X, BoxMax.Y, BoxMax.Z)
    NewBoxMin = XYZ(BoxMin.X, BoxMin.Y, BoxMin.Z)
    outline = Outline(NewBoxMin, NewBoxMax)
    bbfilter = BoundingBoxIntersectsFilter(outline)
    CatFilter = DB.ElementMulticategoryFilter(List[DB.BuiltInCategory]([DB.BuiltInCategory.OST_Walls, DB.BuiltInCategory.OST_Floors, \
                                                                        DB.BuiltInCategory.OST_StructuralFraming, DB.BuiltInCategory.OST_StructuralColumns, \
                                                                        DB.BuiltInCategory.OST_StructuralFoundation]))
    # Creating collectior instance and collecting all the structural elements from the element bounding box
    bb_collector = FilteredElementCollector(doc, doc.ActiveView.Id).WhereElementIsNotElementType() \
                                                   .WherePasses(bbfilter).WherePasses(CatFilter)
    for el in bb_collector:
        try:
            JoinGeometryUtils.JoinGeometry(doc, elementToJoin, el)
        except:
            pass

def NotIntersectedElements():
    t = Transaction(doc, "Unjoin Not Intersected Elements")
    t.Start()
    warnings = doc.GetWarnings()
    # select 'joined but do not intersect' warnings
    for warning in warnings:
        selection = []
        elementId = warning.GetFailingElements()
        text = warning.GetDescriptionText()
        if 'joined but do not intersect' in text:
            try:
                JoinGeometryUtils.UnjoinGeometry(doc, doc.GetElement(elementId[0]), doc.GetElement(elementId[1]))
            except:
                pass
    t.Commit()

if (DavidEng.checkPermission()):
    if not selection:
        customFilter = CustomSelectionFilter(DB.BuiltInCategory)
        try:
            Elements_selection = [doc.GetElement(reference) for reference in uidoc.Selection.PickObjects(
                ObjectType.Element, customFilter, "Select Elements")]
        except Exceptions.OperationCanceledException:
            import sys
            sys.exit()
        if Elements_selection == []:
            DavidEng.ErrorWindow(errortext="You have not selected any Element").ShowDialog()
    else:
        Elements_selection = []
        for element in selection:
            if element.Category.Name == 'Walls' or element.Category.Name == 'Floors' \
                or element.Category.Name == 'Structural Framing' or element.Category.Name == 'Structural Columns' \
                or element.Category.Name == 'Structural Foundations':
                Elements_selection.append(element)
        if Elements_selection == []:
            DavidEng.ErrorWindow(errortext="You have not selected any Element").ShowDialog()

    if not Elements_selection == []:
        tg = TransactionGroup(doc, 'Join Multiple Elements')
        tg.Start()
        t = Transaction(doc, "Join Multiple Elements")
        t.Start()
        failure_ops = t.GetFailureHandlingOptions()
        failure_ops.SetFailuresPreprocessor(ViewConverterFailurePreProcessor())
        t.SetFailureHandlingOptions(failure_ops)
        for element in Elements_selection:
            Join(element)
        t.Commit()
        NotIntersectedElements()
        tg.Assimilate() 
else:
    DavidEng.ErrorWindow(errortext="You don't have permission\nto use the tool").ShowDialog()