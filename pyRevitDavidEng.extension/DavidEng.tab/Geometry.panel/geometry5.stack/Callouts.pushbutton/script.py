#-*- coding: utf-8 -*-
"""Version: 1.1.0"""
# pylint: disable=E0401,W0703,W0613

# App, Author Name and Version
__title__ = 'Callouts'
__authors__ = 'Yuval Dolin'
__version__ = '1.1.0'

import re
from pyrevit.revit.db import query
from pyrevit import framework
from pyrevit import PyRevitException
from pyrevit import revit, DB, forms, script
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, BuiltInParameter, Transaction, XYZ, ElementId, ViewSheet, Viewport, ViewPlan, Transaction, \
                              Outline, Options, ElementTransformUtils, View
from Autodesk.Revit.UI.Selection import ObjectType, ISelectionFilter
from Autodesk.Revit import Exceptions
from pyrevit import HOST_APP
from System.Windows import FlowDirection

from System.Collections.Generic import List

# dependencies
import clr
clr.AddReference('System.Windows.Forms')

# find the path of InstructionsWindow.xaml
OptionsXamlFile = script.get_bundle_file('OptionsWindow.xaml')

# import WPF creator and base Window
import wpf
import DavidEng
from System import Windows

# Identify the current Revit file
doc = __revit__.ActiveUIDocument.Document
uidoc = __revit__.ActiveUIDocument

geometry_options = Options(ComputeReferences=True, IncludeNonVisibleObjects=True)

viewPlanFilter = DB.ElementClassFilter(ViewPlan)
viewport_filter = DB.ElementClassFilter(Viewport)

# Define class to filter elements of category to select
class CustomSelectionFilter(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, self.category).Id \
            and e.get_Parameter(BuiltInParameter.VIEW_FAMILY_SCHEDULES).AsString() == "Structural Plan" \
            and e.get_Parameter(BuiltInParameter.PLAN_VIEW_NORTH).AsValueString() != "By Callout":
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

class CopyUseDestination(DB.IDuplicateTypeNamesHandler):
    def OnDuplicateTypeNamesFound(self, args):
        return DB.DuplicateTypeAction.UseDestinationTypes

def SelectStructuralPlan(status_prompt):
    customFilter = CustomSelectionFilter(DB.BuiltInCategory.OST_Viewports)
    try:
        Main_link_selection_reference = uidoc.Selection.PickObject(ObjectType.Element, customFilter, status_prompt)
        Main_link_selection = doc.GetElement(Main_link_selection_reference)
        return Main_link_selection
    except Exceptions.OperationCanceledException:
        import sys
        sys.exit()

def get_biparam_stringequals_filter(bip_paramvalue_dict):
    filters = []
    for bip, fvalue in bip_paramvalue_dict.items():
        bip_id = DB.ElementId(bip)
        bip_valueprovider = DB.ParameterValueProvider(bip_id)
        bip_valuerule = DB.FilterStringRule(bip_valueprovider,
                                            DB.FilterStringEquals(),
                                            fvalue)
        filters.append(bip_valuerule)

    if filters:
        return DB.ElementParameterFilter(
            framework.List[DB.FilterRule](filters)
            )
    else:
        raise PyRevitException('Error creating filters.')

def rectangle(ax, ay, bx, by):
    return (ax, ay, bx, by)

def collision(A, B):
    ax, ay, bx, by = A
    bax, bay, bbx, bby = B
    if ax <= bbx and bx >= bax and ay <= bby and by >= bay:
        return True
    else:
        return False

def divide_box(box, distance):
    # Unpack the coordinates of the box
    ax, ay, bx, by = box

    # Calculate the width and height of the box
    width = bx - ax - distance
    height = by - ay - distance

    # Generate a 2D matrix filled with dictionary objects
    # Each dictionary object has keys 'valid', 'ax', 'ay', 'bx' and 'by'
    matrix = [[{"valid": True, "ax": ax + j*distance, "ay": by - (i+1)*distance, "bx": ax + (j+1)*distance, "by": by - (i)*distance}
               for j in range(int(width/distance) + 1)] for i in range(int(height/distance) + 1)]

    return matrix

# Define a function to collect structural plans
def get_structural_plans(doc, view_id, param, value):
    if HOST_APP.is_newer_than(2022):
        filter = get_biparam_stringequals_filter({param: value})
    else:
        filter = query.get_biparam_stringequals_filter({param: value})
    return FilteredElementCollector(doc, view_id).OfClass(Viewport).WherePasses(filter)

# Define a function to collect callouts
def get_callouts(doc, view_id, param, value):
    if HOST_APP.is_newer_than(2022):
        filter = get_biparam_stringequals_filter({param: value})
    else:
        filter = query.get_biparam_stringequals_filter({param: value})
    return FilteredElementCollector(doc, view_id).OfCategory(BuiltInCategory.OST_Viewers).WherePasses(filter)

# Define a function to collect unique callouts
def get_unique_callouts(callouts):
    unique_callouts = []
    for callout in callouts:
        if callout.Name not in [elem.Name for elem in unique_callouts]:
            unique_callouts.append(callout)
    return unique_callouts

# Define a function to collect callouts view plans
def get_callouts_view_plans(doc, callouts):
    view_plans_collector = FilteredElementCollector(doc).OfClass(ViewPlan)
    callouts_view_plans = []
    for callout in callouts:
        if callout.get_Parameter(BuiltInParameter.VIEWER_SHEET_NUMBER).AsString() == "---":
            if callout.ViewSpecific == True:
                callouts_view_plans += [view_plan for view_plan in view_plans_collector if callout.Name == view_plan.Name]
            else:
                view_plan_list = callout.GetDependentElements(viewPlanFilter)
                matching_view_plan = view_plan_list[0]
                callouts_view_plans.append(doc.GetElement(matching_view_plan))
    
    # sort the list of view plans by the numeric part at the end of the name
    try:
        sorted_callouts_view_plans = sorted(callouts_view_plans, key=lambda x: int(re.search(r'\d+$', x.Name).group()))
    except:
        sorted_callouts_view_plans = callouts_view_plans
    
    return sorted_callouts_view_plans

def unique_name(name, names_list):
    # Counter for the number of times the name appears
    counter = 1
    base_name = name
    # Check if the name is already in the list
    while name in names_list:
        # Append the counter to the name if it's already in the list
        name = "{}({})".format(base_name,counter)
        counter += 1
    if not name in names_list:
        names_list.append(name)
    return name, names_list

def get_view_template():
    # collect all view plans in the model
    view_plans = FilteredElementCollector(doc).OfClass(ViewPlan).ToElements()
    callout_view_template = None
    # loop through the view plans and collect their names
    for view_plan in view_plans:
        if view_plan.IsTemplate and view_plan.Name == '09 DENG Rebar':
            callout_view_template = view_plan
    return callout_view_template

def CenterViewTitle(viewport, offset):
    # set label line length to 0 to get approximate length by bounding box
    viewport.LabelOffset = XYZ.Zero
    viewport.LabelLineLength = 0
    labelOutline = viewport.GetLabelOutline()
    width = labelOutline.MaximumPoint.X - labelOutline.MinimumPoint.X
    # set the label line to the length
    viewport.LabelLineLength = width
    
    #viewport box center for X value
    boxCenter = viewport.GetBoxCenter()
    bottomLeft = viewport.GetBoxOutline().MinimumPoint
    topRight = viewport.GetBoxOutline().MaximumPoint
    #build new location
    calculatedCenter = boxCenter.Subtract(bottomLeft)
    newLocation = XYZ(calculatedCenter.X - (width + offset) + ((topRight.X -  bottomLeft.X)/2), calculatedCenter.Y + ((topRight.Y -  bottomLeft.Y)/2) ,0)
    viewport.LabelOffset = newLocation

def set_stretch_plan_detail_params(copied_annotations):
    """
    Sets two parameters if the name of the copied annotation is "DENG_Stretch Plan Detail-Wall".
    """
    # Loop through each element ID in the list of copied annotations
    for copied_annotation_id in copied_annotations:
        # Retrieve the Revit element corresponding to the ID
        copied_annotation_element = doc.GetElement(copied_annotation_id)
        try:
            # Check if the element is a "DENG_Stretch Plan Detail-Wall"
            if copied_annotation_element.Name == "DENG_Stretch Plan Detail-Wall":
                copied_annotation_element.LookupParameter('L TAG info (A)  ↕').Set(15/30.48)
                copied_annotation_element.LookupParameter('L TAG info (B)  ↕').Set(20/30.48)
        except:
            pass

def place_callouts(callouts_view_plans, matrix, distance, doc, titleBlockBox):
    # Unpack the coordinates of the box
    ax, ay, _, _ = titleBlockBox
    # variable to keep track of the width of the last callout placed 
    last_callout_width = 0
    global detail_numbers_list
    detail_numbers_list = []
    copy_options = DB.CopyPasteOptions()
    copy_options.SetDuplicateTypeNamesHandler(CopyUseDestination())
    row_index = 0
    column_index = 0
    maximum_row_height = 0
    max_value = len(callouts_view_plans)
    counter = 0
    
    t = Transaction(doc, 'Callouts Tool')
    t.Start()
    
    global viewports_detail_number
    viewport_detail_number, viewports_detail_number = unique_name("999", viewports_detail_number)
    selected_view_port.get_Parameter(BuiltInParameter.VIEWPORT_DETAIL_NUMBER).Set(viewport_detail_number)
    
    callout_view_template = get_view_template()
    for callouts_view_plan in callouts_view_plans:
        view_template_param = callouts_view_plan.get_Parameter(BuiltInParameter.VIEW_TEMPLATE)
        if view_template_param.AsValueString() and callout_view_template != None:
            view_template_param.Set(callout_view_template.Id)
        try:
            callouts_view_plan.Scale = 25
        except:
            pass
    
    with DavidEng.ProgressBar(title='Insert Callouts to Sheet ... ({value} of {max_value})', cancellable=True) as pb:
        for callouts_view_plan in callouts_view_plans:
            if pb.cancelled:
                t.RollBack()
                import sys
                sys.exit()
            # Compute width and height of callout
            width = (callouts_view_plan.CropBox.Max.X - callouts_view_plan.CropBox.Min.X)/(callouts_view_plan.Scale)
            height  = (callouts_view_plan.CropBox.Max.Y - callouts_view_plan.CropBox.Min.Y)/(callouts_view_plan.Scale)
            # Flag to check if callout is placed
            placed_flag = False
            created_viewport = None
            
            # Check if callout can be placed at current location
            for i in range(row_index, len(matrix)):
                # If i is not the first row, update row_index, i and column_index
                if i > row_index:
                    row_index = i + maximum_row_height
                    i += maximum_row_height
                    column_index = 0
                    maximum_row_height = 0
                    
                for j in range(column_index, len(matrix[0])):
                    if is_valid_location(matrix, i, j, width, height, distance):
                        # Mark the location as used
                        mark_location_as_used(matrix, i, j, width, height)
                        # Create viewport and place callout
                        created_viewport = Viewport.Create(doc, doc.ActiveView.Id, callouts_view_plan.Id, XYZ(matrix[i][j]["ax"] + (width/2), matrix[i][j]["ay"] - (height/2), 0))
                        set_detail_number(created_viewport)
                        if detail_number_viewport != None:
                            created_viewport.get_Parameter(BuiltInParameter.ELEM_TYPE_PARAM).Set(detail_number_viewport)
                        if HOST_APP.is_newer_than(2021):
                            CenterViewTitle(created_viewport, 0.1)
                        placed_flag = True
                        column_index = j + int(round(width/distance + 0.5))
                        maximum_row_height = max(int(round(height/distance + 0.5)), maximum_row_height)
                        break
                if placed_flag:
                    break
            if not created_viewport:
                # Create viewport and place callout next to the previous one
                created_viewport = Viewport.Create(doc, doc.ActiveView.Id, callouts_view_plan.Id, XYZ(ax + (width/2) + last_callout_width, ay - (height/2), 0))
                set_detail_number(created_viewport)
                if detail_number_viewport != None:
                    created_viewport.get_Parameter(BuiltInParameter.ELEM_TYPE_PARAM).Set(detail_number_viewport)
                if HOST_APP.is_newer_than(2021):
                    CenterViewTitle(created_viewport, 0.1)
                last_callout_width += width
                
            elements_in_callout = filter_crop_box(callouts_view_plan, primary_view)
            
            view_scope_box_elementId = cancel_scope_box(primary_view)
            
            view_orientation_integer, view_orientation_read_only, view_template_id, view_template_param = set_project_north(primary_view)
            
            if len(elements_in_callout) > 0:
                copied_annotations = ElementTransformUtils.CopyElements(primary_view, elements_in_callout, callouts_view_plan, None, copy_options)
                set_stretch_plan_detail_params(copied_annotations)
            
            retrieve_view_orientation(view_orientation_integer, view_orientation_read_only, view_template_id, view_template_param, primary_view)
            
            retrieve_scope_box(view_scope_box_elementId, primary_view)
            
            counter += 1
            pb.update_progress(counter, max_value)
            revit.uidoc.RefreshActiveView()
            doc.Regenerate()
            
    t.Commit()

# Filter only the elements that are visible in the active crop box
def filter_crop_box(callouts_view_plan, main_view):
    cropBox = callouts_view_plan.CropBox
    
    annotation_crop_shape = callouts_view_plan.GetCropRegionShapeManager().GetAnnotationCropShape()
    min_crop_shape_point_x = None
    for curve in annotation_crop_shape:
        curve_origin = curve.Origin
        if min_crop_shape_point_x == None:
            min_crop_shape_point_x = curve_origin.X
            max_crop_shape_point_x = curve_origin.X
            min_crop_shape_point_y = curve_origin.Y
            max_crop_shape_point_y = curve_origin.Y
            min_crop_shape_point_z = curve_origin.Z
            max_crop_shape_point_z = curve_origin.Z
        else:
            if curve_origin.X < min_crop_shape_point_x:
                min_crop_shape_point_x = curve_origin.X
            if curve_origin.Y < min_crop_shape_point_y:
                min_crop_shape_point_y = curve_origin.Y
            if curve_origin.Z < min_crop_shape_point_z:
                min_crop_shape_point_z = curve_origin.Z
            if curve_origin.X > max_crop_shape_point_x:
                max_crop_shape_point_x = curve_origin.X
            if curve_origin.Y > max_crop_shape_point_y:
                max_crop_shape_point_y = curve_origin.Y
            if curve_origin.Z > max_crop_shape_point_z:
                max_crop_shape_point_z = curve_origin.Z
    
    # transform basis of the old coordinate system in the new coordinate // system
    # transform = cropBox.Transform
    # b0 = transform.BasisX
    # b1 = transform.BasisY
    # b2 = transform.BasisZ
    # origin = transform.Origin
    
    # transform the origin of the old coordinate system in the new coordinate system
    #xTempMin = cropBox.Min.X * b0.X + cropBox.Min.Y * b1.X + cropBox.Min.Z * b2.X + origin.X
    #yTempMin = cropBox.Min.X * b0.Y + cropBox.Min.Y * b1.Y + cropBox.Min.Z * b2.Y + origin.Y
    #xTempMax = cropBox.Max.X * b0.X + cropBox.Max.Y * b1.X + cropBox.Max.Z * b2.X + origin.X
    #yTempMax = cropBox.Max.X * b0.Y + cropBox.Max.Y * b1.Y + cropBox.Max.Z * b2.Y + origin.Y
    
    xTempMin = min_crop_shape_point_x
    yTempMin = min_crop_shape_point_y
    xTempMax = max_crop_shape_point_x
    yTempMax = max_crop_shape_point_y
    
    outlineCropBox = Outline(XYZ(min(xTempMin, xTempMax), min(yTempMin, yTempMax), 0), XYZ(max(xTempMin, xTempMax), max(yTempMin, yTempMax), 0))
    newElementsList = []
    for element in annotations_collector:
        elementBBox = element.BoundingBox[main_view]
        outlineElement = Outline(XYZ(elementBBox.Min.X, elementBBox.Min.Y, 0), XYZ(elementBBox.Max.X, elementBBox.Max.Y, 0))
        checkIntersects = outlineCropBox.Intersects(outlineElement,0)
        if checkIntersects:
            newElementsList.append(element)

    # list of elements ids 
    ids_2D = List[ElementId]()
    for i in newElementsList:
        try:
            ids_2D.Add(i.Id)
        except:
            pass
    return ids_2D

def set_detail_number(created_viewport):
    # Get the view name of the viewport as a string
    view_name = created_viewport.get_Parameter(BuiltInParameter.VIEWPORT_VIEW_NAME).AsString()
    # Extract the detail number from the view name, which is either after the last dash or dot.
    # Choose the detail number with the manimum length.
    detail_number = min(view_name.split("-")[-1], view_name.split(".")[-1], key=len)

    global viewports_detail_number
    detail_number, viewports_detail_number = unique_name(detail_number, viewports_detail_number)
    created_viewport.get_Parameter(BuiltInParameter.VIEWPORT_DETAIL_NUMBER).Set(detail_number)

def is_valid_location(matrix, i, j, width, height, distance):
    """
    Check if the location at (i, j) in the matrix is valid to place a callout of size (width, height)
    """
    for ii in range(i, i + int(round(height/distance + 0.5)) + 1):
        for jj in range(j, j + int(round(width/distance + 0.5)) + 1):
            if (ii < len(matrix) and jj < len(matrix[i])):
                if matrix[ii][jj]["valid"] == False:
                    return False
            else:
                return False
    return True

def mark_location_as_used(matrix, i, j, width, height):
    """
    Mark the location at (i, j) in the matrix as used for a callout of size (width, height)
    """
    for ii in range(i, i + int(round(height/distance + 0.5)) + 1):
        for jj in range(j, j + int(round(width/distance + 0.5)) + 1):
            matrix[ii][jj]["valid"] = False

def get_viewport_outline(viewport):
    """
    Extracts the outline of a viewport in the form of a tuple (min_x, min_y, max_x, max_y)
    """
    min_point = viewport.GetBoxOutline().MinimumPoint
    max_point = viewport.GetBoxOutline().MaximumPoint
    return (min_point.X, min_point.Y, max_point.X, max_point.Y)

def check_collision(viewport_box, matrix):
    """
    Check for collision between the viewport box and the boxes in the matrix
    If a collision is found, sets the "valid" key of the matrix's box to False
    """
    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            box_to_check = (matrix[i][j]["ax"], matrix[i][j]["ay"], matrix[i][j]["bx"], matrix[i][j]["by"])
            result = collision(viewport_box, box_to_check)
            if result:
                matrix[i][j]["valid"] = False

def get_title_block_box(titleBlocksCollector):
    """
    This function takes in a title block collector and a document,
    and returns the bounding box coordinates of the first title block in the collector.
    """
    titleBlock = titleBlocksCollector.FirstElement()
    titleBlockMin = titleBlock.get_BoundingBox(doc.ActiveView).Min
    titleBlockMax = titleBlock.get_BoundingBox(doc.ActiveView).Max
    titleBlockBox = (titleBlockMin.X + (1.6/30.48), titleBlockMin.Y + (3.5/30.48), titleBlockMax.X - (19/30.48), titleBlockMax.Y - (3.5/30.48))
    return titleBlockBox

def get_parent_callouts():
    # Define the filter for elements with the "Structural Plan" family
    family_param = DB.BuiltInParameter.ELEM_FAMILY_PARAM
    family_value = "Structural Plan"
    if HOST_APP.is_newer_than(2022):
        family_filter = get_biparam_stringequals_filter({family_param: family_value})
    else:
        family_filter = query.get_biparam_stringequals_filter({family_param: family_value})
    
    # Collect all callouts with the specified family
    all_callouts = DB.FilteredElementCollector(doc).OfCategory(DB.BuiltInCategory.OST_Viewers).WherePasses(family_filter)
    
    # Define the filter for elements that belong to the active view
    parent_view_param = DB.BuiltInParameter.SECTION_PARENT_VIEW_NAME
    parent_view_value = primary_copy_from.Name
    if HOST_APP.is_newer_than(2022):
        parent_view_filter = get_biparam_stringequals_filter({parent_view_param: parent_view_value})
    else:
        parent_view_filter = query.get_biparam_stringequals_filter({parent_view_param: parent_view_value})
    
    # Collect all callouts that belong to the active view
    callouts = all_callouts.WherePasses(parent_view_filter)
    
    # Get only the callouts that are not view-specific
    parent_callouts = [callout for callout in callouts if not callout.ViewSpecific]
    
    # Return the parent callouts
    return parent_callouts

def get_viewport_element(parent_callout):
    get_dependent_viewports = parent_callout.GetDependentElements(viewport_filter)
    for dependent_viewport in get_dependent_viewports:
        if not doc.GetElement(dependent_viewport).SheetId.IntegerValue < 0:
            return doc.GetElement(dependent_viewport)
    return None

def filter_reference_callouts(callouts, all_parent_callouts):
    """
    Filters the reference callouts from the given list of callouts and adds the missing parent callouts.
    :param callouts: The list of all callouts.
    :param all_parent_callouts: The list of all parent callouts.
    :return: The filtered list of reference callouts.
    """
    # Separate reference callouts and parent callouts
    reference_callouts = [c for c in callouts if c.ViewSpecific == True]
    parent_callouts_list = [c for c in callouts if c.ViewSpecific == False]
    parent_callouts = []
    for parent_callout in parent_callouts_list:
        viewport_element = get_viewport_element(parent_callout)
        parent_callouts.append([parent_callout, False, viewport_element])

    # Iterate through reference callouts and check if the corresponding parent callout is in parent_callouts or all_parent_callouts
    for reference_callout in reference_callouts[:]:
        parent_callout = next((p for p,_,_ in parent_callouts if p.Name == reference_callout.Name), None)
        if not parent_callout:
            parent_callout = next((p for p in all_parent_callouts if p.Name == reference_callout.Name), None)
            if parent_callout:
                if parent_callout.IsHidden(copy_from_view):
                    is_hidden = True
                else:
                    is_hidden = False
                viewport_element = get_viewport_element(parent_callout)
                # Add missing parent callout to parent_callouts
                parent_callouts.append([parent_callout, is_hidden, viewport_element])
            else:
                # Remove reference callout if no corresponding parent callout is found
                reference_callouts.remove(reference_callout)
    return reference_callouts, parent_callouts

def get_ids_list(parent_callout, reference_callouts):
    """
    Returns a list of `ElementId` of `parent_callout` and all `reference_callouts` that have the same name as `parent_callout`.
    """
    global counter
    ids_list = List[ElementId]()
    ids_list.Add(parent_callout.Id)
    for reference_callout in reference_callouts:
        if parent_callout.Name == reference_callout.Name:
            counter += 1
            ids_list.Add(reference_callout.Id)
    return ids_list

def copy_annotations(from_view, to_view):
    # Create a filter to collect all annotations in the specified categories
    category_filter = DB.ElementMulticategoryFilter(List[DB.BuiltInCategory]([DB.BuiltInCategory.OST_DetailComponents, DB.BuiltInCategory.OST_Lines, \
                                                                              DB.BuiltInCategory.OST_TextNotes, DB.BuiltInCategory.OST_GenericAnnotation, \
                                                                              DB.BuiltInCategory.OST_DetailComponentTags]))
    # Collect the annotations using the filter
    global annotations_collector
    annotations_collector = FilteredElementCollector(doc, from_view.Id).WhereElementIsNotElementType().WherePasses(category_filter)
    
    # Set the geometry options to use the "from" view
    geometry_options.View = from_view
    
    # Collect elements that are within the crop box of the "from" view
    elements_in_callout = filter_crop_box(from_view, from_view)
    
    # If there are elements within the crop box, copy them to the "to" view with rotation
    if len(elements_in_callout) > 0:
        # Get the transform of the crop box
        crop_box_transform = from_view.CropBox.Transform
        # Get the X basis of the crop box transform
        transform_basis_x = crop_box_transform.BasisX
        # Define a horizontal X basis vector
        horizontal_basis_x = XYZ(1,0,0)
        # Find the angle between the horizontal X basis vector and the crop box X basis vector
        rotation_angle = horizontal_basis_x.AngleOnPlaneTo(transform_basis_x, XYZ.BasisZ)
        # Create a rotation transform using the rotation angle
        rotation_transform = DB.Transform.CreateRotation(XYZ.BasisZ, rotation_angle)
        # Copy the elements to the "to" view with the rotation transform and copy options
        ElementTransformUtils.CopyElements(from_view, elements_in_callout, to_view, rotation_transform, copy_options)

def set_view_template_parameters(matching_view_plan_copied_element, matching_view_plan):
    """
    Applies view template parameters to a copied view plan element based on another view plan element.
    """
    # apply primary_copy_to view template parameters to matching_view_plan_copied_element
    matching_view_plan_copied_element.ApplyViewTemplateParameters(primary_copy_to)
    
    # create a view template for matching_view_plan and exclude "V/G Overrides RVT Links" parameter
    matching_view_plan_template = matching_view_plan.CreateViewTemplate()
    template_not_include = matching_view_plan_template.GetNonControlledTemplateParameterIds()
    parameter_link_id = matching_view_plan_template.LookupParameter("V/G Overrides RVT Links").Id
    template_not_include.Add(parameter_link_id)
    matching_view_plan_template.SetNonControlledTemplateParameterIds(template_not_include)
    
    # apply the modified view template to matching_view_plan_copied_element
    matching_view_plan_copied_element.ApplyViewTemplateParameters(matching_view_plan_template)
    
    # delete the view template created for matching_view_plan
    doc.Delete(matching_view_plan_template.Id)
    
    # set "Crop Region Visible" parameter of matching_view_plan_copied_element to 1 (true = visible)
    matching_view_plan_copied_element.LookupParameter("Crop Region Visible").Set(1)

def copy_and_modify_elements(doc, selected_view, parent_callout, reference_callouts, parent_index, parent_callouts, copy_options, matrix, distance):
    """
    Copies the elements with the same name as `parent_callout` from `selected_view` to `selected_view` and modifies their parameters.
    Elements that have the same name as `parent_callout` in `reference_callouts` are also copied.
    If the copied element has `ViewSpecific` property set to False, its folder name and view name are updated based on the `selected_view` name.
    If the original `parent_callout` was hidden, the copied element is also hidden in `selected_view`.
    """
    global ax, ay, last_callout_width, detail_numbers_list, row_index, column_index, maximum_row_height
    ids_list = get_ids_list(parent_callout, reference_callouts)
    copied_element_ids = ElementTransformUtils.CopyElements(
        primary_copy_from, ids_list, primary_copy_to, None, copy_options)
    
    for copied_element_id in copied_element_ids:
        copied_element = doc.GetElement(copied_element_id)
        if copied_element.ViewSpecific == False:
            is_hidden = parent_callouts[parent_index][1]
            if is_hidden == True:
                id_to_hide = List[ElementId]()
                id_to_hide.Add(copied_element_id)
                copy_to_view.HideElements(id_to_hide)
            if not (folder_name == None):
                copied_element.LookupParameter('Folder').Set(folder_name)

            # Get the name of the parent callout view
            parent_callout_name = parent_callout.get_Parameter(BuiltInParameter.VIEW_NAME).AsString()

            # Convert the name to lowercase
            parent_callout_name_lower_case = parent_callout_name.lower()

            # If the name contains the substring "det"
            if "det" in parent_callout_name_lower_case:
                # Split the name at the "det" substring and create a new string starting with "DET"
                split_string = parent_callout_name_lower_case.split("det")
                after_det = "DET" + split_string[1]
                
                # Calculate the split index to separate the name into two parts, before and after the "DET" string
                split_index = -len(after_det)
                parent_callout_name_parts = [parent_callout_name[:split_index], parent_callout_name[split_index:]]
                
                # Concatenate the "DET" string with the second part of the split name and add it to the name of another view to create a new view name
                view_name = copy_to_view.Name + "-" + parent_callout_name_parts[1]
                
            # If the name does not contain the substring "det", simply use the name of the other view as the new view name
            else:
                view_name = copy_to_view.Name

            # Counter for the number of times the view name appears
            counter = 1
            base_view_name = view_name
            # Check if the view name is already in the list
            while view_name in view_plans_names:
                # Append the counter to the view name if it's already in the list
                view_name = "{}({})".format(base_view_name,counter)
                counter += 1
            if not view_name in view_plans_names:
                view_plans_names.append(view_name)
            copied_element.get_Parameter(BuiltInParameter.VIEW_NAME).Set(view_name)
            
            view_plan_list = parent_callouts[parent_index][0].GetDependentElements(viewPlanFilter)
            matching_view_plan = doc.GetElement(view_plan_list[0])
            view_plan_list_copied_element = copied_element.GetDependentElements(viewPlanFilter)
            matching_view_plan_copied_element = doc.GetElement(view_plan_list_copied_element[0])
            copy_annotations(matching_view_plan, matching_view_plan_copied_element)
            
            set_view_template_parameters(matching_view_plan_copied_element, matching_view_plan)
            
            # If the callout that we copied from the selected view is not placed in sheet
            if(parent_callouts[parent_index][2] == None):
                # Compute width and height of callout
                width = (matching_view_plan_copied_element.CropBox.Max.X - matching_view_plan_copied_element.CropBox.Min.X)/(matching_view_plan_copied_element.Scale)
                height  = (matching_view_plan_copied_element.CropBox.Max.Y - matching_view_plan_copied_element.CropBox.Min.Y)/(matching_view_plan_copied_element.Scale)
                # Flag to check if callout is placed
                placed_flag = False
                created_viewport = None
                
                # Check if callout can be placed at current location
                for i in range(row_index, len(matrix)):
                    # If i is not the first row, update row_index, i and column_index
                    if i > row_index:
                        row_index = i + maximum_row_height
                        i += maximum_row_height
                        column_index = 0
                        maximum_row_height = 0
                        
                    for j in range(column_index, len(matrix[0])):
                        if is_valid_location(matrix, i, j, width, height, distance):
                            # Mark the location as used
                            mark_location_as_used(matrix, i, j, width, height)
                            # Create viewport and place callout
                            created_viewport = Viewport.Create(doc, doc.ActiveView.Id, matching_view_plan_copied_element.Id, XYZ(matrix[i][j]["ax"] + (width/2), matrix[i][j]["ay"] - (height/2), 0))
                            set_detail_number(created_viewport)
                            if detail_number_viewport != None:
                                created_viewport.get_Parameter(BuiltInParameter.ELEM_TYPE_PARAM).Set(detail_number_viewport)
                            placed_flag = True
                            column_index = j + int(round(width/distance + 0.5))
                            maximum_row_height = max(int(round(height/distance + 0.5)), maximum_row_height)
                            break
                    if placed_flag:
                        break
                if not created_viewport:
                    # Create viewport and place callout next to the previous one
                    created_viewport = Viewport.Create(doc, doc.ActiveView.Id, matching_view_plan_copied_element.Id, XYZ(ax + (width/2) + last_callout_width, ay - (height/2), 0))
                    set_detail_number(created_viewport)
                    if detail_number_viewport != None:
                        created_viewport.get_Parameter(BuiltInParameter.ELEM_TYPE_PARAM).Set(detail_number_viewport)
                    last_callout_width += width
            # If the callout that we copied from the selected view is placed in sheet and we already have the location to place it
            else:
                created_viewport = Viewport.Create(doc, doc.ActiveView.Id, matching_view_plan_copied_element.Id, parent_callouts[parent_index][2].GetBoxCenter())
                set_detail_number(created_viewport)
                if detail_number_viewport != None:
                    created_viewport.get_Parameter(BuiltInParameter.ELEM_TYPE_PARAM).Set(detail_number_viewport)
                if HOST_APP.is_newer_than(2021):
                    created_viewport.LabelLineLength = parent_callouts[parent_index][2].LabelLineLength
                    created_viewport.LabelOffset = parent_callouts[parent_index][2].LabelOffset

def cancel_scope_box(view_plan):
    """
    This function cancels the scope box of a given view plan.
    If the scope box element exists, it is set to InvalidElementId, which turns it off.
    The element ID of the scope box is returned.
    """
    # Get the parameter Scope Box from the primary copy view
    view_scope_box = view_plan.get_Parameter(BuiltInParameter.VIEWER_VOLUME_OF_INTEREST_CROP)
    # Get the element ID of the Scope Box
    view_scope_box_elementId = view_scope_box.AsElementId()
    # If the Scope Box element exists, set it to InvalidElementId (i.e. turn it off)
    if doc.GetElement(view_scope_box_elementId) is not None:
        view_plan.get_Parameter(BuiltInParameter.VIEWER_VOLUME_OF_INTEREST_CROP).Set(DB.ElementId.InvalidElementId)
    
    return view_scope_box_elementId

def retrieve_scope_box(view_scope_box_elementId, view_plan):
    """
    This function retrieves the scope box of a given view plan.
    If the scope box element exists, it is set back to its original element ID.
    """
    # If the Scope Box element exists, set it back to its original element ID
    if doc.GetElement(view_scope_box_elementId) is not None:
        view_plan.get_Parameter(BuiltInParameter.VIEWER_VOLUME_OF_INTEREST_CROP).Set(view_scope_box_elementId)

def set_project_north(view_plan):
    """
    This function sets the orientation of a given view plan to "Project North".
    If the orientation parameter is already set to "Project North", nothing happens.
    If the orientation parameter is read-only, the view template is removed, the parameter value is set to 0, and the view template is set to an invalid ID.
    The original value of the orientation parameter, the read-only status of the parameter, the ID of the view template, and the view template parameter are returned.
    """
    # Retrieve the Orientation parameter of the view
    view_orientation = view_plan.get_Parameter(BuiltInParameter.PLAN_VIEW_NORTH)
    
    # Get the integer value of the parameter
    view_orientation_integer = view_orientation.AsInteger()
    
    view_orientation_read_only = None
    view_template_id = None
    view_template_param = None
    
    # Check if the parameter value is not already set to "Project North"
    if not (view_orientation_integer == 0):
        # Check if the parameter is read-only
        view_orientation_read_only = view_orientation.IsReadOnly
        if not (view_orientation_read_only):
            # If the parameter is not read-only, set its value to 0 ("Project North")
            view_plan.get_Parameter(BuiltInParameter.PLAN_VIEW_NORTH).Set(0)
        else:
            # If the parameter is read-only, remove the view template, set the parameter value to 0, and set the view template to an invalid ID
            view_template_id = view_plan.ViewTemplateId
            view_template_param = view_plan.get_Parameter(BuiltInParameter.VIEW_TEMPLATE)
            view_template_param.Set(DB.ElementId.InvalidElementId)
            view_plan.get_Parameter(BuiltInParameter.PLAN_VIEW_NORTH).Set(0)
    
    return view_orientation_integer, view_orientation_read_only, view_template_id, view_template_param

def retrieve_view_orientation(view_orientation_integer, view_orientation_read_only, view_template_id, view_template_param, view_plan):
    """
    This function retrieves the orientation of a given view plan.
    If the orientation parameter is not originally set to "Project North", it is set back to its original value.
    If the orientation is originally read only because it set in the view template, set back the original view template.
    """
    # If the parameter value is not originaly set to "Project North", set it back to its original value
    if not (view_orientation_integer == 0):
        if not (view_orientation_read_only):
            view_plan.get_Parameter(BuiltInParameter.PLAN_VIEW_NORTH).Set(view_orientation_integer)
        else:
            view_template_param.Set(view_template_id)

class OptionsWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name):
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)
        self.option1_grid.FlowDirection = FlowDirection.LeftToRight
        self.option1_first_row.Text = "Insert annotations from a view into Callouts you created"
        self.option1_second_row.Text = "Insert those Callouts into a sheet automatically"
        self.option1_button_text.Text = "Choose Option"
        self.option2_grid.FlowDirection = FlowDirection.LeftToRight
        self.option2_first_row.Text = "Copy Callouts from previous floor to next floor"
        self.option2_second_row.Text = "Copy View annotations from previous floor to the next floor"
        self.option2_third_row.Text = "Copy Callouts annotations from previous floor to next floor"
        self.option2_fourth_row.Text = "Insert the created Callouts into the sheet automatically"
        self.option2_button_text.Text = "Choose Option"

    def language_toggle_changed(self, sender, args):
        if self.language_toggle.IsChecked == True:
            self.option1_grid.FlowDirection = FlowDirection.RightToLeft
            self.option1_first_row.Text = "הכנס אנוטציות ממבט (View) לתוך Callouts שיצרת בעצמך"
            self.option1_second_row.Text = "הכנס את אותם Callouts לתוך הגיליון באופן אוטומטי"
            self.option1_button_text.Text = "בחר אפשרות"
            self.option2_grid.FlowDirection = FlowDirection.RightToLeft
            self.option2_first_row.Text = "העתק Callouts מקומה קודמת לקומה הבאה"
            self.option2_second_row.Text = "העתק אנוטציות ממבט (View) מקומה קודמת לקומה הבאה"
            self.option2_third_row.Text = "העתק אנוטציות של Callouts מקומה קודמת לקומה הבאה"
            self.option2_fourth_row.Text = "הכנס Callouts חדשים שנוצרו לתוך הגיליון באופן אוטומטי"
            self.option2_button_text.Text = "בחר אפשרות"
        else:
            self.option1_grid.FlowDirection = FlowDirection.LeftToRight
            self.option1_first_row.Text = "Insert annotations from a view into Callouts you created"
            self.option1_second_row.Text = "Insert those Callouts into a sheet automatically"
            self.option1_button_text.Text = "Choose Option"
            self.option2_grid.FlowDirection = FlowDirection.LeftToRight
            self.option2_first_row.Text = "Copy Callouts from previous floor to next floor"
            self.option2_second_row.Text = "Copy View annotations from previous floor to the next floor"
            self.option2_third_row.Text = "Copy Callouts annotations from previous floor to next floor"
            self.option2_fourth_row.Text = "Insert the created Callouts into the sheet automatically"
            self.option2_button_text.Text = "Choose Option"

    def choose_option1(self, sender, args):
        self.Close()
        global selected_view_port, distance, detail_number_viewport, primary_view
        # Collect structural plans from active view
        structuralPlans = get_structural_plans(doc, doc.ActiveView.Id, DB.BuiltInParameter.VIEW_FAMILY_SCHEDULES, "Structural Plan")
        if structuralPlans.GetElementCount() != 0:
            selected_view_port = SelectStructuralPlan("Select the Structural Plan from which you want to collect the Callouts")
            if not selected_view_port == None:
                viewport_types = selected_view_port.GetValidTypes()
                detail_number_viewport = None
                for viewport_type in viewport_types:
                    if doc.GetElement(viewport_type).get_Parameter(BuiltInParameter.ALL_MODEL_TYPE_NAME).AsString() == "Detail Number Only":
                        detail_number_viewport = viewport_type
                
                # Get the value of the "VIEW_DEPENDENCY" parameter of the selected view port
                dependency_value = selected_view_port.get_Parameter(BuiltInParameter.VIEW_DEPENDENCY).AsString()

                # If the dependency value doesn't start with "Dependent on ", the primary view is the view associated with the selected view port
                # Otherwise, set primary_view to None
                primary_view = doc.GetElement(selected_view_port.ViewId) if not dependency_value.startswith("Dependent on ") else None

                # If the primary view is None, find the view plan with the same name as the dependency value (after removing the prefix "Dependent on ")
                if primary_view is None:
                    dependency_value = dependency_value[len("Dependent on "):]
                    view_plans_collector = FilteredElementCollector(doc).OfClass(ViewPlan)
                    for view_plan in view_plans_collector:
                        if dependency_value == view_plan.Name:
                            primary_view = view_plan
                            break
                
                selected_view = doc.GetElement(selected_view_port.ViewId)
                geometry_options.View = selected_view
                
                category_filter = DB.ElementMulticategoryFilter(List[DB.BuiltInCategory]([DB.BuiltInCategory.OST_DetailComponents, DB.BuiltInCategory.OST_Lines, \
                                                                                          DB.BuiltInCategory.OST_TextNotes, DB.BuiltInCategory.OST_GenericAnnotation, \
                                                                                          DB.BuiltInCategory.OST_DetailComponentTags]))
                global annotations_collector
                annotations_collector = FilteredElementCollector(doc, primary_view.Id).WhereElementIsNotElementType().WherePasses(category_filter)
                
                callouts = get_callouts(doc, selected_view.Id, DB.BuiltInParameter.ELEM_FAMILY_PARAM, "Structural Plan")
                unique_callouts = get_unique_callouts(callouts)
                callouts_view_plans = get_callouts_view_plans(doc, unique_callouts)
                
                titleBlocksCollector = FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_TitleBlocks)
                
                if titleBlocksCollector.GetElementCount() == 1:
                    distance = 2/30.48
                    titleBlockBox = get_title_block_box(titleBlocksCollector)
                    matrix = divide_box(titleBlockBox, distance)
                    
                    # Collect all viewports in the active view
                    viewports = FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_Viewports)
                    
                    global viewports_detail_number
                    viewports_detail_number = []
                    for viewport in viewports:
                        if type(doc.GetElement(viewport.ViewId)) != View and viewport.Id != selected_view_port.Id:
                            viewports_detail_number.append(doc.GetElement(viewport.ViewId).get_Parameter(BuiltInParameter.VIEWPORT_DETAIL_NUMBER).AsString())
                    
                    # Iterate through each viewport and check for collisions with the boxes in the matrix
                    for viewport in viewports:
                        viewport_box = get_viewport_outline(viewport)
                        check_collision(viewport_box, matrix)
                    
                    place_callouts(callouts_view_plans, matrix, distance, doc, titleBlockBox)
        else:
            DavidEng.ErrorWindow(errortext="You must have at least one Structural Plan in the sheet in order to run this tool.").ShowDialog()
        
    def choose_option2(self, sender, args):
        self.Close()
        global copy_from_view, copy_to_view, copy_options, primary_copy_from, primary_copy_to, view_plans_names, selected_view_port, folder_name, view_level_substring
        global ax, ay, last_callout_width, detail_numbers_list, row_index, column_index, maximum_row_height, matrix, distance, detail_number_viewport
        copy_options = DB.CopyPasteOptions()
        copy_options.SetDuplicateTypeNamesHandler(CopyUseDestination())
        # Collect structural plans from active view
        structuralPlans = get_structural_plans(doc, doc.ActiveView.Id, DB.BuiltInParameter.VIEW_FAMILY_SCHEDULES, "Structural Plan")
        if structuralPlans.GetElementCount() != 0:
            selected_view_port = SelectStructuralPlan("Select the Structural Plan to which you want to copy the Callouts")
            if not selected_view_port == None:
                viewport_types = selected_view_port.GetValidTypes()
                detail_number_viewport = None
                for viewport_type in viewport_types:
                    if doc.GetElement(viewport_type).get_Parameter(BuiltInParameter.ALL_MODEL_TYPE_NAME).AsString() == "Detail Number Only":
                        detail_number_viewport = viewport_type
                        
                copy_to_view = doc.GetElement(selected_view_port.ViewId)
                # selected_view = doc.GetElement(selected_view_port.ViewId)
                copy_from_view = forms.select_views(filterfunc=lambda x: isinstance(x, DB.ViewPlan), multiple=False)
                if copy_from_view:
                    # Get the value of the "VIEW_DEPENDENCY" parameter of the selected view port
                    dependency_value = copy_from_view.get_Parameter(BuiltInParameter.VIEW_DEPENDENCY).AsString()

                    # If the dependency value doesn't start with "Dependent on ", the primary view is the view associated with the selected view port
                    # Otherwise, set primary_copy_from to None
                    primary_copy_from = copy_from_view if not dependency_value.startswith("Dependent on ") else None

                    view_plans_collector = FilteredElementCollector(doc).OfClass(ViewPlan)
                    # If the primary view is None, find the view plan with the same name as the dependency value (after removing the prefix "Dependent on ")
                    if primary_copy_from is None:
                        dependency_value = dependency_value[len("Dependent on "):]
                        for view_plan in view_plans_collector:
                            if dependency_value == view_plan.Name:
                                primary_copy_from = view_plan
                                break

                    callouts = get_callouts(doc, copy_from_view.Id, DB.BuiltInParameter.ELEM_FAMILY_PARAM, "Structural Plan")
                    all_parent_callouts = get_parent_callouts()
                    
                    reference_callouts, parent_callouts = filter_reference_callouts(callouts, all_parent_callouts)
                    
                    # collect all view plans in the model
                    view_plans = FilteredElementCollector(doc).OfClass(ViewPlan).ToElements()
                    # create a list to store the names of the view plans
                    view_plans_names = []
                    # loop through the view plans and collect their names
                    for view_plan in view_plans:
                        if not view_plan.IsTemplate:
                            view_plans_names.append(view_plan.Name)
                    
                    max_value = (len(reference_callouts) + len(parent_callouts))
                    global counter
                    counter = 0
                    
                    titleBlocksCollector = FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_TitleBlocks)
                    
                    if titleBlocksCollector.GetElementCount() == 1:
                        distance = 2/30.48
                        titleBlockBox = get_title_block_box(titleBlocksCollector)
                        matrix = divide_box(titleBlockBox, distance)
                        
                        # Collect all viewports in the active view
                        viewports = FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_Viewports)
                        # Merge the viewports from the current sheet and the once that we are going to copy and placed on sheet (the once that are not None)
                        all_viewports = list(viewports) + [p[2] for p in parent_callouts if p[2] is not None]
                        
                        global viewports_detail_number
                        viewports_detail_number = []
                        for viewport in viewports:
                            if type(doc.GetElement(viewport.ViewId)) != View and viewport.Id != selected_view_port.Id:
                                viewports_detail_number.append(doc.GetElement(viewport.ViewId).get_Parameter(BuiltInParameter.VIEWPORT_DETAIL_NUMBER).AsString())
                        
                        # Iterate through each viewport and check for collisions with the boxes in the matrix
                        for viewport in all_viewports:
                            viewport_box = get_viewport_outline(viewport)
                            check_collision(viewport_box, matrix)
                        
                        # Start a new transaction 
                        t = Transaction(doc, 'Callouts Tool')
                        t.Start()
                        
                        with DavidEng.ProgressBar(title='Copy Callouts to Level ... ({value} of {max_value})', cancellable=True) as pb:
                            # Get the value of the "VIEW_DEPENDENCY" parameter of the selected view port
                            dependency_value = copy_to_view.get_Parameter(BuiltInParameter.VIEW_DEPENDENCY).AsString()

                            # If the dependency value doesn't start with "Dependent on ", the primary view is the view associated with the selected view port
                            # Otherwise, set primary_copy_to to None
                            primary_copy_to = copy_to_view if not dependency_value.startswith("Dependent on ") else None

                            # If the primary view is None, find the view plan with the same name as the dependency value (after removing the prefix "Dependent on ")
                            if primary_copy_to is None:
                                dependency_value = dependency_value[len("Dependent on "):]
                                for view_plan in view_plans_collector:
                                    if dependency_value == view_plan.Name:
                                        primary_copy_to = view_plan
                                        break
                            
                            copy_annotations(primary_copy_from, primary_copy_to)
                            
                            global viewports_detail_number
                            viewport_detail_number, viewports_detail_number = unique_name("999", viewports_detail_number)
                            selected_view_port.get_Parameter(BuiltInParameter.VIEWPORT_DETAIL_NUMBER).Set(viewport_detail_number)
                            
                            # Unpack the coordinates of the box
                            ax, ay, _, _ = titleBlockBox
                            # variable to keep track of the width of the last callout placed 
                            last_callout_width = 0
                            detail_numbers_list = []
                            row_index = 0
                            column_index = 0
                            maximum_row_height = 0
                            
                            try:
                                folder_name = selected_view_port.LookupParameter('Folder').AsString()
                            except:
                                folder_name = None
                            
                            try:
                                string = copy_to_view.Name
                                start = string.index("(") + 1
                                end = string.index(")")
                                view_level_substring = string[start:end]
                            except:
                                view_level_substring = None
                            
                            view_scope_box_elementId = cancel_scope_box(primary_copy_from)
                            
                            view_orientation_integer, view_orientation_read_only, view_template_id, view_template_param = set_project_north(primary_copy_from)

                            # Iterate through each parent callout and copy/modify its elements
                            for parent_index, (parent_callout, is_hidden, viewport_element) in enumerate(parent_callouts):
                                counter += 1
                                
                                # If the process is cancelled, rollback changes and exit the program
                                if pb.cancelled:
                                    t.RollBack()
                                    import sys
                                    sys.exit()
                                
                                # Copy and modify the parent callout's elements to the new view
                                copy_and_modify_elements(doc, copy_to_view, parent_callout, reference_callouts, parent_index, parent_callouts, copy_options, matrix, distance)
                                
                                # Update the progress bar and refresh the active view
                                pb.update_progress(counter, max_value)
                                revit.uidoc.RefreshActiveView()
                                doc.Regenerate()
                            
                            retrieve_view_orientation(view_orientation_integer, view_orientation_read_only, view_template_id, view_template_param, primary_copy_from)
                            
                            retrieve_scope_box(view_scope_box_elementId, primary_copy_from)

                        t.Commit()
        else:
            DavidEng.ErrorWindow(errortext="You must have at least one Structural Plan in the sheet in order to run this tool.").ShowDialog()

if (DavidEng.checkPermission()):
    if type(doc.ActiveView) == ViewSheet:
        OptionsWindow(OptionsXamlFile).show(modal=True)
    else:
        DavidEng.ErrorWindow(errortext="You can only run this\nadd-in on Sheets").ShowDialog()
else:
    DavidEng.ErrorWindow(errortext="You don't have permission\nto use the tool").ShowDialog()