"""Copies specified legend views from a project file and saves them in the current project. For Revit 2019 and older (In Revit 2020 and newer you can simply copy paste legends between sheets)."""

from pyrevit import revit, forms, HOST_APP, DB, framework, coreutils, BIN_DIR, EXEC_PARAMS, script
from pyrevit.framework import wpf, Interop, Media, Controls, Input, List
from pyrevit.api import AdWindows
from pyrevit.forms import utils
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, TransactionGroup, Transaction, ElementId, ElementTransformUtils
from Autodesk.Revit.UI import PreviewControl
from System import Windows

import os
import os.path as op
import DavidEng

logger = script.get_logger()

# find the path of ErrorWindow.xaml
xamlfile = script.get_bundle_file('ErrorWindow.xaml')

DEFAULT_RECOGNIZE_ACCESS_KEY = False

WPF_COLLAPSED = framework.Windows.Visibility.Collapsed
WPF_VISIBLE = framework.Windows.Visibility.Visible

XAML_FILES_DIR = op.dirname(__file__)

# App and Author name
__title__ = 'Insert Details\nfrom File'
__authors__ = 'Yuval Dolin'

# Identify the current Revit file
doc = __revit__.ActiveUIDocument.Document
app = __revit__.Application   

# Show Error Window Form
class ErrorWindow(Windows.Window):
    def __init__(self, errortext):
        self.errortext=errortext
        wpf.LoadComponent(self, xamlfile)
        self.error.Text = "{}".format(self.errortext)

    def ok(self, sender, args):
        self.Close()

class CopyUseDestination(DB.IDuplicateTypeNamesHandler):
    def OnDuplicateTypeNamesFound(self, args):
        return DB.DuplicateTypeAction.UseDestinationTypes

class WPFWindow(framework.Windows.Window):
    """WPF Window base class for all pyRevit forms."""
    def __init__(self, xaml_source, literal_string=False, handle_esc=True, set_owner=True):
        """Initialize WPF window and resources."""
        # load xaml
        self.load_xaml(xaml_source,literal_string=literal_string, handle_esc=handle_esc, set_owner=set_owner)

    def load_xaml(self, xaml_source, literal_string=False, handle_esc=True, set_owner=True):
        # create new id for this window
        self.window_id = coreutils.new_uuid()

        if not literal_string:
            if not op.exists(xaml_source):
                wpf.LoadComponent(self, os.path.join(EXEC_PARAMS.command_path, xaml_source))
            else:
                wpf.LoadComponent(self, xaml_source)
        else:
            wpf.LoadComponent(self, framework.StringReader(xaml_source))

        # set properties
        self.thread_id = framework.get_current_thread_id()
        if set_owner:
            self.setup_owner()
        self.setup_icon()
        self.setup_resources()
        if handle_esc:
            self.setup_default_handlers()

    def setup_owner(self):
        wih = Interop.WindowInteropHelper(self)
        wih.Owner = AdWindows.ComponentManager.ApplicationWindow

    def setup_resources(self):
        #2c3e50
        self.Resources['pyRevitDarkColor'] = \
            Media.Color.FromArgb(0xFF, 0x2c, 0x3e, 0x50)

        #23303d
        self.Resources['pyRevitDarkerDarkColor'] = \
            Media.Color.FromArgb(0xFF, 0x23, 0x30, 0x3d)

        #ffffff
        self.Resources['pyRevitButtonColor'] = \
            Media.Color.FromArgb(0xFF, 0xff, 0xff, 0xff)

        #f39c12
        self.Resources['pyRevitAccentColor'] = \
            Media.Color.FromArgb(0xFF, 0xf3, 0x9c, 0x12)

        self.Resources['pyRevitDarkBrush'] = \
            Media.SolidColorBrush(self.Resources['pyRevitDarkColor'])
        self.Resources['pyRevitAccentBrush'] = \
            Media.SolidColorBrush(self.Resources['pyRevitAccentColor'])

        self.Resources['pyRevitDarkerDarkBrush'] = \
            Media.SolidColorBrush(self.Resources['pyRevitDarkerDarkColor'])

        self.Resources['pyRevitButtonForgroundBrush'] = \
            Media.SolidColorBrush(self.Resources['pyRevitButtonColor'])

        self.Resources['pyRevitRecognizesAccessKey'] = \
            DEFAULT_RECOGNIZE_ACCESS_KEY

    def setup_default_handlers(self):
        self.PreviewKeyDown += self.handle_input_key    #pylint: disable=E1101

    def handle_input_key(self, sender, args):    #pylint: disable=W0613
        """Handle keyboard input and close the window on Escape."""
        if args.Key == Input.Key.Escape:
            self.Close()

    def set_icon(self, icon_path):
        """Set window icon to given icon path."""
        self.Icon = utils.bitmap_from_file(icon_path)

    def setup_icon(self):
        """Setup default window icon."""
        self.set_icon(op.join(BIN_DIR, 'pyrevit_settings.png'))

    def hide(self):
        self.Hide()

    def show(self, modal=False):
        """Show window."""
        if modal:
            return self.ShowDialog()
        # else open non-modal
        self.Show()

    def show_dialog(self):
        """Show modal window."""
        return self.ShowDialog()

    def set_image_source(self, wpf_element, image_file):
        """Set source file for image element."""
        if not op.exists(image_file):
            wpf_element.Source = utils.bitmap_from_file(os.path.join(EXEC_PARAMS.command_path, image_file))
        else:
            wpf_element.Source = utils.bitmap_from_file(image_file)

    def dispatch(self, func, *args, **kwargs):
        if framework.get_current_thread_id() == self.thread_id:
            t = threading.Thread(target=func, args=args, kwargs=kwargs)
            t.start()
        else:
            # ask ui thread to call the func with args and kwargs
            self.Dispatcher.Invoke(System.Action(lambda: func(*args, **kwargs)),Threading.DispatcherPriority.Background)

    def conceal(self):
        return WindowToggler(self)

    @property
    def pyrevit_version(self):
        """Active pyRevit formatted version e.g. '4.9-beta'"""
        return 'pyRevit {}'.format(
            versionmgr.get_pyrevit_version().get_formatted()
            )

    @staticmethod
    def hide_element(*wpf_elements):
        """Collapse elements.

        Args:
            *wpf_elements: WPF framework elements to be collaped
        """
        for wpfel in wpf_elements:
            wpfel.Visibility = WPF_COLLAPSED

    @staticmethod
    def show_element(*wpf_elements):
        """Show collapsed elements.

        Args:
            *wpf_elements: WPF framework elements to be set to visible.
        """
        for wpfel in wpf_elements:
            wpfel.Visibility = WPF_VISIBLE

    @staticmethod
    def toggle_element(*wpf_elements):
        """Toggle visibility of elements.

        Args:
            *wpf_elements: WPF framework elements to be toggled.
        """
        for wpfel in wpf_elements:
            if wpfel.Visibility == WPF_VISIBLE:
                WPFWindow.hide_element(wpfel)
            elif wpfel.Visibility == WPF_COLLAPSED:
                WPFWindow.show_element(wpfel)

    @staticmethod
    def disable_element(*wpf_elements):
        """Enable elements.

        Args:
            *wpf_elements: WPF framework elements to be enabled
        """
        for wpfel in wpf_elements:
            wpfel.IsEnabled = False

    @staticmethod
    def enable_element(*wpf_elements):
        """Enable elements.

        Args:
            *wpf_elements: WPF framework elements to be enabled
        """
        for wpfel in wpf_elements:
            wpfel.IsEnabled = True

    def handle_url_click(self, sender, args): #pylint: disable=unused-argument
        """Callback for handling click on package website url"""
        return webbrowser.open_new_tab(sender.NavigateUri.AbsoluteUri)

class TemplateUserInputWindow(WPFWindow):
    """Base class for pyRevit user input standard forms."""

    xaml_source = 'BaseWindow.xaml'

    def __init__(self, context, **kwargs):
        """Initialize user input window."""
        WPFWindow.__init__(self, op.join(XAML_FILES_DIR, self.xaml_source), handle_esc=True)

        self._context = context
        self.response = None

        # parent window?
        owner = kwargs.get('owner', None)
        if owner:
            # set wpf windows directly
            self.Owner = owner
            self.WindowStartupLocation = framework.Windows.WindowStartupLocation.CenterOwner

        self._setup(**kwargs)

    def _setup(self, **kwargs):
        """Private method to be overriden by subclasses for window setup."""
        pass

    @classmethod
    def show(cls, context, **kwargs):
        """Show user input window."""
        dlg = cls(context, **kwargs)
        dlg.ShowDialog()
        return dlg.response

class TemplateListItem(object):
    """Base class for checkbox option wrapping another object."""

    def __init__(self, orig_item, checked=False, checkable=True, name_attr=None):
        """Initialize the checkbox option and wrap given obj."""
        self.item = orig_item
        self.state = checked
        self._nameattr = name_attr
        self._checkable = checkable

    def __nonzero__(self):
        return self.state

    def __str__(self):
        return self.name or str(self.item)

    def __contains__(self, value):
        return value in self.name

    def __getattr__(self, param_name):
        return getattr(self.item, param_name)

    @property
    def name(self):
        """Name property."""
        # get custom attr, or name or just str repr
        if self._nameattr:
            return safe_strtype(getattr(self.item, self._nameattr))
        elif hasattr(self.item, 'name'):
            return getattr(self.item, 'name', '')
        else:
            return safe_strtype(self.item)

    def unwrap(self):
        """Unwrap and return wrapped object."""
        return self.item

    @property
    def checkable(self):
        """List Item CheckBox Visibility."""
        return WPF_VISIBLE if self._checkable \
            else WPF_COLLAPSED

    @checkable.setter
    def checkable(self, value):
        self._checkable = value

    @classmethod
    def is_checkbox(cls, item):
        """Check if the object has all necessary attribs for a checkbox."""
        return isinstance(item, TemplateListItem)

class SelectFromList(TemplateUserInputWindow):
    """Standard form to select from a list of items."""

    ControlFlag = False
    xaml_source = 'SelectFromList.xaml'

    def on_selection_change(self, sender, args):
        if self.ControlFlag == False:
            selected_legend = self._unwrap_options([self.list_lb.SelectedItem])[0]
            self.SetPreviewImage = PreviewControl(tempDoc, selected_legend.Id)
            self.grid1.Children.Add(self.SetPreviewImage)
            self.SetPreviewImage.UpdateLayout()
            self.SetPreviewImage.UIView.ZoomToFit()
            self.ControlFlag = True
        else:
            if len(self.list_lb.SelectedItems) == 1:
                self.SetPreviewImage.Dispose()
                selected_legend = self._unwrap_options([self.list_lb.SelectedItem])[0]
                self.SetPreviewImage = PreviewControl(tempDoc, selected_legend.Id)
                self.grid1.Children.Add(self.SetPreviewImage)
                self.SetPreviewImage.UpdateLayout()
                self.SetPreviewImage.UIView.ZoomToFit()

    def cancel(self, sender, args):
        self.Close()
        t = Transaction(doc, 'Close Temporary Doc')
        t.Start()
        tempDoc.Close(False)
        t.Commit()
        tg.RollBack()

    @property
    def use_regex(self):
        return self.regexToggle_b.IsChecked

    def _setup(self, **kwargs):

        # attribute to use as name?
        self._nameattr = kwargs.get('name_attr', None)

        # multiselect?
        if kwargs.get('multiselect', False):
            self.multiselect = True
            self.list_lb.SelectionMode = Controls.SelectionMode.Extended
            self.show_element(self.checkboxbuttons_g)
        else:
            self.multiselect = False
            self.list_lb.SelectionMode = Controls.SelectionMode.Single
            self.hide_element(self.checkboxbuttons_g)

        # return checked items only?
        self.return_all = kwargs.get('return_all', False)

        # filter function?
        self.filter_func = kwargs.get('filterfunc', None)

        # context group title?
        self.ctx_groups_title = kwargs.get('group_selector_title', 'List Group')
        self.ctx_groups_title_tb.Text = self.ctx_groups_title

        self.ctx_groups_active = kwargs.get('default_group', None)

        # check for custom templates
        items_panel_template = kwargs.get('items_panel_template', None)
        if items_panel_template:
            self.Resources["ItemsPanelTemplate"] = items_panel_template

        item_container_template = kwargs.get('item_container_template', None)
        if item_container_template:
            self.Resources["ItemContainerTemplate"] = item_container_template

        item_template = kwargs.get('item_template', None)
        if item_template:
            self.Resources["ItemTemplate"] = \
                item_template

        # nicely wrap and prepare context for presentation, then present
        self._prepare_context()

        # list options now
        self._list_options()

        # setup search and filter fields
        self.hide_element(self.clrsearch_b)
        self.clear_search(None, None)

    def _prepare_context_items(self, ctx_items):
        new_ctx = []
        # filter context if necessary
        if self.filter_func:
            ctx_items = filter(self.filter_func, ctx_items)

        for item in ctx_items:
            if TemplateListItem.is_checkbox(item):
                item.checkable = self.multiselect
                new_ctx.append(item)
            else:
                new_ctx.append(
                    TemplateListItem(item, checkable=self.multiselect, name_attr=self._nameattr))

        return new_ctx

    def _prepare_context(self):
        if isinstance(self._context, dict) and self._context.keys():
            self._update_ctx_groups(sorted(self._context.keys()))
            new_ctx = {}
            for ctx_grp, ctx_items in self._context.items():
                new_ctx[ctx_grp] = self._prepare_context_items(ctx_items)
        else:
            new_ctx = self._prepare_context_items(self._context)

        self._context = new_ctx

    def _update_ctx_groups(self, ctx_group_names):
        self.show_element(self.ctx_groups_dock)
        self.ctx_groups_selector_cb.ItemsSource = ctx_group_names
        if self.ctx_groups_active in ctx_group_names:
            self.ctx_groups_selector_cb.SelectedIndex = ctx_group_names.index(self.ctx_groups_active)
        else:
            self.ctx_groups_selector_cb.SelectedIndex = 0

    def _get_active_ctx_group(self):
        return self.ctx_groups_selector_cb.SelectedItem

    def _get_active_ctx(self):
        if isinstance(self._context, dict):
            return self._context[self._get_active_ctx_group()]
        else:
            return self._context

    def _list_options(self, option_filter=None):
        if option_filter:
            self.checkall_b.Content = 'Check'
            self.uncheckall_b.Content = 'Uncheck'
            self.toggleall_b.Content = 'Toggle'
            # get a match score for every item and sort high to low
            fuzzy_matches = sorted([(x, coreutils.fuzzy_search_ratio(target_string=x.name, sfilter=option_filter, regex=self.use_regex))for x in self._get_active_ctx()], key=lambda x: x[1], reverse=True)
            # filter out any match with score less than 80
            self.list_lb.ItemsSource = [x[0] for x in fuzzy_matches if x[1] >= 80]
        else:
            self.checkall_b.Content = 'Check All'
            self.uncheckall_b.Content = 'Uncheck All'
            self.toggleall_b.Content = 'Toggle All'
            self.list_lb.ItemsSource = [x for x in self._get_active_ctx()]

    @staticmethod
    def _unwrap_options(options):
        unwrapped = []
        for optn in options:
            if isinstance(optn, TemplateListItem):
                unwrapped.append(optn.unwrap())
            else:
                unwrapped.append(optn)
        return unwrapped

    def _get_options(self):
        if self.multiselect:
            if self.return_all:
                return [x for x in self._get_active_ctx()]
            else:
                return self._unwrap_options([x for x in self._get_active_ctx() if x.state or x in self.list_lb.SelectedItems])
        else:
            return self._unwrap_options([self.list_lb.SelectedItem])[0]

    def _set_states(self, state=True, flip=False, selected=False):
        all_items = self.list_lb.ItemsSource
        if selected:
            current_list = self.list_lb.SelectedItems
        else:
            current_list = self.list_lb.ItemsSource
        for checkbox in current_list:
            if flip:
                checkbox.state = not checkbox.state
            else:
                checkbox.state = state

        # push list view to redraw
        self.list_lb.ItemsSource = None
        self.list_lb.ItemsSource = all_items

    def toggle_all(self, sender, args):    #pylint: disable=W0613
        """Handle toggle all button to toggle state of all check boxes."""
        self._set_states(flip=True)

    def check_all(self, sender, args):    #pylint: disable=W0613
        """Handle check all button to mark all check boxes as checked."""
        self._set_states(state=True)

    def uncheck_all(self, sender, args):    #pylint: disable=W0613
        """Handle uncheck all button to mark all check boxes as un-checked."""
        self._set_states(state=False)

    def check_selected(self, sender, args):    #pylint: disable=W0613
        """Mark selected checkboxes as checked."""
        self._set_states(state=True, selected=True)

    def uncheck_selected(self, sender, args):    #pylint: disable=W0613
        """Mark selected checkboxes as unchecked."""
        self._set_states(state=False, selected=True)

    def button_select(self, sender, args):    #pylint: disable=W0613
        """Handle select button click."""
        self.response = self._get_options()
        self.Close()

    def search_txt_changed(self, sender, args):    #pylint: disable=W0613
        """Handle text change in search box."""
        if self.search_tb.Text == '':
            self.hide_element(self.clrsearch_b)
        else:
            self.show_element(self.clrsearch_b)

        self._list_options(option_filter=self.search_tb.Text)

    def toggle_regex(self, sender, args):
        self.regexToggle_b.Content = self.Resources['regexIcon'] if self.use_regex \
                else self.Resources['filterIcon']
        self.search_txt_changed(sender, args)
        self.search_tb.Focus()

    def clear_search(self, sender, args):    #pylint: disable=W0613
        """Clear search box."""
        self.search_tb.Text = ' '
        self.search_tb.Clear()
        self.search_tb.Focus()

class ViewOption(TemplateListItem):
    """View wrapper for :func:`select_views`."""
    def __init__(self, view_element):
        super(ViewOption, self).__init__(view_element)

    @property
    def name(self):
        """View name."""
        return '{}'.format(revit.query.get_name(self.item))

def select_views(multiple=True, filterfunc=None, doc=None, use_selection=False):

    doc = doc or HOST_APP.doc

    if filterfunc:
        all_graphviews = filter(filterfunc, legend_views)

    selected_views = SelectFromList.show(sorted([ViewOption(x) for x in legend_views],key=lambda x: x.name), multiselect=multiple, checked_only=True)

    return selected_views

def get_files():
    return forms.pick_file(files_filter='Revit Files |*.rvt;*.rte|'
                                        'Revit Model |*.rvt|'
                                        'Revit Template |*.rte')

def old_revit_version(selected_views):
    # get all views and collect names
    all_graphviews = revit.query.get_all_views(doc=doc)
    all_legend_names = [revit.query.get_name(x) for x in all_graphviews if x.ViewType == DB.ViewType.Legend]

    # finding first available legend view
    base_legend = revit.query.find_first_legend(doc=doc)
    if not base_legend:
        forms.alert('At least one Legend must exist in target document.', exitscript=True)

    # iterate over interfacetypes legend views
    for src_legend in selected_views:
        # get legend view elements and exclude non-copyable elements
        view_elements = DB.FilteredElementCollector(tempDoc, src_legend.Id).ToElements()

        elements_to_copy = []
        for el in view_elements:
            if isinstance(el, DB.Element) and el.Category:
                elements_to_copy.append(el.Id)
            else:
                logger.debug('Skipping element: %s', el.Id)
        if not elements_to_copy:
            logger.debug('Skipping empty view: %s', revit.query.get_name(src_legend))
            continue

        # start creating views and copying elements
        dest_view = doc.GetElement(base_legend.Duplicate(DB.ViewDuplicateOption.Duplicate))

        options = DB.CopyPasteOptions()
        options.SetDuplicateTypeNamesHandler(CopyUseDestination())
        copied_elements = DB.ElementTransformUtils.CopyElements(src_legend, List[DB.ElementId](elements_to_copy), dest_view, None, options)

        # matching element graphics overrides and view properties
        for dest, src in zip(copied_elements, elements_to_copy):
            dest_view.SetElementOverrides(dest, src_legend.GetElementOverrides(src))

        # matching view name and scale
        src_name = revit.query.get_name(src_legend)
        count = 0
        new_name = src_name
        while new_name in all_legend_names:
            count += 1
            new_name = src_name + ' (Duplicate %s)' % count
            logger.warning('Legend already exists. Renaming to: "%s"', new_name)
        revit.update.set_name(dest_view, new_name)
        dest_view.Scale = src_legend.Scale

def copy_legends(selected_views):
    options = DB.CopyPasteOptions()
    options.SetDuplicateTypeNamesHandler(CopyUseDestination())
    ids = List[ElementId]()
    for i in selected_views:
        ids.Add(i.Id)
    t = Transaction(doc, 'Copy Legends')
    t.Start()
    if HOST_APP.is_newer_than(2019):
        try:
            copy = ElementTransformUtils.CopyElements(tempDoc, ids, doc, None, options)
        except:
            pass
    else:
        old_revit_version(selected_views)
    t.Commit()
    t = Transaction(doc, 'Close Temporary Doc')
    t.Start()
    tempDoc.Close(False)
    t.Commit()
    tg.Assimilate() 

if (DavidEng.checkPermission()):
    patfile = get_files()
    if patfile:
        tempDoc = None
        try:
            tempDoc = app.OpenDocumentFile(patfile)
        except:
            pass
        if tempDoc:
            linkViews = FilteredElementCollector(tempDoc).OfCategory(BuiltInCategory.OST_Views).WhereElementIsNotElementType().ToElements()
            Legend_Flag = False
            for view in linkViews:
                if view.LookupParameter("Family").AsValueString() == "Legend":
                    Legend_Flag = True
                    # otherwise get all sheets and prompt for selection
                    all_graphviews = revit.query.get_all_views(doc=tempDoc)
                    legend_views = []
                    for view in all_graphviews:
                        if view.LookupParameter("Family").AsValueString() == "Legend":
                            legend_views.append(view)
                    break
            if Legend_Flag == True:
                Version_Flag = True
                if not HOST_APP.is_newer_than(2019):
                    # finding first available legend view
                    base_legend = revit.query.find_first_legend(doc=doc)
                    if not base_legend:
                        Version_Flag = False
                if Version_Flag == True:
                    tg = TransactionGroup(doc, 'Insert Details from File')
                    tg.Start()
                    selected_views = select_views()
                    if selected_views:
                        copy_legends(selected_views)
                    if not tg.HasEnded():
                        t = Transaction(doc, 'Close Temporary Doc')
                        t.Start()
                        tempDoc.Close(False)
                        t.Commit()
                        tg.RollBack()
                else:
                    tempDoc.Close(False)
                    ErrorWindow(errortext="At least one Legend must exist in target document.").ShowDialog()
            else:
                t = Transaction(doc, 'Close Temporary Doc')
                t.Start()
                tempDoc.Close(False)
                t.Commit()
                ErrorWindow(errortext="There are no legend views in the target file").ShowDialog()
else:
    DavidEng.ErrorWindow(errortext="You don't have permission\nto use the tool").ShowDialog()