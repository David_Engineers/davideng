"""Creates Dimension Lines from a Detail Lines.
The Detail Lines must be drawn before using the command."""
# pylint: disable=E0401,W0703,W0613
import math
import System
from pyrevit import coreutils, UI, revit, DB, forms, script
from pyrevit.forms import ProgressBar
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, BuiltInParameter, Transaction, XYZ, ElementId, ViewFamilyType, ViewFamily, View3D, ElementClassFilter, \
                              Wall, ReferenceIntersector, FindReferenceTarget, ReferenceArray, Line, ElementTransformUtils, ElementMulticategoryFilter, \
                              ReferenceWithContext, Outline, BoundingBoxIntersectsFilter, ViewPlan, ViewSection, SetComparisonResult
from Autodesk.Revit.UI.Selection import ObjectType, ISelectionFilter
from Autodesk.Revit import Exceptions
import DavidEng

from System.Collections.Generic import List

# dependencies
import clr

# App and Author name
__title__ = 'Dimension\nby Line'
__authors__ = 'Yuval Dolin'

# Identify the current Revit file
doc = __revit__.ActiveUIDocument.Document
uidoc = __revit__.ActiveUIDocument

# if the user selected elements before runing the add-in
selection = [doc.GetElement(id) for id in uidoc.Selection.GetElementIds()]

# Define class to filter elements of category to select 
class CustomSelectionFilter(ISelectionFilter):
    def __init__(self, category):
        self.category = category
    def AllowElement(self, e):
        if e.Category.Id == DB.Category.GetCategory(doc, self.category).Id:
            if type(e.GeometryCurve) == Line:
                return True
            else:
                return False
    def AllowReference(self, ref, point):
        return True

# find crossing wall faces and return true or false
def isParallel(v1,v2):
	return v1.CrossProduct(v2).IsAlmostEqualTo(XYZ(0,0,0))

def GetReferences(origin, vec, view3D):
    category_list = List[BuiltInCategory]()
    category_list.Add(BuiltInCategory.OST_Walls)
    category_list.Add(BuiltInCategory.OST_Floors)
    category_list.Add(BuiltInCategory.OST_StructuralFraming)
    category_list.Add(BuiltInCategory.OST_StructuralColumns)
    category_filter = ElementMulticategoryFilter(category_list)
    refIntersector = ReferenceIntersector(category_filter, FindReferenceTarget.Face, view3D)
    referenceWithContext = refIntersector.Find( origin, vec )

    return referenceWithContext

def CreateRefArray(origin, vec, References, LinLength, refLine):
    refArray = ReferenceArray()
    NoParallelList = []
    for ref in References:
        GetRef = ref.GetReference()
        refPoint = GetRef.GlobalPoint
        EqualFlag = False
        if round(origin.X, 3) != round(refPoint.X, 3) or round(origin.Y, 3) != round(refPoint.Y, 3) \
        or round(origin.Z, 3) != round(refPoint.Z, 3):
            refCheckLine = Line.CreateBound(origin, refPoint)
            refCheckLineLength = refCheckLine.Length
        else:
            EqualFlag = True
            refCheckLineLength = 1
        if round(refCheckLineLength, 3) <= round(LinLength, 3) or EqualFlag == True:
            elem = doc.GetElement(GetRef)
            face = elem.GetGeometryObjectFromReference(GetRef)
            try:
                faceNormal = face.FaceNormal
                if isParallel(faceNormal, vec):
                    CheckDoubleFlag = CheckDouble(refArray, GetRef, refLine)
                    if CheckDoubleFlag == False:
                        refArray.Append(GetRef)
                else:
                    NoParallelList.append(GetRef)
            except:
                NoParallelList.append(GetRef)
    NewLinesList = NoParallel(NoParallelList, refArray, origin, refLine)
    return refArray, NewLinesList

def CreateXYZ(refPoint, AddValue, origin):
    refCheckLine = Line.CreateBound(origin, refPoint)
    refCheckLineLength = refCheckLine.Length
    refLine = refCheckLineLength + AddValue
    refLineX = ((refLine * (refPoint.X - origin.X))/refCheckLineLength)+origin.X
    refLineY = ((refLine * (refPoint.Y - origin.Y))/refCheckLineLength)+origin.Y
    refLineZ = ((refLine * (refPoint.Z - origin.Z))/refCheckLineLength)+origin.Z
    refLineXYZ = XYZ(refLineX, refLineY, refLineZ)
    return refLineXYZ

# create detail lines for no parallel elements
def NoParallel(NoParallelList, refArray, origin, refLine):
    LinesList = []
    NewLinesList = []
    for ref in NoParallelList:
        CheckDoubleFlag = CheckDouble(refArray, ref, refLine)
        CheckDoubleFlagLines = CheckDouble(LinesList, ref, refLine)
        if CheckDoubleFlagLines == False and CheckDoubleFlag == False:
            try:
                refPoint = ref.GlobalPoint
                refLinePlusXYZ = CreateXYZ(refPoint, 0.1, origin)
                refLineMinusXYZ = CreateXYZ(refPoint, -0.1, origin)
                NewLine = Line.CreateBound(refLinePlusXYZ, refLineMinusXYZ)
            except:
                pass
            ErrorFlag = False
            try:
                CreateNewLine = doc.Create.NewDetailCurve(doc.ActiveView, NewLine) 
            except:
                try:
                    Plus = Line.CreateUnbound(refLinePlusXYZ, -(doc.ActiveView.CropBox.Transform.BasisZ))
                    out = clr.Reference[revit.DB.IntersectionResultArray]()
                    inter = Plus.Intersect(refLine, out)
                    if inter == SetComparisonResult.Overlap or inter == SetComparisonResult.Subset:
                        PlusPoint = out.Item[0].XYZPoint
                    Minus = Line.CreateUnbound(refLineMinusXYZ, -(doc.ActiveView.CropBox.Transform.BasisZ))
                    out = clr.Reference[revit.DB.IntersectionResultArray]()
                    inter = Minus.Intersect(refLine, out)
                    if inter == SetComparisonResult.Overlap or inter == SetComparisonResult.Subset:
                        MinusPoint = out.Item[0].XYZPoint
                except:
                    pass
                try:
                    NewLine = Line.CreateBound(PlusPoint, MinusPoint)
                    CreateNewLine = doc.Create.NewDetailCurve(doc.ActiveView, NewLine) 
                except:
                    ErrorFlag = True
            if ErrorFlag == False:
                AxisLine = Line.CreateUnbound(refPoint, doc.ActiveView.CropBox.Transform.BasisZ)
                ElementTransformUtils.RotateElement(doc, CreateNewLine.Id, AxisLine, math.pi / 2)
                NewLinesList.append(CreateNewLine)
                NewLineRef = CreateNewLine.GeometryCurve.Reference
                refArray.Append(NewLineRef)
                LinesList.append(ref)
    return NewLinesList

def CheckDouble(refArray, ref, refLine):
    CheckDoubleFlag = False
    GetOriginalRefPoint = ref.GlobalPoint
    ParallelLine = Line.CreateUnbound(GetOriginalRefPoint, -(doc.ActiveView.CropBox.Transform.BasisZ))
    out = clr.Reference[revit.DB.IntersectionResultArray]()
    inter = ParallelLine.Intersect(refLine, out)
    if inter == SetComparisonResult.Overlap or inter == SetComparisonResult.Subset:
        GetRefPoint = out.Item[0].XYZPoint
    else:
        GetRefPoint = GetOriginalRefPoint
    for reference in refArray:
        refOriginalPoint = reference.GlobalPoint
        try:
            ParallelLine = Line.CreateUnbound(refOriginalPoint, -(doc.ActiveView.CropBox.Transform.BasisZ))
            out = clr.Reference[revit.DB.IntersectionResultArray]()
            inter = ParallelLine.Intersect(refLine, out)
            if inter == SetComparisonResult.Overlap or inter == SetComparisonResult.Subset:
                refPoint = out.Item[0].XYZPoint
        except:
            refPoint = refOriginalPoint
        try:
            if type(doc.ActiveView) == ViewPlan:
                if round(GetRefPoint.X, 1) == round(refPoint.X, 1) and round(GetRefPoint.Y, 1) == round(refPoint.Y, 1):
                    CheckDoubleFlag = True
                    break
            else:
                if round(GetRefPoint.X, 1) == round(refPoint.X, 1) and round(GetRefPoint.Y, 1) == round(refPoint.Y, 1) \
                and round(GetRefPoint.Z, 1) == round(refPoint.Z, 1):
                    CheckDoubleFlag = True
                    break
        except:
            pass
    return CheckDoubleFlag

def CreatExtensionStorage(DetailLine, DimensionId):
    schemaBuilder=DB.ExtensibleStorage.SchemaBuilder(System.Guid("d64e3bab-7b93-415a-a20a-4d09ec106aad"))
    schemaBuilder.SetReadAccessLevel(DB.ExtensibleStorage.AccessLevel.Public)
    schemaBuilder.SetWriteAccessLevel(DB.ExtensibleStorage.AccessLevel.Public)
    schemaBuilder.SetVendorId("YuvalDolin")
    schemaBuilder.SetSchemaName("DavidEng_Schema_DimensionByLine")
    fieldBuilder = schemaBuilder.AddSimpleField("Id_Dimension", DetailLine.UniqueId.GetType())
    fieldBuilder.SetDocumentation("The Id of the associated dimension")
    schema = schemaBuilder.Finish()
    
    entity =DB.ExtensibleStorage.Entity(schema)
    fieldSpliceLocation = schema.GetField("Id_Dimension")
    entity.Set(fieldSpliceLocation, str(DimensionId))
    DetailLine.SetEntity(entity)

def FloorsCollector(bbfilter):
    # Creating collectior instance and collecting all the floors from the model
    floor_collector_all = FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_Floors) \
                                                   .WhereElementIsNotElementType().WherePasses(bbfilter)
    FloorElevationsList = []
    for floor in floor_collector_all:
        try:
            if not floor.SlabShapeEditor == None:
                if floor.SlabShapeEditor.IsEnabled == False:
                    FloorLevel = doc.GetElement(floor.LevelId)
                    LevelElevation = FloorLevel.ProjectElevation
                    FloorOffset = floor.get_Parameter(BuiltInParameter.FLOOR_HEIGHTABOVELEVEL_PARAM).AsDouble()
                    FloorThickness = floor.get_Parameter(BuiltInParameter.FLOOR_ATTR_THICKNESS_PARAM).AsDouble()
                    FloorElevationTop = LevelElevation + FloorOffset
                    FloorElevationsList.append(FloorElevationTop)
                    FloorElevationBot = FloorElevationTop - FloorThickness
                    FloorElevationsList.append(FloorElevationBot)
        except:
            pass
    FloorElevationsList = list(set(FloorElevationsList))
    return FloorElevationsList

def WallsCollector(bbfilter):
    # Creating collectior instance and collecting all the floors from the model
    walls_collector_all = FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_Walls) \
                                                   .WhereElementIsNotElementType().WherePasses(bbfilter)
    return walls_collector_all

def WallIntersector(wall, StartPoint, EndPoint, refLineLength, wallFilter, view3D, PlusMinus):
    FirstPoint = None
    SecondPoint = None
    CheckLength = 0
    Counter = 0
    CheckPoint = StartPoint
    while CheckLength < refLineLength:
        try:
            referenceIntersector = DB.ReferenceIntersector(wallFilter, DB.FindReferenceTarget.Face, view3D)
            sReferenceWithContext = referenceIntersector.FindNearest(CheckPoint, PlusMinus * (doc.ActiveView.CropBox.Transform.BasisZ))
            if sReferenceWithContext.GetReference().ElementId == wall.Id:
                if Counter == 0:
                    FirstPoint = sReferenceWithContext.GetReference().GlobalPoint
                    Counter = Counter + 1
                    CheckPoint = CreateXYZ(CheckPoint, -10/30.48, EndPoint)
                    CheckLine = Line.CreateBound(StartPoint, CheckPoint)
                    CheckLength = CheckLine.Length
                else:
                    SecondPoint = sReferenceWithContext.GetReference().GlobalPoint
                    Counter = Counter + 1
                    break
            else:
                CheckPoint = CreateXYZ(CheckPoint, -10/30.48, EndPoint)
                CheckLine = Line.CreateBound(StartPoint, CheckPoint)
                CheckLength = CheckLine.Length
        except:
            if not Counter == 2:
                try:
                    CheckPoint = CreateXYZ(CheckPoint, -10/30.48, EndPoint)
                    CheckLine = Line.CreateBound(StartPoint, CheckPoint)
                    CheckLength = CheckLine.Length
                except:
                    CheckLength = refLineLength
    return Counter, FirstPoint, SecondPoint

def ViewWalls(CombineReferencesWithContext, StartPoint, EndPoint, refLineLength, view3D):
    RefElementId = []
    for ref in CombineReferencesWithContext:
        GetRef = ref.GetReference()
        if not GetRef.ElementId in RefElementId:
            RefElementId.append(GetRef.ElementId)
    WallsViewList = []        
    for wall in WallsList:
        if not wall.Id in RefElementId:
            WallsViewList.append(wall)	
    wallFilter = ElementClassFilter(Wall)
    CombineReferencesWithContext = List[ReferenceWithContext]()
    for wall in WallsViewList:
        Counter, FirstPoint, SecondPoint = WallIntersector(wall, StartPoint, EndPoint, refLineLength, wallFilter, view3D, -1)
        if SecondPoint == None:
            Counter, FirstPoint, SecondPoint = WallIntersector(wall, StartPoint, EndPoint, refLineLength, wallFilter, view3D, 1)
        if Counter == 2:
            refLine = Line.CreateBound(FirstPoint, SecondPoint)
            refIntersector = ReferenceIntersector(wallFilter, FindReferenceTarget.Face, view3D)
            referenceWithContext = refIntersector.Find(FirstPoint, refLine.Direction)
            for reference in referenceWithContext:
                if reference.GetReference().ElementId == wall.Id:
                    CombineReferencesWithContext.Add(reference)
            referenceWithContext = refIntersector.Find(FirstPoint, -(refLine.Direction))
            for reference in referenceWithContext:
                if reference.GetReference().ElementId == wall.Id:
                    CombineReferencesWithContext.Add(reference)

    return CombineReferencesWithContext

def CreateTempView3D():
    viewTypeCollector = FilteredElementCollector(doc).OfClass(ViewFamilyType).ToElements()
    for i in viewTypeCollector:
        if i.ViewFamily == ViewFamily.ThreeDimensional:
            viewType = i
            break
    view3D = View3D.CreateIsometric(doc, viewType.Id)
	
    return view3D

if (DavidEng.checkPermission()):
    if type(doc.ActiveView) == ViewPlan or type(doc.ActiveView) == ViewSection:
        if not selection:
            customFilter = CustomSelectionFilter(DB.BuiltInCategory.OST_Lines)
            try:
                Lines_selection = [doc.GetElement(reference) for reference in uidoc.Selection.PickObjects(
                    ObjectType.Element, customFilter, "Select Detail Lines")]
            except Exceptions.OperationCanceledException:
                import sys
                sys.exit()
            if Lines_selection == []:
                DavidEng.ErrorWindow(errortext="You have not selected any Detail Line").ShowDialog()
        else:
            Lines_selection = []
            for element in selection:
                if element.Category.Name == 'Lines':
                    if type(element.GeometryCurve) == Line:
                        Lines_selection.append(element)
            if Lines_selection == []:
                DavidEng.ErrorWindow(errortext="You have not selected any Detail Line").ShowDialog()

        if not Lines_selection == []:
            t = Transaction(doc, "Create Dimensions")
            t.Start()
            view3D = CreateTempView3D()
            max_value = len(Lines_selection)
            counter = 0
            with DavidEng.ProgressBar(title='Create Dimensions ... ({value} of {max_value})', cancellable=True) as pb:
                for line in Lines_selection:
                    if pb.cancelled:
                        t.RollBack()
                        import sys
                        sys.exit()
                    if type(doc.ActiveView) == ViewPlan:
                        BoxMax = line.BoundingBox[doc.ActiveView].Max
                        BoxMin = line.BoundingBox[doc.ActiveView].Min
                        NewBoxMax = XYZ(BoxMax.X, BoxMax.Y, BoxMax.Z + 1000000)
                        NewBoxMin = XYZ(BoxMin.X, BoxMin.Y, BoxMin.Z - 1000000)
                        outline = Outline(NewBoxMin, NewBoxMax)
                        bbfilter = BoundingBoxIntersectsFilter(outline)
                        FloorElevationsList = FloorsCollector(bbfilter)
                    else:
                        BoxMax = line.BoundingBox[doc.ActiveView].Max
                        BoxMin = line.BoundingBox[doc.ActiveView].Min
                        BasisXX = doc.ActiveView.CropBox.Transform.BasisX.X
                        BasisXY = doc.ActiveView.CropBox.Transform.BasisX.Y
                        NewBoxMax = XYZ(BoxMax.X + 1000000 * abs(BasisXY), BoxMax.Y + 1000000 * abs(BasisXX), BoxMax.Z)
                        NewBoxMin = XYZ(BoxMin.X - 1000000 * abs(BasisXY), BoxMin.Y - 1000000 * abs(BasisXX), BoxMin.Z)
                        outline = Outline(NewBoxMin, NewBoxMax)
                        bbfilter = BoundingBoxIntersectsFilter(outline)
                        WallsList = WallsCollector(bbfilter)
                        FloorElevationsList = []
                    LineDirection = line.GeometryCurve.Direction
                    StartPoint = line.GeometryCurve.GetEndPoint(0)
                    EndPoint = line.GeometryCurve.GetEndPoint(1)
                    refLine = Line.CreateBound(StartPoint, EndPoint)
                    refLineLength = refLine.Length
                    CombineReferencesWithContext = List[ReferenceWithContext]()
                    if FloorElevationsList == []:
                        FloorElevationsList.append(StartPoint.Z)
                    for elevation in FloorElevationsList:
                        StartPointXYZ = XYZ(StartPoint.X, StartPoint.Y, elevation)
                        WallReferencesWithContext = GetReferences(StartPointXYZ, LineDirection, view3D)
                        for reference in WallReferencesWithContext:
                            CombineReferencesWithContext.Add(reference)
                    if type(doc.ActiveView) == ViewSection:
                        WallReferencesWithContext = ViewWalls(CombineReferencesWithContext, StartPoint, EndPoint, refLineLength, view3D)
                        for reference in WallReferencesWithContext:
                            CombineReferencesWithContext.Add(reference)
                    references, DetailLinesList = CreateRefArray(StartPoint, LineDirection, CombineReferencesWithContext, refLineLength, refLine)
                    if references.Size > 1:
                        try:
                            CreateDimension = doc.Create.NewDimension(doc.ActiveView, refLine, references)
                            for DetailLine in DetailLinesList:
                                CreatExtensionStorage(DetailLine, CreateDimension.Id)
                            doc.Delete(line.Id)
                        except:
                            pass
                    counter += 1
                    pb.update_progress(counter, max_value)
                    revit.uidoc.RefreshActiveView()
            doc.Delete(view3D.Id)
            t.Commit()
    else:
        DavidEng.ErrorWindow(errortext="You can only run this add-in on\nPlan Views or Section Views").ShowDialog()
else:
    DavidEng.ErrorWindow(errortext="You don't have permission\nto use the tool").ShowDialog()