"""This tool allows you to replace dimensions values with text or leave the dimensions blank. This operation has no effect on model geometry."""
# pylint: disable=E0401,W0703,W0613
from pyrevit import coreutils, UI, revit, DB, forms, script
from Autodesk.Revit.DB import BuiltInCategory, BuiltInParameter, Transaction
from Autodesk.Revit.UI.Selection import ObjectType, ISelectionFilter
from Autodesk.Revit import Exceptions

# dependencies
import clr
import DavidEng
clr.AddReference('System.Windows.Forms')

# find the path of ErrorWindow.xaml
xamlfile = script.get_bundle_file('ErrorWindow.xaml')

# import WPF creator and base Window
import wpf
from System import Windows

# App and Author name
__title__ = 'Dimention Replace\nWith Text'
__authors__ = 'Yuval Dolin'

# Identify the current Revit file
doc = __revit__.ActiveUIDocument.Document
uidoc = __revit__.ActiveUIDocument

# if the user selected elements before runing the add-in
selection = [doc.GetElement(id) for id in uidoc.Selection.GetElementIds()]

# Define class to filter elements of category to select 
class CustomSelectionFilter(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, self.category).Id:
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

# Show Error Window Form
class ErrorWindow(Windows.Window):
    def __init__(self, errortext):
        self.errortext=errortext
        wpf.LoadComponent(self, xamlfile)
        self.error.Text = "{}".format(self.errortext)

    def ok(self, sender, args):
        self.Close()

class ChooseWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name):
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)

    # user select to leave the dimension text blank
    def leave_blank(self, sender, args):
        self.Close()
        with revit.Transaction('Dimensions Value Override', log_errors=False):
            for dimension in Dimensions_selection:
                try:
                    dimension.ValueOverride = " "
                except Exception, ex:
                    segments = dimension.Segments
                    for segment in segments:
                        segment.ValueOverride = " "

    # user select to replace the dimension with text
    def define_value(self, sender, args):
        self.Close()
        DefineValueWindow('DefineValueWindow.xaml').show(modal=True)

class DefineValueWindow(forms.WPFWindow):
    def __init__(self, xaml_file_name):
        # create pattern maker window and process options
        forms.WPFWindow.__init__(self, xaml_file_name)

    # user select to leave the dimension text blank
    def click_ok(self, sender, args):
        self.Close()
        with revit.Transaction('Dimensions Value Override', log_errors=False):
            for dimension in Dimensions_selection:
                try:
                    dimension.ValueOverride = self.value.Text
                except Exception, ex:
                    segments = dimension.Segments
                    for segment in segments:
                        segment.ValueOverride = self.value.Text

    # user select to replace the dimension with text
    def cancel(self, sender, args):
        self.Close()

if (DavidEng.checkPermission()):
    if not selection:
        customFilter = CustomSelectionFilter(DB.BuiltInCategory.OST_Dimensions)
        try:
            Dimensions_selection = [doc.GetElement(reference) for reference in uidoc.Selection.PickObjects(
                ObjectType.Element, customFilter, "Select Dimensions")]
        except Exceptions.OperationCanceledException:
            import sys
            sys.exit()
        if Dimensions_selection == []:
            ErrorWindow(errortext="You have not selected any Dimension").ShowDialog()
    else:
        Dimensions_selection = []
        for element in selection:
            if element.Category.Name == 'Dimensions':
                Dimensions_selection.append(element)
        if Dimensions_selection == []:
            ErrorWindow(errortext="You have not selected any Dimension").ShowDialog()

    if not Dimensions_selection == []:
        ChooseWindow('SelectWindow.xaml').show(modal=True)
else:
    DavidEng.ErrorWindow(errortext="You don't have permission\nto use the tool").ShowDialog()