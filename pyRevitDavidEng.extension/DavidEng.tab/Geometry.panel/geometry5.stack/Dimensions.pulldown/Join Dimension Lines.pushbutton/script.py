"""The selected parallel dimension lines are joined to form one chain dimension line.
If there was no selection, you are asked to make the selection of the First Main Dimension and then the Parallel Dimensions to Join."""
# pylint: disable=E0401,W0703,W0613
import System
from pyrevit import coreutils, UI, revit, DB, forms, script
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, BuiltInParameter, Transaction, XYZ, ElementId, Reference, Line, ReferenceArray, DetailLine
from Autodesk.Revit.UI.Selection import ObjectType, ISelectionFilter
from Autodesk.Revit import Exceptions
import DavidEng

# App and Author name
__title__ = 'Join Dimension Lines'
__authors__ = 'Yuval Dolin'

# Identify the current Revit file
doc = __revit__.ActiveUIDocument.Document
uidoc = __revit__.ActiveUIDocument

# Define class to filter elements of category to select 
class CustomSelectionFilter(ISelectionFilter):
	def __init__(self, category):
		self.category = category
	def AllowElement(self, e):
		if e.Category.Id == DB.Category.GetCategory(doc, self.category).Id:
			return True
		else:
			return False
	def AllowReference(self, ref, point):
		return True

# Define class to filter elements of category to select 
class CustomSelectionFilterParallel(ISelectionFilter):
    def __init__(self, category, DirectionX, DirectionY, DirectionZ):
        self.category = category
        self.DirectionX = DirectionX
        self.DirectionY = DirectionY
        self.DirectionZ = DirectionZ
    def AllowElement(self, e):
        if e.Category.Id == DB.Category.GetCategory(doc, self.category).Id:
            if e.Id != DimensionId and abs(round(e.Curve.Direction.X, 5)) == abs(round(self.DirectionX, 5)) \
            and abs(round(e.Curve.Direction.Y, 5)) == abs(round(self.DirectionY, 5)) \
            and abs(round(e.Curve.Direction.Z, 5)) == abs(round(self.DirectionZ, 5)):
                return True
        else:
            return False
    def AllowReference(self, ref, point):
        return True

# let the user to select parallel dimensions to join
def ParallelDimensions():
    customFilter = CustomSelectionFilterParallel(DB.BuiltInCategory.OST_Dimensions, DimensionDirection.X, DimensionDirection.Y, DimensionDirection.Z)
    try:
        ParallelDimension_selection = [doc.GetElement(reference) for reference in uidoc.Selection.PickObjects(
            ObjectType.Element, customFilter, "Select Parallel Dimensions to Join")]
    except Exceptions.OperationCanceledException:
        import sys
        sys.exit()
    return ParallelDimension_selection

def CombineAndCreateDimension(DimensionReferences, ParallelDimension_selection, refLine):
    refArray = DimensionReferences
    for ParallelDimension in ParallelDimension_selection:
        RefArray = ParallelDimension.References
        for ref in RefArray:
            refArray.Append(ref)
    CreateDimension = doc.Create.NewDimension(doc.ActiveView, refLine, refArray)
    DimensionReferences = CreateDimension.References
    DimensionSegments = CreateDimension.Segments
    return DimensionReferences, DimensionSegments, CreateDimension

def CheckZeroSegments(DimensionSegments, CreateDimension, DimensionReferences, refLine):
    Counter = 0
    ZeroList = []
    for segment in DimensionSegments:
        if segment.Value < 0.0328:
            ZeroList.append(Counter)
        Counter = Counter + 1
    if not ZeroList == []:
        doc.Delete(CreateDimension.Id)
        refArray = ReferenceArray()
        Counter = 0
        while Counter < DimensionReferences.Size:
            if not Counter in ZeroList:
                refArray.Append(DimensionReferences[Counter])
            Counter = Counter + 1
        CreateDimension = doc.Create.NewDimension(doc.ActiveView, refLine, refArray)
    return CreateDimension

# update the id of the associated dimension
def UpdateExtensibleStorage(DimensionReferences, CreateDimensionFinal):
    SchemaDimension=DB.ExtensibleStorage.Schema.Lookup(System.Guid("d64e3bab-7b93-415a-a20a-4d09ec106aad"))
    if SchemaDimension:
        for ref in DimensionReferences:
            refElementId = ref.ElementId
            refElement = doc.GetElement(refElementId)
            if type(refElement) == DetailLine:
                try:
                    entity =DB.ExtensibleStorage.Entity(SchemaDimension)
                    FieldDimension=SchemaDimension.GetField("Id_Dimension")
                    entity.Set(FieldDimension, str(CreateDimensionFinal.Id))
                    refElement.SetEntity(entity)
                except:
                    pass

if (DavidEng.checkPermission()):
    
    # if the user selected elements before runing the add-in
    selection = [doc.GetElement(id) for id in uidoc.Selection.GetElementIds()]
    
    if not selection:
        customFilter = CustomSelectionFilter(DB.BuiltInCategory.OST_Dimensions)
        try:
            Dimension_selection = uidoc.Selection.PickObject(ObjectType.Element, customFilter, "Select First Main Dimension")
        except Exception, ex:
            Dimension_selection = None
        if Dimension_selection == None:
            DavidEng.ErrorWindow(errortext="You have not selected any Dimension").ShowDialog()
    else:
        Dimension_selection = []
        for element in selection:
            if element.Category.Name == 'Dimensions':
                Dimension_selection.append(element)
        if Dimension_selection == []:
            Dimension_selection = None
            DavidEng.ErrorWindow(errortext="You have not selected any Dimension").ShowDialog()
        elif len(Dimension_selection) > 1:
            Dimension_selection = None
            DavidEng.ErrorWindow(errortext="You have to select only one\nMain Dimension").ShowDialog()
        else:
            Dimension_selection = Dimension_selection[0]
    
    if not Dimension_selection == None:
        if type(Dimension_selection) == Reference:
            Dimension_selection = doc.GetElement(Dimension_selection)
        DimensionDirection = Dimension_selection.Curve.Direction
        DimensionOrigin = Dimension_selection.Curve.Origin
        DimensionId = Dimension_selection.Id
        DimensionReferences = Dimension_selection.References
        ParallelDimension_selection = ParallelDimensions()
        if ParallelDimension_selection == []:
            DavidEng.ErrorWindow(errortext="You have not selected any\nDimensions to Join").ShowDialog()
        if not ParallelDimension_selection == []:
            t = Transaction(doc, "Join Dimensions")
            t.Start()
            refLine = Line.CreateUnbound(DimensionOrigin, DimensionDirection)
            DimensionReferences, DimensionSegments, CreateDimension = CombineAndCreateDimension(DimensionReferences, ParallelDimension_selection, refLine)
            CreateDimensionFinal = CheckZeroSegments(DimensionSegments, CreateDimension, DimensionReferences, refLine)
            DimensionReferences = CreateDimensionFinal.References
            UpdateExtensibleStorage(DimensionReferences, CreateDimensionFinal)
            doc.Delete(DimensionId)
            for ParallelDimension in ParallelDimension_selection:
                doc.Delete(ParallelDimension.Id)
            t.Commit()
else:
    DavidEng.ErrorWindow(errortext="You don't have permission\nto use the tool").ShowDialog()