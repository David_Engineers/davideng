#-*- coding: utf-8 -*-
#pylint: disable=C0103,E0401,W0703
from pyrevit import HOST_APP, DB, framework
import System
from System import Guid
from Autodesk.Revit.UI import TaskDialog
from Autodesk.Revit.DB import SubTransaction, ElementId
import sys
import os
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, BuiltInParameter
from pyrevit import forms

from System.Collections.Generic import List

#Updater class
class DavidEngUpdater(DB.IUpdater):
    #constructor
    def __init__(self, addin_id): 
        #updater of ID
        self.id = DB.UpdaterId(addin_id, Guid("A7931BDA-F0DC-41B5-83C9-C6FE03CC5031"))
        #Get transaction name
        self.transactionName = None


    def GetUpdaterId(self):
        return self.id

    def GetUpdaterName(self):
        return "David Engineers Updater"

    def GetAdditionalInformation(self):
        return "David Engineers Dynamic Model Updater"
 
    def GetChangePriority(self):
        return DB.ChangePriority.Views

    #Change transaction notification
    def docchanged_eventhandler(self, sender, args):
        #Get transaction name
        self.transactionName = args.GetTransactionNames()[0]


    #Hook processing
    def Execute(self, data):
        
        doc = data.GetDocument()
        #Modified element Id
        changedIds = data.GetModifiedElementIds()
        #Id of deleted element
        deletedIds = data.GetDeletedElementIds()

        # Creating collectior instance and collecting all the area loads from the model
        AreasCollector = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Areas).WhereElementIsNotElementType()
        
        SchemaAreaLoad=DB.ExtensibleStorage.Schema.Lookup(System.Guid("d64e3bab-7b93-415a-a20a-4d09ec106aab"))
        if SchemaAreaLoad:
        #Processing for change area loads
            for id in changedIds:
                elemChanged = doc.GetElement(id)
                for area in AreasCollector:
                    try:
                        retrievedEntity = area.GetEntity(SchemaAreaLoad)
                        FieldDL=SchemaAreaLoad.GetField("Id_Area_Load_DL")
                        retrievedDataDL =retrievedEntity.Get[area.Id.GetType()](FieldDL)
                        FieldLL=SchemaAreaLoad.GetField("Id_Area_Load_LL")
                        retrievedDataLL =retrievedEntity.Get[area.Id.GetType()](FieldLL)
                        if retrievedDataDL.IntegerValue == id.IntegerValue:
                            DeadLoadValue = elemChanged.get_Parameter(BuiltInParameter.LOAD_AREA_FORCE_FZ1).AsDouble()
                            DeadLoad = area.LookupParameter('Dead Load').Set(DeadLoadValue)
                        elif retrievedDataLL.IntegerValue == id.IntegerValue:
                            LiveLoadValue = elemChanged.get_Parameter(BuiltInParameter.LOAD_AREA_FORCE_FZ1).AsDouble()
                            DeadLoad = area.LookupParameter('Live Load').Set(LiveLoadValue)
                    except:
                        pass
                        
        SchemaDimension=DB.ExtensibleStorage.Schema.Lookup(System.Guid("d64e3bab-7b93-415a-a20a-4d09ec106aad"))
        if SchemaDimension:
            # list of elements ids 
            ids_lines = List[ElementId]()
            # Creating collectior instance and collecting all the detail lines from the active view
            LinesCollector = FilteredElementCollector(doc, doc.ActiveView.Id).OfCategory(BuiltInCategory.OST_Lines).WhereElementIsNotElementType()
            #Processing for deleted dimensions
            for line in LinesCollector:
                try:
                    retrievedEntity = line.GetEntity(SchemaDimension)
                    FieldDimension=SchemaDimension.GetField("Id_Dimension")
                    retrievedDataDimensionId =retrievedEntity.Get[line.UniqueId.GetType()](FieldDimension)
                    if retrievedDataDimensionId in str(deletedIds):
                        ids_lines.Add(line.Id)
                except:
                    pass
            t = SubTransaction(doc)
            t.Start()
            doc.Delete(ids_lines)
            t.Commit()

#Generate updater when launching a new Revit
updater = DavidEngUpdater(HOST_APP.addin_id)

#Unregister the UpdaterRegistry if there is an Updater already registered
if DB.UpdaterRegistry.IsUpdaterRegistered(updater.GetUpdaterId()):
    DB.UpdaterRegistry.UnregisterUpdater(updater.GetUpdaterId())
    
#UpdaterRegistry Register
DB.UpdaterRegistry.RegisterUpdater(updater)

#Specify parts for hook target elements
AreaLoads_filter = DB.ElementCategoryFilter(DB.BuiltInCategory.OST_AreaLoads)
Dimensions_filter = DB.ElementCategoryFilter(DB.BuiltInCategory.OST_Dimensions)
#Get all change types
change_type = DB.Element.GetChangeTypeAny()
#Get element deletion
delete_type = DB.Element.GetChangeTypeElementDeletion()
#Change trigger registration
DB.UpdaterRegistry.AddTrigger(updater.GetUpdaterId(), AreaLoads_filter, change_type)
#Delete trigger registration
DB.UpdaterRegistry.AddTrigger(updater.GetUpdaterId(), Dimensions_filter, delete_type)

#DocumentChanged 
HOST_APP.app.DocumentChanged += \
        framework.EventHandler[DB.Events.DocumentChangedEventArgs](
        updater.docchanged_eventhandler
        )